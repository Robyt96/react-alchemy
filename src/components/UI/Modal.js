import React from 'react';
import ReactDOM from 'react-dom';

import classes from './Modal.module.css';

const Backdrop = props => (
  <div
    className={classes.backdrop} 
    onClick={props.onClose}
    style={{zIndex: 20 * props.zIndexLevel}}
  />
);

const ModalOverlay = props => (
  <div className={classes.modal} style={{zIndex: 30 * props.zIndexLevel}}>
    <div className={classes.content}>
      {props.children}
    </div>
  </div>
);

const portalElement = document.getElementById('overlays');

const Modal = (props) => {
  const { zIndexLevel = 1 } = props;

  return (
    <React.Fragment>
      {ReactDOM.createPortal(
        <Backdrop
          zIndexLevel={zIndexLevel}
          onClose={props.onClose}
        />, portalElement
      )}
      {ReactDOM.createPortal(<ModalOverlay zIndexLevel={zIndexLevel}>
        {props.children}
      </ModalOverlay>, portalElement)}
    </React.Fragment>
  );
};
 
export default Modal;
