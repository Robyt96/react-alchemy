import React from 'react';

import classes from './ActionIcon.module.css';

const ActionIcon = ({ icon, onClick, style, disabled, cssClass, iconCss }) => {
  const Icon = icon;
  const cssClasses = cssClass ? [cssClass] : [classes.iconContainer];
  const iconCssClass = iconCss ? iconCss : classes.icon;
  if (disabled) {
    cssClasses.push(classes.disabled);
  }

  return (
    <div className={cssClasses.join(' ')} style={style} onClick={onClick}>
      <Icon className={iconCssClass} />
    </div>
  );
};
 
export default ActionIcon;
