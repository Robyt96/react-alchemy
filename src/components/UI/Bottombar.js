import React, { useState } from 'react';
import { IoMdSettings } from 'react-icons/io';
import { GiBookmarklet } from 'react-icons/gi';

import Settings from './Settings';
import ActionIcon from './ActionIcon';
import TrashBin from '../TrashBin';
import Duplicator from '../Duplicator';
import classes from './Bottombar.module.css';

const Bottombar = ({onShowAdder, onShowEncyclopedia}) => {
  const [showingSettings, setShowingSettings] = useState(false);

  const openSettingsHandler = () => {
    setShowingSettings(true);
  }

  const closeSettingsHandler = () => {
    setShowingSettings(false);
  }

  return (
    <div className={classes.bottombar}>
      {showingSettings && <Settings onClose={closeSettingsHandler} />}
      <ActionIcon
        cssClass={classes.settings}
        iconCss={classes.settingsIcon}
        icon={IoMdSettings}
        onClick={openSettingsHandler}
      />
      <ActionIcon
        cssClass={classes.encyclopedia}
        iconCss={classes.encyclopediaIcon}
        icon={GiBookmarklet}
        onClick={onShowEncyclopedia}
      />
      <TrashBin />
      <Duplicator onClick={onShowAdder} />
    </div>
  );
};
 
export default Bottombar;
