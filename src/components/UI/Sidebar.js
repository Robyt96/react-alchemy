import React from 'react';
import { useDispatch } from 'react-redux';

import Duplicator from '../Duplicator';
import TrashBin from '../TrashBin';
import Button from './Button';
import SoundManager from './SoundManager';
import LoginStatus from '../auth/LoginStatus';
import { elementsActions } from '../../store/elementsSlice';
import classes from './Sidebar.module.css';

const Sidebar = (props) => {
  const dispatch = useDispatch();

  return (
    <div className={classes.sidebar}>
      <div className={classes.mainElements}>
        <Button
          btnType='Success'
          clicked={props.onShowAdder}
        >
          ADD ELEMENTS
        </Button>
        <Button
          btnType='Neutral'
          clicked={props.onShowEncyclopedia}
        >
          ENCYCLOPEDIA
        </Button>
        <Button
          btnType='Danger'
          clicked={() => dispatch(elementsActions.removeAllElements())}
        >
          CLEAR ALL
        </Button>
        <TrashBin />
        <Duplicator />
        <SoundManager />
      </div>
      <LoginStatus />
    </div>
  );
};
 
export default Sidebar;
