import React from 'react';

import Modal from './Modal';
import Button from '../UI/Button';
import SoundManager from './SoundManager';
import LoginStatus from '../auth/LoginStatus';
import classes from './Settings.module.css';

const Settings = ({ onClose }) => {
  return (
    <Modal onClose={onClose}>
      <div className={classes.title}>Settings</div>
      <SoundManager style={{marginTop: 0}} />
      <LoginStatus />
      <Button
        style={{width: '100px', marginTop: 20, marginBottom: 0}}
        btnType='Danger'
        clicked={onClose}
      >
        Close
      </Button>
    </Modal>
  );
};
 
export default Settings;
