import React, { useContext } from 'react';

import SoundsContext from '../../store/soundsContext';
import classes from './SoundManager.module.css';

const SoundManager = (props) => {
  const ctx = useContext(SoundsContext);

  const changeMusicVolumeHandler = (event) => {
    ctx.changeMusicVolume(event.target.value);
  }

  const changeSoundsVolumeHandler = (event) => {
    ctx.changeSoundsVolume(event.target.value);
  }

  return (
    <div className={classes.container} style={{...props.style}}>
      <label className={classes.label} htmlFor='musicVolume'>Music</label>
      <input
        id='musicVolume'
        className={classes.slider}
        type='range'
        min='0'
        max='1'
        step='0.1'
        value={ctx.musicVolume}
        onChange={changeMusicVolumeHandler}
      />
      <label className={classes.label} htmlFor='musicVolume'>Sounds</label>
      <input
        id='soundsVolume'
        className={classes.slider}
        type='range'
        min='0'
        max='1'
        step='0.1'
        value={ctx.soundsVolume}
        onChange={changeSoundsVolumeHandler}
      />
    </div>
  );
};
 
export default SoundManager;
