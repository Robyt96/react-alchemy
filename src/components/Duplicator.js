import React from 'react';
import { useDispatch } from 'react-redux';
import { useDrop } from 'react-dnd';
import { FaPlus } from 'react-icons/fa';

import ActionIcon from './UI/ActionIcon';
import useWindowDimensions from '../hooks/useWindowDimensions';
import { elementsActions } from '../store/elementsSlice';
import classes from './Duplicator.module.css';

const Duplicator = (props) => {
  const { width } = useWindowDimensions();
  const dispatch = useDispatch();

  const [{ isOver }, drop] = useDrop(() => ({
    accept: 'element',
    drop(item, monitor) {
      dispatch(elementsActions.duplicateElement({
        key: item.id,
        ...item
      }));
    },
    collect: monitor => ({ isOver: monitor.isOver() })
  }));

  let duplicator = (
    <div
      ref={drop}
      className={classes.duplicator}
      style={{backgroundColor: isOver ? '#68ce53' : ''}}
    >
      <span className={classes.label}>Drop Here to Duplicate</span>
    </div>
  );
  if (width <= 600) {
    duplicator = (
      <div ref={drop}>
        <ActionIcon
          style={{backgroundColor: isOver ? '#2bce0a' : ''}}
          cssClass={classes.addButton}
          iconCss={classes.addIcon}
          icon={FaPlus}
          onClick={props.onClick}
        />
      </div>
    );
  }
  
  return duplicator;
};
 
export default Duplicator;
