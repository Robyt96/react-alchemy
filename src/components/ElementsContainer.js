import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useDrop } from 'react-dnd';
import { IoIosUndo, IoIosRedo } from 'react-icons/io';

import ELEMENTS from '../elements';
import DragDropElement from './DragDropElement';
import ElementsAdder from './ElementsAdder';
import Encyclopedia from './Encyclopedia';
import Sidebar from './UI/Sidebar';
import Bottombar from './UI/Bottombar';
import NewElementAlert from './NewElementAlert';
import ActionIcon from './UI/ActionIcon';
import useWindowDimensions from '../hooks/useWindowDimensions';
import { elementsActions } from '../store/elementsSlice';
import classes from './ElementsContainer.module.css';

const ElementsContainer = () => {
  const past = useSelector(state => state.elements.past);
  const future = useSelector(state => state.elements.future);
  const displayedElements = useSelector(state => state.elements.present.displayedElements);
  const discoveredElements = useSelector(state => state.elements.present.discoveredElements);
  const newElementsToShow = useSelector(state => state.elements.present.newElementsToShow);
  const isShowingNewElements = newElementsToShow.length > 0;
  const specialElementToShow = useSelector(state => state.elements.present.specialElementToShow);
  const [isShowingEncyclopedia, setIsShowingEncyclopedia] = useState(false);
  const [isAddingElements, setIsAddingElements] = useState(false);
  const { width } = useWindowDimensions();

  const dispatch = useDispatch();

  const [, drop] = useDrop(() => ({
    accept: 'element',
    drop(item, monitor) {
      if (monitor.getSourceClientOffset()) {
        const { x: left, y: top } = monitor.getSourceClientOffset();

        dispatch(elementsActions.moveElement({ key: item.id, left, top }));
      }
      
      return undefined;
    },
  }), [displayedElements]);

  const doubleClickHandler = event => {
    if (event.target.id !== 'elementContainer') return;
    const { clientX: left, clientY: top } = event;
    dispatch(elementsActions.addStartingElements({ left, top }));
  };

  const showAdderHandler = () => {
    setIsAddingElements(true);
  }

  const hideAdderHandler = () => {
    setIsAddingElements(false);
  }

  const showEncyclopediaHandler = () => {
    setIsShowingEncyclopedia(true);
  }

  const hideEncyclopediaHandler = () => {
    setIsShowingEncyclopedia(false);
  }

  const undoHandler = () => {
    if (past.length > 0) {
      dispatch({ type: 'UNDO' });
    }
  }

  const redoHandler = () => {
    if (future.length > 0) {
      dispatch({ type: 'REDO' });
    }
  }

  const elementsList = Object.keys(displayedElements).map((key) => {
    const { elementId, left, top, justCreated } = displayedElements[key];
    return (
      <DragDropElement
        key={key}
        id={key}
        left={left}
        top={top}
        elementId={elementId}
        justCreated={justCreated}
      />
    );
  })

  return (
    <React.Fragment>
      {isAddingElements && <ElementsAdder onClose={hideAdderHandler} />}
      {isShowingEncyclopedia && <Encyclopedia onClose={hideEncyclopediaHandler} />}
      {isShowingNewElements && <NewElementAlert />}
      {!isShowingNewElements && specialElementToShow && <NewElementAlert special />}
      <div id='elementContainer' ref={drop} className={classes.elementContainer} onDoubleClick={doubleClickHandler}>
        {elementsList}
        <div className={classes.counter}>
          Discovered Elements:<div className={classes.bounce}>{discoveredElements.length}</div>/ {Object.keys(ELEMENTS).length}
        </div>
        <ActionIcon
          style={{bottom: 15, left: 15}} icon={IoIosUndo} onClick={undoHandler}
          disabled={past.length === 0}
        />
        <ActionIcon
          style={{bottom: 15, left: 60}} icon={IoIosRedo} onClick={redoHandler}
          disabled={future.length === 0}
        />
        {width <= 600 && <Bottombar
          onShowAdder={showAdderHandler}
          onShowEncyclopedia={showEncyclopediaHandler}
        />}
      </div>
      {width > 600 && <Sidebar
        onShowAdder={showAdderHandler}
        onShowEncyclopedia={showEncyclopediaHandler}
      />}
    </React.Fragment>
  );
};
 
export default ElementsContainer;
