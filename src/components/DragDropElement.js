import React, { useEffect, useRef, useState, useContext } from 'react';
import { useDispatch } from 'react-redux';
import { useDrag, useDrop } from 'react-dnd';
import { getEmptyImage } from 'react-dnd-html5-backend';
import useSound from 'use-sound';

import badSound from '../assets/sounds/no_element.mp3';
import goodSound from '../assets/sounds/element_found_old.mp3';
import Element from './Element';
import SoundsContext from '../store/soundsContext';
import { elementsActions } from '../store/elementsSlice';
import classes from './DragDropElement.module.css';

const DragDropElement = (props) => {
  const ref = useRef();
  const isMounted = useRef(false);
  const { id, top, left, elementId } = props;

  const [additionalCssClass, setAdditionalCssClass] = useState('');
  const [zIndex, setZIndex] = useState(0);
  const [height, setHeight] = useState(null);
  
  const soundsCtx = useContext(SoundsContext);
  const [playBad] = useSound(badSound, { volume: soundsCtx.soundsVolume });
  const [playGood] = useSound(goodSound, { volume: soundsCtx.soundsVolume });
  const dispatch = useDispatch();

  useEffect(() => {
    isMounted.current = true;
    return () => { isMounted.current = false }
  }, []);

  const { justCreated } = props;
  useEffect(() => {
    if (justCreated) {
      playGood();
      setTimeout(() => dispatch({
        type: 'IGNORE_HISTORY',
        actionType: elementsActions.clearJustCreatedProp().type,
        payload: id
      }), 1000);
    }
  }, [justCreated, playGood, dispatch, id]);

  const [{ isDragging }, drag, preview] = useDrag(() => ({
    type: 'element',
    item: () => {
      // called on drag start
      setAdditionalCssClass('');
      setHeight(0);
      return { id, left, top, elementId }
    },
    collect: (monitor) => ({
        isDragging: monitor.isDragging()
    }),
    end: (item, monitor) => {
      if (isMounted.current) {
        setHeight(null);
        if (monitor.getDropResult() && monitor.getDropResult().id) {
          const coveredElementId = monitor.getDropResult().id
          if (coveredElementId && coveredElementId !== id) {
            setAdditionalCssClass(classes.bounce);
            setZIndex(1);
            playBad();
          }
        }
      }
    }
  }), [id, left, top, elementId]);

  const [{ isOver }, drop] = useDrop(() => ({
    accept: 'element',
    drop: (item, monitor) => {
      const { x, y } = monitor.getSourceClientOffset();
      if (item.id !== id) {
        dispatch(elementsActions.combineElements({
          droppedElement: {
            key: item.id,
            elementId: item.elementId,
            left: x,
            top: y
          },
          coveredElementKey: id
        }));
      } else {
        dispatch(elementsActions.moveElement({ key: item.id, left: x, top: y }));
      }
      setZIndex(0);
      return { id }
    },
    collect: monitor => ({ isOver: monitor.isOver() })
  }), [id]);

  const doubleClickHandler = () => {
    dispatch(elementsActions.duplicateElement(
      { key: id, top, left, elementId }
    ));
  }
  
  useEffect(() => {
    preview(getEmptyImage(), { captureDraggingState: true });
  }, [preview]);

  drag(drop(ref));
  
  return (
    <Element
      dragDropRef={ref}
      cssClass={[classes.element, additionalCssClass].join(' ')}
      // cssClass={[classes.element, classes.bounce].join(' ')}
      style={{
        zIndex,
        height,
        left,
        top,
        opacity: isDragging ? 0 : '',
        backgroundColor: isOver && !isDragging ? 'indigo' : '',
        transition: 'background-color 200ms ease-out'
      }}
      onDoubleClick={doubleClickHandler}
      isTerminal={props.isTerminal}
      elementId={elementId}
      name={props.name}
    />
  );
};
 
export default DragDropElement;
