import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import ELEMENTS from '../elements';
import Modal from './UI/Modal';
import Element from './Element';
import Button from './UI/Button';
import useWindowDimensions from '../hooks/useWindowDimensions';
import { elementsActions } from '../store/elementsSlice';
import classes from './ElementsAdder.module.css';

const ElementsAdder = (props) => {
  const [selectedElements, setSelectedElements] = useState([]);
  const [filter, setFilter] = useState('');
  const discoveredElements = useSelector(state => state.elements.present.discoveredElements);
  const dispatch = useDispatch();

  const filteredElements = discoveredElements.filter(elementId => 
    ELEMENTS[elementId].name.toLocaleLowerCase()
      .includes(filter.trim().toLocaleLowerCase())
  );

  const toggleSelectionHandler = (elementId) => {
    setSelectedElements(currentSelectedElements => {
      let updatedSelectedElements;
      if (currentSelectedElements.includes(elementId)) {
        updatedSelectedElements = currentSelectedElements.filter(id => id !== elementId);
      } else {
        updatedSelectedElements = currentSelectedElements.concat(elementId);
      }

      return updatedSelectedElements;
    });
  };
  
  const renderElementItem = (elementId) => {
    const isTerminal = ELEMENTS[elementId].combine.length === 0;
    
    return (
      <Element 
        key={elementId}
        elementId={elementId}
        cssClass={[classes.element, isTerminal ? classes.terminal : null].join(' ')}
        onClick={isTerminal ? null : () => toggleSelectionHandler(elementId)}
        style={selectedElements.includes(elementId) ? {border: '3px solid green'} : null}
      />
    );
  };

  const { height: maxTop, width: maxLeft } = useWindowDimensions();
  const buttonClickHandler = () => {
    dispatch(elementsActions.addElements({
      elementsIds: selectedElements,
      maxLeft,
      maxTop
    }));
    props.onClose();
  }

  const changeFilterHandler = (event) => {
    setFilter(event.target.value);
  };

  return (
    <Modal onClose={props.onClose}>
      {maxTop > 400 && <div className={classes.title}>Add one or more Elements</div>}
      <input
        className={classes.input}
        type='text'
        placeholder='Type to filter elements...'
        value={filter}
        onChange={changeFilterHandler}
      />
      <div className={classes.list}>
        {filteredElements.sort().map((id) => 
          renderElementItem(id)
        )}
      </div>
      <Button
        style={{marginBottom: 5}}
        clicked={buttonClickHandler}
        btnType='Success'>
          {`ADD ELEMENTS (${selectedElements.length})`}
        </Button>
    </Modal>
  );
};
 
export default ElementsAdder;
