import React, { useEffect, useContext } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import useSound from 'use-sound';

import Modal from './UI/Modal';
import Button from './UI/Button';
import Element from './Element';
import { elementsActions } from '../store/elementsSlice';
import SoundsContext from '../store/soundsContext';
import newElementSound from '../assets/sounds/element_found_new.mp3';
import classes from './NewElementAlert.module.css';

const NewElementAlert = (props) => {
  const newElements = useSelector(state => state.elements.present.newElementsToShow);
  const soundsCtx = useContext(SoundsContext);
  const [play] = useSound(newElementSound, { volume: soundsCtx.soundsVolume });
  const dispatch = useDispatch();

  useEffect(() => {
    play();
  }, [play]);

  let onCloseHandler = () => {
    dispatch(elementsActions.clearNewElementsToShow());
    dispatch({ type: 'CLEAR_HISTORY' });
  };
  const numNewElements = newElements.length;
  let title = `You Have Unlocked ${numNewElements} New Element${numNewElements > 1 ? 's' : ''}!`;
  let elements = [...newElements];

  if (props.special) {
    title = 'Congratulations! You Discovered 100 Elements and Unlocked "Time"!';
    onCloseHandler = () => {
      dispatch(elementsActions.clearSpecialElementToShow());
      dispatch({ type: 'CLEAR_HISTORY' });
    };
    elements = ['time']
  }

  return (
    <Modal onClose={onCloseHandler}>
      <div className={classes.title}>
        {title}
      </div>
      <div className={classes.list}>
        {elements.sort().map((id) => 
          <Element key={id} elementId={id} cssClass={classes.element} />
        )}
      </div>
      <Button style={{width: '150px'}} clicked={onCloseHandler} btnType='Success'>OK</Button>
    </Modal>
  );
};
 
export default NewElementAlert;
