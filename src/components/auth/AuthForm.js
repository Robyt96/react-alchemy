import { useState, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import Spinner from '../UI/Spinner';
import { authenticate } from '../../store/authActions';
import classes from './AuthForm.module.css';

const AuthForm = () => {
  const emailInputRef = useRef();
  const passwordInputRef = useRef();
  
  const isLoading = useSelector(state => state.auth.loading);
  const error = useSelector(state => state.auth.error);
  const dispatch = useDispatch();

  const [isLogin, setIsLogin] = useState(true);
  const [validationError, setValidationError] = useState(null);

  const switchAuthModeHandler = () => {
    setIsLogin((prevState) => !prevState);
  };

  const submitHandler = (event) => {
    event.preventDefault();

    const enteredEmail = emailInputRef.current.value;
    const enteredPassword = passwordInputRef.current.value;

    if (!enteredEmail.includes('@') || enteredEmail.length < 6) {
      setValidationError('Please enter a valid email address');
      return;
    }
    if (enteredPassword.length < 6) {
      setValidationError('Please enter a valid password (min 6 characters)');
      return;
    }

    setValidationError(null);
    dispatch(authenticate(enteredEmail, enteredPassword, !isLogin));
  };

  return (
    <section className={classes.auth}>
      <h1>Welcome to React-Alchemy!</h1>
      <label>Login or create a new account to play</label>
      <h3>{isLogin ? 'Login' : 'Sign Up'}</h3>
      <form onSubmit={submitHandler}>
        <div className={classes.control}>
          <label htmlFor='email'>Your Email</label>
          <input type='email' id='email' required ref={emailInputRef} />
        </div>
        <div className={classes.control}>
          <label htmlFor='password'>Your Password</label>
          <input
            minLength={6}
            type='password'
            id='password'
            required
            ref={passwordInputRef}
          />
        </div>
        {error && <p className={classes.error}>{error}</p>}
        {validationError && <p className={classes.error}>{validationError}</p>}
        <div className={classes.actions}>
          {!isLoading && (
            <button>{isLogin ? 'Login' : 'Create Account'}</button>
          )}
          {isLoading && <Spinner />}
          <button
            type='button'
            className={classes.toggle}
            onClick={switchAuthModeHandler}
          >
            {isLogin ? 'Create new account' : 'Login with existing account'}
          </button>
        </div>
      </form>
    </section>
  );
};

export default AuthForm;
