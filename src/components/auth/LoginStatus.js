import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import LogoutConfirm from '../auth/LogoutConfirm';
import { logout } from '../../store/authActions';
import classes from './LoginStatus.module.css';

const LoginStatus = (props) => {
  const [isLoggingOut, setIsLoggingOut] = useState(false);

  const email = useSelector(state => state.auth.email);
  const dispatch = useDispatch();

  const logoutClickHandler = () => {
    setIsLoggingOut(true);
  }

  const logoutCancelHandler = () => {
    setIsLoggingOut(false);
  }

  return (
    <React.Fragment>
    <div className={classes.logoutContainer}>
      <p>Logged in as</p>
      <p className={classes.email}>{email}</p>
      <button
        className={classes.logout}
        onClick={logoutClickHandler}
      >
        Logout
      </button>
    </div>
      {isLoggingOut && <LogoutConfirm
        onLogoutConfirm={() => dispatch(logout())}
        onLogoutCancel={logoutCancelHandler}
      />}
    </React.Fragment>
  );
};
 
export default LoginStatus;
