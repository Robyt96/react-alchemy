import React from 'react';

import Modal from '../UI/Modal';
import Button from '../UI/Button';
import classes from './LogoutConfirm.module.css';

const LogoutConfirm = ({ onLogoutConfirm, onLogoutCancel }) => {
  return (
    <Modal zIndexLevel={2} onClose={onLogoutCancel}>
      <div className={classes.title}>Are you sure?</div>
      <div className={classes.buttonsContainer}>
        <Button
          style={{width: '100px', marginRight: '5px'}}
          btnType='Danger'
          clicked={onLogoutCancel}
        >
          Cancel
        </Button>
        <Button
          style={{width: '100px', marginLeft: '5px'}}
          btnType='Success'
          clicked={onLogoutConfirm}
        >
          Yes
        </Button>
      </div>
    </Modal>
  );
};
 
export default LogoutConfirm;
