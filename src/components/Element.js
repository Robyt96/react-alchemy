import React from 'react';

import ELEMENTS from '../elements';
import questionMark from '../assets/images/question-mark.png';
import classes from './Element.module.css';

const Element = (props) => {
  const elementId = props.elementId;
  const elementName = ELEMENTS[elementId].name;
  const isTerminal = ELEMENTS[elementId].combine.length === 0

  const elementIdUrl = elementId.replaceAll('_', '-').replaceAll('!', '').replaceAll('’','');
  const src = props.undiscovered
    ? questionMark
    : `https://littlealchemy2.gambledude.com/assets/${elementIdUrl}.svg`;

  const cssClasses = [classes.element, props.cssClass].join(' ');

  return (
    <div
      ref={props.dragDropRef}
      className={cssClasses}
      style={{...props.style}}
      onClick={props.onClick}
      onDoubleClick={props.onDoubleClick}
    >
        <img
          src={src}
          alt={elementIdUrl}
        />
        <p style={isTerminal & !props.undiscovered
          ? {color: 'red'}
          : null
        }>{elementName}</p>
    </div>
  );
};
 
export default Element;
