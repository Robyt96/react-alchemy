import React from 'react';
import { ImBin } from 'react-icons/im';
import { useDispatch } from 'react-redux';
import { useDrop } from 'react-dnd';

import ActionIcon from './UI/ActionIcon';
import useWindowDimensions from '../hooks/useWindowDimensions';
import { elementsActions } from '../store/elementsSlice';
import classes from './TrashBin.module.css';

const TrashBin = () => {
  const { width } = useWindowDimensions();
  const dispatch = useDispatch();

  const [{ isOver }, drop] = useDrop(() => ({
    accept: 'element',
    drop(item, monitor) {
      dispatch(elementsActions.removeElement(item.id));
    },
    collect: monitor => ({ isOver: monitor.isOver() })
  }));

  let bin = (
    <div
      ref={drop}
      className={classes.bin}
      style={{backgroundColor: isOver ? 'rosybrown' : ''}}
    >
      <div className={classes.label}>Drop Here to Remove</div>
    </div>
  );

  if (width <= 600) {
    bin = (
      <div ref={drop}>
        <ActionIcon
          style={{backgroundColor: isOver ? '#e04f0c' : ''}}
          cssClass={classes.binIconContainer}
          iconCss={classes.binIcon}
          icon={ImBin}
          onClick={() => dispatch(elementsActions.removeAllElements())}
        />
      </div>
    );
  }

  return bin;
};
 
export default TrashBin;
