import React from 'react';
import { useSelector } from 'react-redux';

import ELEMENTS from '../elements';
import Modal from './UI/Modal';
import Button from './UI/Button';
import Element from './Element';
import classes from './Encyclopedia.module.css';

const Encyclopedia = (props) => {
  const discoveredElements = useSelector(state => state.elements.present.discoveredElements);

  const navClickHandler = (id) => {
    document.getElementById(id).scrollIntoView({
      behavior: 'smooth'
    });
  }

  const renderElementItem = (elementId) => {
    const isTerminal = ELEMENTS[elementId].combine.length === 0;
    const undiscovered = !discoveredElements.includes(elementId);
    
    return (
      <Element 
        key={elementId}
        elementId={elementId}
        cssClass={[classes.element, isTerminal ? classes.terminal : null].join(' ')}
        undiscovered={undiscovered}
      />
    );
  };

  
  const navContent = []
  const sections = [];
  for(let i = 'a'.charCodeAt(0); i <= 'z'.charCodeAt(0); i++) {
    const letter = String.fromCharCode(i);

    const elements = Object.keys(ELEMENTS).filter(elementId => 
      elementId.startsWith(letter)
    );
    if (elements.length > 0) {
      sections.push(
        <div key={letter}>
          <p className={classes.letter} id={letter}>{letter.toUpperCase()}</p>
          <div className={classes.section}>
            {elements.map(elementId => renderElementItem(elementId))}
          </div>
        </div>
      );
  
      navContent.push(
        <div
          key={`navto_${letter}`}
          className={classes.navItem}
          onClick={() => navClickHandler(letter)}
        >
          {letter.toUpperCase()}
        </div>
      );
    }
  }


  return (
    <Modal onClose={props.onClose}>
      <div className={classes.navbar}>
        {navContent}
      </div>
      <div className={classes.list}>
        {sections}
      </div>
      <Button
        clicked={props.onClose}
        btnType='Danger'
      >
        Close
      </Button>
    </Modal>
  );
};
 
export default Encyclopedia;
