import { Redirect, Route, Switch } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { DndProvider } from 'react-dnd';
// import { HTML5Backend } from 'react-dnd-html5-backend';
import { TouchBackend } from 'react-dnd-touch-backend';

import CustomDragLayer from './components/UI/CustomDragLayer';
import AuthPage from './pages/AuthPage';
import MainPage from './pages/MainPage';

function App() {
  const token = useSelector(state => state.auth.token);
  const isLoggedIn = !!token;
  
  return (
    <div className="App">
      {isLoggedIn &&
        <Switch>
          <Route path='/' exact>
            <DndProvider backend={TouchBackend} options={{enableMouseEvents: true}}>
              <CustomDragLayer />
              <MainPage />
            </DndProvider>
          </Route>
          <Redirect to='/' />
        </Switch>
      }
      {!isLoggedIn &&
        <Switch>
          <Route path='/auth' exact>
            <AuthPage />
          </Route>
          <Redirect to='/auth' />
        </Switch>
      }
    </div>
  );
}

export default App;
