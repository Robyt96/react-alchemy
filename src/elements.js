const ELEMENTS = {
  "acid_rain": {
    "combine": [
      {
        "toCreate": "venus",
        "with": "planet"
      }
    ],
    "name": "Acid Rain"
  },
  "air": {
    "combine": [
      {
        "toCreate": "atmosphere",
        "with": "planet"
      },
      {
        "toCreate": "atmosphere",
        "with": "sky"
      },
      {
        "toCreate": "bat",
        "with": "mouse"
      },
      {
        "toCreate": "bird",
        "with": "animal"
      },
      {
        "toCreate": "bird",
        "with": "egg"
      },
      {
        "toCreate": "chill",
        "with": "cold"
      },
      {
        "toCreate": "cotton_candy",
        "with": "sugar"
      },
      {
        "toCreate": "dragon",
        "with": "lizard"
      },
      {
        "toCreate": "dust",
        "with": "earth"
      },
      {
        "toCreate": "dust",
        "with": "soil"
      },
      {
        "toCreate": "dust",
        "with": "land"
      },
      {
        "toCreate": "flute",
        "with": "wood"
      },
      {
        "toCreate": "flying_fish",
        "with": "fish"
      },
      {
        "toCreate": "flying_squirrel",
        "with": "squirrel"
      },
      {
        "toCreate": "gas",
        "with": "idea"
      },
      {
        "toCreate": "gas",
        "with": "science"
      },
      {
        "toCreate": "gust",
        "with": "small"
      },
      {
        "toCreate": "heat",
        "with": "energy"
      },
      {
        "toCreate": "kite",
        "with": "paper"
      },
      {
        "toCreate": "meteor",
        "with": "meteoroid"
      },
      {
        "toCreate": "mist",
        "with": "water"
      },
      {
        "toCreate": "mist",
        "with": "steam"
      },
      {
        "toCreate": "mist",
        "with": "rain"
      },
      {
        "toCreate": "ozone",
        "with": "electricity"
      },
      {
        "toCreate": "pressure",
        "with": "air"
      },
      {
        "toCreate": "pressure",
        "with": "atmosphere"
      },
      {
        "toCreate": "pterodactyl",
        "with": "dinosaur"
      },
      {
        "toCreate": "rust",
        "with": "metal"
      },
      {
        "toCreate": "rust",
        "with": "steel"
      },
      {
        "toCreate": "sand",
        "with": "stone"
      },
      {
        "toCreate": "sand",
        "with": "rock"
      },
      {
        "toCreate": "sand",
        "with": "pebble"
      },
      {
        "toCreate": "scuba_tank",
        "with": "container"
      },
      {
        "toCreate": "sky",
        "with": "cloud"
      },
      {
        "toCreate": "smog",
        "with": "city"
      },
      {
        "toCreate": "smoke",
        "with": "fire"
      },
      {
        "toCreate": "sound",
        "with": "wave"
      },
      {
        "toCreate": "stone",
        "with": "lava"
      },
      {
        "toCreate": "vinegar",
        "with": "wine"
      },
      {
        "toCreate": "warmth",
        "with": "heat"
      },
      {
        "toCreate": "wind",
        "with": "motion"
      },
      {
        "toCreate": "wind",
        "with": "pressure"
      }
    ],
    "name": "Air"
  },
  "airplane": {
    "combine": [
      {
        "toCreate": "bird",
        "with": "animal"
      },
      {
        "toCreate": "bird",
        "with": "egg"
      },
      {
        "toCreate": "dragon",
        "with": "lizard"
      },
      {
        "toCreate": "drone",
        "with": "robot"
      },
      {
        "toCreate": "flying_squirrel",
        "with": "squirrel"
      },
      {
        "toCreate": "hangar",
        "with": "container"
      },
      {
        "toCreate": "hangar",
        "with": "house"
      },
      {
        "toCreate": "hangar",
        "with": "wall"
      },
      {
        "toCreate": "hangar",
        "with": "barn"
      },
      {
        "toCreate": "helicopter",
        "with": "blade"
      },
      {
        "toCreate": "helicopter",
        "with": "windmill"
      },
      {
        "toCreate": "helicopter",
        "with": "wind_turbine"
      },
      {
        "toCreate": "paper_airplane",
        "with": "paper"
      },
      {
        "toCreate": "parachute",
        "with": "fabric"
      },
      {
        "toCreate": "parachute",
        "with": "umbrella"
      },
      {
        "toCreate": "pilot",
        "with": "human"
      },
      {
        "toCreate": "pterodactyl",
        "with": "dinosaur"
      },
      {
        "toCreate": "rocket",
        "with": "atmosphere"
      },
      {
        "toCreate": "seaplane",
        "with": "sea"
      },
      {
        "toCreate": "seaplane",
        "with": "ocean"
      },
      {
        "toCreate": "seaplane",
        "with": "lake"
      },
      {
        "toCreate": "seaplane",
        "with": "water"
      },
      {
        "toCreate": "spaceship",
        "with": "space"
      },
      {
        "toCreate": "ufo",
        "with": "alien"
      }
    ],
    "name": "Airplane"
  },
  "alarm_clock": {
    "combine": [
      {
        "toCreate": "cuckoo",
        "with": "bird"
      },
      {
        "toCreate": "cuckoo",
        "with": "owl"
      },
      {
        "toCreate": "cuckoo",
        "with": "hummingbird"
      },
      {
        "toCreate": "egg_timer",
        "with": "egg"
      }
    ],
    "name": "Alarm Clock"
  },
  "alchemist": {
    "combine": [
      {
        "toCreate": "gold",
        "with": "metal"
      },
      {
        "toCreate": "gold",
        "with": "steel"
      },
      {
        "toCreate": "idea",
        "with": "alchemist"
      },
      {
        "toCreate": "little_alchemy",
        "with": "small"
      }
    ],
    "name": "Alchemist"
  },
  "alcohol": {
    "combine": [
      {
        "toCreate": "beer",
        "with": "wheat"
      },
      {
        "toCreate": "bottle",
        "with": "container"
      },
      {
        "toCreate": "drunk",
        "with": "human"
      },
      {
        "toCreate": "fire",
        "with": "fire"
      },
      {
        "toCreate": "perfume",
        "with": "flower"
      },
      {
        "toCreate": "perfume",
        "with": "rose"
      },
      {
        "toCreate": "perfume",
        "with": "sunflower"
      },
      {
        "toCreate": "sugar",
        "with": "fire"
      },
      {
        "toCreate": "sugar",
        "with": "energy"
      },
      {
        "toCreate": "wine",
        "with": "fruit"
      }
    ],
    "name": "Alcohol"
  },
  "algae": {
    "combine": [
      {
        "toCreate": "moss",
        "with": "stone"
      },
      {
        "toCreate": "moss",
        "with": "rock"
      },
      {
        "toCreate": "moss",
        "with": "boulder"
      },
      {
        "toCreate": "oxygen",
        "with": "sun"
      },
      {
        "toCreate": "oxygen",
        "with": "carbon_dioxide"
      },
      {
        "toCreate": "plant",
        "with": "land"
      },
      {
        "toCreate": "plant",
        "with": "earth"
      },
      {
        "toCreate": "swamp",
        "with": "lake"
      },
      {
        "toCreate": "swamp",
        "with": "pond"
      }
    ],
    "name": "Algae"
  },
  "alien": {
    "combine": [
      {
        "toCreate": "ufo",
        "with": "rocket"
      },
      {
        "toCreate": "ufo",
        "with": "airplane"
      },
      {
        "toCreate": "ufo",
        "with": "spaceship"
      },
      {
        "toCreate": "ufo",
        "with": "space_station"
      },
      {
        "toCreate": "ufo",
        "with": "sky"
      },
      {
        "toCreate": "ufo",
        "with": "container"
      }
    ],
    "name": "Alien"
  },
  "allergy": {
    "combine": [],
    "name": "Allergy"
  },
  "alligator": {
    "combine": [],
    "name": "Alligator"
  },
  "alpaca": {
    "combine": [],
    "name": "Alpaca"
  },
  "ambulance": {
    "combine": [
      {
        "toCreate": "garage",
        "with": "house"
      },
      {
        "toCreate": "garage",
        "with": "container"
      },
      {
        "toCreate": "garage",
        "with": "wall"
      },
      {
        "toCreate": "garage",
        "with": "barn"
      },
      {
        "toCreate": "hospital",
        "with": "house"
      },
      {
        "toCreate": "hospital",
        "with": "wall"
      },
      {
        "toCreate": "scalpel",
        "with": "blade"
      },
      {
        "toCreate": "scalpel",
        "with": "sword"
      }
    ],
    "name": "Ambulance"
  },
  "angel": {
    "combine": [
      {
        "toCreate": "harp",
        "with": "music"
      },
      {
        "toCreate": "harp",
        "with": "musician"
      },
      {
        "toCreate": "harp",
        "with": "wire"
      }
    ],
    "name": "Angel"
  },
  "angler": {
    "combine": [
      {
        "toCreate": "idea",
        "with": "angler"
      },
      {
        "toCreate": "net",
        "with": "rope"
      }
    ],
    "name": "Angler"
  },
  "animal": {
    "combine": [
      {
        "toCreate": "ant",
        "with": "grass"
      },
      {
        "toCreate": "armadillo",
        "with": "armor"
      },
      {
        "toCreate": "beaver",
        "with": "wood"
      },
      {
        "toCreate": "beaver",
        "with": "dam"
      },
      {
        "toCreate": "beaver",
        "with": "river"
      },
      {
        "toCreate": "beaver",
        "with": "stream"
      },
      {
        "toCreate": "bee",
        "with": "flower"
      },
      {
        "toCreate": "bee",
        "with": "garden"
      },
      {
        "toCreate": "bird",
        "with": "sky"
      },
      {
        "toCreate": "bird",
        "with": "air"
      },
      {
        "toCreate": "bird",
        "with": "airplane"
      },
      {
        "toCreate": "butterfly",
        "with": "flower"
      },
      {
        "toCreate": "butterfly",
        "with": "garden"
      },
      {
        "toCreate": "butterfly",
        "with": "rainbow"
      },
      {
        "toCreate": "butterfly",
        "with": "double_rainbow!"
      },
      {
        "toCreate": "camel",
        "with": "desert"
      },
      {
        "toCreate": "camel",
        "with": "dune"
      },
      {
        "toCreate": "carbon_dioxide",
        "with": "oxygen"
      },
      {
        "toCreate": "cat",
        "with": "milk"
      },
      {
        "toCreate": "cat",
        "with": "coconut_milk"
      },
      {
        "toCreate": "cat",
        "with": "night"
      },
      {
        "toCreate": "computer_mouse",
        "with": "computer"
      },
      {
        "toCreate": "domestication",
        "with": "farmer"
      },
      {
        "toCreate": "domestication",
        "with": "human"
      },
      {
        "toCreate": "domestication",
        "with": "science"
      },
      {
        "toCreate": "fish",
        "with": "water"
      },
      {
        "toCreate": "fish",
        "with": "lake"
      },
      {
        "toCreate": "fish",
        "with": "sea"
      },
      {
        "toCreate": "fish",
        "with": "ocean"
      },
      {
        "toCreate": "fox",
        "with": "chicken"
      },
      {
        "toCreate": "fox",
        "with": "chicken_coop"
      },
      {
        "toCreate": "frog",
        "with": "pond"
      },
      {
        "toCreate": "frog",
        "with": "puddle"
      },
      {
        "toCreate": "hedgehog",
        "with": "needle"
      },
      {
        "toCreate": "horse",
        "with": "field"
      },
      {
        "toCreate": "horse",
        "with": "land"
      },
      {
        "toCreate": "horse",
        "with": "horseshoe"
      },
      {
        "toCreate": "horse",
        "with": "saddle"
      },
      {
        "toCreate": "human",
        "with": "time"
      },
      {
        "toCreate": "human",
        "with": "tool"
      },
      {
        "toCreate": "lion",
        "with": "cat"
      },
      {
        "toCreate": "livestock",
        "with": "field"
      },
      {
        "toCreate": "livestock",
        "with": "farmer"
      },
      {
        "toCreate": "livestock",
        "with": "domestication"
      },
      {
        "toCreate": "livestock",
        "with": "barn"
      },
      {
        "toCreate": "lizard",
        "with": "stone"
      },
      {
        "toCreate": "lizard",
        "with": "rock"
      },
      {
        "toCreate": "lizard",
        "with": "swamp"
      },
      {
        "toCreate": "meat",
        "with": "tool"
      },
      {
        "toCreate": "meat",
        "with": "axe"
      },
      {
        "toCreate": "meat",
        "with": "sword"
      },
      {
        "toCreate": "meat",
        "with": "butcher"
      },
      {
        "toCreate": "monkey",
        "with": "tree"
      },
      {
        "toCreate": "mouse",
        "with": "cheese"
      },
      {
        "toCreate": "origami",
        "with": "paper"
      },
      {
        "toCreate": "penguin",
        "with": "antarctica"
      },
      {
        "toCreate": "pig",
        "with": "mud"
      },
      {
        "toCreate": "polar_bear",
        "with": "ice"
      },
      {
        "toCreate": "polar_bear",
        "with": "arctic"
      },
      {
        "toCreate": "rabbit",
        "with": "carrot"
      },
      {
        "toCreate": "rat",
        "with": "pirate_ship"
      },
      {
        "toCreate": "rat",
        "with": "sailboat"
      },
      {
        "toCreate": "reindeer",
        "with": "santa"
      },
      {
        "toCreate": "reindeer",
        "with": "christmas_tree"
      },
      {
        "toCreate": "reindeer",
        "with": "christmas_stocking"
      },
      {
        "toCreate": "scorpion",
        "with": "dune"
      },
      {
        "toCreate": "scorpion",
        "with": "sand"
      },
      {
        "toCreate": "sloth",
        "with": "time"
      },
      {
        "toCreate": "snake",
        "with": "rope"
      },
      {
        "toCreate": "snake",
        "with": "wire"
      },
      {
        "toCreate": "sound",
        "with": "wave"
      },
      {
        "toCreate": "spider",
        "with": "thread"
      },
      {
        "toCreate": "spider",
        "with": "web"
      },
      {
        "toCreate": "spider",
        "with": "net"
      },
      {
        "toCreate": "squirrel",
        "with": "nuts"
      },
      {
        "toCreate": "turtle",
        "with": "beach"
      },
      {
        "toCreate": "wild_boar",
        "with": "pig"
      },
      {
        "toCreate": "wolf",
        "with": "moon"
      },
      {
        "toCreate": "wolf",
        "with": "dog"
      },
      {
        "toCreate": "zoo",
        "with": "container"
      },
      {
        "toCreate": "zoo",
        "with": "cage"
      }
    ],
    "name": "Animal"
  },
  "ant": {
    "combine": [
      {
        "toCreate": "ant_farm",
        "with": "farm"
      },
      {
        "toCreate": "ant_farm",
        "with": "jar"
      },
      {
        "toCreate": "ant_farm",
        "with": "glass"
      },
      {
        "toCreate": "anthill",
        "with": "house"
      },
      {
        "toCreate": "anthill",
        "with": "container"
      },
      {
        "toCreate": "anthill",
        "with": "hill"
      },
      {
        "toCreate": "anthill",
        "with": "earth"
      },
      {
        "toCreate": "anthill",
        "with": "land"
      },
      {
        "toCreate": "anthill",
        "with": "soil"
      },
      {
        "toCreate": "egg",
        "with": "ant"
      },
      {
        "toCreate": "small",
        "with": "philosophy"
      }
    ],
    "name": "Ant"
  },
  "ant_farm": {
    "combine": [],
    "name": "Ant Farm"
  },
  "antarctica": {
    "combine": [
      {
        "toCreate": "aurora",
        "with": "electricity"
      },
      {
        "toCreate": "aurora",
        "with": "sky"
      },
      {
        "toCreate": "husky",
        "with": "dog"
      },
      {
        "toCreate": "iceberg",
        "with": "sea"
      },
      {
        "toCreate": "iceberg",
        "with": "ocean"
      },
      {
        "toCreate": "penguin",
        "with": "bird"
      },
      {
        "toCreate": "penguin",
        "with": "animal"
      },
      {
        "toCreate": "sleigh",
        "with": "cart"
      },
      {
        "toCreate": "sleigh",
        "with": "wagon"
      },
      {
        "toCreate": "yeti",
        "with": "story"
      },
      {
        "toCreate": "yeti",
        "with": "legend"
      }
    ],
    "name": "Antarctica"
  },
  "anthill": {
    "combine": [
      {
        "toCreate": "ant_farm",
        "with": "farm"
      },
      {
        "toCreate": "ant_farm",
        "with": "jar"
      },
      {
        "toCreate": "ant_farm",
        "with": "farm"
      }
    ],
    "name": "Anthill"
  },
  "apron": {
    "combine": [],
    "name": "Apron"
  },
  "aquarium": {
    "combine": [
      {
        "toCreate": "greenhouse",
        "with": "plant"
      },
      {
        "toCreate": "greenhouse",
        "with": "grass"
      },
      {
        "toCreate": "greenhouse",
        "with": "tree"
      },
      {
        "toCreate": "swimming_pool",
        "with": "big"
      }
    ],
    "name": "Aquarium"
  },
  "archeologist": {
    "combine": [],
    "name": "Archeologist"
  },
  "archipelago": {
    "combine": [],
    "name": "Archipelago"
  },
  "arctic": {
    "combine": [
      {
        "toCreate": "aurora",
        "with": "electricity"
      },
      {
        "toCreate": "aurora",
        "with": "sky"
      },
      {
        "toCreate": "husky",
        "with": "dog"
      },
      {
        "toCreate": "iceberg",
        "with": "sea"
      },
      {
        "toCreate": "iceberg",
        "with": "ocean"
      },
      {
        "toCreate": "polar_bear",
        "with": "animal"
      },
      {
        "toCreate": "seal",
        "with": "dog"
      },
      {
        "toCreate": "sleigh",
        "with": "cart"
      },
      {
        "toCreate": "sleigh",
        "with": "wagon"
      }
    ],
    "name": "Arctic"
  },
  "armadillo": {
    "combine": [],
    "name": "Armadillo"
  },
  "armor": {
    "combine": [
      {
        "toCreate": "armadillo",
        "with": "animal"
      },
      {
        "toCreate": "armadillo",
        "with": "dog"
      },
      {
        "toCreate": "armadillo",
        "with": "cat"
      },
      {
        "toCreate": "bulletproof_vest",
        "with": "bullet"
      },
      {
        "toCreate": "bulletproof_vest",
        "with": "gun"
      },
      {
        "toCreate": "knight",
        "with": "human"
      },
      {
        "toCreate": "knight",
        "with": "warrior"
      },
      {
        "toCreate": "robot",
        "with": "life"
      },
      {
        "toCreate": "robot",
        "with": "golem"
      },
      {
        "toCreate": "safety_glasses",
        "with": "glasses"
      },
      {
        "toCreate": "tank",
        "with": "car"
      }
    ],
    "name": "Armor"
  },
  "arrow": {
    "combine": [
      {
        "toCreate": "corpse",
        "with": "human"
      }
    ],
    "name": "Arrow"
  },
  "ash": {
    "combine": [
      {
        "toCreate": "soap",
        "with": "oil"
      },
      {
        "toCreate": "soap",
        "with": "wax"
      },
      {
        "toCreate": "steel",
        "with": "metal"
      }
    ],
    "name": "Ash"
  },
  "astronaut": {
    "combine": [
      {
        "toCreate": "cyborg",
        "with": "robot"
      },
      {
        "toCreate": "idea",
        "with": "astronaut"
      },
      {
        "toCreate": "spaceship",
        "with": "container"
      },
      {
        "toCreate": "statue",
        "with": "medusa"
      }
    ],
    "name": "Astronaut"
  },
  "atmosphere": {
    "combine": [
      {
        "toCreate": "aurora",
        "with": "electricity"
      },
      {
        "toCreate": "aurora",
        "with": "sun"
      },
      {
        "toCreate": "bat",
        "with": "mouse"
      },
      {
        "toCreate": "cloud",
        "with": "mist"
      },
      {
        "toCreate": "cloud",
        "with": "water"
      },
      {
        "toCreate": "energy",
        "with": "fire"
      },
      {
        "toCreate": "fireworks",
        "with": "explosion"
      },
      {
        "toCreate": "meteor",
        "with": "meteoroid"
      },
      {
        "toCreate": "ozone",
        "with": "electricity"
      },
      {
        "toCreate": "parachute",
        "with": "umbrella"
      },
      {
        "toCreate": "pressure",
        "with": "atmosphere"
      },
      {
        "toCreate": "pressure",
        "with": "air"
      },
      {
        "toCreate": "rocket",
        "with": "airplane"
      },
      {
        "toCreate": "rocket",
        "with": "metal"
      },
      {
        "toCreate": "rocket",
        "with": "steel"
      },
      {
        "toCreate": "rocket",
        "with": "train"
      },
      {
        "toCreate": "rocket",
        "with": "machine"
      },
      {
        "toCreate": "rocket",
        "with": "boat"
      },
      {
        "toCreate": "rocket",
        "with": "steamboat"
      },
      {
        "toCreate": "rocket",
        "with": "car"
      },
      {
        "toCreate": "rocket",
        "with": "pirate_ship"
      },
      {
        "toCreate": "scuba_tank",
        "with": "container"
      },
      {
        "toCreate": "sky",
        "with": "sun"
      },
      {
        "toCreate": "sky",
        "with": "light"
      },
      {
        "toCreate": "smog",
        "with": "city"
      },
      {
        "toCreate": "space_station",
        "with": "house"
      },
      {
        "toCreate": "space_station",
        "with": "wall"
      },
      {
        "toCreate": "space_station",
        "with": "village"
      },
      {
        "toCreate": "storm",
        "with": "electricity"
      },
      {
        "toCreate": "wind",
        "with": "motion"
      }
    ],
    "name": "Atmosphere"
  },
  "atomic_bomb": {
    "combine": [],
    "name": "Atomic Bomb"
  },
  "aurora": {
    "combine": [],
    "name": "Aurora"
  },
  "avalanche": {
    "combine": [],
    "name": "Avalanche"
  },
  "axe": {
    "combine": [
      {
        "toCreate": "bayonet",
        "with": "gun"
      },
      {
        "toCreate": "chainsaw",
        "with": "machine"
      },
      {
        "toCreate": "chainsaw",
        "with": "electricity"
      },
      {
        "toCreate": "chainsaw",
        "with": "motion"
      },
      {
        "toCreate": "coconut_milk",
        "with": "coconut"
      },
      {
        "toCreate": "lumberjack",
        "with": "human"
      },
      {
        "toCreate": "meat",
        "with": "livestock"
      },
      {
        "toCreate": "meat",
        "with": "chicken"
      },
      {
        "toCreate": "meat",
        "with": "pig"
      },
      {
        "toCreate": "meat",
        "with": "cow"
      },
      {
        "toCreate": "meat",
        "with": "animal"
      },
      {
        "toCreate": "meat",
        "with": "fish"
      },
      {
        "toCreate": "meat",
        "with": "swordfish"
      },
      {
        "toCreate": "meat",
        "with": "flying_fish"
      },
      {
        "toCreate": "meat",
        "with": "shark"
      },
      {
        "toCreate": "meat",
        "with": "frog"
      },
      {
        "toCreate": "scythe",
        "with": "grass"
      },
      {
        "toCreate": "scythe",
        "with": "wheat"
      },
      {
        "toCreate": "wood",
        "with": "tree"
      },
      {
        "toCreate": "wood",
        "with": "forest"
      }
    ],
    "name": "Axe"
  },
  "bacon": {
    "combine": [
      {
        "toCreate": "sandwich",
        "with": "bread"
      }
    ],
    "name": "Bacon"
  },
  "bacteria": {
    "combine": [
      {
        "toCreate": "cheese",
        "with": "milk"
      },
      {
        "toCreate": "microscope",
        "with": "glass"
      },
      {
        "toCreate": "microscope",
        "with": "glasses"
      },
      {
        "toCreate": "microscope",
        "with": "safety_glasses"
      },
      {
        "toCreate": "microscope",
        "with": "lens"
      },
      {
        "toCreate": "mold",
        "with": "bread"
      },
      {
        "toCreate": "mold",
        "with": "vegetable"
      },
      {
        "toCreate": "mold",
        "with": "fruit"
      },
      {
        "toCreate": "organic_matter",
        "with": "death"
      },
      {
        "toCreate": "plankton",
        "with": "water"
      },
      {
        "toCreate": "plankton",
        "with": "ocean"
      },
      {
        "toCreate": "plankton",
        "with": "sea"
      },
      {
        "toCreate": "sickness",
        "with": "human"
      },
      {
        "toCreate": "small",
        "with": "philosophy"
      },
      {
        "toCreate": "yogurt",
        "with": "milk"
      },
      {
        "toCreate": "yogurt",
        "with": "ice_cream"
      },
      {
        "toCreate": "zombie",
        "with": "corpse"
      }
    ],
    "name": "Bacteria"
  },
  "baker": {
    "combine": [
      {
        "toCreate": "apron",
        "with": "fabric"
      },
      {
        "toCreate": "bakery",
        "with": "house"
      },
      {
        "toCreate": "bakery",
        "with": "city"
      },
      {
        "toCreate": "bakery",
        "with": "village"
      },
      {
        "toCreate": "bakery",
        "with": "container"
      },
      {
        "toCreate": "cookie",
        "with": "cookie_dough"
      },
      {
        "toCreate": "cyborg",
        "with": "robot"
      },
      {
        "toCreate": "idea",
        "with": "baker"
      },
      {
        "toCreate": "pie",
        "with": "fruit"
      },
      {
        "toCreate": "recipe",
        "with": "paper"
      },
      {
        "toCreate": "recipe",
        "with": "newspaper"
      },
      {
        "toCreate": "statue",
        "with": "medusa"
      }
    ],
    "name": "Baker"
  },
  "bakery": {
    "combine": [
      {
        "toCreate": "baker",
        "with": "human"
      },
      {
        "toCreate": "pie",
        "with": "fruit"
      },
      {
        "toCreate": "recipe",
        "with": "paper"
      },
      {
        "toCreate": "recipe",
        "with": "newspaper"
      },
      {
        "toCreate": "village",
        "with": "container"
      },
      {
        "toCreate": "village",
        "with": "house"
      }
    ],
    "name": "Bakery"
  },
  "banana": {
    "combine": [
      {
        "toCreate": "banana_bread",
        "with": "bread"
      },
      {
        "toCreate": "banana_bread",
        "with": "dough"
      }
    ],
    "name": "Banana"
  },
  "banana_bread": {
    "combine": [
      {
        "toCreate": "baker",
        "with": "human"
      }
    ],
    "name": "Banana Bread"
  },
  "bandage": {
    "combine": [],
    "name": "Bandage"
  },
  "bank": {
    "combine": [
      {
        "toCreate": "city",
        "with": "container"
      },
      {
        "toCreate": "city",
        "with": "skyscraper"
      },
      {
        "toCreate": "city",
        "with": "post_office"
      },
      {
        "toCreate": "money",
        "with": "paper"
      },
      {
        "toCreate": "silo",
        "with": "wheat"
      }
    ],
    "name": "Bank"
  },
  "barn": {
    "combine": [
      {
        "toCreate": "chicken",
        "with": "bird"
      },
      {
        "toCreate": "chicken",
        "with": "egg"
      },
      {
        "toCreate": "chicken_coop",
        "with": "chicken"
      },
      {
        "toCreate": "cow",
        "with": "livestock"
      },
      {
        "toCreate": "dog",
        "with": "wolf"
      },
      {
        "toCreate": "farm",
        "with": "farmer"
      },
      {
        "toCreate": "farm",
        "with": "house"
      },
      {
        "toCreate": "farmer",
        "with": "human"
      },
      {
        "toCreate": "garage",
        "with": "car"
      },
      {
        "toCreate": "garage",
        "with": "ambulance"
      },
      {
        "toCreate": "garage",
        "with": "motorcycle"
      },
      {
        "toCreate": "garage",
        "with": "bus"
      },
      {
        "toCreate": "garage",
        "with": "sleigh"
      },
      {
        "toCreate": "garage",
        "with": "electric_car"
      },
      {
        "toCreate": "garage",
        "with": "snowmobile"
      },
      {
        "toCreate": "garage",
        "with": "tractor"
      },
      {
        "toCreate": "garage",
        "with": "rv"
      },
      {
        "toCreate": "garage",
        "with": "ice_cream_truck"
      },
      {
        "toCreate": "hangar",
        "with": "airplane"
      },
      {
        "toCreate": "hangar",
        "with": "seaplane"
      },
      {
        "toCreate": "hangar",
        "with": "helicopter"
      },
      {
        "toCreate": "hangar",
        "with": "rocket"
      },
      {
        "toCreate": "hangar",
        "with": "spaceship"
      },
      {
        "toCreate": "hay",
        "with": "grass"
      },
      {
        "toCreate": "hay",
        "with": "pitchfork"
      },
      {
        "toCreate": "hay_bale",
        "with": "hay"
      },
      {
        "toCreate": "horse",
        "with": "livestock"
      },
      {
        "toCreate": "livestock",
        "with": "animal"
      },
      {
        "toCreate": "mouse",
        "with": "cheese"
      },
      {
        "toCreate": "scarecrow",
        "with": "statue"
      },
      {
        "toCreate": "silo",
        "with": "wheat"
      }
    ],
    "name": "Barn"
  },
  "bat": {
    "combine": [
      {
        "toCreate": "airplane",
        "with": "metal"
      },
      {
        "toCreate": "airplane",
        "with": "steel"
      },
      {
        "toCreate": "airplane",
        "with": "machine"
      },
      {
        "toCreate": "cave",
        "with": "house"
      },
      {
        "toCreate": "cave",
        "with": "container"
      },
      {
        "toCreate": "vampire",
        "with": "human"
      }
    ],
    "name": "Bat"
  },
  "batter": {
    "combine": [
      {
        "toCreate": "baker",
        "with": "human"
      }
    ],
    "name": "Batter"
  },
  "battery": {
    "combine": [],
    "name": "Battery"
  },
  "bayonet": {
    "combine": [
      {
        "toCreate": "pirate",
        "with": "sailor"
      }
    ],
    "name": "Bayonet"
  },
  "bbq": {
    "combine": [
      {
        "toCreate": "chicken_wing",
        "with": "chicken"
      },
      {
        "toCreate": "steak",
        "with": "meat"
      },
      {
        "toCreate": "steak",
        "with": "cow"
      }
    ],
    "name": "BBQ"
  },
  "beach": {
    "combine": [
      {
        "toCreate": "animal",
        "with": "life"
      },
      {
        "toCreate": "coconut",
        "with": "fruit"
      },
      {
        "toCreate": "coconut",
        "with": "vegetable"
      },
      {
        "toCreate": "dune",
        "with": "wind"
      },
      {
        "toCreate": "lighthouse",
        "with": "light"
      },
      {
        "toCreate": "lighthouse",
        "with": "spotlight"
      },
      {
        "toCreate": "palm",
        "with": "tree"
      },
      {
        "toCreate": "picnic",
        "with": "sandwich"
      },
      {
        "toCreate": "sand_castle",
        "with": "castle"
      },
      {
        "toCreate": "seagull",
        "with": "bird"
      },
      {
        "toCreate": "seagull",
        "with": "pigeon"
      },
      {
        "toCreate": "seagull",
        "with": "rat"
      },
      {
        "toCreate": "sunglasses",
        "with": "glasses"
      },
      {
        "toCreate": "surfer",
        "with": "human"
      },
      {
        "toCreate": "turtle",
        "with": "animal"
      },
      {
        "toCreate": "turtle",
        "with": "lizard"
      },
      {
        "toCreate": "turtle",
        "with": "egg"
      }
    ],
    "name": "Beach"
  },
  "beaver": {
    "combine": [
      {
        "toCreate": "dam",
        "with": "river"
      },
      {
        "toCreate": "dam",
        "with": "tree"
      },
      {
        "toCreate": "dam",
        "with": "stream"
      },
      {
        "toCreate": "dam",
        "with": "house"
      },
      {
        "toCreate": "dam",
        "with": "lake"
      },
      {
        "toCreate": "dam",
        "with": "water"
      },
      {
        "toCreate": "platypus",
        "with": "duck"
      },
      {
        "toCreate": "platypus",
        "with": "bird"
      },
      {
        "toCreate": "platypus",
        "with": "seagull"
      }
    ],
    "name": "Beaver"
  },
  "bee": {
    "combine": [
      {
        "toCreate": "beehive",
        "with": "house"
      },
      {
        "toCreate": "beehive",
        "with": "tree"
      },
      {
        "toCreate": "beehive",
        "with": "forest"
      },
      {
        "toCreate": "beehive",
        "with": "wall"
      },
      {
        "toCreate": "beehive",
        "with": "wood"
      },
      {
        "toCreate": "beehive",
        "with": "container"
      },
      {
        "toCreate": "beekeeper",
        "with": "human"
      },
      {
        "toCreate": "beekeeper",
        "with": "farmer"
      },
      {
        "toCreate": "butterfly_net",
        "with": "net"
      },
      {
        "toCreate": "egg",
        "with": "bee"
      },
      {
        "toCreate": "honey",
        "with": "flower"
      },
      {
        "toCreate": "honey",
        "with": "beehive"
      },
      {
        "toCreate": "honey",
        "with": "time"
      },
      {
        "toCreate": "honey",
        "with": "bee"
      },
      {
        "toCreate": "hummingbird",
        "with": "bird"
      },
      {
        "toCreate": "seed",
        "with": "pollen"
      },
      {
        "toCreate": "small",
        "with": "philosophy"
      },
      {
        "toCreate": "wax",
        "with": "beehive"
      }
    ],
    "name": "Bee"
  },
  "beehive": {
    "combine": [
      {
        "toCreate": "beekeeper",
        "with": "human"
      },
      {
        "toCreate": "beekeeper",
        "with": "farmer"
      },
      {
        "toCreate": "honey",
        "with": "bee"
      },
      {
        "toCreate": "honey",
        "with": "beekeeper"
      },
      {
        "toCreate": "wax",
        "with": "bee"
      },
      {
        "toCreate": "wax",
        "with": "wall"
      },
      {
        "toCreate": "wax",
        "with": "blade"
      },
      {
        "toCreate": "wax",
        "with": "sword"
      },
      {
        "toCreate": "wax",
        "with": "tool"
      },
      {
        "toCreate": "wax",
        "with": "beekeeper"
      }
    ],
    "name": "Beehive"
  },
  "beekeeper": {
    "combine": [
      {
        "toCreate": "honey",
        "with": "beehive"
      },
      {
        "toCreate": "wax",
        "with": "beehive"
      }
    ],
    "name": "Beekeeper"
  },
  "beer": {
    "combine": [
      {
        "toCreate": "bottle",
        "with": "container"
      },
      {
        "toCreate": "drunk",
        "with": "human"
      },
      {
        "toCreate": "sugar",
        "with": "fire"
      },
      {
        "toCreate": "sugar",
        "with": "energy"
      }
    ],
    "name": "Beer"
  },
  "bell": {
    "combine": [
      {
        "toCreate": "alarm_clock",
        "with": "clock"
      },
      {
        "toCreate": "alarm_clock",
        "with": "watch"
      }
    ],
    "name": "Bell"
  },
  "bicycle": {
    "combine": [
      {
        "toCreate": "car",
        "with": "bicycle"
      },
      {
        "toCreate": "car",
        "with": "wheel"
      },
      {
        "toCreate": "cyclist",
        "with": "human"
      },
      {
        "toCreate": "motorcycle",
        "with": "machine"
      },
      {
        "toCreate": "motorcycle",
        "with": "steel"
      },
      {
        "toCreate": "motorcycle",
        "with": "metal"
      },
      {
        "toCreate": "motorcycle",
        "with": "steam_engine"
      },
      {
        "toCreate": "motorcycle",
        "with": "combustion_engine"
      }
    ],
    "name": "Bicycle"
  },
  "big": {
    "combine": [
      {
        "toCreate": "atomic_bomb",
        "with": "explosion"
      },
      {
        "toCreate": "blizzard",
        "with": "snow"
      },
      {
        "toCreate": "boulder",
        "with": "rock"
      },
      {
        "toCreate": "boulder",
        "with": "stone"
      },
      {
        "toCreate": "bus",
        "with": "car"
      },
      {
        "toCreate": "city",
        "with": "village"
      },
      {
        "toCreate": "clock",
        "with": "watch"
      },
      {
        "toCreate": "continent",
        "with": "land"
      },
      {
        "toCreate": "dinosaur",
        "with": "lizard"
      },
      {
        "toCreate": "duck",
        "with": "duckling"
      },
      {
        "toCreate": "flood",
        "with": "rain"
      },
      {
        "toCreate": "hill",
        "with": "boulder"
      },
      {
        "toCreate": "jupiter",
        "with": "planet"
      },
      {
        "toCreate": "jupiter",
        "with": "saturn"
      },
      {
        "toCreate": "lake",
        "with": "pond"
      },
      {
        "toCreate": "land",
        "with": "soil"
      },
      {
        "toCreate": "legend",
        "with": "story"
      },
      {
        "toCreate": "lion",
        "with": "cat"
      },
      {
        "toCreate": "mountain",
        "with": "hill"
      },
      {
        "toCreate": "mountain",
        "with": "earth"
      },
      {
        "toCreate": "ocean",
        "with": "sea"
      },
      {
        "toCreate": "ostrich",
        "with": "bird"
      },
      {
        "toCreate": "plant",
        "with": "grass"
      },
      {
        "toCreate": "pond",
        "with": "puddle"
      },
      {
        "toCreate": "rat",
        "with": "mouse"
      },
      {
        "toCreate": "river",
        "with": "stream"
      },
      {
        "toCreate": "rock",
        "with": "pebble"
      },
      {
        "toCreate": "sea",
        "with": "lake"
      },
      {
        "toCreate": "skyscraper",
        "with": "house"
      },
      {
        "toCreate": "stream",
        "with": "rivulet"
      },
      {
        "toCreate": "swimming_pool",
        "with": "aquarium"
      },
      {
        "toCreate": "tornado",
        "with": "wind"
      },
      {
        "toCreate": "tree",
        "with": "plant"
      },
      {
        "toCreate": "vault",
        "with": "safe"
      }
    ],
    "name": "Big"
  },
  "bird": {
    "combine": [
      {
        "toCreate": "airplane",
        "with": "metal"
      },
      {
        "toCreate": "airplane",
        "with": "steel"
      },
      {
        "toCreate": "airplane",
        "with": "machine"
      },
      {
        "toCreate": "airplane",
        "with": "steam_engine"
      },
      {
        "toCreate": "airplane",
        "with": "train"
      },
      {
        "toCreate": "airplane",
        "with": "car"
      },
      {
        "toCreate": "airplane",
        "with": "boat"
      },
      {
        "toCreate": "airplane",
        "with": "steamboat"
      },
      {
        "toCreate": "airplane",
        "with": "sailboat"
      },
      {
        "toCreate": "angel",
        "with": "human"
      },
      {
        "toCreate": "bat",
        "with": "mouse"
      },
      {
        "toCreate": "birdcage",
        "with": "container"
      },
      {
        "toCreate": "birdcage",
        "with": "wall"
      },
      {
        "toCreate": "birdcage",
        "with": "safe"
      },
      {
        "toCreate": "birdhouse",
        "with": "house"
      },
      {
        "toCreate": "birdhouse",
        "with": "wall"
      },
      {
        "toCreate": "carbon_dioxide",
        "with": "oxygen"
      },
      {
        "toCreate": "chicken",
        "with": "domestication"
      },
      {
        "toCreate": "chicken",
        "with": "livestock"
      },
      {
        "toCreate": "chicken",
        "with": "farmer"
      },
      {
        "toCreate": "chicken",
        "with": "barn"
      },
      {
        "toCreate": "chicken",
        "with": "farm"
      },
      {
        "toCreate": "crow",
        "with": "scarecrow"
      },
      {
        "toCreate": "crow",
        "with": "field"
      },
      {
        "toCreate": "cuckoo",
        "with": "clock"
      },
      {
        "toCreate": "cuckoo",
        "with": "alarm_clock"
      },
      {
        "toCreate": "duck",
        "with": "water"
      },
      {
        "toCreate": "duck",
        "with": "pond"
      },
      {
        "toCreate": "duck",
        "with": "lake"
      },
      {
        "toCreate": "eagle",
        "with": "mountain"
      },
      {
        "toCreate": "eagle",
        "with": "mountain_range"
      },
      {
        "toCreate": "egg",
        "with": "bird"
      },
      {
        "toCreate": "flying_fish",
        "with": "fish"
      },
      {
        "toCreate": "flying_squirrel",
        "with": "squirrel"
      },
      {
        "toCreate": "hummingbird",
        "with": "flower"
      },
      {
        "toCreate": "hummingbird",
        "with": "garden"
      },
      {
        "toCreate": "hummingbird",
        "with": "small"
      },
      {
        "toCreate": "hummingbird",
        "with": "bee"
      },
      {
        "toCreate": "hummingbird",
        "with": "butterfly"
      },
      {
        "toCreate": "nest",
        "with": "tree"
      },
      {
        "toCreate": "nest",
        "with": "hay"
      },
      {
        "toCreate": "nest",
        "with": "grass"
      },
      {
        "toCreate": "nest",
        "with": "house"
      },
      {
        "toCreate": "origami",
        "with": "paper"
      },
      {
        "toCreate": "ostrich",
        "with": "earth"
      },
      {
        "toCreate": "ostrich",
        "with": "sand"
      },
      {
        "toCreate": "ostrich",
        "with": "big"
      },
      {
        "toCreate": "owl",
        "with": "night"
      },
      {
        "toCreate": "owl",
        "with": "wizard"
      },
      {
        "toCreate": "owl",
        "with": "twilight"
      },
      {
        "toCreate": "owl",
        "with": "moon"
      },
      {
        "toCreate": "parrot",
        "with": "pirate"
      },
      {
        "toCreate": "parrot",
        "with": "pirate_ship"
      },
      {
        "toCreate": "peacock",
        "with": "leaf"
      },
      {
        "toCreate": "peacock",
        "with": "double_rainbow!"
      },
      {
        "toCreate": "peacock",
        "with": "rainbow"
      },
      {
        "toCreate": "pegasus",
        "with": "horse"
      },
      {
        "toCreate": "pegasus",
        "with": "unicorn"
      },
      {
        "toCreate": "penguin",
        "with": "antarctica"
      },
      {
        "toCreate": "penguin",
        "with": "cold"
      },
      {
        "toCreate": "penguin",
        "with": "ice"
      },
      {
        "toCreate": "penguin",
        "with": "snow"
      },
      {
        "toCreate": "phoenix",
        "with": "fire"
      },
      {
        "toCreate": "pigeon",
        "with": "letter"
      },
      {
        "toCreate": "pigeon",
        "with": "city"
      },
      {
        "toCreate": "pigeon",
        "with": "statue"
      },
      {
        "toCreate": "pigeon",
        "with": "skyscraper"
      },
      {
        "toCreate": "pigeon",
        "with": "village"
      },
      {
        "toCreate": "platypus",
        "with": "beaver"
      },
      {
        "toCreate": "pterodactyl",
        "with": "dinosaur"
      },
      {
        "toCreate": "seagull",
        "with": "sea"
      },
      {
        "toCreate": "seagull",
        "with": "ocean"
      },
      {
        "toCreate": "seagull",
        "with": "beach"
      },
      {
        "toCreate": "toucan",
        "with": "rainbow"
      },
      {
        "toCreate": "toucan",
        "with": "palm"
      },
      {
        "toCreate": "toucan",
        "with": "double_rainbow!"
      },
      {
        "toCreate": "vulture",
        "with": "corpse"
      },
      {
        "toCreate": "vulture",
        "with": "desert"
      },
      {
        "toCreate": "woodpecker",
        "with": "wood"
      },
      {
        "toCreate": "woodpecker",
        "with": "tree"
      },
      {
        "toCreate": "woodpecker",
        "with": "forest"
      }
    ],
    "name": "Bird"
  },
  "birdcage": {
    "combine": [],
    "name": "Birdcage"
  },
  "birdhouse": {
    "combine": [],
    "name": "Birdhouse"
  },
  "black_hole": {
    "combine": [],
    "name": "Black Hole"
  },
  "blade": {
    "combine": [
      {
        "toCreate": "axe",
        "with": "wood"
      },
      {
        "toCreate": "bandage",
        "with": "fabric"
      },
      {
        "toCreate": "bayonet",
        "with": "gun"
      },
      {
        "toCreate": "blender",
        "with": "glass"
      },
      {
        "toCreate": "blender",
        "with": "electricity"
      },
      {
        "toCreate": "blender",
        "with": "windmill"
      },
      {
        "toCreate": "blender",
        "with": "wind_turbine"
      },
      {
        "toCreate": "blender",
        "with": "motion"
      },
      {
        "toCreate": "blood",
        "with": "human"
      },
      {
        "toCreate": "coconut_milk",
        "with": "coconut"
      },
      {
        "toCreate": "confetti",
        "with": "paper"
      },
      {
        "toCreate": "cookie_cutter",
        "with": "dough"
      },
      {
        "toCreate": "cookie_cutter",
        "with": "cookie_dough"
      },
      {
        "toCreate": "corpse",
        "with": "human"
      },
      {
        "toCreate": "helicopter",
        "with": "airplane"
      },
      {
        "toCreate": "helicopter",
        "with": "seaplane"
      },
      {
        "toCreate": "katana",
        "with": "heat"
      },
      {
        "toCreate": "katana",
        "with": "shuriken"
      },
      {
        "toCreate": "katana",
        "with": "ninja"
      },
      {
        "toCreate": "knife",
        "with": "cook"
      },
      {
        "toCreate": "leather",
        "with": "cow"
      },
      {
        "toCreate": "leather",
        "with": "pig"
      },
      {
        "toCreate": "leather",
        "with": "sheep"
      },
      {
        "toCreate": "mousetrap",
        "with": "mouse"
      },
      {
        "toCreate": "pencil_sharpener",
        "with": "pencil"
      },
      {
        "toCreate": "rose",
        "with": "flower"
      },
      {
        "toCreate": "rose",
        "with": "plant"
      },
      {
        "toCreate": "sap",
        "with": "tree"
      },
      {
        "toCreate": "scalpel",
        "with": "doctor"
      },
      {
        "toCreate": "scalpel",
        "with": "hospital"
      },
      {
        "toCreate": "scalpel",
        "with": "ambulance"
      },
      {
        "toCreate": "scissors",
        "with": "blade"
      },
      {
        "toCreate": "scissors",
        "with": "sword"
      },
      {
        "toCreate": "scissors",
        "with": "paper"
      },
      {
        "toCreate": "scythe",
        "with": "grass"
      },
      {
        "toCreate": "scythe",
        "with": "wheat"
      },
      {
        "toCreate": "shuriken",
        "with": "star"
      },
      {
        "toCreate": "shuriken",
        "with": "ninja"
      },
      {
        "toCreate": "sword",
        "with": "wood"
      },
      {
        "toCreate": "sword",
        "with": "metal"
      },
      {
        "toCreate": "sword",
        "with": "steel"
      },
      {
        "toCreate": "swordfish",
        "with": "fish"
      },
      {
        "toCreate": "swordfish",
        "with": "shark"
      },
      {
        "toCreate": "wax",
        "with": "beehive"
      },
      {
        "toCreate": "wool",
        "with": "sheep"
      }
    ],
    "name": "Blade"
  },
  "blender": {
    "combine": [
      {
        "toCreate": "paper",
        "with": "wood"
      },
      {
        "toCreate": "pencil_sharpener",
        "with": "pencil"
      },
      {
        "toCreate": "smoothie",
        "with": "fruit"
      }
    ],
    "name": "Blender"
  },
  "blizzard": {
    "combine": [
      {
        "toCreate": "husky",
        "with": "dog"
      },
      {
        "toCreate": "snow_globe",
        "with": "crystal_ball"
      },
      {
        "toCreate": "snow_globe",
        "with": "glass"
      }
    ],
    "name": "Blizzard"
  },
  "blood": {
    "combine": [
      {
        "toCreate": "bandage",
        "with": "fabric"
      },
      {
        "toCreate": "blood_bag",
        "with": "container"
      },
      {
        "toCreate": "blood_bag",
        "with": "sack"
      },
      {
        "toCreate": "blood_bag",
        "with": "bottle"
      },
      {
        "toCreate": "lion",
        "with": "cat"
      },
      {
        "toCreate": "lizard",
        "with": "cold"
      },
      {
        "toCreate": "piranha",
        "with": "fish"
      },
      {
        "toCreate": "shark",
        "with": "ocean"
      },
      {
        "toCreate": "shark",
        "with": "sea"
      },
      {
        "toCreate": "shark",
        "with": "fish"
      },
      {
        "toCreate": "tyrannosaurus_rex",
        "with": "dinosaur"
      },
      {
        "toCreate": "vampire",
        "with": "human"
      },
      {
        "toCreate": "wolf",
        "with": "dog"
      }
    ],
    "name": "Blood"
  },
  "blood_bag": {
    "combine": [],
    "name": "Blood Bag"
  },
  "boat": {
    "combine": [
      {
        "toCreate": "airplane",
        "with": "bird"
      },
      {
        "toCreate": "grim_reaper",
        "with": "scythe"
      },
      {
        "toCreate": "pirate_ship",
        "with": "pirate"
      },
      {
        "toCreate": "rocket",
        "with": "atmosphere"
      },
      {
        "toCreate": "rope",
        "with": "tool"
      },
      {
        "toCreate": "sailboat",
        "with": "wind"
      },
      {
        "toCreate": "sailboat",
        "with": "fabric"
      },
      {
        "toCreate": "sailor",
        "with": "human"
      },
      {
        "toCreate": "seasickness",
        "with": "sickness"
      },
      {
        "toCreate": "spaceship",
        "with": "space"
      },
      {
        "toCreate": "steamboat",
        "with": "steam_engine"
      },
      {
        "toCreate": "titanic",
        "with": "iceberg"
      }
    ],
    "name": "Boat"
  },
  "boiler": {
    "combine": [
      {
        "toCreate": "fire_extinguisher",
        "with": "carbon_dioxide"
      },
      {
        "toCreate": "machine",
        "with": "wheel"
      },
      {
        "toCreate": "machine",
        "with": "tool"
      },
      {
        "toCreate": "machine",
        "with": "chain"
      },
      {
        "toCreate": "steam_engine",
        "with": "wheel"
      }
    ],
    "name": "Boiler"
  },
  "bone": {
    "combine": [
      {
        "toCreate": "chicken_wing",
        "with": "chicken"
      },
      {
        "toCreate": "coral",
        "with": "ocean"
      },
      {
        "toCreate": "coral",
        "with": "sea"
      },
      {
        "toCreate": "dog",
        "with": "wolf"
      },
      {
        "toCreate": "fossil",
        "with": "time"
      },
      {
        "toCreate": "fossil",
        "with": "earth"
      },
      {
        "toCreate": "fossil",
        "with": "stone"
      },
      {
        "toCreate": "fossil",
        "with": "rock"
      },
      {
        "toCreate": "skeleton",
        "with": "bone"
      },
      {
        "toCreate": "skeleton",
        "with": "corpse"
      }
    ],
    "name": "Bone"
  },
  "bonsai_tree": {
    "combine": [],
    "name": "Bonsai Tree"
  },
  "book": {
    "combine": [
      {
        "toCreate": "cookbook",
        "with": "cook"
      },
      {
        "toCreate": "librarian",
        "with": "human"
      },
      {
        "toCreate": "library",
        "with": "container"
      },
      {
        "toCreate": "library",
        "with": "house"
      },
      {
        "toCreate": "sheet_music",
        "with": "music"
      }
    ],
    "name": "Book"
  },
  "bottle": {
    "combine": [
      {
        "toCreate": "blood_bag",
        "with": "blood"
      },
      {
        "toCreate": "bucket",
        "with": "river"
      },
      {
        "toCreate": "bucket",
        "with": "paint"
      },
      {
        "toCreate": "container",
        "with": "philosophy"
      },
      {
        "toCreate": "cup",
        "with": "smoothie"
      },
      {
        "toCreate": "cup",
        "with": "tea"
      },
      {
        "toCreate": "cup",
        "with": "milk_shake"
      },
      {
        "toCreate": "jar",
        "with": "jam"
      },
      {
        "toCreate": "vase",
        "with": "rose"
      },
      {
        "toCreate": "vase",
        "with": "flower"
      },
      {
        "toCreate": "vase",
        "with": "plant"
      }
    ],
    "name": "Bottle"
  },
  "boulder": {
    "combine": [
      {
        "toCreate": "hill",
        "with": "big"
      },
      {
        "toCreate": "hill",
        "with": "earth"
      },
      {
        "toCreate": "hill",
        "with": "stone"
      },
      {
        "toCreate": "hill",
        "with": "boulder"
      },
      {
        "toCreate": "meteoroid",
        "with": "space"
      },
      {
        "toCreate": "meteoroid",
        "with": "solar_system"
      },
      {
        "toCreate": "meteoroid",
        "with": "sun"
      },
      {
        "toCreate": "mineral",
        "with": "organic_matter"
      },
      {
        "toCreate": "moss",
        "with": "plant"
      },
      {
        "toCreate": "moss",
        "with": "grass"
      },
      {
        "toCreate": "moss",
        "with": "algae"
      },
      {
        "toCreate": "ore",
        "with": "hammer"
      },
      {
        "toCreate": "rock",
        "with": "small"
      },
      {
        "toCreate": "statue",
        "with": "hammer"
      }
    ],
    "name": "Boulder"
  },
  "bow": {
    "combine": [
      {
        "toCreate": "arrow",
        "with": "wood"
      },
      {
        "toCreate": "gun",
        "with": "metal"
      },
      {
        "toCreate": "gun",
        "with": "steel"
      },
      {
        "toCreate": "gun",
        "with": "bullet"
      },
      {
        "toCreate": "gun",
        "with": "gunpowder"
      },
      {
        "toCreate": "stun_gun",
        "with": "electricity"
      },
      {
        "toCreate": "stun_gun",
        "with": "energy"
      },
      {
        "toCreate": "warrior",
        "with": "human"
      }
    ],
    "name": "Bow"
  },
  "box": {
    "combine": [
      {
        "toCreate": "bucket",
        "with": "chicken_wing"
      },
      {
        "toCreate": "container",
        "with": "philosophy"
      },
      {
        "toCreate": "mailbox",
        "with": "letter"
      },
      {
        "toCreate": "mailbox",
        "with": "mailman"
      },
      {
        "toCreate": "toolbox",
        "with": "hammer"
      },
      {
        "toCreate": "toolbox",
        "with": "tool"
      },
      {
        "toCreate": "toolbox",
        "with": "ruler"
      },
      {
        "toCreate": "toolbox",
        "with": "steel_wool"
      },
      {
        "toCreate": "toolbox",
        "with": "chain"
      }
    ],
    "name": "Box"
  },
  "bread": {
    "combine": [
      {
        "toCreate": "baker",
        "with": "human"
      },
      {
        "toCreate": "bakery",
        "with": "house"
      },
      {
        "toCreate": "bakery",
        "with": "city"
      },
      {
        "toCreate": "bakery",
        "with": "village"
      },
      {
        "toCreate": "banana_bread",
        "with": "banana"
      },
      {
        "toCreate": "cake",
        "with": "sugar"
      },
      {
        "toCreate": "hamburger",
        "with": "meat"
      },
      {
        "toCreate": "mold",
        "with": "time"
      },
      {
        "toCreate": "mold",
        "with": "bacteria"
      },
      {
        "toCreate": "pizza",
        "with": "cheese"
      },
      {
        "toCreate": "sandwich",
        "with": "meat"
      },
      {
        "toCreate": "sandwich",
        "with": "bacon"
      },
      {
        "toCreate": "sandwich",
        "with": "cheese"
      },
      {
        "toCreate": "sandwich",
        "with": "vegetable"
      },
      {
        "toCreate": "sandwich",
        "with": "ham"
      },
      {
        "toCreate": "toast",
        "with": "fire"
      },
      {
        "toCreate": "toast",
        "with": "warmth"
      }
    ],
    "name": "Bread"
  },
  "brick": {
    "combine": [
      {
        "toCreate": "chimney",
        "with": "smoke"
      },
      {
        "toCreate": "chimney",
        "with": "fireplace"
      },
      {
        "toCreate": "fireplace",
        "with": "campfire"
      },
      {
        "toCreate": "wall",
        "with": "brick"
      }
    ],
    "name": "Brick"
  },
  "bridge": {
    "combine": [],
    "name": "Bridge"
  },
  "broom": {
    "combine": [
      {
        "toCreate": "closet",
        "with": "container"
      },
      {
        "toCreate": "robot_vacuum",
        "with": "robot"
      },
      {
        "toCreate": "robot_vacuum",
        "with": "computer"
      },
      {
        "toCreate": "vacuum_cleaner",
        "with": "electricity"
      },
      {
        "toCreate": "vacuum_cleaner",
        "with": "machine"
      },
      {
        "toCreate": "witch",
        "with": "wizard"
      },
      {
        "toCreate": "witch",
        "with": "human"
      },
      {
        "toCreate": "witch",
        "with": "legend"
      },
      {
        "toCreate": "witch",
        "with": "story"
      },
      {
        "toCreate": "witch",
        "with": "magic"
      },
      {
        "toCreate": "witch",
        "with": "cauldron"
      }
    ],
    "name": "Broom"
  },
  "bucket": {
    "combine": [
      {
        "toCreate": "container",
        "with": "philosophy"
      },
      {
        "toCreate": "jar",
        "with": "jam"
      }
    ],
    "name": "Bucket"
  },
  "bullet": {
    "combine": [
      {
        "toCreate": "arrow",
        "with": "wood"
      },
      {
        "toCreate": "bulletproof_vest",
        "with": "armor"
      },
      {
        "toCreate": "corpse",
        "with": "human"
      },
      {
        "toCreate": "gun",
        "with": "metal"
      },
      {
        "toCreate": "gun",
        "with": "steel"
      },
      {
        "toCreate": "gun",
        "with": "bow"
      },
      {
        "toCreate": "gun",
        "with": "container"
      }
    ],
    "name": "Bullet"
  },
  "bulletproof_vest": {
    "combine": [
      {
        "toCreate": "safety_glasses",
        "with": "glasses"
      }
    ],
    "name": "Bulletproof Vest"
  },
  "bus": {
    "combine": [
      {
        "toCreate": "garage",
        "with": "house"
      },
      {
        "toCreate": "garage",
        "with": "container"
      },
      {
        "toCreate": "garage",
        "with": "wall"
      },
      {
        "toCreate": "garage",
        "with": "barn"
      },
      {
        "toCreate": "ice_cream_truck",
        "with": "ice_cream"
      },
      {
        "toCreate": "rv",
        "with": "house"
      }
    ],
    "name": "Bus"
  },
  "butcher": {
    "combine": [
      {
        "toCreate": "idea",
        "with": "butcher"
      },
      {
        "toCreate": "meat",
        "with": "livestock"
      },
      {
        "toCreate": "meat",
        "with": "chicken"
      },
      {
        "toCreate": "meat",
        "with": "pig"
      },
      {
        "toCreate": "meat",
        "with": "cow"
      },
      {
        "toCreate": "meat",
        "with": "animal"
      },
      {
        "toCreate": "meat",
        "with": "fish"
      },
      {
        "toCreate": "meat",
        "with": "swordfish"
      },
      {
        "toCreate": "meat",
        "with": "flying_fish"
      },
      {
        "toCreate": "meat",
        "with": "shark"
      },
      {
        "toCreate": "meat",
        "with": "frog"
      },
      {
        "toCreate": "statue",
        "with": "medusa"
      }
    ],
    "name": "Butcher"
  },
  "butter": {
    "combine": [
      {
        "toCreate": "gold",
        "with": "metal"
      },
      {
        "toCreate": "gold",
        "with": "steel"
      },
      {
        "toCreate": "peanut_butter",
        "with": "nuts"
      }
    ],
    "name": "Butter"
  },
  "butterfly": {
    "combine": [
      {
        "toCreate": "butterfly_net",
        "with": "net"
      },
      {
        "toCreate": "egg",
        "with": "butterfly"
      },
      {
        "toCreate": "hummingbird",
        "with": "bird"
      },
      {
        "toCreate": "moth",
        "with": "moon"
      },
      {
        "toCreate": "moth",
        "with": "night"
      },
      {
        "toCreate": "moth",
        "with": "candle"
      },
      {
        "toCreate": "moth",
        "with": "light"
      },
      {
        "toCreate": "moth",
        "with": "lamp"
      },
      {
        "toCreate": "moth",
        "with": "flashlight"
      }
    ],
    "name": "Butterfly"
  },
  "butterfly_net": {
    "combine": [],
    "name": "Butterfly Net"
  },
  "cable_car": {
    "combine": [],
    "name": "Cable Car"
  },
  "cactus": {
    "combine": [
      {
        "toCreate": "desert",
        "with": "sand"
      }
    ],
    "name": "Cactus"
  },
  "cage": {
    "combine": [
      {
        "toCreate": "zoo",
        "with": "animal"
      }
    ],
    "name": "Cage"
  },
  "cake": {
    "combine": [
      {
        "toCreate": "bakery",
        "with": "house"
      },
      {
        "toCreate": "bakery",
        "with": "city"
      },
      {
        "toCreate": "bakery",
        "with": "village"
      },
      {
        "toCreate": "box",
        "with": "container"
      },
      {
        "toCreate": "gingerbread_house",
        "with": "house"
      }
    ],
    "name": "Cake"
  },
  "camel": {
    "combine": [],
    "name": "Camel"
  },
  "campfire": {
    "combine": [
      {
        "toCreate": "ash",
        "with": "water"
      },
      {
        "toCreate": "ash",
        "with": "time"
      },
      {
        "toCreate": "ash",
        "with": "rain"
      },
      {
        "toCreate": "ash",
        "with": "storm"
      },
      {
        "toCreate": "ash",
        "with": "paper"
      },
      {
        "toCreate": "bacon",
        "with": "pig"
      },
      {
        "toCreate": "bacon",
        "with": "ham"
      },
      {
        "toCreate": "bbq",
        "with": "metal"
      },
      {
        "toCreate": "bbq",
        "with": "steel"
      },
      {
        "toCreate": "bbq",
        "with": "garden"
      },
      {
        "toCreate": "bbq",
        "with": "house"
      },
      {
        "toCreate": "bbq",
        "with": "meat"
      },
      {
        "toCreate": "cauldron",
        "with": "witch"
      },
      {
        "toCreate": "cook",
        "with": "human"
      },
      {
        "toCreate": "dog",
        "with": "wolf"
      },
      {
        "toCreate": "fireplace",
        "with": "house"
      },
      {
        "toCreate": "fireplace",
        "with": "brick"
      },
      {
        "toCreate": "fireplace",
        "with": "wall"
      },
      {
        "toCreate": "fireplace",
        "with": "container"
      },
      {
        "toCreate": "flamethrower",
        "with": "gun"
      },
      {
        "toCreate": "marshmallows",
        "with": "sugar"
      },
      {
        "toCreate": "smoke",
        "with": "water"
      },
      {
        "toCreate": "smoke",
        "with": "time"
      },
      {
        "toCreate": "smoke",
        "with": "storm"
      },
      {
        "toCreate": "smoke_signal",
        "with": "fabric"
      },
      {
        "toCreate": "story",
        "with": "human"
      }
    ],
    "name": "Campfire"
  },
  "candle": {
    "combine": [
      {
        "toCreate": "cake",
        "with": "dough"
      },
      {
        "toCreate": "cake",
        "with": "donut"
      },
      {
        "toCreate": "christmas_tree",
        "with": "tree"
      },
      {
        "toCreate": "jack-o-lantern",
        "with": "pumpkin"
      },
      {
        "toCreate": "moth",
        "with": "butterfly"
      }
    ],
    "name": "Candle"
  },
  "candy_cane": {
    "combine": [],
    "name": "Candy Cane"
  },
  "cannon": {
    "combine": [],
    "name": "Cannon"
  },
  "canvas": {
    "combine": [
      {
        "toCreate": "painter",
        "with": "human"
      },
      {
        "toCreate": "painting",
        "with": "painter"
      },
      {
        "toCreate": "painting",
        "with": "paint"
      }
    ],
    "name": "Canvas"
  },
  "car": {
    "combine": [
      {
        "toCreate": "airplane",
        "with": "bird"
      },
      {
        "toCreate": "ambulance",
        "with": "hospital"
      },
      {
        "toCreate": "ambulance",
        "with": "doctor"
      },
      {
        "toCreate": "bus",
        "with": "car"
      },
      {
        "toCreate": "bus",
        "with": "train"
      },
      {
        "toCreate": "bus",
        "with": "city"
      },
      {
        "toCreate": "bus",
        "with": "big"
      },
      {
        "toCreate": "cable_car",
        "with": "mountain"
      },
      {
        "toCreate": "cable_car",
        "with": "mountain_range"
      },
      {
        "toCreate": "electric_car",
        "with": "electricity"
      },
      {
        "toCreate": "electric_car",
        "with": "wind_turbine"
      },
      {
        "toCreate": "electric_car",
        "with": "solar_cell"
      },
      {
        "toCreate": "firetruck",
        "with": "firefighter"
      },
      {
        "toCreate": "firetruck",
        "with": "fire"
      },
      {
        "toCreate": "firetruck",
        "with": "fire_extinguisher"
      },
      {
        "toCreate": "garage",
        "with": "house"
      },
      {
        "toCreate": "garage",
        "with": "container"
      },
      {
        "toCreate": "garage",
        "with": "wall"
      },
      {
        "toCreate": "garage",
        "with": "barn"
      },
      {
        "toCreate": "ice_cream_truck",
        "with": "ice_cream"
      },
      {
        "toCreate": "moon_rover",
        "with": "moon"
      },
      {
        "toCreate": "rocket",
        "with": "atmosphere"
      },
      {
        "toCreate": "roller_coaster",
        "with": "park"
      },
      {
        "toCreate": "rv",
        "with": "house"
      },
      {
        "toCreate": "snowmobile",
        "with": "snow"
      },
      {
        "toCreate": "snowmobile",
        "with": "glacier"
      },
      {
        "toCreate": "snowmobile",
        "with": "ice"
      },
      {
        "toCreate": "spaceship",
        "with": "space"
      },
      {
        "toCreate": "tank",
        "with": "armor"
      },
      {
        "toCreate": "tank",
        "with": "gun"
      },
      {
        "toCreate": "tank",
        "with": "metal"
      },
      {
        "toCreate": "tank",
        "with": "steel"
      },
      {
        "toCreate": "tractor",
        "with": "farmer"
      },
      {
        "toCreate": "tractor",
        "with": "field"
      },
      {
        "toCreate": "tractor",
        "with": "cow"
      }
    ],
    "name": "Car"
  },
  "caramel": {
    "combine": [],
    "name": "Caramel"
  },
  "carbon_dioxide": {
    "combine": [
      {
        "toCreate": "dry_ice",
        "with": "cold"
      },
      {
        "toCreate": "dry_ice",
        "with": "ice"
      },
      {
        "toCreate": "fire_extinguisher",
        "with": "fire"
      },
      {
        "toCreate": "fire_extinguisher",
        "with": "pressure"
      },
      {
        "toCreate": "fire_extinguisher",
        "with": "metal"
      },
      {
        "toCreate": "fire_extinguisher",
        "with": "steel"
      },
      {
        "toCreate": "fire_extinguisher",
        "with": "boiler"
      },
      {
        "toCreate": "oxygen",
        "with": "plant"
      },
      {
        "toCreate": "oxygen",
        "with": "tree"
      },
      {
        "toCreate": "oxygen",
        "with": "grass"
      },
      {
        "toCreate": "oxygen",
        "with": "algae"
      },
      {
        "toCreate": "small",
        "with": "philosophy"
      },
      {
        "toCreate": "soda",
        "with": "water"
      },
      {
        "toCreate": "soda",
        "with": "juice"
      },
      {
        "toCreate": "soda",
        "with": "tea"
      }
    ],
    "name": "Carbon Dioxide"
  },
  "carrot": {
    "combine": [
      {
        "toCreate": "rabbit",
        "with": "animal"
      },
      {
        "toCreate": "snowman",
        "with": "snow"
      },
      {
        "toCreate": "snowman",
        "with": "snowball"
      }
    ],
    "name": "Carrot"
  },
  "cart": {
    "combine": [
      {
        "toCreate": "car",
        "with": "combustion_engine"
      },
      {
        "toCreate": "roller_coaster",
        "with": "park"
      },
      {
        "toCreate": "sleigh",
        "with": "snow"
      },
      {
        "toCreate": "sleigh",
        "with": "ice"
      },
      {
        "toCreate": "sleigh",
        "with": "arctic"
      },
      {
        "toCreate": "sleigh",
        "with": "antarctica"
      },
      {
        "toCreate": "wagon",
        "with": "horse"
      },
      {
        "toCreate": "wagon",
        "with": "fabric"
      },
      {
        "toCreate": "wagon",
        "with": "house"
      },
      {
        "toCreate": "wagon",
        "with": "wall"
      },
      {
        "toCreate": "wagon",
        "with": "cow"
      }
    ],
    "name": "Cart"
  },
  "cashmere": {
    "combine": [],
    "name": "Cashmere"
  },
  "castle": {
    "combine": [
      {
        "toCreate": "cannon",
        "with": "gunpowder"
      },
      {
        "toCreate": "cannon",
        "with": "gun"
      },
      {
        "toCreate": "fairy_tale",
        "with": "story"
      },
      {
        "toCreate": "ghost",
        "with": "night"
      },
      {
        "toCreate": "monarch",
        "with": "human"
      },
      {
        "toCreate": "ruins",
        "with": "time"
      },
      {
        "toCreate": "sand_castle",
        "with": "sand"
      },
      {
        "toCreate": "sand_castle",
        "with": "beach"
      },
      {
        "toCreate": "sand_castle",
        "with": "desert"
      },
      {
        "toCreate": "sand_castle",
        "with": "dune"
      }
    ],
    "name": "Castle"
  },
  "cat": {
    "combine": [
      {
        "toCreate": "armadillo",
        "with": "armor"
      },
      {
        "toCreate": "catnip",
        "with": "plant"
      },
      {
        "toCreate": "catnip",
        "with": "grass"
      },
      {
        "toCreate": "catnip",
        "with": "flower"
      },
      {
        "toCreate": "lion",
        "with": "animal"
      },
      {
        "toCreate": "lion",
        "with": "blood"
      },
      {
        "toCreate": "lion",
        "with": "big"
      },
      {
        "toCreate": "lion",
        "with": "monarch"
      },
      {
        "toCreate": "origami",
        "with": "paper"
      }
    ],
    "name": "Cat"
  },
  "catnip": {
    "combine": [],
    "name": "Catnip"
  },
  "cauldron": {
    "combine": [
      {
        "toCreate": "witch",
        "with": "broom"
      },
      {
        "toCreate": "witch",
        "with": "legend"
      },
      {
        "toCreate": "witch",
        "with": "story"
      }
    ],
    "name": "Cauldron"
  },
  "cave": {
    "combine": [
      {
        "toCreate": "tunnel",
        "with": "mountain"
      },
      {
        "toCreate": "tunnel",
        "with": "hill"
      },
      {
        "toCreate": "tunnel",
        "with": "mountain_range"
      }
    ],
    "name": "Cave"
  },
  "caviar": {
    "combine": [
      {
        "toCreate": "sushi",
        "with": "seaweed"
      }
    ],
    "name": "Caviar"
  },
  "centaur": {
    "combine": [],
    "name": "Centaur"
  },
  "cereal": {
    "combine": [
      {
        "toCreate": "box",
        "with": "container"
      }
    ],
    "name": "Cereal"
  },
  "chain": {
    "combine": [
      {
        "toCreate": "bicycle",
        "with": "wheel"
      },
      {
        "toCreate": "machine",
        "with": "boiler"
      },
      {
        "toCreate": "machine",
        "with": "tool"
      },
      {
        "toCreate": "toolbox",
        "with": "box"
      },
      {
        "toCreate": "toolbox",
        "with": "container"
      }
    ],
    "name": "Chain"
  },
  "chainsaw": {
    "combine": [
      {
        "toCreate": "lumberjack",
        "with": "human"
      },
      {
        "toCreate": "wood",
        "with": "tree"
      },
      {
        "toCreate": "wood",
        "with": "forest"
      }
    ],
    "name": "Chainsaw"
  },
  "chameleon": {
    "combine": [
      {
        "toCreate": "egg",
        "with": "chameleon"
      }
    ],
    "name": "Chameleon"
  },
  "charcoal": {
    "combine": [
      {
        "toCreate": "gunpowder",
        "with": "mineral"
      },
      {
        "toCreate": "gunpowder",
        "with": "energy"
      },
      {
        "toCreate": "pencil",
        "with": "wood"
      },
      {
        "toCreate": "steel",
        "with": "metal"
      }
    ],
    "name": "Charcoal"
  },
  "cheese": {
    "combine": [
      {
        "toCreate": "cheeseburger",
        "with": "hamburger"
      },
      {
        "toCreate": "cheeseburger",
        "with": "sandwich"
      },
      {
        "toCreate": "grilled_cheese",
        "with": "toast"
      },
      {
        "toCreate": "hamburger",
        "with": "meat"
      },
      {
        "toCreate": "mac_and_cheese",
        "with": "pasta"
      },
      {
        "toCreate": "moon",
        "with": "sky"
      },
      {
        "toCreate": "moon",
        "with": "night"
      },
      {
        "toCreate": "mouse",
        "with": "animal"
      },
      {
        "toCreate": "mouse",
        "with": "barn"
      },
      {
        "toCreate": "mouse",
        "with": "house"
      },
      {
        "toCreate": "mouse",
        "with": "farm"
      },
      {
        "toCreate": "mouse",
        "with": "wall"
      },
      {
        "toCreate": "mousetrap",
        "with": "wood"
      },
      {
        "toCreate": "mousetrap",
        "with": "metal"
      },
      {
        "toCreate": "mousetrap",
        "with": "steel"
      },
      {
        "toCreate": "pizza",
        "with": "dough"
      },
      {
        "toCreate": "pizza",
        "with": "bread"
      },
      {
        "toCreate": "pizza",
        "with": "wheel"
      },
      {
        "toCreate": "sandwich",
        "with": "bread"
      }
    ],
    "name": "Cheese"
  },
  "cheeseburger": {
    "combine": [],
    "name": "Cheeseburger"
  },
  "chicken": {
    "combine": [
      {
        "toCreate": "chicken_coop",
        "with": "house"
      },
      {
        "toCreate": "chicken_coop",
        "with": "wall"
      },
      {
        "toCreate": "chicken_coop",
        "with": "barn"
      },
      {
        "toCreate": "chicken_coop",
        "with": "container"
      },
      {
        "toCreate": "chicken_soup",
        "with": "water"
      },
      {
        "toCreate": "chicken_soup",
        "with": "liquid"
      },
      {
        "toCreate": "chicken_wing",
        "with": "bone"
      },
      {
        "toCreate": "chicken_wing",
        "with": "bbq"
      },
      {
        "toCreate": "duck",
        "with": "water"
      },
      {
        "toCreate": "duck",
        "with": "pond"
      },
      {
        "toCreate": "duck",
        "with": "lake"
      },
      {
        "toCreate": "egg",
        "with": "chicken"
      },
      {
        "toCreate": "fox",
        "with": "animal"
      },
      {
        "toCreate": "fox",
        "with": "wolf"
      },
      {
        "toCreate": "fox",
        "with": "dog"
      },
      {
        "toCreate": "meat",
        "with": "tool"
      },
      {
        "toCreate": "meat",
        "with": "axe"
      },
      {
        "toCreate": "meat",
        "with": "sword"
      },
      {
        "toCreate": "meat",
        "with": "butcher"
      },
      {
        "toCreate": "origami",
        "with": "paper"
      },
      {
        "toCreate": "ostrich",
        "with": "earth"
      },
      {
        "toCreate": "ostrich",
        "with": "sand"
      },
      {
        "toCreate": "philosophy",
        "with": "egg"
      },
      {
        "toCreate": "vulture",
        "with": "corpse"
      }
    ],
    "name": "Chicken"
  },
  "chicken_coop": {
    "combine": [
      {
        "toCreate": "fox",
        "with": "wolf"
      },
      {
        "toCreate": "fox",
        "with": "animal"
      },
      {
        "toCreate": "fox",
        "with": "dog"
      }
    ],
    "name": "Chicken Coop"
  },
  "chicken_soup": {
    "combine": [],
    "name": "Chicken Soup"
  },
  "chicken_wing": {
    "combine": [
      {
        "toCreate": "bucket",
        "with": "container"
      },
      {
        "toCreate": "bucket",
        "with": "box"
      },
      {
        "toCreate": "chicken_soup",
        "with": "water"
      },
      {
        "toCreate": "chicken_soup",
        "with": "liquid"
      }
    ],
    "name": "Chicken wing"
  },
  "chill": {
    "combine": [],
    "name": "Chill"
  },
  "chimney": {
    "combine": [
      {
        "toCreate": "gift",
        "with": "santa"
      }
    ],
    "name": "Chimney"
  },
  "chocolate": {
    "combine": [
      {
        "toCreate": "chocolate_milk",
        "with": "milk"
      },
      {
        "toCreate": "chocolate_milk",
        "with": "coconut_milk"
      },
      {
        "toCreate": "cookie_dough",
        "with": "dough"
      },
      {
        "toCreate": "hot_chocolate",
        "with": "heat"
      }
    ],
    "name": "Chocolate"
  },
  "chocolate_milk": {
    "combine": [
      {
        "toCreate": "bottle",
        "with": "container"
      },
      {
        "toCreate": "cereal",
        "with": "wheat"
      },
      {
        "toCreate": "hot_chocolate",
        "with": "heat"
      }
    ],
    "name": "Chocolate Milk"
  },
  "christmas_stocking": {
    "combine": [
      {
        "toCreate": "candy_cane",
        "with": "sugar"
      },
      {
        "toCreate": "gift",
        "with": "santa"
      },
      {
        "toCreate": "gift",
        "with": "wrapping_paper"
      },
      {
        "toCreate": "reindeer",
        "with": "animal"
      },
      {
        "toCreate": "santa",
        "with": "human"
      },
      {
        "toCreate": "santa",
        "with": "legend"
      },
      {
        "toCreate": "santa",
        "with": "story"
      },
      {
        "toCreate": "snow_globe",
        "with": "crystal_ball"
      },
      {
        "toCreate": "snow_globe",
        "with": "glass"
      },
      {
        "toCreate": "wrapping_paper",
        "with": "paper"
      }
    ],
    "name": "Christmas Stocking"
  },
  "christmas_tree": {
    "combine": [
      {
        "toCreate": "candy_cane",
        "with": "sugar"
      },
      {
        "toCreate": "christmas_stocking",
        "with": "wool"
      },
      {
        "toCreate": "gift",
        "with": "santa"
      },
      {
        "toCreate": "gift",
        "with": "wrapping_paper"
      },
      {
        "toCreate": "reindeer",
        "with": "animal"
      },
      {
        "toCreate": "reindeer",
        "with": "livestock"
      },
      {
        "toCreate": "santa",
        "with": "human"
      },
      {
        "toCreate": "santa",
        "with": "legend"
      },
      {
        "toCreate": "santa",
        "with": "story"
      },
      {
        "toCreate": "snow_globe",
        "with": "crystal_ball"
      },
      {
        "toCreate": "snow_globe",
        "with": "glass"
      },
      {
        "toCreate": "wrapping_paper",
        "with": "paper"
      }
    ],
    "name": "Christmas Tree"
  },
  "cigarette": {
    "combine": [],
    "name": "Cigarette"
  },
  "city": {
    "combine": [
      {
        "toCreate": "acid_rain",
        "with": "rain"
      },
      {
        "toCreate": "bakery",
        "with": "bread"
      },
      {
        "toCreate": "bakery",
        "with": "baker"
      },
      {
        "toCreate": "bakery",
        "with": "donut"
      },
      {
        "toCreate": "bakery",
        "with": "cake"
      },
      {
        "toCreate": "bank",
        "with": "gold"
      },
      {
        "toCreate": "bank",
        "with": "safe"
      },
      {
        "toCreate": "bank",
        "with": "money"
      },
      {
        "toCreate": "bus",
        "with": "car"
      },
      {
        "toCreate": "firestation",
        "with": "firefighter"
      },
      {
        "toCreate": "flood",
        "with": "river"
      },
      {
        "toCreate": "flood",
        "with": "tsunami"
      },
      {
        "toCreate": "fog",
        "with": "cloud"
      },
      {
        "toCreate": "kaiju",
        "with": "dinosaur"
      },
      {
        "toCreate": "map",
        "with": "paper"
      },
      {
        "toCreate": "park",
        "with": "forest"
      },
      {
        "toCreate": "park",
        "with": "garden"
      },
      {
        "toCreate": "park",
        "with": "grass"
      },
      {
        "toCreate": "park",
        "with": "field"
      },
      {
        "toCreate": "pigeon",
        "with": "bird"
      },
      {
        "toCreate": "rat",
        "with": "mouse"
      },
      {
        "toCreate": "ruins",
        "with": "time"
      },
      {
        "toCreate": "smog",
        "with": "fog"
      },
      {
        "toCreate": "smog",
        "with": "smoke"
      },
      {
        "toCreate": "smog",
        "with": "atmosphere"
      },
      {
        "toCreate": "smog",
        "with": "air"
      }
    ],
    "name": "City"
  },
  "clay": {
    "combine": [
      {
        "toCreate": "brick",
        "with": "fire"
      },
      {
        "toCreate": "brick",
        "with": "sun"
      },
      {
        "toCreate": "brick",
        "with": "stone"
      },
      {
        "toCreate": "golem",
        "with": "story"
      },
      {
        "toCreate": "golem",
        "with": "legend"
      },
      {
        "toCreate": "human",
        "with": "life"
      },
      {
        "toCreate": "potter",
        "with": "human"
      },
      {
        "toCreate": "pottery",
        "with": "wheel"
      },
      {
        "toCreate": "pottery",
        "with": "tool"
      },
      {
        "toCreate": "soap",
        "with": "wax"
      },
      {
        "toCreate": "soap",
        "with": "oil"
      }
    ],
    "name": "Clay"
  },
  "clock": {
    "combine": [
      {
        "toCreate": "alarm_clock",
        "with": "sound"
      },
      {
        "toCreate": "alarm_clock",
        "with": "dawn"
      },
      {
        "toCreate": "alarm_clock",
        "with": "bell"
      },
      {
        "toCreate": "cuckoo",
        "with": "bird"
      },
      {
        "toCreate": "cuckoo",
        "with": "owl"
      },
      {
        "toCreate": "cuckoo",
        "with": "hummingbird"
      },
      {
        "toCreate": "egg_timer",
        "with": "egg"
      },
      {
        "toCreate": "sundial",
        "with": "sun"
      },
      {
        "toCreate": "sundial",
        "with": "light"
      },
      {
        "toCreate": "watch",
        "with": "human"
      },
      {
        "toCreate": "watch",
        "with": "small"
      }
    ],
    "name": "Clock"
  },
  "closet": {
    "combine": [],
    "name": "Closet"
  },
  "cloud": {
    "combine": [
      {
        "toCreate": "acid_rain",
        "with": "smoke"
      },
      {
        "toCreate": "acid_rain",
        "with": "smog"
      },
      {
        "toCreate": "acid_rain",
        "with": "sickness"
      },
      {
        "toCreate": "cotton",
        "with": "plant"
      },
      {
        "toCreate": "cotton_candy",
        "with": "sugar"
      },
      {
        "toCreate": "fog",
        "with": "earth"
      },
      {
        "toCreate": "fog",
        "with": "hill"
      },
      {
        "toCreate": "fog",
        "with": "mountain"
      },
      {
        "toCreate": "fog",
        "with": "city"
      },
      {
        "toCreate": "fog",
        "with": "field"
      },
      {
        "toCreate": "fog",
        "with": "forest"
      },
      {
        "toCreate": "hail",
        "with": "ice"
      },
      {
        "toCreate": "jupiter",
        "with": "planet"
      },
      {
        "toCreate": "lightning",
        "with": "electricity"
      },
      {
        "toCreate": "lightning",
        "with": "energy"
      },
      {
        "toCreate": "rain",
        "with": "heat"
      },
      {
        "toCreate": "rainbow",
        "with": "light"
      },
      {
        "toCreate": "sheep",
        "with": "livestock"
      },
      {
        "toCreate": "sky",
        "with": "air"
      },
      {
        "toCreate": "skyscraper",
        "with": "house"
      },
      {
        "toCreate": "storm",
        "with": "electricity"
      },
      {
        "toCreate": "storm",
        "with": "cloud"
      }
    ],
    "name": "Cloud"
  },
  "coal": {
    "combine": [
      {
        "toCreate": "diamond",
        "with": "pressure"
      },
      {
        "toCreate": "fire",
        "with": "fire"
      },
      {
        "toCreate": "pencil",
        "with": "wood"
      },
      {
        "toCreate": "snowman",
        "with": "snow"
      },
      {
        "toCreate": "snowman",
        "with": "snowball"
      },
      {
        "toCreate": "steel",
        "with": "metal"
      }
    ],
    "name": "Coal"
  },
  "coconut": {
    "combine": [
      {
        "toCreate": "coconut_milk",
        "with": "milk"
      },
      {
        "toCreate": "coconut_milk",
        "with": "tool"
      },
      {
        "toCreate": "coconut_milk",
        "with": "hammer"
      },
      {
        "toCreate": "coconut_milk",
        "with": "sword"
      },
      {
        "toCreate": "coconut_milk",
        "with": "blade"
      },
      {
        "toCreate": "coconut_milk",
        "with": "axe"
      }
    ],
    "name": "Coconut"
  },
  "coconut_milk": {
    "combine": [
      {
        "toCreate": "batter",
        "with": "flour"
      },
      {
        "toCreate": "bottle",
        "with": "container"
      },
      {
        "toCreate": "cat",
        "with": "animal"
      },
      {
        "toCreate": "cereal",
        "with": "wheat"
      },
      {
        "toCreate": "chocolate",
        "with": "sugar"
      },
      {
        "toCreate": "chocolate_milk",
        "with": "chocolate"
      }
    ],
    "name": "Coconut Milk"
  },
  "coffin": {
    "combine": [
      {
        "toCreate": "grave",
        "with": "earth"
      },
      {
        "toCreate": "grave",
        "with": "field"
      },
      {
        "toCreate": "grave",
        "with": "forest"
      },
      {
        "toCreate": "grave",
        "with": "gravestone"
      },
      {
        "toCreate": "grave",
        "with": "container"
      }
    ],
    "name": "Coffin"
  },
  "cold": {
    "combine": [
      {
        "toCreate": "arctic",
        "with": "ocean"
      },
      {
        "toCreate": "arctic",
        "with": "sea"
      },
      {
        "toCreate": "chill",
        "with": "air"
      },
      {
        "toCreate": "chill",
        "with": "human"
      },
      {
        "toCreate": "current",
        "with": "ocean"
      },
      {
        "toCreate": "current",
        "with": "sea"
      },
      {
        "toCreate": "dry_ice",
        "with": "carbon_dioxide"
      },
      {
        "toCreate": "fridge",
        "with": "metal"
      },
      {
        "toCreate": "fridge",
        "with": "steel"
      },
      {
        "toCreate": "fridge",
        "with": "machine"
      },
      {
        "toCreate": "fridge",
        "with": "electricity"
      },
      {
        "toCreate": "fridge",
        "with": "container"
      },
      {
        "toCreate": "frozen_yogurt",
        "with": "yogurt"
      },
      {
        "toCreate": "ice",
        "with": "water"
      },
      {
        "toCreate": "ice",
        "with": "puddle"
      },
      {
        "toCreate": "ice_cream",
        "with": "milk"
      },
      {
        "toCreate": "iced_tea",
        "with": "tea"
      },
      {
        "toCreate": "igloo",
        "with": "house"
      },
      {
        "toCreate": "lizard",
        "with": "blood"
      },
      {
        "toCreate": "obsidian",
        "with": "lava"
      },
      {
        "toCreate": "penguin",
        "with": "bird"
      },
      {
        "toCreate": "popsicle",
        "with": "juice"
      },
      {
        "toCreate": "ski_goggles",
        "with": "glasses"
      },
      {
        "toCreate": "ski_goggles",
        "with": "sunglasses"
      },
      {
        "toCreate": "smoothie",
        "with": "fruit"
      },
      {
        "toCreate": "snow",
        "with": "rain"
      },
      {
        "toCreate": "snow",
        "with": "steam"
      },
      {
        "toCreate": "snow_globe",
        "with": "crystal_ball"
      }
    ],
    "name": "Cold"
  },
  "combustion_engine": {
    "combine": [
      {
        "toCreate": "car",
        "with": "wagon"
      },
      {
        "toCreate": "car",
        "with": "cart"
      },
      {
        "toCreate": "motorcycle",
        "with": "bicycle"
      }
    ],
    "name": "Combustion Engine"
  },
  "computer": {
    "combine": [
      {
        "toCreate": "computer_mouse",
        "with": "mouse"
      },
      {
        "toCreate": "computer_mouse",
        "with": "animal"
      },
      {
        "toCreate": "doge",
        "with": "dog"
      },
      {
        "toCreate": "email",
        "with": "letter"
      },
      {
        "toCreate": "hacker",
        "with": "human"
      },
      {
        "toCreate": "internet",
        "with": "computer"
      },
      {
        "toCreate": "internet",
        "with": "wire"
      },
      {
        "toCreate": "internet",
        "with": "web"
      },
      {
        "toCreate": "internet",
        "with": "net"
      },
      {
        "toCreate": "printer",
        "with": "paper"
      },
      {
        "toCreate": "printer",
        "with": "newspaper"
      },
      {
        "toCreate": "robot_vacuum",
        "with": "vacuum_cleaner"
      },
      {
        "toCreate": "robot_vacuum",
        "with": "broom"
      }
    ],
    "name": "Computer"
  },
  "computer_mouse": {
    "combine": [
      {
        "toCreate": "email",
        "with": "letter"
      },
      {
        "toCreate": "hacker",
        "with": "human"
      }
    ],
    "name": "Computer Mouse"
  },
  "confetti": {
    "combine": [
      {
        "toCreate": "small",
        "with": "philosophy"
      },
      {
        "toCreate": "sprinkles",
        "with": "sugar"
      }
    ],
    "name": "Confetti"
  },
  "constellation": {
    "combine": [],
    "name": "Constellation"
  },
  "container": {
    "combine": [
      {
        "toCreate": "airplane",
        "with": "pilot"
      },
      {
        "toCreate": "anthill",
        "with": "ant"
      },
      {
        "toCreate": "aquarium",
        "with": "fish"
      },
      {
        "toCreate": "bakery",
        "with": "baker"
      },
      {
        "toCreate": "bank",
        "with": "money"
      },
      {
        "toCreate": "barn",
        "with": "livestock"
      },
      {
        "toCreate": "barn",
        "with": "cow"
      },
      {
        "toCreate": "barn",
        "with": "horse"
      },
      {
        "toCreate": "barn",
        "with": "pig"
      },
      {
        "toCreate": "barn",
        "with": "hay"
      },
      {
        "toCreate": "barn",
        "with": "sheep"
      },
      {
        "toCreate": "barn",
        "with": "hay_bale"
      },
      {
        "toCreate": "barn",
        "with": "goat"
      },
      {
        "toCreate": "battery",
        "with": "electricity"
      },
      {
        "toCreate": "battery",
        "with": "energy"
      },
      {
        "toCreate": "beehive",
        "with": "bee"
      },
      {
        "toCreate": "birdcage",
        "with": "bird"
      },
      {
        "toCreate": "birdhouse",
        "with": "egg"
      },
      {
        "toCreate": "blood_bag",
        "with": "blood"
      },
      {
        "toCreate": "boiler",
        "with": "pressure"
      },
      {
        "toCreate": "book",
        "with": "story"
      },
      {
        "toCreate": "book",
        "with": "fairy_tale"
      },
      {
        "toCreate": "book",
        "with": "legend"
      },
      {
        "toCreate": "book",
        "with": "idea"
      },
      {
        "toCreate": "bottle",
        "with": "milk"
      },
      {
        "toCreate": "bottle",
        "with": "water"
      },
      {
        "toCreate": "bottle",
        "with": "coconut_milk"
      },
      {
        "toCreate": "bottle",
        "with": "juice"
      },
      {
        "toCreate": "bottle",
        "with": "oil"
      },
      {
        "toCreate": "bottle",
        "with": "alcohol"
      },
      {
        "toCreate": "bottle",
        "with": "wine"
      },
      {
        "toCreate": "bottle",
        "with": "beer"
      },
      {
        "toCreate": "bottle",
        "with": "chocolate_milk"
      },
      {
        "toCreate": "bottle",
        "with": "soda"
      },
      {
        "toCreate": "bottle",
        "with": "perfume"
      },
      {
        "toCreate": "bottle",
        "with": "liquid"
      },
      {
        "toCreate": "box",
        "with": "pizza"
      },
      {
        "toCreate": "box",
        "with": "pencil"
      },
      {
        "toCreate": "box",
        "with": "cookie"
      },
      {
        "toCreate": "box",
        "with": "cereal"
      },
      {
        "toCreate": "box",
        "with": "donut"
      },
      {
        "toCreate": "box",
        "with": "cake"
      },
      {
        "toCreate": "box",
        "with": "crayon"
      },
      {
        "toCreate": "bucket",
        "with": "wood"
      },
      {
        "toCreate": "bucket",
        "with": "yogurt"
      },
      {
        "toCreate": "bucket",
        "with": "paint"
      },
      {
        "toCreate": "bucket",
        "with": "chicken_wing"
      },
      {
        "toCreate": "bullet",
        "with": "gunpowder"
      },
      {
        "toCreate": "cage",
        "with": "hamster"
      },
      {
        "toCreate": "castle",
        "with": "knight"
      },
      {
        "toCreate": "castle",
        "with": "monarch"
      },
      {
        "toCreate": "cave",
        "with": "bat"
      },
      {
        "toCreate": "cave",
        "with": "wolf"
      },
      {
        "toCreate": "cave",
        "with": "lion"
      },
      {
        "toCreate": "cave",
        "with": "fox"
      },
      {
        "toCreate": "chicken_coop",
        "with": "chicken"
      },
      {
        "toCreate": "city",
        "with": "skyscraper"
      },
      {
        "toCreate": "city",
        "with": "bank"
      },
      {
        "toCreate": "clock",
        "with": "cuckoo"
      },
      {
        "toCreate": "closet",
        "with": "broom"
      },
      {
        "toCreate": "closet",
        "with": "toolbox"
      },
      {
        "toCreate": "closet",
        "with": "umbrella"
      },
      {
        "toCreate": "closet",
        "with": "vacuum_cleaner"
      },
      {
        "toCreate": "closet",
        "with": "robot_vacuum"
      },
      {
        "toCreate": "coffin",
        "with": "corpse"
      },
      {
        "toCreate": "coffin",
        "with": "vampire"
      },
      {
        "toCreate": "computer",
        "with": "email"
      },
      {
        "toCreate": "cup",
        "with": "smoothie"
      },
      {
        "toCreate": "cup",
        "with": "tea"
      },
      {
        "toCreate": "cup",
        "with": "milkshake"
      },
      {
        "toCreate": "doghouse",
        "with": "dog"
      },
      {
        "toCreate": "doghouse",
        "with": "husky"
      },
      {
        "toCreate": "dynamite",
        "with": "gunpowder"
      },
      {
        "toCreate": "egg",
        "with": "life"
      },
      {
        "toCreate": "family",
        "with": "love"
      },
      {
        "toCreate": "farm",
        "with": "farmer"
      },
      {
        "toCreate": "fireplace",
        "with": "wood"
      },
      {
        "toCreate": "fireplace",
        "with": "campfire"
      },
      {
        "toCreate": "forest",
        "with": "tree"
      },
      {
        "toCreate": "fridge",
        "with": "cold"
      },
      {
        "toCreate": "fridge",
        "with": "ice"
      },
      {
        "toCreate": "fridge",
        "with": "ice_cream"
      },
      {
        "toCreate": "galaxy",
        "with": "solar_system"
      },
      {
        "toCreate": "galaxy",
        "with": "supernova"
      },
      {
        "toCreate": "galaxy",
        "with": "star"
      },
      {
        "toCreate": "galaxy_cluster",
        "with": "galaxy"
      },
      {
        "toCreate": "garage",
        "with": "car"
      },
      {
        "toCreate": "garage",
        "with": "ambulance"
      },
      {
        "toCreate": "garage",
        "with": "motorcycle"
      },
      {
        "toCreate": "garage",
        "with": "bus"
      },
      {
        "toCreate": "garage",
        "with": "sleigh"
      },
      {
        "toCreate": "garage",
        "with": "electric_car"
      },
      {
        "toCreate": "garage",
        "with": "snowmobile"
      },
      {
        "toCreate": "garage",
        "with": "tractor"
      },
      {
        "toCreate": "garage",
        "with": "rv"
      },
      {
        "toCreate": "garage",
        "with": "ice_cream_truck"
      },
      {
        "toCreate": "garden",
        "with": "flower"
      },
      {
        "toCreate": "grave",
        "with": "coffin"
      },
      {
        "toCreate": "graveyard",
        "with": "gravestone"
      },
      {
        "toCreate": "greenhouse",
        "with": "plant"
      },
      {
        "toCreate": "gun",
        "with": "bullet"
      },
      {
        "toCreate": "hangar",
        "with": "airplane"
      },
      {
        "toCreate": "hangar",
        "with": "seaplane"
      },
      {
        "toCreate": "hangar",
        "with": "helicopter"
      },
      {
        "toCreate": "hangar",
        "with": "rocket"
      },
      {
        "toCreate": "hangar",
        "with": "spaceship"
      },
      {
        "toCreate": "hospital",
        "with": "doctor"
      },
      {
        "toCreate": "hourglass",
        "with": "sand"
      },
      {
        "toCreate": "hourglass",
        "with": "time"
      },
      {
        "toCreate": "house",
        "with": "fireplace"
      },
      {
        "toCreate": "house",
        "with": "family"
      },
      {
        "toCreate": "jar",
        "with": "jam"
      },
      {
        "toCreate": "lawn",
        "with": "grass"
      },
      {
        "toCreate": "library",
        "with": "book"
      },
      {
        "toCreate": "light_bulb",
        "with": "light"
      },
      {
        "toCreate": "nest",
        "with": "egg"
      },
      {
        "toCreate": "ocean",
        "with": "tide"
      },
      {
        "toCreate": "orchard",
        "with": "fruit_tree"
      },
      {
        "toCreate": "pipe",
        "with": "tobacco"
      },
      {
        "toCreate": "pirate_ship",
        "with": "pirate"
      },
      {
        "toCreate": "post_office",
        "with": "mailman"
      },
      {
        "toCreate": "pyramid",
        "with": "mummy"
      },
      {
        "toCreate": "sack",
        "with": "flour"
      },
      {
        "toCreate": "sack",
        "with": "salt"
      },
      {
        "toCreate": "sack",
        "with": "letter"
      },
      {
        "toCreate": "safe",
        "with": "gun"
      },
      {
        "toCreate": "safe",
        "with": "gold"
      },
      {
        "toCreate": "scuba_tank",
        "with": "air"
      },
      {
        "toCreate": "scuba_tank",
        "with": "atmosphere"
      },
      {
        "toCreate": "scuba_tank",
        "with": "oxygen"
      },
      {
        "toCreate": "silo",
        "with": "wheat"
      },
      {
        "toCreate": "solar_system",
        "with": "sun"
      },
      {
        "toCreate": "solar_system",
        "with": "planet"
      },
      {
        "toCreate": "solar_system",
        "with": "mars"
      },
      {
        "toCreate": "solar_system",
        "with": "mercury"
      },
      {
        "toCreate": "solar_system",
        "with": "venus"
      },
      {
        "toCreate": "solar_system",
        "with": "jupiter"
      },
      {
        "toCreate": "spaceship",
        "with": "astronaut"
      },
      {
        "toCreate": "toolbox",
        "with": "tool"
      },
      {
        "toCreate": "toolbox",
        "with": "hammer"
      },
      {
        "toCreate": "toolbox",
        "with": "ruler"
      },
      {
        "toCreate": "toolbox",
        "with": "steel_wool"
      },
      {
        "toCreate": "toolbox",
        "with": "chain"
      },
      {
        "toCreate": "trainyard",
        "with": "train"
      },
      {
        "toCreate": "tree",
        "with": "nest"
      },
      {
        "toCreate": "ufo",
        "with": "alien"
      },
      {
        "toCreate": "universe",
        "with": "space"
      },
      {
        "toCreate": "universe",
        "with": "galaxy_cluster"
      },
      {
        "toCreate": "vase",
        "with": "rose"
      },
      {
        "toCreate": "vase",
        "with": "flower"
      },
      {
        "toCreate": "village",
        "with": "house"
      },
      {
        "toCreate": "village",
        "with": "bakery"
      },
      {
        "toCreate": "volcano",
        "with": "lava"
      },
      {
        "toCreate": "zoo",
        "with": "animal"
      }
    ],
    "name": "Container"
  },
  "continent": {
    "combine": [
      {
        "toCreate": "antarctica",
        "with": "snow"
      },
      {
        "toCreate": "antarctica",
        "with": "ice"
      },
      {
        "toCreate": "earthquake",
        "with": "motion"
      },
      {
        "toCreate": "horizon",
        "with": "sky"
      },
      {
        "toCreate": "land",
        "with": "small"
      },
      {
        "toCreate": "map",
        "with": "paper"
      },
      {
        "toCreate": "mountain_range",
        "with": "mountain"
      },
      {
        "toCreate": "planet",
        "with": "continent"
      },
      {
        "toCreate": "planet",
        "with": "ocean"
      }
    ],
    "name": "Continent"
  },
  "cook": {
    "combine": [
      {
        "toCreate": "apron",
        "with": "fabric"
      },
      {
        "toCreate": "caviar",
        "with": "roe"
      },
      {
        "toCreate": "cheese",
        "with": "milk"
      },
      {
        "toCreate": "cookbook",
        "with": "book"
      },
      {
        "toCreate": "cutting_board",
        "with": "wood"
      },
      {
        "toCreate": "cyborg",
        "with": "robot"
      },
      {
        "toCreate": "knife",
        "with": "blade"
      },
      {
        "toCreate": "knife",
        "with": "sword"
      },
      {
        "toCreate": "recipe",
        "with": "paper"
      }
    ],
    "name": "Cook"
  },
  "cookbook": {
    "combine": [],
    "name": "Cookbook"
  },
  "cookie": {
    "combine": [
      {
        "toCreate": "baker",
        "with": "human"
      },
      {
        "toCreate": "box",
        "with": "container"
      },
      {
        "toCreate": "cookie_dough",
        "with": "dough"
      },
      {
        "toCreate": "fortune_cookie",
        "with": "paper"
      },
      {
        "toCreate": "gift",
        "with": "santa"
      },
      {
        "toCreate": "gingerbread_house",
        "with": "house"
      },
      {
        "toCreate": "gingerbread_man",
        "with": "life"
      },
      {
        "toCreate": "gingerbread_man",
        "with": "magic"
      },
      {
        "toCreate": "gingerbread_man",
        "with": "story"
      }
    ],
    "name": "Cookie"
  },
  "cookie_cutter": {
    "combine": [
      {
        "toCreate": "cookie",
        "with": "cookie_dough"
      },
      {
        "toCreate": "cookie",
        "with": "dough"
      }
    ],
    "name": "Cookie Cutter"
  },
  "cookie_dough": {
    "combine": [
      {
        "toCreate": "baker",
        "with": "human"
      },
      {
        "toCreate": "cookie",
        "with": "heat"
      },
      {
        "toCreate": "cookie",
        "with": "fire"
      },
      {
        "toCreate": "cookie",
        "with": "baker"
      },
      {
        "toCreate": "cookie",
        "with": "cookie_cutter"
      },
      {
        "toCreate": "cookie_cutter",
        "with": "blade"
      },
      {
        "toCreate": "donut",
        "with": "oil"
      },
      {
        "toCreate": "donut",
        "with": "wheel"
      },
      {
        "toCreate": "fortune_cookie",
        "with": "paper"
      },
      {
        "toCreate": "gingerbread_house",
        "with": "house"
      },
      {
        "toCreate": "gingerbread_man",
        "with": "life"
      },
      {
        "toCreate": "gingerbread_man",
        "with": "magic"
      },
      {
        "toCreate": "gingerbread_man",
        "with": "story"
      },
      {
        "toCreate": "pie",
        "with": "fruit"
      }
    ],
    "name": "Cookie Dough"
  },
  "coral": {
    "combine": [],
    "name": "Coral"
  },
  "corpse": {
    "combine": [
      {
        "toCreate": "bone",
        "with": "time"
      },
      {
        "toCreate": "bone",
        "with": "vulture"
      },
      {
        "toCreate": "bone",
        "with": "wolf"
      },
      {
        "toCreate": "charcoal",
        "with": "fire"
      },
      {
        "toCreate": "coffin",
        "with": "wood"
      },
      {
        "toCreate": "coffin",
        "with": "container"
      },
      {
        "toCreate": "death",
        "with": "philosophy"
      },
      {
        "toCreate": "fossil",
        "with": "stone"
      },
      {
        "toCreate": "fossil",
        "with": "rock"
      },
      {
        "toCreate": "frankenstein’s_monster",
        "with": "legend"
      },
      {
        "toCreate": "frankenstein’s_monster",
        "with": "lightning"
      },
      {
        "toCreate": "frankenstein’s_monster",
        "with": "electricity"
      },
      {
        "toCreate": "frankenstein’s_monster",
        "with": "story"
      },
      {
        "toCreate": "frankenstein’s_monster",
        "with": "monster"
      },
      {
        "toCreate": "frankenstein’s_monster",
        "with": "monster"
      },
      {
        "toCreate": "grave",
        "with": "earth"
      },
      {
        "toCreate": "grave",
        "with": "field"
      },
      {
        "toCreate": "grave",
        "with": "forest"
      },
      {
        "toCreate": "grim_reaper",
        "with": "scythe"
      },
      {
        "toCreate": "mummy",
        "with": "pyramid"
      },
      {
        "toCreate": "mummy",
        "with": "fabric"
      },
      {
        "toCreate": "organic_matter",
        "with": "science"
      },
      {
        "toCreate": "skeleton",
        "with": "time"
      },
      {
        "toCreate": "skeleton",
        "with": "bone"
      },
      {
        "toCreate": "vulture",
        "with": "bird"
      },
      {
        "toCreate": "vulture",
        "with": "eagle"
      },
      {
        "toCreate": "vulture",
        "with": "hummingbird"
      },
      {
        "toCreate": "vulture",
        "with": "owl"
      },
      {
        "toCreate": "vulture",
        "with": "pigeon"
      },
      {
        "toCreate": "vulture",
        "with": "penguin"
      },
      {
        "toCreate": "vulture",
        "with": "chicken"
      },
      {
        "toCreate": "vulture",
        "with": "duck"
      },
      {
        "toCreate": "zombie",
        "with": "life"
      },
      {
        "toCreate": "zombie",
        "with": "story"
      },
      {
        "toCreate": "zombie",
        "with": "bacteria"
      }
    ],
    "name": "Corpse"
  },
  "cotton": {
    "combine": [
      {
        "toCreate": "thread",
        "with": "tool"
      },
      {
        "toCreate": "thread",
        "with": "cotton"
      },
      {
        "toCreate": "thread",
        "with": "wheel"
      },
      {
        "toCreate": "thread",
        "with": "machine"
      },
      {
        "toCreate": "web",
        "with": "spider"
      }
    ],
    "name": "Cotton"
  },
  "cotton_candy": {
    "combine": [],
    "name": "Cotton Candy"
  },
  "cow": {
    "combine": [
      {
        "toCreate": "barn",
        "with": "house"
      },
      {
        "toCreate": "barn",
        "with": "container"
      },
      {
        "toCreate": "camel",
        "with": "desert"
      },
      {
        "toCreate": "camel",
        "with": "sand"
      },
      {
        "toCreate": "camel",
        "with": "dune"
      },
      {
        "toCreate": "goat",
        "with": "mountain"
      },
      {
        "toCreate": "goat",
        "with": "hill"
      },
      {
        "toCreate": "goat",
        "with": "mountain_range"
      },
      {
        "toCreate": "hippo",
        "with": "river"
      },
      {
        "toCreate": "hippo",
        "with": "water"
      },
      {
        "toCreate": "lasso",
        "with": "rope"
      },
      {
        "toCreate": "leather",
        "with": "blade"
      },
      {
        "toCreate": "leather",
        "with": "sword"
      },
      {
        "toCreate": "leather",
        "with": "tool"
      },
      {
        "toCreate": "manatee",
        "with": "sea"
      },
      {
        "toCreate": "manatee",
        "with": "ocean"
      },
      {
        "toCreate": "manatee",
        "with": "fish"
      },
      {
        "toCreate": "manatee",
        "with": "lake"
      },
      {
        "toCreate": "manatee",
        "with": "river"
      },
      {
        "toCreate": "meat",
        "with": "tool"
      },
      {
        "toCreate": "meat",
        "with": "axe"
      },
      {
        "toCreate": "meat",
        "with": "sword"
      },
      {
        "toCreate": "meat",
        "with": "butcher"
      },
      {
        "toCreate": "milk",
        "with": "farmer"
      },
      {
        "toCreate": "milk",
        "with": "tool"
      },
      {
        "toCreate": "milk",
        "with": "water"
      },
      {
        "toCreate": "milk",
        "with": "liquid"
      },
      {
        "toCreate": "minotaur",
        "with": "human"
      },
      {
        "toCreate": "minotaur",
        "with": "werewolf"
      },
      {
        "toCreate": "minotaur",
        "with": "story"
      },
      {
        "toCreate": "minotaur",
        "with": "legend"
      },
      {
        "toCreate": "origami",
        "with": "paper"
      },
      {
        "toCreate": "pig",
        "with": "mud"
      },
      {
        "toCreate": "steak",
        "with": "fire"
      },
      {
        "toCreate": "steak",
        "with": "bbq"
      },
      {
        "toCreate": "tractor",
        "with": "car"
      },
      {
        "toCreate": "tractor",
        "with": "wagon"
      },
      {
        "toCreate": "wagon",
        "with": "cart"
      }
    ],
    "name": "Cow"
  },
  "crayon": {
    "combine": [
      {
        "toCreate": "box",
        "with": "container"
      }
    ],
    "name": "Crayon"
  },
  "crow": {
    "combine": [
      {
        "toCreate": "airplane",
        "with": "metal"
      },
      {
        "toCreate": "airplane",
        "with": "steel"
      },
      {
        "toCreate": "airplane",
        "with": "machine"
      },
      {
        "toCreate": "egg",
        "with": "crow"
      }
    ],
    "name": "Crow"
  },
  "crystal_ball": {
    "combine": [
      {
        "toCreate": "prism",
        "with": "rainbow"
      },
      {
        "toCreate": "prism",
        "with": "double_rainbow!"
      },
      {
        "toCreate": "snow_globe",
        "with": "snow"
      },
      {
        "toCreate": "snow_globe",
        "with": "snowman"
      },
      {
        "toCreate": "snow_globe",
        "with": "snowball"
      },
      {
        "toCreate": "snow_globe",
        "with": "snowmobile"
      },
      {
        "toCreate": "snow_globe",
        "with": "santa"
      },
      {
        "toCreate": "snow_globe",
        "with": "christmas_tree"
      },
      {
        "toCreate": "snow_globe",
        "with": "christmas_stocking"
      },
      {
        "toCreate": "snow_globe",
        "with": "blizzard"
      },
      {
        "toCreate": "snow_globe",
        "with": "ice"
      },
      {
        "toCreate": "snow_globe",
        "with": "cold"
      }
    ],
    "name": "Crystal Ball"
  },
  "cuckoo": {
    "combine": [
      {
        "toCreate": "clock",
        "with": "container"
      }
    ],
    "name": "Cuckoo"
  },
  "cup": {
    "combine": [
      {
        "toCreate": "paper_cup",
        "with": "paper"
      },
      {
        "toCreate": "string_phone",
        "with": "wire"
      },
      {
        "toCreate": "string_phone",
        "with": "thread"
      }
    ],
    "name": "Cup"
  },
  "current": {
    "combine": [],
    "name": "Current"
  },
  "cutting_board": {
    "combine": [],
    "name": "Cutting Board"
  },
  "cyborg": {
    "combine": [],
    "name": "Cyborg"
  },
  "cyclist": {
    "combine": [
      {
        "toCreate": "idea",
        "with": "cyclist"
      }
    ],
    "name": "Cyclist"
  },
  "dam": {
    "combine": [
      {
        "toCreate": "beaver",
        "with": "animal"
      }
    ],
    "name": "Dam"
  },
  "darkness": {
    "combine": [
      {
        "toCreate": "black_hole",
        "with": "star"
      },
      {
        "toCreate": "black_hole",
        "with": "sun"
      }
    ],
    "name": "Darkness"
  },
  "dawn": {
    "combine": [
      {
        "toCreate": "alarm_clock",
        "with": "clock"
      },
      {
        "toCreate": "alarm_clock",
        "with": "watch"
      },
      {
        "toCreate": "ash",
        "with": "vampire"
      },
      {
        "toCreate": "day",
        "with": "time"
      },
      {
        "toCreate": "dew",
        "with": "grass"
      },
      {
        "toCreate": "dew",
        "with": "plant"
      },
      {
        "toCreate": "dew",
        "with": "tree"
      },
      {
        "toCreate": "dew",
        "with": "water"
      },
      {
        "toCreate": "dew",
        "with": "fog"
      }
    ],
    "name": "Dawn"
  },
  "day": {
    "combine": [
      {
        "toCreate": "dawn",
        "with": "night"
      },
      {
        "toCreate": "meteor",
        "with": "meteoroid"
      },
      {
        "toCreate": "night",
        "with": "time"
      },
      {
        "toCreate": "sun",
        "with": "space"
      },
      {
        "toCreate": "sun",
        "with": "sky"
      },
      {
        "toCreate": "sun",
        "with": "light"
      },
      {
        "toCreate": "sundial",
        "with": "wheel"
      },
      {
        "toCreate": "sundial",
        "with": "tool"
      },
      {
        "toCreate": "sunglasses",
        "with": "glasses"
      },
      {
        "toCreate": "twilight",
        "with": "night"
      },
      {
        "toCreate": "twilight",
        "with": "time"
      }
    ],
    "name": "Day"
  },
  "death": {
    "combine": [
      {
        "toCreate": "corpse",
        "with": "human"
      },
      {
        "toCreate": "gravestone",
        "with": "stone"
      },
      {
        "toCreate": "gravestone",
        "with": "rock"
      },
      {
        "toCreate": "grim_reaper",
        "with": "scythe"
      },
      {
        "toCreate": "grim_reaper",
        "with": "deity"
      },
      {
        "toCreate": "grim_reaper",
        "with": "deity"
      },
      {
        "toCreate": "organic_matter",
        "with": "life"
      },
      {
        "toCreate": "organic_matter",
        "with": "bacteria"
      },
      {
        "toCreate": "phoenix",
        "with": "fire"
      }
    ],
    "name": "Death"
  },
  "desert": {
    "combine": [
      {
        "toCreate": "animal",
        "with": "life"
      },
      {
        "toCreate": "antarctica",
        "with": "snow"
      },
      {
        "toCreate": "antarctica",
        "with": "ice"
      },
      {
        "toCreate": "cactus",
        "with": "plant"
      },
      {
        "toCreate": "cactus",
        "with": "tree"
      },
      {
        "toCreate": "camel",
        "with": "horse"
      },
      {
        "toCreate": "camel",
        "with": "animal"
      },
      {
        "toCreate": "camel",
        "with": "livestock"
      },
      {
        "toCreate": "camel",
        "with": "cow"
      },
      {
        "toCreate": "camel",
        "with": "saddle"
      },
      {
        "toCreate": "camel",
        "with": "horseshoe"
      },
      {
        "toCreate": "dune",
        "with": "wind"
      },
      {
        "toCreate": "dune",
        "with": "sand"
      },
      {
        "toCreate": "dune",
        "with": "sandstorm"
      },
      {
        "toCreate": "mars",
        "with": "planet"
      },
      {
        "toCreate": "oasis",
        "with": "water"
      },
      {
        "toCreate": "oasis",
        "with": "puddle"
      },
      {
        "toCreate": "oasis",
        "with": "pond"
      },
      {
        "toCreate": "oasis",
        "with": "lake"
      },
      {
        "toCreate": "oasis",
        "with": "river"
      },
      {
        "toCreate": "oasis",
        "with": "stream"
      },
      {
        "toCreate": "oasis",
        "with": "tree"
      },
      {
        "toCreate": "oasis",
        "with": "palm"
      },
      {
        "toCreate": "pyramid",
        "with": "stone"
      },
      {
        "toCreate": "pyramid",
        "with": "grave"
      },
      {
        "toCreate": "pyramid",
        "with": "graveyard"
      },
      {
        "toCreate": "pyramid",
        "with": "mountain"
      },
      {
        "toCreate": "pyramid",
        "with": "hill"
      },
      {
        "toCreate": "pyramid",
        "with": "gravestone"
      },
      {
        "toCreate": "sand_castle",
        "with": "castle"
      },
      {
        "toCreate": "sandstorm",
        "with": "storm"
      },
      {
        "toCreate": "sandstorm",
        "with": "tornado"
      },
      {
        "toCreate": "sandstorm",
        "with": "wind"
      },
      {
        "toCreate": "sandstorm",
        "with": "motion"
      },
      {
        "toCreate": "scorpion",
        "with": "spider"
      },
      {
        "toCreate": "sphinx",
        "with": "statue"
      },
      {
        "toCreate": "vulture",
        "with": "bird"
      },
      {
        "toCreate": "vulture",
        "with": "eagle"
      },
      {
        "toCreate": "vulture",
        "with": "hummingbird"
      },
      {
        "toCreate": "vulture",
        "with": "owl"
      },
      {
        "toCreate": "vulture",
        "with": "pigeon"
      },
      {
        "toCreate": "vulture",
        "with": "penguin"
      },
      {
        "toCreate": "vulture",
        "with": "duck"
      }
    ],
    "name": "Desert"
  },
  "dew": {
    "combine": [],
    "name": "Dew"
  },
  "diamond": {
    "combine": [
      {
        "toCreate": "money",
        "with": "paper"
      },
      {
        "toCreate": "ring",
        "with": "metal"
      },
      {
        "toCreate": "ring",
        "with": "gold"
      },
      {
        "toCreate": "ring",
        "with": "steel"
      },
      {
        "toCreate": "ring",
        "with": "love"
      }
    ],
    "name": "Diamond"
  },
  "dinosaur": {
    "combine": [
      {
        "toCreate": "bird",
        "with": "time"
      },
      {
        "toCreate": "egg",
        "with": "dinosaur"
      },
      {
        "toCreate": "fossil",
        "with": "earth"
      },
      {
        "toCreate": "fossil",
        "with": "stone"
      },
      {
        "toCreate": "fossil",
        "with": "rock"
      },
      {
        "toCreate": "fossil",
        "with": "time"
      },
      {
        "toCreate": "kaiju",
        "with": "city"
      },
      {
        "toCreate": "kaiju",
        "with": "skyscraper"
      },
      {
        "toCreate": "kaiju",
        "with": "legend"
      },
      {
        "toCreate": "kaiju",
        "with": "story"
      },
      {
        "toCreate": "lizard",
        "with": "small"
      },
      {
        "toCreate": "nessie",
        "with": "legend"
      },
      {
        "toCreate": "nessie",
        "with": "story"
      },
      {
        "toCreate": "paleontologist",
        "with": "human"
      },
      {
        "toCreate": "paleontologist",
        "with": "science"
      },
      {
        "toCreate": "pterodactyl",
        "with": "air"
      },
      {
        "toCreate": "pterodactyl",
        "with": "sky"
      },
      {
        "toCreate": "pterodactyl",
        "with": "airplane"
      },
      {
        "toCreate": "pterodactyl",
        "with": "bird"
      },
      {
        "toCreate": "pterodactyl",
        "with": "eagle"
      },
      {
        "toCreate": "pterodactyl",
        "with": "vulture"
      },
      {
        "toCreate": "tyrannosaurus_rex",
        "with": "meat"
      },
      {
        "toCreate": "tyrannosaurus_rex",
        "with": "blood"
      },
      {
        "toCreate": "tyrannosaurus_rex",
        "with": "monarch"
      }
    ],
    "name": "Dinosaur"
  },
  "diver": {
    "combine": [
      {
        "toCreate": "idea",
        "with": "diver"
      }
    ],
    "name": "Diver"
  },
  "doctor": {
    "combine": [
      {
        "toCreate": "ambulance",
        "with": "car"
      },
      {
        "toCreate": "cyborg",
        "with": "robot"
      },
      {
        "toCreate": "hospital",
        "with": "house"
      },
      {
        "toCreate": "hospital",
        "with": "wall"
      },
      {
        "toCreate": "hospital",
        "with": "container"
      },
      {
        "toCreate": "idea",
        "with": "doctor"
      },
      {
        "toCreate": "penicillin",
        "with": "mold"
      },
      {
        "toCreate": "scalpel",
        "with": "blade"
      },
      {
        "toCreate": "scalpel",
        "with": "sword"
      },
      {
        "toCreate": "statue",
        "with": "medusa"
      },
      {
        "toCreate": "stethoscope",
        "with": "tool"
      },
      {
        "toCreate": "stethoscope",
        "with": "drum"
      },
      {
        "toCreate": "stethoscope",
        "with": "sound"
      },
      {
        "toCreate": "syringe",
        "with": "needle"
      }
    ],
    "name": "Doctor"
  },
  "dog": {
    "combine": [
      {
        "toCreate": "armadillo",
        "with": "armor"
      },
      {
        "toCreate": "doge",
        "with": "internet"
      },
      {
        "toCreate": "doge",
        "with": "computer"
      },
      {
        "toCreate": "doghouse",
        "with": "house"
      },
      {
        "toCreate": "doghouse",
        "with": "wall"
      },
      {
        "toCreate": "doghouse",
        "with": "container"
      },
      {
        "toCreate": "fox",
        "with": "chicken"
      },
      {
        "toCreate": "fox",
        "with": "chicken_coop"
      },
      {
        "toCreate": "husky",
        "with": "snow"
      },
      {
        "toCreate": "husky",
        "with": "ice"
      },
      {
        "toCreate": "husky",
        "with": "antarctica"
      },
      {
        "toCreate": "husky",
        "with": "arctic"
      },
      {
        "toCreate": "husky",
        "with": "blizzard"
      },
      {
        "toCreate": "husky",
        "with": "glacier"
      },
      {
        "toCreate": "origami",
        "with": "paper"
      },
      {
        "toCreate": "seal",
        "with": "sea"
      },
      {
        "toCreate": "seal",
        "with": "ocean"
      },
      {
        "toCreate": "seal",
        "with": "arctic"
      },
      {
        "toCreate": "seal",
        "with": "lake"
      },
      {
        "toCreate": "seal",
        "with": "water"
      },
      {
        "toCreate": "wolf",
        "with": "animal"
      },
      {
        "toCreate": "wolf",
        "with": "forest"
      },
      {
        "toCreate": "wolf",
        "with": "blood"
      }
    ],
    "name": "Dog"
  },
  "doge": {
    "combine": [],
    "name": "Doge"
  },
  "doghouse": {
    "combine": [],
    "name": "Doghouse"
  },
  "domestication": {
    "combine": [
      {
        "toCreate": "chicken",
        "with": "bird"
      },
      {
        "toCreate": "chicken",
        "with": "egg"
      },
      {
        "toCreate": "dog",
        "with": "wolf"
      },
      {
        "toCreate": "hamster",
        "with": "mouse"
      },
      {
        "toCreate": "hamster",
        "with": "rat"
      },
      {
        "toCreate": "livestock",
        "with": "animal"
      },
      {
        "toCreate": "nuts",
        "with": "tree"
      },
      {
        "toCreate": "sheep",
        "with": "goat"
      },
      {
        "toCreate": "sheep",
        "with": "mountain_goat"
      },
      {
        "toCreate": "vegetable",
        "with": "plant"
      },
      {
        "toCreate": "wheat",
        "with": "grass"
      }
    ],
    "name": "Domestication"
  },
  "don_quixote": {
    "combine": [],
    "name": "Don Quixote"
  },
  "donut": {
    "combine": [
      {
        "toCreate": "baker",
        "with": "human"
      },
      {
        "toCreate": "bakery",
        "with": "house"
      },
      {
        "toCreate": "bakery",
        "with": "city"
      },
      {
        "toCreate": "bakery",
        "with": "village"
      },
      {
        "toCreate": "box",
        "with": "container"
      },
      {
        "toCreate": "cake",
        "with": "candle"
      },
      {
        "toCreate": "gingerbread_house",
        "with": "house"
      }
    ],
    "name": "Donut"
  },
  "double_rainbow!": {
    "combine": [
      {
        "toCreate": "butterfly",
        "with": "animal"
      },
      {
        "toCreate": "chameleon",
        "with": "lizard"
      },
      {
        "toCreate": "chameleon",
        "with": "snake"
      },
      {
        "toCreate": "chameleon",
        "with": "turtle"
      },
      {
        "toCreate": "crayon",
        "with": "pencil"
      },
      {
        "toCreate": "crayon",
        "with": "wax"
      },
      {
        "toCreate": "flower",
        "with": "plant"
      },
      {
        "toCreate": "flower",
        "with": "grass"
      },
      {
        "toCreate": "magic",
        "with": "life"
      },
      {
        "toCreate": "paint",
        "with": "water"
      },
      {
        "toCreate": "paint",
        "with": "pottery"
      },
      {
        "toCreate": "paint",
        "with": "tool"
      },
      {
        "toCreate": "paint",
        "with": "liquid"
      },
      {
        "toCreate": "peacock",
        "with": "bird"
      },
      {
        "toCreate": "prism",
        "with": "glass"
      },
      {
        "toCreate": "prism",
        "with": "crystal_ball"
      },
      {
        "toCreate": "sprinkles",
        "with": "sugar"
      },
      {
        "toCreate": "toucan",
        "with": "bird"
      },
      {
        "toCreate": "unicorn",
        "with": "horse"
      },
      {
        "toCreate": "wizard",
        "with": "human"
      }
    ],
    "name": "Double Rainbow!"
  },
  "dough": {
    "combine": [
      {
        "toCreate": "baker",
        "with": "human"
      },
      {
        "toCreate": "banana_bread",
        "with": "banana"
      },
      {
        "toCreate": "bread",
        "with": "fire"
      },
      {
        "toCreate": "bread",
        "with": "warmth"
      },
      {
        "toCreate": "bread",
        "with": "energy"
      },
      {
        "toCreate": "cake",
        "with": "candle"
      },
      {
        "toCreate": "cookie",
        "with": "cookie_cutter"
      },
      {
        "toCreate": "cookie_cutter",
        "with": "blade"
      },
      {
        "toCreate": "cookie_dough",
        "with": "sugar"
      },
      {
        "toCreate": "cookie_dough",
        "with": "chocolate"
      },
      {
        "toCreate": "cookie_dough",
        "with": "cookie"
      },
      {
        "toCreate": "donut",
        "with": "oil"
      },
      {
        "toCreate": "donut",
        "with": "wheel"
      },
      {
        "toCreate": "gingerbread_house",
        "with": "house"
      },
      {
        "toCreate": "gingerbread_man",
        "with": "life"
      },
      {
        "toCreate": "gingerbread_man",
        "with": "magic"
      },
      {
        "toCreate": "gingerbread_man",
        "with": "story"
      },
      {
        "toCreate": "pie",
        "with": "fruit"
      },
      {
        "toCreate": "pie",
        "with": "meat"
      },
      {
        "toCreate": "pizza",
        "with": "cheese"
      }
    ],
    "name": "Dough"
  },
  "dragon": {
    "combine": [
      {
        "toCreate": "egg",
        "with": "dragon"
      },
      {
        "toCreate": "fairy_tale",
        "with": "story"
      },
      {
        "toCreate": "hero",
        "with": "knight"
      }
    ],
    "name": "Dragon"
  },
  "drone": {
    "combine": [],
    "name": "Drone"
  },
  "drum": {
    "combine": [
      {
        "toCreate": "music",
        "with": "musician"
      },
      {
        "toCreate": "musician",
        "with": "human"
      },
      {
        "toCreate": "stethoscope",
        "with": "doctor"
      }
    ],
    "name": "Drum"
  },
  "drunk": {
    "combine": [
      {
        "toCreate": "statue",
        "with": "medusa"
      }
    ],
    "name": "Drunk"
  },
  "dry_ice": {
    "combine": [],
    "name": "Dry Ice"
  },
  "duck": {
    "combine": [
      {
        "toCreate": "airplane",
        "with": "metal"
      },
      {
        "toCreate": "airplane",
        "with": "steel"
      },
      {
        "toCreate": "airplane",
        "with": "machine"
      },
      {
        "toCreate": "duckling",
        "with": "egg"
      },
      {
        "toCreate": "egg",
        "with": "duck"
      },
      {
        "toCreate": "origami",
        "with": "paper"
      },
      {
        "toCreate": "platypus",
        "with": "beaver"
      },
      {
        "toCreate": "vulture",
        "with": "corpse"
      },
      {
        "toCreate": "vulture",
        "with": "desert"
      }
    ],
    "name": "Duck"
  },
  "duckling": {
    "combine": [
      {
        "toCreate": "duck",
        "with": "time"
      },
      {
        "toCreate": "duck",
        "with": "big"
      }
    ],
    "name": "Duckling"
  },
  "dune": {
    "combine": [
      {
        "toCreate": "camel",
        "with": "horse"
      },
      {
        "toCreate": "camel",
        "with": "livestock"
      },
      {
        "toCreate": "camel",
        "with": "cow"
      },
      {
        "toCreate": "camel",
        "with": "animal"
      },
      {
        "toCreate": "sand_castle",
        "with": "castle"
      },
      {
        "toCreate": "scorpion",
        "with": "animal"
      },
      {
        "toCreate": "scorpion",
        "with": "spider"
      }
    ],
    "name": "Dune"
  },
  "dust": {
    "combine": [
      {
        "toCreate": "allergy",
        "with": "human"
      },
      {
        "toCreate": "gunpowder",
        "with": "fire"
      },
      {
        "toCreate": "gunpowder",
        "with": "energy"
      },
      {
        "toCreate": "pollen",
        "with": "plant"
      },
      {
        "toCreate": "pollen",
        "with": "flower"
      }
    ],
    "name": "Dust"
  },
  "dynamite": {
    "combine": [],
    "name": "Dynamite"
  },
  "eagle": {
    "combine": [
      {
        "toCreate": "airplane",
        "with": "metal"
      },
      {
        "toCreate": "airplane",
        "with": "steel"
      },
      {
        "toCreate": "airplane",
        "with": "machine"
      },
      {
        "toCreate": "egg",
        "with": "eagle"
      },
      {
        "toCreate": "origami",
        "with": "paper"
      },
      {
        "toCreate": "ostrich",
        "with": "earth"
      },
      {
        "toCreate": "ostrich",
        "with": "sand"
      },
      {
        "toCreate": "pterodactyl",
        "with": "dinosaur"
      },
      {
        "toCreate": "vulture",
        "with": "corpse"
      },
      {
        "toCreate": "vulture",
        "with": "desert"
      }
    ],
    "name": "Eagle"
  },
  "earth": {
    "combine": [
      {
        "toCreate": "anthill",
        "with": "ant"
      },
      {
        "toCreate": "boulder",
        "with": "rock"
      },
      {
        "toCreate": "coal",
        "with": "peat"
      },
      {
        "toCreate": "continent",
        "with": "land"
      },
      {
        "toCreate": "dust",
        "with": "air"
      },
      {
        "toCreate": "earthquake",
        "with": "motion"
      },
      {
        "toCreate": "field",
        "with": "farmer"
      },
      {
        "toCreate": "field",
        "with": "tool"
      },
      {
        "toCreate": "field",
        "with": "plow"
      },
      {
        "toCreate": "fog",
        "with": "cloud"
      },
      {
        "toCreate": "forest",
        "with": "tree"
      },
      {
        "toCreate": "fossil",
        "with": "dinosaur"
      },
      {
        "toCreate": "fossil",
        "with": "bone"
      },
      {
        "toCreate": "fossil",
        "with": "skeleton"
      },
      {
        "toCreate": "geyser",
        "with": "steam"
      },
      {
        "toCreate": "grass",
        "with": "plant"
      },
      {
        "toCreate": "grave",
        "with": "coffin"
      },
      {
        "toCreate": "grave",
        "with": "corpse"
      },
      {
        "toCreate": "hill",
        "with": "boulder"
      },
      {
        "toCreate": "horizon",
        "with": "sky"
      },
      {
        "toCreate": "juice",
        "with": "fruit"
      },
      {
        "toCreate": "land",
        "with": "earth"
      },
      {
        "toCreate": "land",
        "with": "stone"
      },
      {
        "toCreate": "lava",
        "with": "fire"
      },
      {
        "toCreate": "lava",
        "with": "heat"
      },
      {
        "toCreate": "lava",
        "with": "liquid"
      },
      {
        "toCreate": "mineral",
        "with": "organic_matter"
      },
      {
        "toCreate": "moon",
        "with": "night"
      },
      {
        "toCreate": "mountain",
        "with": "earthquake"
      },
      {
        "toCreate": "mountain",
        "with": "hill"
      },
      {
        "toCreate": "mountain",
        "with": "big"
      },
      {
        "toCreate": "mud",
        "with": "water"
      },
      {
        "toCreate": "ore",
        "with": "hammer"
      },
      {
        "toCreate": "ostrich",
        "with": "bird"
      },
      {
        "toCreate": "ostrich",
        "with": "eagle"
      },
      {
        "toCreate": "ostrich",
        "with": "chicken"
      },
      {
        "toCreate": "pebble",
        "with": "small"
      },
      {
        "toCreate": "planet",
        "with": "space"
      },
      {
        "toCreate": "planet",
        "with": "solar_system"
      },
      {
        "toCreate": "planet",
        "with": "sky"
      },
      {
        "toCreate": "plant",
        "with": "seed"
      },
      {
        "toCreate": "plant",
        "with": "algae"
      },
      {
        "toCreate": "plow",
        "with": "metal"
      },
      {
        "toCreate": "plow",
        "with": "steel"
      },
      {
        "toCreate": "plow",
        "with": "wood"
      },
      {
        "toCreate": "potato",
        "with": "vegetable"
      },
      {
        "toCreate": "primordial_soup",
        "with": "ocean"
      },
      {
        "toCreate": "primordial_soup",
        "with": "sea"
      },
      {
        "toCreate": "rock",
        "with": "pebble"
      },
      {
        "toCreate": "sandstone",
        "with": "sand"
      },
      {
        "toCreate": "smoke",
        "with": "gas"
      },
      {
        "toCreate": "snowball",
        "with": "snow"
      },
      {
        "toCreate": "soil",
        "with": "life"
      },
      {
        "toCreate": "soil",
        "with": "organic_matter"
      },
      {
        "toCreate": "solid",
        "with": "idea"
      },
      {
        "toCreate": "solid",
        "with": "science"
      },
      {
        "toCreate": "stone",
        "with": "solid"
      },
      {
        "toCreate": "stone",
        "with": "pressure"
      },
      {
        "toCreate": "volcano",
        "with": "lava"
      }
    ],
    "name": "Earth"
  },
  "earthquake": {
    "combine": [
      {
        "toCreate": "avalanche",
        "with": "snow"
      },
      {
        "toCreate": "avalanche",
        "with": "glacier"
      },
      {
        "toCreate": "avalanche",
        "with": "mountain"
      },
      {
        "toCreate": "avalanche",
        "with": "mountain_range"
      },
      {
        "toCreate": "mountain",
        "with": "earth"
      },
      {
        "toCreate": "mountain",
        "with": "hill"
      },
      {
        "toCreate": "tsunami",
        "with": "ocean"
      },
      {
        "toCreate": "tsunami",
        "with": "sea"
      }
    ],
    "name": "Earthquake"
  },
  "eclipse": {
    "combine": [],
    "name": "Eclipse"
  },
  "egg": {
    "combine": [
      {
        "toCreate": "bird",
        "with": "sky"
      },
      {
        "toCreate": "bird",
        "with": "air"
      },
      {
        "toCreate": "bird",
        "with": "airplane"
      },
      {
        "toCreate": "birdhouse",
        "with": "container"
      },
      {
        "toCreate": "chicken",
        "with": "domestication"
      },
      {
        "toCreate": "chicken",
        "with": "livestock"
      },
      {
        "toCreate": "chicken",
        "with": "farmer"
      },
      {
        "toCreate": "chicken",
        "with": "barn"
      },
      {
        "toCreate": "chicken",
        "with": "farm"
      },
      {
        "toCreate": "duckling",
        "with": "duck"
      },
      {
        "toCreate": "egg_timer",
        "with": "clock"
      },
      {
        "toCreate": "egg_timer",
        "with": "alarm_clock"
      },
      {
        "toCreate": "egg_timer",
        "with": "watch"
      },
      {
        "toCreate": "fish",
        "with": "water"
      },
      {
        "toCreate": "fish",
        "with": "lake"
      },
      {
        "toCreate": "fish",
        "with": "sea"
      },
      {
        "toCreate": "fish",
        "with": "ocean"
      },
      {
        "toCreate": "frog",
        "with": "pond"
      },
      {
        "toCreate": "frog",
        "with": "puddle"
      },
      {
        "toCreate": "lizard",
        "with": "stone"
      },
      {
        "toCreate": "lizard",
        "with": "rock"
      },
      {
        "toCreate": "lizard",
        "with": "swamp"
      },
      {
        "toCreate": "mayonnaise",
        "with": "oil"
      },
      {
        "toCreate": "nest",
        "with": "hay"
      },
      {
        "toCreate": "nest",
        "with": "tree"
      },
      {
        "toCreate": "nest",
        "with": "grass"
      },
      {
        "toCreate": "nest",
        "with": "container"
      },
      {
        "toCreate": "omelette",
        "with": "fire"
      },
      {
        "toCreate": "omelette",
        "with": "heat"
      },
      {
        "toCreate": "omelette",
        "with": "tool"
      },
      {
        "toCreate": "pasta",
        "with": "flour"
      },
      {
        "toCreate": "philosophy",
        "with": "chicken"
      },
      {
        "toCreate": "phoenix",
        "with": "fire"
      },
      {
        "toCreate": "roe",
        "with": "fish"
      },
      {
        "toCreate": "roe",
        "with": "water"
      },
      {
        "toCreate": "roe",
        "with": "ocean"
      },
      {
        "toCreate": "roe",
        "with": "sea"
      },
      {
        "toCreate": "roe",
        "with": "lake"
      },
      {
        "toCreate": "roe",
        "with": "flying_fish"
      },
      {
        "toCreate": "turtle",
        "with": "sand"
      },
      {
        "toCreate": "turtle",
        "with": "beach"
      }
    ],
    "name": "Egg"
  },
  "egg_timer": {
    "combine": [],
    "name": "Egg Timer"
  },
  "electric_car": {
    "combine": [
      {
        "toCreate": "garage",
        "with": "house"
      },
      {
        "toCreate": "garage",
        "with": "container"
      },
      {
        "toCreate": "garage",
        "with": "wall"
      },
      {
        "toCreate": "garage",
        "with": "barn"
      },
      {
        "toCreate": "moon_rover",
        "with": "moon"
      }
    ],
    "name": "Electric Car"
  },
  "electric_eel": {
    "combine": [
      {
        "toCreate": "snake",
        "with": "land"
      }
    ],
    "name": "Electric Eel"
  },
  "electrician": {
    "combine": [
      {
        "toCreate": "cyborg",
        "with": "robot"
      },
      {
        "toCreate": "idea",
        "with": "electrician"
      },
      {
        "toCreate": "wind_turbine",
        "with": "windmill"
      }
    ],
    "name": "Electrician"
  },
  "electricity": {
    "combine": [
      {
        "toCreate": "aurora",
        "with": "atmosphere"
      },
      {
        "toCreate": "aurora",
        "with": "antarctica"
      },
      {
        "toCreate": "aurora",
        "with": "sky"
      },
      {
        "toCreate": "aurora",
        "with": "arctic"
      },
      {
        "toCreate": "battery",
        "with": "container"
      },
      {
        "toCreate": "battery",
        "with": "mineral"
      },
      {
        "toCreate": "battery",
        "with": "ore"
      },
      {
        "toCreate": "blender",
        "with": "blade"
      },
      {
        "toCreate": "chainsaw",
        "with": "axe"
      },
      {
        "toCreate": "chainsaw",
        "with": "lumberjack"
      },
      {
        "toCreate": "clock",
        "with": "time"
      },
      {
        "toCreate": "clock",
        "with": "sundial"
      },
      {
        "toCreate": "computer",
        "with": "hacker"
      },
      {
        "toCreate": "electric_car",
        "with": "car"
      },
      {
        "toCreate": "electric_car",
        "with": "wagon"
      },
      {
        "toCreate": "electric_eel",
        "with": "fish"
      },
      {
        "toCreate": "electrician",
        "with": "human"
      },
      {
        "toCreate": "email",
        "with": "letter"
      },
      {
        "toCreate": "explosion",
        "with": "gunpowder"
      },
      {
        "toCreate": "frankenstein’s_monster",
        "with": "corpse"
      },
      {
        "toCreate": "frankenstein’s_monster",
        "with": "zombie"
      },
      {
        "toCreate": "fridge",
        "with": "cold"
      },
      {
        "toCreate": "glass",
        "with": "sand"
      },
      {
        "toCreate": "lawn_mower",
        "with": "scythe"
      },
      {
        "toCreate": "life",
        "with": "primordial_soup"
      },
      {
        "toCreate": "life",
        "with": "ocean"
      },
      {
        "toCreate": "life",
        "with": "sea"
      },
      {
        "toCreate": "life",
        "with": "lake"
      },
      {
        "toCreate": "light",
        "with": "light_bulb"
      },
      {
        "toCreate": "light",
        "with": "flashlight"
      },
      {
        "toCreate": "light_bulb",
        "with": "glass"
      },
      {
        "toCreate": "light_sword",
        "with": "sword"
      },
      {
        "toCreate": "lightning",
        "with": "cloud"
      },
      {
        "toCreate": "lightning",
        "with": "storm"
      },
      {
        "toCreate": "lightning",
        "with": "rain"
      },
      {
        "toCreate": "ozone",
        "with": "oxygen"
      },
      {
        "toCreate": "ozone",
        "with": "atmosphere"
      },
      {
        "toCreate": "ozone",
        "with": "air"
      },
      {
        "toCreate": "sewing_machine",
        "with": "needle"
      },
      {
        "toCreate": "sewing_machine",
        "with": "thread"
      },
      {
        "toCreate": "solar_cell",
        "with": "sun"
      },
      {
        "toCreate": "storm",
        "with": "cloud"
      },
      {
        "toCreate": "storm",
        "with": "atmosphere"
      },
      {
        "toCreate": "stun_gun",
        "with": "gun"
      },
      {
        "toCreate": "stun_gun",
        "with": "bow"
      },
      {
        "toCreate": "vacuum_cleaner",
        "with": "broom"
      },
      {
        "toCreate": "wind_turbine",
        "with": "windmill"
      },
      {
        "toCreate": "wind_turbine",
        "with": "wind"
      },
      {
        "toCreate": "wire",
        "with": "metal"
      },
      {
        "toCreate": "wire",
        "with": "steel"
      },
      {
        "toCreate": "wire",
        "with": "rope"
      }
    ],
    "name": "Electricity"
  },
  "email": {
    "combine": [
      {
        "toCreate": "computer",
        "with": "container"
      }
    ],
    "name": "Email"
  },
  "energy": {
    "combine": [
      {
        "toCreate": "atomic_bomb",
        "with": "explosion"
      },
      {
        "toCreate": "battery",
        "with": "container"
      },
      {
        "toCreate": "bread",
        "with": "dough"
      },
      {
        "toCreate": "butter",
        "with": "milk"
      },
      {
        "toCreate": "gas",
        "with": "liquid"
      },
      {
        "toCreate": "gunpowder",
        "with": "mineral"
      },
      {
        "toCreate": "gunpowder",
        "with": "charcoal"
      },
      {
        "toCreate": "gunpowder",
        "with": "dust"
      },
      {
        "toCreate": "heat",
        "with": "air"
      },
      {
        "toCreate": "light_sword",
        "with": "sword"
      },
      {
        "toCreate": "lightning",
        "with": "storm"
      },
      {
        "toCreate": "lightning",
        "with": "cloud"
      },
      {
        "toCreate": "lightning",
        "with": "rain"
      },
      {
        "toCreate": "liquid",
        "with": "solid"
      },
      {
        "toCreate": "magic",
        "with": "wizard"
      },
      {
        "toCreate": "magic",
        "with": "witch"
      },
      {
        "toCreate": "solar_cell",
        "with": "sun"
      },
      {
        "toCreate": "stun_gun",
        "with": "gun"
      },
      {
        "toCreate": "stun_gun",
        "with": "bow"
      },
      {
        "toCreate": "sugar",
        "with": "juice"
      },
      {
        "toCreate": "sugar",
        "with": "fruit"
      },
      {
        "toCreate": "sugar",
        "with": "alcohol"
      },
      {
        "toCreate": "sugar",
        "with": "beer"
      },
      {
        "toCreate": "sugar",
        "with": "wine"
      }
    ],
    "name": "Energy"
  },
  "engineer": {
    "combine": [
      {
        "toCreate": "cyborg",
        "with": "robot"
      },
      {
        "toCreate": "idea",
        "with": "light_bulb"
      },
      {
        "toCreate": "idea",
        "with": "engineer"
      },
      {
        "toCreate": "lens",
        "with": "glass"
      },
      {
        "toCreate": "machine",
        "with": "tool"
      },
      {
        "toCreate": "safety_glasses",
        "with": "glasses"
      },
      {
        "toCreate": "statue",
        "with": "medusa"
      },
      {
        "toCreate": "thermometer",
        "with": "quicksilver"
      },
      {
        "toCreate": "tunnel",
        "with": "mountain"
      },
      {
        "toCreate": "tunnel",
        "with": "mountain_range"
      },
      {
        "toCreate": "tunnel",
        "with": "hill"
      }
    ],
    "name": "Engineer"
  },
  "eruption": {
    "combine": [],
    "name": "Eruption"
  },
  "excalibur": {
    "combine": [
      {
        "toCreate": "monarch",
        "with": "human"
      }
    ],
    "name": "Excalibur"
  },
  "explosion": {
    "combine": [
      {
        "toCreate": "atomic_bomb",
        "with": "energy"
      },
      {
        "toCreate": "atomic_bomb",
        "with": "big"
      },
      {
        "toCreate": "atomic_bomb",
        "with": "explosion"
      },
      {
        "toCreate": "combustion_engine",
        "with": "machine"
      },
      {
        "toCreate": "combustion_engine",
        "with": "steam_engine"
      },
      {
        "toCreate": "corpse",
        "with": "human"
      },
      {
        "toCreate": "explosion",
        "with": "petroleum"
      },
      {
        "toCreate": "fireworks",
        "with": "sky"
      },
      {
        "toCreate": "fireworks",
        "with": "atmosphere"
      },
      {
        "toCreate": "grenade",
        "with": "metal"
      },
      {
        "toCreate": "grenade",
        "with": "steel"
      },
      {
        "toCreate": "grenade",
        "with": "warrior"
      },
      {
        "toCreate": "juice",
        "with": "fruit"
      },
      {
        "toCreate": "juice",
        "with": "vegetable"
      },
      {
        "toCreate": "safety_glasses",
        "with": "glasses"
      },
      {
        "toCreate": "supernova",
        "with": "star"
      },
      {
        "toCreate": "supernova",
        "with": "sun"
      },
      {
        "toCreate": "supernova",
        "with": "galaxy"
      },
      {
        "toCreate": "supernova",
        "with": "space"
      },
      {
        "toCreate": "tsunami",
        "with": "ocean"
      },
      {
        "toCreate": "tsunami",
        "with": "sea"
      }
    ],
    "name": "Explosion"
  },
  "fabric": {
    "combine": [
      {
        "toCreate": "apron",
        "with": "cook"
      },
      {
        "toCreate": "apron",
        "with": "baker"
      },
      {
        "toCreate": "armor",
        "with": "metal"
      },
      {
        "toCreate": "armor",
        "with": "steel"
      },
      {
        "toCreate": "bandage",
        "with": "blood"
      },
      {
        "toCreate": "bandage",
        "with": "sword"
      },
      {
        "toCreate": "bandage",
        "with": "blade"
      },
      {
        "toCreate": "canvas",
        "with": "paint"
      },
      {
        "toCreate": "cashmere",
        "with": "mountain_goat"
      },
      {
        "toCreate": "cotton",
        "with": "plant"
      },
      {
        "toCreate": "drum",
        "with": "wood"
      },
      {
        "toCreate": "mummy",
        "with": "pyramid"
      },
      {
        "toCreate": "mummy",
        "with": "corpse"
      },
      {
        "toCreate": "parachute",
        "with": "pilot"
      },
      {
        "toCreate": "parachute",
        "with": "airplane"
      },
      {
        "toCreate": "picnic",
        "with": "sandwich"
      },
      {
        "toCreate": "saddle",
        "with": "horse"
      },
      {
        "toCreate": "sailboat",
        "with": "boat"
      },
      {
        "toCreate": "sailboat",
        "with": "steamboat"
      },
      {
        "toCreate": "sandpaper",
        "with": "sand"
      },
      {
        "toCreate": "smoke_signal",
        "with": "campfire"
      },
      {
        "toCreate": "smoke_signal",
        "with": "smoke"
      },
      {
        "toCreate": "tent",
        "with": "wood"
      },
      {
        "toCreate": "tent",
        "with": "house"
      },
      {
        "toCreate": "tent",
        "with": "village"
      },
      {
        "toCreate": "tent",
        "with": "wall"
      },
      {
        "toCreate": "umbrella",
        "with": "rain"
      },
      {
        "toCreate": "umbrella",
        "with": "storm"
      },
      {
        "toCreate": "wagon",
        "with": "cart"
      },
      {
        "toCreate": "web",
        "with": "spider"
      }
    ],
    "name": "Fabric"
  },
  "fairy_tale": {
    "combine": [
      {
        "toCreate": "book",
        "with": "container"
      },
      {
        "toCreate": "excalibur",
        "with": "sword"
      },
      {
        "toCreate": "gnome",
        "with": "garden"
      },
      {
        "toCreate": "legend",
        "with": "story"
      },
      {
        "toCreate": "legend",
        "with": "time"
      }
    ],
    "name": "Fairy Tale"
  },
  "family": {
    "combine": [
      {
        "toCreate": "family_tree",
        "with": "tree"
      },
      {
        "toCreate": "family_tree",
        "with": "time"
      },
      {
        "toCreate": "house",
        "with": "container"
      },
      {
        "toCreate": "village",
        "with": "family"
      }
    ],
    "name": "Family"
  },
  "family_tree": {
    "combine": [],
    "name": "Family tree"
  },
  "farm": {
    "combine": [
      {
        "toCreate": "ant_farm",
        "with": "ant"
      },
      {
        "toCreate": "ant_farm",
        "with": "anthill"
      },
      {
        "toCreate": "ant_farm",
        "with": "anthill"
      },
      {
        "toCreate": "barn",
        "with": "house"
      },
      {
        "toCreate": "chicken",
        "with": "bird"
      },
      {
        "toCreate": "chicken",
        "with": "egg"
      },
      {
        "toCreate": "dog",
        "with": "wolf"
      },
      {
        "toCreate": "hay",
        "with": "grass"
      },
      {
        "toCreate": "hay",
        "with": "pitchfork"
      },
      {
        "toCreate": "mouse",
        "with": "cheese"
      },
      {
        "toCreate": "orchard",
        "with": "fruit_tree"
      },
      {
        "toCreate": "ruins",
        "with": "time"
      },
      {
        "toCreate": "scarecrow",
        "with": "statue"
      },
      {
        "toCreate": "silo",
        "with": "wheat"
      }
    ],
    "name": "Farm"
  },
  "farmer": {
    "combine": [
      {
        "toCreate": "beekeeper",
        "with": "beehive"
      },
      {
        "toCreate": "beekeeper",
        "with": "bee"
      },
      {
        "toCreate": "chicken",
        "with": "bird"
      },
      {
        "toCreate": "chicken",
        "with": "egg"
      },
      {
        "toCreate": "cow",
        "with": "livestock"
      },
      {
        "toCreate": "cyborg",
        "with": "robot"
      },
      {
        "toCreate": "dog",
        "with": "wolf"
      },
      {
        "toCreate": "domestication",
        "with": "animal"
      },
      {
        "toCreate": "domestication",
        "with": "science"
      },
      {
        "toCreate": "farm",
        "with": "house"
      },
      {
        "toCreate": "farm",
        "with": "barn"
      },
      {
        "toCreate": "farm",
        "with": "container"
      },
      {
        "toCreate": "field",
        "with": "earth"
      },
      {
        "toCreate": "field",
        "with": "land"
      },
      {
        "toCreate": "field",
        "with": "soil"
      },
      {
        "toCreate": "fruit",
        "with": "tree"
      },
      {
        "toCreate": "fruit",
        "with": "orchard"
      },
      {
        "toCreate": "gardener",
        "with": "garden"
      },
      {
        "toCreate": "hay",
        "with": "grass"
      },
      {
        "toCreate": "hay",
        "with": "pitchfork"
      },
      {
        "toCreate": "idea",
        "with": "farmer"
      },
      {
        "toCreate": "livestock",
        "with": "animal"
      },
      {
        "toCreate": "milk",
        "with": "cow"
      },
      {
        "toCreate": "milk",
        "with": "goat"
      },
      {
        "toCreate": "nuts",
        "with": "tree"
      },
      {
        "toCreate": "orchard",
        "with": "fruit_tree"
      },
      {
        "toCreate": "pumpkin",
        "with": "vegetable"
      },
      {
        "toCreate": "statue",
        "with": "medusa"
      },
      {
        "toCreate": "tractor",
        "with": "car"
      },
      {
        "toCreate": "tractor",
        "with": "wagon"
      },
      {
        "toCreate": "vegetable",
        "with": "field"
      },
      {
        "toCreate": "vegetable",
        "with": "forest"
      },
      {
        "toCreate": "vegetable",
        "with": "plant"
      },
      {
        "toCreate": "wheat",
        "with": "grass"
      }
    ],
    "name": "Farmer"
  },
  "faun": {
    "combine": [],
    "name": "Faun"
  },
  "fence": {
    "combine": [
      {
        "toCreate": "hedge",
        "with": "plant"
      },
      {
        "toCreate": "hedge",
        "with": "leaf"
      },
      {
        "toCreate": "hedge",
        "with": "garden"
      }
    ],
    "name": "Fence"
  },
  "field": {
    "combine": [
      {
        "toCreate": "barn",
        "with": "house"
      },
      {
        "toCreate": "cow",
        "with": "livestock"
      },
      {
        "toCreate": "crow",
        "with": "bird"
      },
      {
        "toCreate": "crow",
        "with": "pigeon"
      },
      {
        "toCreate": "crow",
        "with": "owl"
      },
      {
        "toCreate": "crow",
        "with": "hummingbird"
      },
      {
        "toCreate": "dog",
        "with": "wolf"
      },
      {
        "toCreate": "farmer",
        "with": "human"
      },
      {
        "toCreate": "fence",
        "with": "wood"
      },
      {
        "toCreate": "fence",
        "with": "wall"
      },
      {
        "toCreate": "fog",
        "with": "cloud"
      },
      {
        "toCreate": "grave",
        "with": "coffin"
      },
      {
        "toCreate": "grave",
        "with": "corpse"
      },
      {
        "toCreate": "graveyard",
        "with": "grave"
      },
      {
        "toCreate": "graveyard",
        "with": "gravestone"
      },
      {
        "toCreate": "horse",
        "with": "animal"
      },
      {
        "toCreate": "lawn",
        "with": "lawn_mower"
      },
      {
        "toCreate": "lawn_mower",
        "with": "helicopter"
      },
      {
        "toCreate": "livestock",
        "with": "animal"
      },
      {
        "toCreate": "nuts",
        "with": "tree"
      },
      {
        "toCreate": "orchard",
        "with": "fruit_tree"
      },
      {
        "toCreate": "park",
        "with": "city"
      },
      {
        "toCreate": "park",
        "with": "village"
      },
      {
        "toCreate": "plow",
        "with": "metal"
      },
      {
        "toCreate": "plow",
        "with": "steel"
      },
      {
        "toCreate": "plow",
        "with": "wood"
      },
      {
        "toCreate": "plow",
        "with": "tool"
      },
      {
        "toCreate": "pumpkin",
        "with": "vegetable"
      },
      {
        "toCreate": "tractor",
        "with": "car"
      },
      {
        "toCreate": "tractor",
        "with": "wagon"
      },
      {
        "toCreate": "vegetable",
        "with": "farmer"
      },
      {
        "toCreate": "vegetable",
        "with": "plant"
      },
      {
        "toCreate": "wheat",
        "with": "grass"
      }
    ],
    "name": "Field"
  },
  "fire": {
    "combine": [
      {
        "toCreate": "ash",
        "with": "mineral"
      },
      {
        "toCreate": "ash",
        "with": "plant"
      },
      {
        "toCreate": "ash",
        "with": "tree"
      },
      {
        "toCreate": "ash",
        "with": "grass"
      },
      {
        "toCreate": "ash",
        "with": "paper"
      },
      {
        "toCreate": "bacon",
        "with": "pig"
      },
      {
        "toCreate": "bacon",
        "with": "ham"
      },
      {
        "toCreate": "bread",
        "with": "dough"
      },
      {
        "toCreate": "brick",
        "with": "mud"
      },
      {
        "toCreate": "brick",
        "with": "clay"
      },
      {
        "toCreate": "campfire",
        "with": "wood"
      },
      {
        "toCreate": "candle",
        "with": "wax"
      },
      {
        "toCreate": "charcoal",
        "with": "wood"
      },
      {
        "toCreate": "charcoal",
        "with": "tree"
      },
      {
        "toCreate": "charcoal",
        "with": "corpse"
      },
      {
        "toCreate": "charcoal",
        "with": "organic_matter"
      },
      {
        "toCreate": "cookie",
        "with": "cookie_dough"
      },
      {
        "toCreate": "dragon",
        "with": "lizard"
      },
      {
        "toCreate": "energy",
        "with": "atmosphere"
      },
      {
        "toCreate": "energy",
        "with": "science"
      },
      {
        "toCreate": "energy",
        "with": "fire"
      },
      {
        "toCreate": "explosion",
        "with": "gunpowder"
      },
      {
        "toCreate": "explosion",
        "with": "petroleum"
      },
      {
        "toCreate": "fire",
        "with": "alcohol"
      },
      {
        "toCreate": "fire",
        "with": "coal"
      },
      {
        "toCreate": "fire_extinguisher",
        "with": "carbon_dioxide"
      },
      {
        "toCreate": "firefighter",
        "with": "human"
      },
      {
        "toCreate": "firetruck",
        "with": "car"
      },
      {
        "toCreate": "firetruck",
        "with": "wagon"
      },
      {
        "toCreate": "flamethrower",
        "with": "gun"
      },
      {
        "toCreate": "french_fries",
        "with": "potato"
      },
      {
        "toCreate": "glass",
        "with": "sand"
      },
      {
        "toCreate": "gunpowder",
        "with": "dust"
      },
      {
        "toCreate": "heat",
        "with": "idea"
      },
      {
        "toCreate": "heat",
        "with": "science"
      },
      {
        "toCreate": "jack-o-lantern",
        "with": "pumpkin"
      },
      {
        "toCreate": "lava",
        "with": "earth"
      },
      {
        "toCreate": "lava_lamp",
        "with": "lamp"
      },
      {
        "toCreate": "metal",
        "with": "ore"
      },
      {
        "toCreate": "omelette",
        "with": "egg"
      },
      {
        "toCreate": "phoenix",
        "with": "bird"
      },
      {
        "toCreate": "phoenix",
        "with": "death"
      },
      {
        "toCreate": "phoenix",
        "with": "life"
      },
      {
        "toCreate": "phoenix",
        "with": "egg"
      },
      {
        "toCreate": "rain",
        "with": "idea"
      },
      {
        "toCreate": "rain",
        "with": "science"
      },
      {
        "toCreate": "salt",
        "with": "ocean"
      },
      {
        "toCreate": "salt",
        "with": "sea"
      },
      {
        "toCreate": "smoke",
        "with": "plant"
      },
      {
        "toCreate": "smoke",
        "with": "grass"
      },
      {
        "toCreate": "smoke",
        "with": "tree"
      },
      {
        "toCreate": "smoke",
        "with": "wood"
      },
      {
        "toCreate": "smoke",
        "with": "air"
      },
      {
        "toCreate": "steak",
        "with": "cow"
      },
      {
        "toCreate": "steak",
        "with": "meat"
      },
      {
        "toCreate": "steam",
        "with": "water"
      },
      {
        "toCreate": "sugar",
        "with": "juice"
      },
      {
        "toCreate": "sugar",
        "with": "fruit"
      },
      {
        "toCreate": "sugar",
        "with": "alcohol"
      },
      {
        "toCreate": "sugar",
        "with": "beer"
      },
      {
        "toCreate": "sugar",
        "with": "wine"
      },
      {
        "toCreate": "sun",
        "with": "sky"
      },
      {
        "toCreate": "sun",
        "with": "planet"
      },
      {
        "toCreate": "toast",
        "with": "bread"
      },
      {
        "toCreate": "toast",
        "with": "sandwich"
      },
      {
        "toCreate": "tobacco",
        "with": "plant"
      },
      {
        "toCreate": "volcano",
        "with": "mountain"
      },
      {
        "toCreate": "volcano",
        "with": "hill"
      }
    ],
    "name": "Fire"
  },
  "fire_extinguisher": {
    "combine": [
      {
        "toCreate": "firefighter",
        "with": "human"
      },
      {
        "toCreate": "firetruck",
        "with": "car"
      },
      {
        "toCreate": "firetruck",
        "with": "wagon"
      }
    ],
    "name": "Fire Extinguisher"
  },
  "firefighter": {
    "combine": [
      {
        "toCreate": "cyborg",
        "with": "robot"
      },
      {
        "toCreate": "firestation",
        "with": "house"
      },
      {
        "toCreate": "firestation",
        "with": "land"
      },
      {
        "toCreate": "firestation",
        "with": "village"
      },
      {
        "toCreate": "firestation",
        "with": "city"
      },
      {
        "toCreate": "firetruck",
        "with": "car"
      },
      {
        "toCreate": "firetruck",
        "with": "wagon"
      },
      {
        "toCreate": "idea",
        "with": "firefighter"
      },
      {
        "toCreate": "statue",
        "with": "medusa"
      }
    ],
    "name": "Firefighter"
  },
  "fireplace": {
    "combine": [
      {
        "toCreate": "cauldron",
        "with": "witch"
      },
      {
        "toCreate": "chimney",
        "with": "house"
      },
      {
        "toCreate": "chimney",
        "with": "brick"
      },
      {
        "toCreate": "chimney",
        "with": "smoke"
      },
      {
        "toCreate": "chimney",
        "with": "stone"
      },
      {
        "toCreate": "christmas_stocking",
        "with": "wool"
      },
      {
        "toCreate": "gift",
        "with": "santa"
      },
      {
        "toCreate": "gift",
        "with": "wrapping_paper"
      },
      {
        "toCreate": "house",
        "with": "container"
      }
    ],
    "name": "Fireplace"
  },
  "firestation": {
    "combine": [],
    "name": "Firestation"
  },
  "firetruck": {
    "combine": [
      {
        "toCreate": "firefighter",
        "with": "human"
      },
      {
        "toCreate": "firestation",
        "with": "house"
      }
    ],
    "name": "Firetruck"
  },
  "fireworks": {
    "combine": [],
    "name": "Fireworks"
  },
  "fish": {
    "combine": [
      {
        "toCreate": "aquarium",
        "with": "glass"
      },
      {
        "toCreate": "aquarium",
        "with": "container"
      },
      {
        "toCreate": "carbon_dioxide",
        "with": "oxygen"
      },
      {
        "toCreate": "egg",
        "with": "fish"
      },
      {
        "toCreate": "electric_eel",
        "with": "electricity"
      },
      {
        "toCreate": "fishing_rod",
        "with": "tool"
      },
      {
        "toCreate": "fishing_rod",
        "with": "wood"
      },
      {
        "toCreate": "fishing_rod",
        "with": "wire"
      },
      {
        "toCreate": "fishing_rod",
        "with": "thread"
      },
      {
        "toCreate": "flying_fish",
        "with": "bird"
      },
      {
        "toCreate": "flying_fish",
        "with": "sky"
      },
      {
        "toCreate": "flying_fish",
        "with": "air"
      },
      {
        "toCreate": "manatee",
        "with": "cow"
      },
      {
        "toCreate": "meat",
        "with": "tool"
      },
      {
        "toCreate": "meat",
        "with": "axe"
      },
      {
        "toCreate": "meat",
        "with": "butcher"
      },
      {
        "toCreate": "meat",
        "with": "net"
      },
      {
        "toCreate": "mermaid",
        "with": "human"
      },
      {
        "toCreate": "mermaid",
        "with": "legend"
      },
      {
        "toCreate": "mermaid",
        "with": "magic"
      },
      {
        "toCreate": "mermaid",
        "with": "swimmer"
      },
      {
        "toCreate": "narwhal",
        "with": "unicorn"
      },
      {
        "toCreate": "net",
        "with": "rope"
      },
      {
        "toCreate": "piranha",
        "with": "blood"
      },
      {
        "toCreate": "piranha",
        "with": "wolf"
      },
      {
        "toCreate": "roe",
        "with": "egg"
      },
      {
        "toCreate": "seahorse",
        "with": "horse"
      },
      {
        "toCreate": "shark",
        "with": "blood"
      },
      {
        "toCreate": "shark",
        "with": "wolf"
      },
      {
        "toCreate": "starfish",
        "with": "star"
      },
      {
        "toCreate": "sushi",
        "with": "seaweed"
      },
      {
        "toCreate": "swordfish",
        "with": "sword"
      },
      {
        "toCreate": "swordfish",
        "with": "blade"
      }
    ],
    "name": "Fish"
  },
  "fishing_rod": {
    "combine": [
      {
        "toCreate": "angler",
        "with": "human"
      },
      {
        "toCreate": "angler",
        "with": "sailor"
      },
      {
        "toCreate": "net",
        "with": "rope"
      }
    ],
    "name": "Fishing Rod"
  },
  "flamethrower": {
    "combine": [
      {
        "toCreate": "campfire",
        "with": "wood"
      }
    ],
    "name": "Flamethrower"
  },
  "flashlight": {
    "combine": [
      {
        "toCreate": "candle",
        "with": "wax"
      },
      {
        "toCreate": "light",
        "with": "electricity"
      },
      {
        "toCreate": "moth",
        "with": "butterfly"
      }
    ],
    "name": "Flashlight"
  },
  "flood": {
    "combine": [],
    "name": "Flood"
  },
  "flour": {
    "combine": [
      {
        "toCreate": "batter",
        "with": "milk"
      },
      {
        "toCreate": "batter",
        "with": "coconut_milk"
      },
      {
        "toCreate": "dough",
        "with": "water"
      },
      {
        "toCreate": "dough",
        "with": "puddle"
      },
      {
        "toCreate": "dough",
        "with": "rain"
      },
      {
        "toCreate": "pasta",
        "with": "egg"
      },
      {
        "toCreate": "recipe",
        "with": "paper"
      },
      {
        "toCreate": "recipe",
        "with": "newspaper"
      },
      {
        "toCreate": "sack",
        "with": "container"
      },
      {
        "toCreate": "windmill",
        "with": "wind"
      }
    ],
    "name": "Flour"
  },
  "flower": {
    "combine": [
      {
        "toCreate": "bee",
        "with": "animal"
      },
      {
        "toCreate": "butterfly",
        "with": "animal"
      },
      {
        "toCreate": "catnip",
        "with": "cat"
      },
      {
        "toCreate": "fruit",
        "with": "time"
      },
      {
        "toCreate": "fruit",
        "with": "water"
      },
      {
        "toCreate": "fruit",
        "with": "rain"
      },
      {
        "toCreate": "fruit",
        "with": "tree"
      },
      {
        "toCreate": "garden",
        "with": "flower"
      },
      {
        "toCreate": "garden",
        "with": "grass"
      },
      {
        "toCreate": "garden",
        "with": "plant"
      },
      {
        "toCreate": "garden",
        "with": "house"
      },
      {
        "toCreate": "garden",
        "with": "lawn"
      },
      {
        "toCreate": "garden",
        "with": "container"
      },
      {
        "toCreate": "honey",
        "with": "bee"
      },
      {
        "toCreate": "hummingbird",
        "with": "bird"
      },
      {
        "toCreate": "hummingbird",
        "with": "owl"
      },
      {
        "toCreate": "hummingbird",
        "with": "pigeon"
      },
      {
        "toCreate": "hummingbird",
        "with": "seagull"
      },
      {
        "toCreate": "leaf",
        "with": "wind"
      },
      {
        "toCreate": "perfume",
        "with": "water"
      },
      {
        "toCreate": "perfume",
        "with": "steam"
      },
      {
        "toCreate": "perfume",
        "with": "alcohol"
      },
      {
        "toCreate": "pollen",
        "with": "dust"
      },
      {
        "toCreate": "pollen",
        "with": "wind"
      },
      {
        "toCreate": "rose",
        "with": "love"
      },
      {
        "toCreate": "rose",
        "with": "blade"
      },
      {
        "toCreate": "seed",
        "with": "time"
      },
      {
        "toCreate": "sunflower",
        "with": "sun"
      },
      {
        "toCreate": "vase",
        "with": "pottery"
      },
      {
        "toCreate": "vase",
        "with": "container"
      },
      {
        "toCreate": "vase",
        "with": "bottle"
      },
      {
        "toCreate": "water_lily",
        "with": "pond"
      },
      {
        "toCreate": "water_lily",
        "with": "puddle"
      },
      {
        "toCreate": "water_lily",
        "with": "lake"
      },
      {
        "toCreate": "water_lily",
        "with": "stream"
      }
    ],
    "name": "Flower"
  },
  "flute": {
    "combine": [
      {
        "toCreate": "music",
        "with": "musician"
      },
      {
        "toCreate": "musician",
        "with": "human"
      },
      {
        "toCreate": "pan_flute",
        "with": "flute"
      }
    ],
    "name": "Flute"
  },
  "flying_fish": {
    "combine": [
      {
        "toCreate": "egg",
        "with": "flying_fish"
      },
      {
        "toCreate": "meat",
        "with": "tool"
      },
      {
        "toCreate": "meat",
        "with": "axe"
      },
      {
        "toCreate": "meat",
        "with": "butcher"
      },
      {
        "toCreate": "meat",
        "with": "net"
      },
      {
        "toCreate": "narwhal",
        "with": "unicorn"
      },
      {
        "toCreate": "net",
        "with": "rope"
      },
      {
        "toCreate": "roe",
        "with": "egg"
      }
    ],
    "name": "Flying Fish"
  },
  "flying_squirrel": {
    "combine": [],
    "name": "Flying Squirrel"
  },
  "fog": {
    "combine": [
      {
        "toCreate": "dew",
        "with": "grass"
      },
      {
        "toCreate": "dew",
        "with": "plant"
      },
      {
        "toCreate": "dew",
        "with": "tree"
      },
      {
        "toCreate": "dew",
        "with": "dawn"
      },
      {
        "toCreate": "smog",
        "with": "smoke"
      },
      {
        "toCreate": "smog",
        "with": "city"
      }
    ],
    "name": "Fog"
  },
  "force_knight": {
    "combine": [
      {
        "toCreate": "light_sword",
        "with": "sword"
      }
    ],
    "name": "Force Knight"
  },
  "forest": {
    "combine": [
      {
        "toCreate": "animal",
        "with": "life"
      },
      {
        "toCreate": "beehive",
        "with": "bee"
      },
      {
        "toCreate": "fog",
        "with": "cloud"
      },
      {
        "toCreate": "grave",
        "with": "corpse"
      },
      {
        "toCreate": "grave",
        "with": "coffin"
      },
      {
        "toCreate": "leaf",
        "with": "wind"
      },
      {
        "toCreate": "orchard",
        "with": "fruit_tree"
      },
      {
        "toCreate": "park",
        "with": "city"
      },
      {
        "toCreate": "park",
        "with": "village"
      },
      {
        "toCreate": "rainforest",
        "with": "rain"
      },
      {
        "toCreate": "vegetable",
        "with": "farmer"
      },
      {
        "toCreate": "wild_boar",
        "with": "pig"
      },
      {
        "toCreate": "wolf",
        "with": "dog"
      },
      {
        "toCreate": "wood",
        "with": "tool"
      },
      {
        "toCreate": "wood",
        "with": "axe"
      },
      {
        "toCreate": "wood",
        "with": "chainsaw"
      },
      {
        "toCreate": "wood",
        "with": "lumberjack"
      },
      {
        "toCreate": "woodpecker",
        "with": "bird"
      }
    ],
    "name": "Forest"
  },
  "fortune_cookie": {
    "combine": [],
    "name": "Fortune Cookie"
  },
  "fossil": {
    "combine": [
      {
        "toCreate": "coral",
        "with": "ocean"
      },
      {
        "toCreate": "coral",
        "with": "sea"
      },
      {
        "toCreate": "paleontologist",
        "with": "human"
      },
      {
        "toCreate": "paleontologist",
        "with": "science"
      },
      {
        "toCreate": "petroleum",
        "with": "pressure"
      },
      {
        "toCreate": "petroleum",
        "with": "time"
      },
      {
        "toCreate": "petroleum",
        "with": "liquid"
      },
      {
        "toCreate": "petroleum",
        "with": "water"
      }
    ],
    "name": "Fossil"
  },
  "fountain": {
    "combine": [],
    "name": "Fountain"
  },
  "fox": {
    "combine": [
      {
        "toCreate": "cage",
        "with": "metal"
      },
      {
        "toCreate": "cage",
        "with": "steel"
      },
      {
        "toCreate": "cage",
        "with": "wall"
      },
      {
        "toCreate": "cave",
        "with": "house"
      },
      {
        "toCreate": "cave",
        "with": "container"
      }
    ],
    "name": "Fox"
  },
  "frankenstein’s_monster": {
    "combine": [],
    "name": "Frankenstein’s Monster"
  },
  "french_fries": {
    "combine": [],
    "name": "French Fries"
  },
  "fridge": {
    "combine": [],
    "name": "Fridge"
  },
  "frog": {
    "combine": [
      {
        "toCreate": "egg",
        "with": "frog"
      },
      {
        "toCreate": "meat",
        "with": "tool"
      },
      {
        "toCreate": "meat",
        "with": "axe"
      },
      {
        "toCreate": "meat",
        "with": "butcher"
      }
    ],
    "name": "Frog"
  },
  "frozen_yogurt": {
    "combine": [],
    "name": "Frozen Yogurt"
  },
  "fruit": {
    "combine": [
      {
        "toCreate": "alcohol",
        "with": "wheat"
      },
      {
        "toCreate": "alcohol",
        "with": "sun"
      },
      {
        "toCreate": "banana",
        "with": "monkey"
      },
      {
        "toCreate": "coconut",
        "with": "palm"
      },
      {
        "toCreate": "coconut",
        "with": "beach"
      },
      {
        "toCreate": "cook",
        "with": "human"
      },
      {
        "toCreate": "fruit_tree",
        "with": "tree"
      },
      {
        "toCreate": "fruit_tree",
        "with": "wood"
      },
      {
        "toCreate": "fruit_tree",
        "with": "plant"
      },
      {
        "toCreate": "jam",
        "with": "machine"
      },
      {
        "toCreate": "jam",
        "with": "juice"
      },
      {
        "toCreate": "jam",
        "with": "heat"
      },
      {
        "toCreate": "juice",
        "with": "pressure"
      },
      {
        "toCreate": "juice",
        "with": "earth"
      },
      {
        "toCreate": "juice",
        "with": "stone"
      },
      {
        "toCreate": "juice",
        "with": "rock"
      },
      {
        "toCreate": "juice",
        "with": "explosion"
      },
      {
        "toCreate": "juice",
        "with": "water"
      },
      {
        "toCreate": "mold",
        "with": "bacteria"
      },
      {
        "toCreate": "mold",
        "with": "time"
      },
      {
        "toCreate": "pie",
        "with": "dough"
      },
      {
        "toCreate": "pie",
        "with": "cookie_dough"
      },
      {
        "toCreate": "pie",
        "with": "baker"
      },
      {
        "toCreate": "pie",
        "with": "bakery"
      },
      {
        "toCreate": "recipe",
        "with": "paper"
      },
      {
        "toCreate": "recipe",
        "with": "newspaper"
      },
      {
        "toCreate": "smoothie",
        "with": "blender"
      },
      {
        "toCreate": "smoothie",
        "with": "ice"
      },
      {
        "toCreate": "smoothie",
        "with": "cold"
      },
      {
        "toCreate": "sugar",
        "with": "energy"
      },
      {
        "toCreate": "sugar",
        "with": "fire"
      },
      {
        "toCreate": "wine",
        "with": "alcohol"
      }
    ],
    "name": "Fruit"
  },
  "fruit_tree": {
    "combine": [
      {
        "toCreate": "orchard",
        "with": "fruit_tree"
      },
      {
        "toCreate": "orchard",
        "with": "field"
      },
      {
        "toCreate": "orchard",
        "with": "tree"
      },
      {
        "toCreate": "orchard",
        "with": "farmer"
      },
      {
        "toCreate": "orchard",
        "with": "farm"
      },
      {
        "toCreate": "orchard",
        "with": "forest"
      },
      {
        "toCreate": "orchard",
        "with": "container"
      }
    ],
    "name": "Fruit Tree"
  },
  "galaxy": {
    "combine": [
      {
        "toCreate": "alien",
        "with": "life"
      },
      {
        "toCreate": "big",
        "with": "philosophy"
      },
      {
        "toCreate": "galaxy_cluster",
        "with": "galaxy"
      },
      {
        "toCreate": "galaxy_cluster",
        "with": "container"
      },
      {
        "toCreate": "supernova",
        "with": "explosion"
      },
      {
        "toCreate": "telescope",
        "with": "glass"
      }
    ],
    "name": "Galaxy"
  },
  "galaxy_cluster": {
    "combine": [
      {
        "toCreate": "alien",
        "with": "life"
      },
      {
        "toCreate": "big",
        "with": "philosophy"
      },
      {
        "toCreate": "telescope",
        "with": "glass"
      },
      {
        "toCreate": "universe",
        "with": "galaxy_cluster"
      },
      {
        "toCreate": "universe",
        "with": "container"
      }
    ],
    "name": "Galaxy Cluster"
  },
  "garage": {
    "combine": [
      {
        "toCreate": "trainyard",
        "with": "train"
      }
    ],
    "name": "Garage"
  },
  "garden": {
    "combine": [
      {
        "toCreate": "bbq",
        "with": "campfire"
      },
      {
        "toCreate": "bee",
        "with": "animal"
      },
      {
        "toCreate": "butterfly",
        "with": "animal"
      },
      {
        "toCreate": "carrot",
        "with": "vegetable"
      },
      {
        "toCreate": "fence",
        "with": "wall"
      },
      {
        "toCreate": "fence",
        "with": "wood"
      },
      {
        "toCreate": "flower",
        "with": "plant"
      },
      {
        "toCreate": "flower",
        "with": "grass"
      },
      {
        "toCreate": "flower",
        "with": "seed"
      },
      {
        "toCreate": "gardener",
        "with": "human"
      },
      {
        "toCreate": "gardener",
        "with": "farmer"
      },
      {
        "toCreate": "gnome",
        "with": "statue"
      },
      {
        "toCreate": "gnome",
        "with": "story"
      },
      {
        "toCreate": "gnome",
        "with": "fairy_tale"
      },
      {
        "toCreate": "gnome",
        "with": "legend"
      },
      {
        "toCreate": "greenhouse",
        "with": "glass"
      },
      {
        "toCreate": "hedge",
        "with": "fence"
      },
      {
        "toCreate": "hedge",
        "with": "wall"
      },
      {
        "toCreate": "hummingbird",
        "with": "bird"
      },
      {
        "toCreate": "hummingbird",
        "with": "owl"
      },
      {
        "toCreate": "hummingbird",
        "with": "pigeon"
      },
      {
        "toCreate": "hummingbird",
        "with": "seagull"
      },
      {
        "toCreate": "park",
        "with": "city"
      },
      {
        "toCreate": "park",
        "with": "village"
      },
      {
        "toCreate": "picnic",
        "with": "sandwich"
      }
    ],
    "name": "Garden"
  },
  "gardener": {
    "combine": [
      {
        "toCreate": "idea",
        "with": "gardener"
      },
      {
        "toCreate": "shovel",
        "with": "tool"
      }
    ],
    "name": "Gardener"
  },
  "gas": {
    "combine": [
      {
        "toCreate": "jupiter",
        "with": "planet"
      },
      {
        "toCreate": "smoke",
        "with": "earth"
      },
      {
        "toCreate": "steam",
        "with": "water"
      }
    ],
    "name": "Gas"
  },
  "geyser": {
    "combine": [
      {
        "toCreate": "pressure",
        "with": "science"
      }
    ],
    "name": "Geyser"
  },
  "ghost": {
    "combine": [
      {
        "toCreate": "jack-o-lantern",
        "with": "pumpkin"
      },
      {
        "toCreate": "jack-o-lantern",
        "with": "vegetable"
      }
    ],
    "name": "Ghost"
  },
  "gift": {
    "combine": [
      {
        "toCreate": "christmas_tree",
        "with": "tree"
      },
      {
        "toCreate": "wrapping_paper",
        "with": "paper"
      }
    ],
    "name": "Gift"
  },
  "gingerbread_house": {
    "combine": [],
    "name": "Gingerbread House"
  },
  "gingerbread_man": {
    "combine": [
      {
        "toCreate": "fortune_cookie",
        "with": "paper"
      },
      {
        "toCreate": "gingerbread_house",
        "with": "house"
      }
    ],
    "name": "Gingerbread Man"
  },
  "glacier": {
    "combine": [
      {
        "toCreate": "avalanche",
        "with": "wave"
      },
      {
        "toCreate": "avalanche",
        "with": "earthquake"
      },
      {
        "toCreate": "avalanche",
        "with": "sound"
      },
      {
        "toCreate": "avalanche",
        "with": "gun"
      },
      {
        "toCreate": "husky",
        "with": "dog"
      },
      {
        "toCreate": "ski_goggles",
        "with": "glasses"
      },
      {
        "toCreate": "ski_goggles",
        "with": "sunglasses"
      },
      {
        "toCreate": "snowmobile",
        "with": "motorcycle"
      },
      {
        "toCreate": "snowmobile",
        "with": "car"
      },
      {
        "toCreate": "yeti",
        "with": "story"
      },
      {
        "toCreate": "yeti",
        "with": "legend"
      }
    ],
    "name": "Glacier"
  },
  "glass": {
    "combine": [
      {
        "toCreate": "ant_farm",
        "with": "ant"
      },
      {
        "toCreate": "aquarium",
        "with": "water"
      },
      {
        "toCreate": "aquarium",
        "with": "fish"
      },
      {
        "toCreate": "aquarium",
        "with": "puddle"
      },
      {
        "toCreate": "aquarium",
        "with": "pond"
      },
      {
        "toCreate": "blender",
        "with": "blade"
      },
      {
        "toCreate": "crystal_ball",
        "with": "witch"
      },
      {
        "toCreate": "crystal_ball",
        "with": "wizard"
      },
      {
        "toCreate": "crystal_ball",
        "with": "magic"
      },
      {
        "toCreate": "glasses",
        "with": "metal"
      },
      {
        "toCreate": "glasses",
        "with": "steel"
      },
      {
        "toCreate": "glasses",
        "with": "glass"
      },
      {
        "toCreate": "glasses",
        "with": "human"
      },
      {
        "toCreate": "greenhouse",
        "with": "plant"
      },
      {
        "toCreate": "greenhouse",
        "with": "grass"
      },
      {
        "toCreate": "greenhouse",
        "with": "tree"
      },
      {
        "toCreate": "greenhouse",
        "with": "garden"
      },
      {
        "toCreate": "hourglass",
        "with": "sand"
      },
      {
        "toCreate": "hourglass",
        "with": "time"
      },
      {
        "toCreate": "jar",
        "with": "jam"
      },
      {
        "toCreate": "lens",
        "with": "tool"
      },
      {
        "toCreate": "lens",
        "with": "engineer"
      },
      {
        "toCreate": "light_bulb",
        "with": "electricity"
      },
      {
        "toCreate": "light_bulb",
        "with": "light"
      },
      {
        "toCreate": "microscope",
        "with": "bacteria"
      },
      {
        "toCreate": "mirror",
        "with": "metal"
      },
      {
        "toCreate": "mirror",
        "with": "steel"
      },
      {
        "toCreate": "mirror",
        "with": "wood"
      },
      {
        "toCreate": "obsidian",
        "with": "lava"
      },
      {
        "toCreate": "prism",
        "with": "rainbow"
      },
      {
        "toCreate": "prism",
        "with": "double_rainbow!"
      },
      {
        "toCreate": "snow_globe",
        "with": "snow"
      },
      {
        "toCreate": "snow_globe",
        "with": "snowman"
      },
      {
        "toCreate": "snow_globe",
        "with": "snowball"
      },
      {
        "toCreate": "snow_globe",
        "with": "snowmobile"
      },
      {
        "toCreate": "snow_globe",
        "with": "santa"
      },
      {
        "toCreate": "snow_globe",
        "with": "christmas_tree"
      },
      {
        "toCreate": "snow_globe",
        "with": "christmas_stocking"
      },
      {
        "toCreate": "snow_globe",
        "with": "blizzard"
      },
      {
        "toCreate": "telescope",
        "with": "sky"
      },
      {
        "toCreate": "telescope",
        "with": "star"
      },
      {
        "toCreate": "telescope",
        "with": "space"
      },
      {
        "toCreate": "telescope",
        "with": "moon"
      },
      {
        "toCreate": "telescope",
        "with": "planet"
      },
      {
        "toCreate": "telescope",
        "with": "mercury"
      },
      {
        "toCreate": "telescope",
        "with": "mars"
      },
      {
        "toCreate": "telescope",
        "with": "jupiter"
      },
      {
        "toCreate": "telescope",
        "with": "saturn"
      },
      {
        "toCreate": "telescope",
        "with": "galaxy"
      },
      {
        "toCreate": "telescope",
        "with": "galaxy_cluster"
      },
      {
        "toCreate": "telescope",
        "with": "universe"
      },
      {
        "toCreate": "telescope",
        "with": "supernova"
      },
      {
        "toCreate": "thermometer",
        "with": "quicksilver"
      }
    ],
    "name": "Glass"
  },
  "glasses": {
    "combine": [
      {
        "toCreate": "hacker",
        "with": "human"
      },
      {
        "toCreate": "microscope",
        "with": "bacteria"
      },
      {
        "toCreate": "safety_glasses",
        "with": "tool"
      },
      {
        "toCreate": "safety_glasses",
        "with": "explosion"
      },
      {
        "toCreate": "safety_glasses",
        "with": "engineer"
      },
      {
        "toCreate": "safety_glasses",
        "with": "bulletproof_vest"
      },
      {
        "toCreate": "safety_glasses",
        "with": "armor"
      },
      {
        "toCreate": "ski_goggles",
        "with": "snow"
      },
      {
        "toCreate": "ski_goggles",
        "with": "cold"
      },
      {
        "toCreate": "ski_goggles",
        "with": "glacier"
      },
      {
        "toCreate": "ski_goggles",
        "with": "skier"
      },
      {
        "toCreate": "sunglasses",
        "with": "sun"
      },
      {
        "toCreate": "sunglasses",
        "with": "beach"
      },
      {
        "toCreate": "sunglasses",
        "with": "day"
      },
      {
        "toCreate": "sunglasses",
        "with": "sky"
      },
      {
        "toCreate": "sunglasses",
        "with": "light"
      },
      {
        "toCreate": "swim_goggles",
        "with": "water"
      },
      {
        "toCreate": "swim_goggles",
        "with": "lake"
      },
      {
        "toCreate": "swim_goggles",
        "with": "sea"
      },
      {
        "toCreate": "swim_goggles",
        "with": "ocean"
      },
      {
        "toCreate": "swim_goggles",
        "with": "river"
      }
    ],
    "name": "Glasses"
  },
  "gnome": {
    "combine": [],
    "name": "Gnome"
  },
  "goat": {
    "combine": [
      {
        "toCreate": "barn",
        "with": "house"
      },
      {
        "toCreate": "barn",
        "with": "container"
      },
      {
        "toCreate": "faun",
        "with": "human"
      },
      {
        "toCreate": "faun",
        "with": "legend"
      },
      {
        "toCreate": "faun",
        "with": "wizard"
      },
      {
        "toCreate": "faun",
        "with": "magic"
      },
      {
        "toCreate": "lasso",
        "with": "rope"
      },
      {
        "toCreate": "milk",
        "with": "farmer"
      },
      {
        "toCreate": "milk",
        "with": "tool"
      },
      {
        "toCreate": "milk",
        "with": "water"
      },
      {
        "toCreate": "milk",
        "with": "liquid"
      },
      {
        "toCreate": "mountain_goat",
        "with": "mountain"
      },
      {
        "toCreate": "mountain_goat",
        "with": "mountain_range"
      },
      {
        "toCreate": "sheep",
        "with": "domestication"
      }
    ],
    "name": "Goat"
  },
  "gold": {
    "combine": [
      {
        "toCreate": "alchemist",
        "with": "human"
      },
      {
        "toCreate": "alchemist",
        "with": "philosophy"
      },
      {
        "toCreate": "bank",
        "with": "house"
      },
      {
        "toCreate": "bank",
        "with": "skyscraper"
      },
      {
        "toCreate": "bank",
        "with": "city"
      },
      {
        "toCreate": "money",
        "with": "paper"
      },
      {
        "toCreate": "piggy_bank",
        "with": "pig"
      },
      {
        "toCreate": "ring",
        "with": "diamond"
      },
      {
        "toCreate": "ring",
        "with": "love"
      },
      {
        "toCreate": "ring",
        "with": "love"
      },
      {
        "toCreate": "safe",
        "with": "metal"
      },
      {
        "toCreate": "safe",
        "with": "steel"
      },
      {
        "toCreate": "safe",
        "with": "container"
      }
    ],
    "name": "Gold"
  },
  "golem": {
    "combine": [
      {
        "toCreate": "pinocchio",
        "with": "story"
      },
      {
        "toCreate": "robot",
        "with": "metal"
      },
      {
        "toCreate": "robot",
        "with": "steel"
      },
      {
        "toCreate": "robot",
        "with": "armor"
      },
      {
        "toCreate": "scarecrow",
        "with": "hay"
      }
    ],
    "name": "Golem"
  },
  "granite": {
    "combine": [],
    "name": "Granite"
  },
  "grass": {
    "combine": [
      {
        "toCreate": "algae",
        "with": "pond"
      },
      {
        "toCreate": "algae",
        "with": "lake"
      },
      {
        "toCreate": "algae",
        "with": "water"
      },
      {
        "toCreate": "ant",
        "with": "animal"
      },
      {
        "toCreate": "ant",
        "with": "spider"
      },
      {
        "toCreate": "ash",
        "with": "fire"
      },
      {
        "toCreate": "carbon_dioxide",
        "with": "night"
      },
      {
        "toCreate": "carrot",
        "with": "vegetable"
      },
      {
        "toCreate": "catnip",
        "with": "cat"
      },
      {
        "toCreate": "cow",
        "with": "livestock"
      },
      {
        "toCreate": "dew",
        "with": "fog"
      },
      {
        "toCreate": "dew",
        "with": "water"
      },
      {
        "toCreate": "dew",
        "with": "dawn"
      },
      {
        "toCreate": "farmer",
        "with": "human"
      },
      {
        "toCreate": "fence",
        "with": "wall"
      },
      {
        "toCreate": "flower",
        "with": "garden"
      },
      {
        "toCreate": "flower",
        "with": "seed"
      },
      {
        "toCreate": "flower",
        "with": "rainbow"
      },
      {
        "toCreate": "flower",
        "with": "double_rainbow!"
      },
      {
        "toCreate": "garden",
        "with": "plant"
      },
      {
        "toCreate": "garden",
        "with": "flower"
      },
      {
        "toCreate": "greenhouse",
        "with": "glass"
      },
      {
        "toCreate": "greenhouse",
        "with": "aquarium"
      },
      {
        "toCreate": "hay",
        "with": "scythe"
      },
      {
        "toCreate": "hay",
        "with": "farmer"
      },
      {
        "toCreate": "hay",
        "with": "sun"
      },
      {
        "toCreate": "hay",
        "with": "grass"
      },
      {
        "toCreate": "hay",
        "with": "barn"
      },
      {
        "toCreate": "hay",
        "with": "farm"
      },
      {
        "toCreate": "hay",
        "with": "pitchfork"
      },
      {
        "toCreate": "ivy",
        "with": "wall"
      },
      {
        "toCreate": "lawn",
        "with": "container"
      },
      {
        "toCreate": "lawn",
        "with": "house"
      },
      {
        "toCreate": "lawn",
        "with": "lawn_mower"
      },
      {
        "toCreate": "lawn_mower",
        "with": "tool"
      },
      {
        "toCreate": "lawn_mower",
        "with": "machine"
      },
      {
        "toCreate": "moss",
        "with": "stone"
      },
      {
        "toCreate": "moss",
        "with": "rock"
      },
      {
        "toCreate": "moss",
        "with": "boulder"
      },
      {
        "toCreate": "nest",
        "with": "bird"
      },
      {
        "toCreate": "nest",
        "with": "egg"
      },
      {
        "toCreate": "oxygen",
        "with": "sun"
      },
      {
        "toCreate": "oxygen",
        "with": "carbon_dioxide"
      },
      {
        "toCreate": "park",
        "with": "city"
      },
      {
        "toCreate": "park",
        "with": "village"
      },
      {
        "toCreate": "peat",
        "with": "time"
      },
      {
        "toCreate": "picnic",
        "with": "sandwich"
      },
      {
        "toCreate": "plant",
        "with": "big"
      },
      {
        "toCreate": "reed",
        "with": "pond"
      },
      {
        "toCreate": "reed",
        "with": "puddle"
      },
      {
        "toCreate": "reed",
        "with": "swamp"
      },
      {
        "toCreate": "reed",
        "with": "river"
      },
      {
        "toCreate": "scythe",
        "with": "blade"
      },
      {
        "toCreate": "scythe",
        "with": "sword"
      },
      {
        "toCreate": "scythe",
        "with": "axe"
      },
      {
        "toCreate": "seaweed",
        "with": "ocean"
      },
      {
        "toCreate": "seaweed",
        "with": "sea"
      },
      {
        "toCreate": "smoke",
        "with": "fire"
      },
      {
        "toCreate": "swamp",
        "with": "mud"
      },
      {
        "toCreate": "tobacco",
        "with": "smoke"
      },
      {
        "toCreate": "wheat",
        "with": "domestication"
      },
      {
        "toCreate": "wheat",
        "with": "farmer"
      },
      {
        "toCreate": "wheat",
        "with": "field"
      }
    ],
    "name": "Grass"
  },
  "grave": {
    "combine": [
      {
        "toCreate": "death",
        "with": "philosophy"
      },
      {
        "toCreate": "fossil",
        "with": "time"
      },
      {
        "toCreate": "ghost",
        "with": "night"
      },
      {
        "toCreate": "ghost",
        "with": "legend"
      },
      {
        "toCreate": "ghost",
        "with": "story"
      },
      {
        "toCreate": "grave",
        "with": "gravestone"
      },
      {
        "toCreate": "gravestone",
        "with": "stone"
      },
      {
        "toCreate": "gravestone",
        "with": "rock"
      },
      {
        "toCreate": "graveyard",
        "with": "grave"
      },
      {
        "toCreate": "graveyard",
        "with": "field"
      },
      {
        "toCreate": "graveyard",
        "with": "land"
      },
      {
        "toCreate": "pyramid",
        "with": "desert"
      }
    ],
    "name": "Grave"
  },
  "gravestone": {
    "combine": [
      {
        "toCreate": "ghost",
        "with": "night"
      },
      {
        "toCreate": "ghost",
        "with": "legend"
      },
      {
        "toCreate": "ghost",
        "with": "story"
      },
      {
        "toCreate": "grave",
        "with": "grave"
      },
      {
        "toCreate": "grave",
        "with": "coffin"
      },
      {
        "toCreate": "graveyard",
        "with": "gravestone"
      },
      {
        "toCreate": "graveyard",
        "with": "field"
      },
      {
        "toCreate": "graveyard",
        "with": "land"
      },
      {
        "toCreate": "graveyard",
        "with": "container"
      },
      {
        "toCreate": "pyramid",
        "with": "desert"
      }
    ],
    "name": "Gravestone"
  },
  "graveyard": {
    "combine": [
      {
        "toCreate": "death",
        "with": "philosophy"
      },
      {
        "toCreate": "ghost",
        "with": "night"
      },
      {
        "toCreate": "ghost",
        "with": "story"
      },
      {
        "toCreate": "ghost",
        "with": "legend"
      },
      {
        "toCreate": "gravestone",
        "with": "stone"
      },
      {
        "toCreate": "gravestone",
        "with": "rock"
      },
      {
        "toCreate": "pyramid",
        "with": "desert"
      }
    ],
    "name": "Graveyard"
  },
  "greenhouse": {
    "combine": [],
    "name": "Greenhouse"
  },
  "grenade": {
    "combine": [],
    "name": "Grenade"
  },
  "grilled_cheese": {
    "combine": [],
    "name": "Grilled Cheese"
  },
  "grim_reaper": {
    "combine": [
      {
        "toCreate": "corpse",
        "with": "human"
      }
    ],
    "name": "Grim Reaper"
  },
  "gun": {
    "combine": [
      {
        "toCreate": "avalanche",
        "with": "glacier"
      },
      {
        "toCreate": "avalanche",
        "with": "mountain"
      },
      {
        "toCreate": "avalanche",
        "with": "mountain_range"
      },
      {
        "toCreate": "bayonet",
        "with": "blade"
      },
      {
        "toCreate": "bayonet",
        "with": "sword"
      },
      {
        "toCreate": "bayonet",
        "with": "axe"
      },
      {
        "toCreate": "bulletproof_vest",
        "with": "armor"
      },
      {
        "toCreate": "cannon",
        "with": "castle"
      },
      {
        "toCreate": "cannon",
        "with": "pirate_ship"
      },
      {
        "toCreate": "flamethrower",
        "with": "fire"
      },
      {
        "toCreate": "flamethrower",
        "with": "volcano"
      },
      {
        "toCreate": "flamethrower",
        "with": "campfire"
      },
      {
        "toCreate": "pirate",
        "with": "sailor"
      },
      {
        "toCreate": "safe",
        "with": "container"
      },
      {
        "toCreate": "stun_gun",
        "with": "electricity"
      },
      {
        "toCreate": "stun_gun",
        "with": "energy"
      },
      {
        "toCreate": "stun_gun",
        "with": "wire"
      },
      {
        "toCreate": "tank",
        "with": "car"
      },
      {
        "toCreate": "water_gun",
        "with": "water"
      },
      {
        "toCreate": "water_gun",
        "with": "stream"
      },
      {
        "toCreate": "water_gun",
        "with": "puddle"
      }
    ],
    "name": "Gun"
  },
  "gunpowder": {
    "combine": [
      {
        "toCreate": "bullet",
        "with": "metal"
      },
      {
        "toCreate": "bullet",
        "with": "steel"
      },
      {
        "toCreate": "bullet",
        "with": "tool"
      },
      {
        "toCreate": "bullet",
        "with": "container"
      },
      {
        "toCreate": "cannon",
        "with": "pirate_ship"
      },
      {
        "toCreate": "cannon",
        "with": "castle"
      },
      {
        "toCreate": "dynamite",
        "with": "wire"
      },
      {
        "toCreate": "dynamite",
        "with": "pipe"
      },
      {
        "toCreate": "dynamite",
        "with": "container"
      },
      {
        "toCreate": "explosion",
        "with": "fire"
      },
      {
        "toCreate": "explosion",
        "with": "electricity"
      },
      {
        "toCreate": "explosion",
        "with": "lightning"
      },
      {
        "toCreate": "gun",
        "with": "bow"
      }
    ],
    "name": "Gunpowder"
  },
  "gust": {
    "combine": [],
    "name": "Gust"
  },
  "hacker": {
    "combine": [
      {
        "toCreate": "computer",
        "with": "tool"
      },
      {
        "toCreate": "computer",
        "with": "electricity"
      },
      {
        "toCreate": "computer",
        "with": "wire"
      },
      {
        "toCreate": "computer",
        "with": "machine"
      },
      {
        "toCreate": "idea",
        "with": "hacker"
      },
      {
        "toCreate": "statue",
        "with": "medusa"
      }
    ],
    "name": "Hacker"
  },
  "hail": {
    "combine": [],
    "name": "Hail"
  },
  "ham": {
    "combine": [
      {
        "toCreate": "bacon",
        "with": "fire"
      },
      {
        "toCreate": "bacon",
        "with": "campfire"
      },
      {
        "toCreate": "butcher",
        "with": "human"
      },
      {
        "toCreate": "sandwich",
        "with": "bread"
      }
    ],
    "name": "Ham"
  },
  "hamburger": {
    "combine": [
      {
        "toCreate": "cheeseburger",
        "with": "cheese"
      }
    ],
    "name": "Hamburger"
  },
  "hammer": {
    "combine": [
      {
        "toCreate": "bell",
        "with": "metal"
      },
      {
        "toCreate": "bell",
        "with": "steel"
      },
      {
        "toCreate": "coconut_milk",
        "with": "coconut"
      },
      {
        "toCreate": "ore",
        "with": "boulder"
      },
      {
        "toCreate": "ore",
        "with": "earth"
      },
      {
        "toCreate": "ore",
        "with": "rock"
      },
      {
        "toCreate": "ore",
        "with": "stone"
      },
      {
        "toCreate": "ore",
        "with": "hill"
      },
      {
        "toCreate": "ore",
        "with": "mountain"
      },
      {
        "toCreate": "statue",
        "with": "stone"
      },
      {
        "toCreate": "statue",
        "with": "boulder"
      },
      {
        "toCreate": "toolbox",
        "with": "container"
      },
      {
        "toCreate": "toolbox",
        "with": "box"
      }
    ],
    "name": "Hammer"
  },
  "hamster": {
    "combine": [
      {
        "toCreate": "cage",
        "with": "container"
      },
      {
        "toCreate": "cage",
        "with": "house"
      },
      {
        "toCreate": "cage",
        "with": "wall"
      }
    ],
    "name": "Hamster"
  },
  "hangar": {
    "combine": [],
    "name": "Hangar"
  },
  "harp": {
    "combine": [],
    "name": "Harp"
  },
  "hay": {
    "combine": [
      {
        "toCreate": "barn",
        "with": "house"
      },
      {
        "toCreate": "barn",
        "with": "container"
      },
      {
        "toCreate": "broom",
        "with": "wood"
      },
      {
        "toCreate": "hay_bale",
        "with": "hay"
      },
      {
        "toCreate": "hay_bale",
        "with": "tractor"
      },
      {
        "toCreate": "hay_bale",
        "with": "barn"
      },
      {
        "toCreate": "hay_bale",
        "with": "machine"
      },
      {
        "toCreate": "horse",
        "with": "livestock"
      },
      {
        "toCreate": "nest",
        "with": "bird"
      },
      {
        "toCreate": "nest",
        "with": "egg"
      },
      {
        "toCreate": "pitchfork",
        "with": "tool"
      },
      {
        "toCreate": "pitchfork",
        "with": "metal"
      },
      {
        "toCreate": "pitchfork",
        "with": "steel"
      },
      {
        "toCreate": "scarecrow",
        "with": "human"
      },
      {
        "toCreate": "scarecrow",
        "with": "statue"
      },
      {
        "toCreate": "scarecrow",
        "with": "golem"
      },
      {
        "toCreate": "scarecrow",
        "with": "sack"
      }
    ],
    "name": "Hay"
  },
  "hay_bale": {
    "combine": [
      {
        "toCreate": "barn",
        "with": "container"
      }
    ],
    "name": "Hay Bale"
  },
  "heat": {
    "combine": [
      {
        "toCreate": "caramel",
        "with": "sugar"
      },
      {
        "toCreate": "cookie",
        "with": "cookie_dough"
      },
      {
        "toCreate": "current",
        "with": "ocean"
      },
      {
        "toCreate": "current",
        "with": "sea"
      },
      {
        "toCreate": "energy",
        "with": "science"
      },
      {
        "toCreate": "explosion",
        "with": "petroleum"
      },
      {
        "toCreate": "glass",
        "with": "sand"
      },
      {
        "toCreate": "hot_chocolate",
        "with": "chocolate_milk"
      },
      {
        "toCreate": "hot_chocolate",
        "with": "chocolate"
      },
      {
        "toCreate": "jam",
        "with": "fruit"
      },
      {
        "toCreate": "jerky",
        "with": "meat"
      },
      {
        "toCreate": "jerky",
        "with": "steak"
      },
      {
        "toCreate": "katana",
        "with": "blade"
      },
      {
        "toCreate": "lava",
        "with": "earth"
      },
      {
        "toCreate": "maple_syrup",
        "with": "sap"
      },
      {
        "toCreate": "mercury",
        "with": "planet"
      },
      {
        "toCreate": "metal",
        "with": "ore"
      },
      {
        "toCreate": "omelette",
        "with": "egg"
      },
      {
        "toCreate": "plasma",
        "with": "pressure"
      },
      {
        "toCreate": "plasma",
        "with": "heat"
      },
      {
        "toCreate": "rain",
        "with": "cloud"
      },
      {
        "toCreate": "steam",
        "with": "water"
      },
      {
        "toCreate": "tea",
        "with": "leaf"
      },
      {
        "toCreate": "warmth",
        "with": "air"
      },
      {
        "toCreate": "warmth",
        "with": "human"
      },
      {
        "toCreate": "water",
        "with": "ice"
      },
      {
        "toCreate": "water",
        "with": "snow"
      }
    ],
    "name": "Heat"
  },
  "hedge": {
    "combine": [],
    "name": "Hedge"
  },
  "hedgehog": {
    "combine": [],
    "name": "Hedgehog"
  },
  "helicopter": {
    "combine": [
      {
        "toCreate": "drone",
        "with": "robot"
      },
      {
        "toCreate": "flying_squirrel",
        "with": "squirrel"
      },
      {
        "toCreate": "hangar",
        "with": "container"
      },
      {
        "toCreate": "hangar",
        "with": "house"
      },
      {
        "toCreate": "hangar",
        "with": "wall"
      },
      {
        "toCreate": "hangar",
        "with": "barn"
      },
      {
        "toCreate": "lawn_mower",
        "with": "scythe"
      },
      {
        "toCreate": "lawn_mower",
        "with": "field"
      }
    ],
    "name": "Helicopter"
  },
  "hero": {
    "combine": [
      {
        "toCreate": "don_quixote",
        "with": "windmill"
      },
      {
        "toCreate": "knight",
        "with": "warrior"
      },
      {
        "toCreate": "medusa",
        "with": "snake"
      },
      {
        "toCreate": "story",
        "with": "human"
      }
    ],
    "name": "Hero"
  },
  "hill": {
    "combine": [
      {
        "toCreate": "anthill",
        "with": "ant"
      },
      {
        "toCreate": "boulder",
        "with": "small"
      },
      {
        "toCreate": "fog",
        "with": "cloud"
      },
      {
        "toCreate": "geyser",
        "with": "steam"
      },
      {
        "toCreate": "goat",
        "with": "livestock"
      },
      {
        "toCreate": "goat",
        "with": "cow"
      },
      {
        "toCreate": "map",
        "with": "paper"
      },
      {
        "toCreate": "mineral",
        "with": "organic_matter"
      },
      {
        "toCreate": "mountain",
        "with": "big"
      },
      {
        "toCreate": "mountain",
        "with": "earth"
      },
      {
        "toCreate": "mountain",
        "with": "hill"
      },
      {
        "toCreate": "mountain",
        "with": "earthquake"
      },
      {
        "toCreate": "ore",
        "with": "hammer"
      },
      {
        "toCreate": "pyramid",
        "with": "desert"
      },
      {
        "toCreate": "river",
        "with": "rain"
      },
      {
        "toCreate": "sheep",
        "with": "livestock"
      },
      {
        "toCreate": "tunnel",
        "with": "engineer"
      },
      {
        "toCreate": "tunnel",
        "with": "train"
      },
      {
        "toCreate": "tunnel",
        "with": "cave"
      },
      {
        "toCreate": "volcano",
        "with": "lava"
      },
      {
        "toCreate": "volcano",
        "with": "fire"
      },
      {
        "toCreate": "volcano",
        "with": "pressure"
      },
      {
        "toCreate": "waterfall",
        "with": "river"
      },
      {
        "toCreate": "waterfall",
        "with": "lake"
      },
      {
        "toCreate": "wild_boar",
        "with": "pig"
      }
    ],
    "name": "Hill"
  },
  "hippo": {
    "combine": [],
    "name": "Hippo"
  },
  "honey": {
    "combine": [],
    "name": "Honey"
  },
  "horizon": {
    "combine": [],
    "name": "Horizon"
  },
  "horse": {
    "combine": [
      {
        "toCreate": "barn",
        "with": "container"
      },
      {
        "toCreate": "camel",
        "with": "desert"
      },
      {
        "toCreate": "camel",
        "with": "sand"
      },
      {
        "toCreate": "camel",
        "with": "dune"
      },
      {
        "toCreate": "centaur",
        "with": "human"
      },
      {
        "toCreate": "centaur",
        "with": "story"
      },
      {
        "toCreate": "hippo",
        "with": "river"
      },
      {
        "toCreate": "hippo",
        "with": "water"
      },
      {
        "toCreate": "horseshoe",
        "with": "metal"
      },
      {
        "toCreate": "horseshoe",
        "with": "steel"
      },
      {
        "toCreate": "knight",
        "with": "warrior"
      },
      {
        "toCreate": "lasso",
        "with": "rope"
      },
      {
        "toCreate": "origami",
        "with": "paper"
      },
      {
        "toCreate": "pegasus",
        "with": "bird"
      },
      {
        "toCreate": "pegasus",
        "with": "sky"
      },
      {
        "toCreate": "saddle",
        "with": "leather"
      },
      {
        "toCreate": "saddle",
        "with": "tool"
      },
      {
        "toCreate": "saddle",
        "with": "fabric"
      },
      {
        "toCreate": "seahorse",
        "with": "ocean"
      },
      {
        "toCreate": "seahorse",
        "with": "sea"
      },
      {
        "toCreate": "seahorse",
        "with": "water"
      },
      {
        "toCreate": "seahorse",
        "with": "fish"
      },
      {
        "toCreate": "seahorse",
        "with": "lake"
      },
      {
        "toCreate": "trojan_horse",
        "with": "wood"
      },
      {
        "toCreate": "trojan_horse",
        "with": "machine"
      },
      {
        "toCreate": "trojan_horse",
        "with": "statue"
      },
      {
        "toCreate": "unicorn",
        "with": "legend"
      },
      {
        "toCreate": "unicorn",
        "with": "rainbow"
      },
      {
        "toCreate": "unicorn",
        "with": "double_rainbow!"
      },
      {
        "toCreate": "unicorn",
        "with": "story"
      },
      {
        "toCreate": "unicorn",
        "with": "magic"
      },
      {
        "toCreate": "wagon",
        "with": "cart"
      }
    ],
    "name": "Horse"
  },
  "horseshoe": {
    "combine": [
      {
        "toCreate": "camel",
        "with": "desert"
      },
      {
        "toCreate": "horse",
        "with": "livestock"
      },
      {
        "toCreate": "horse",
        "with": "animal"
      }
    ],
    "name": "Horseshoe"
  },
  "hospital": {
    "combine": [
      {
        "toCreate": "ambulance",
        "with": "car"
      },
      {
        "toCreate": "doctor",
        "with": "human"
      },
      {
        "toCreate": "penicillin",
        "with": "mold"
      },
      {
        "toCreate": "ruins",
        "with": "time"
      },
      {
        "toCreate": "scalpel",
        "with": "blade"
      },
      {
        "toCreate": "scalpel",
        "with": "sword"
      },
      {
        "toCreate": "stethoscope",
        "with": "tool"
      }
    ],
    "name": "Hospital"
  },
  "hot_chocolate": {
    "combine": [],
    "name": "Hot Chocolate"
  },
  "hourglass": {
    "combine": [],
    "name": "Hourglass"
  },
  "house": {
    "combine": [
      {
        "toCreate": "anthill",
        "with": "ant"
      },
      {
        "toCreate": "bakery",
        "with": "bread"
      },
      {
        "toCreate": "bakery",
        "with": "baker"
      },
      {
        "toCreate": "bakery",
        "with": "donut"
      },
      {
        "toCreate": "bakery",
        "with": "cake"
      },
      {
        "toCreate": "bank",
        "with": "gold"
      },
      {
        "toCreate": "bank",
        "with": "safe"
      },
      {
        "toCreate": "bank",
        "with": "money"
      },
      {
        "toCreate": "bank",
        "with": "vault"
      },
      {
        "toCreate": "barn",
        "with": "cow"
      },
      {
        "toCreate": "barn",
        "with": "livestock"
      },
      {
        "toCreate": "barn",
        "with": "hay"
      },
      {
        "toCreate": "barn",
        "with": "farm"
      },
      {
        "toCreate": "barn",
        "with": "field"
      },
      {
        "toCreate": "barn",
        "with": "sheep"
      },
      {
        "toCreate": "barn",
        "with": "goat"
      },
      {
        "toCreate": "bbq",
        "with": "campfire"
      },
      {
        "toCreate": "beehive",
        "with": "bee"
      },
      {
        "toCreate": "birdhouse",
        "with": "bird"
      },
      {
        "toCreate": "cage",
        "with": "hamster"
      },
      {
        "toCreate": "castle",
        "with": "knight"
      },
      {
        "toCreate": "castle",
        "with": "warrior"
      },
      {
        "toCreate": "castle",
        "with": "vampire"
      },
      {
        "toCreate": "castle",
        "with": "monarch"
      },
      {
        "toCreate": "cave",
        "with": "wolf"
      },
      {
        "toCreate": "cave",
        "with": "lion"
      },
      {
        "toCreate": "cave",
        "with": "fox"
      },
      {
        "toCreate": "cave",
        "with": "bat"
      },
      {
        "toCreate": "chicken_coop",
        "with": "chicken"
      },
      {
        "toCreate": "chimney",
        "with": "fireplace"
      },
      {
        "toCreate": "chimney",
        "with": "smoke"
      },
      {
        "toCreate": "container",
        "with": "philosophy"
      },
      {
        "toCreate": "dam",
        "with": "beaver"
      },
      {
        "toCreate": "doghouse",
        "with": "dog"
      },
      {
        "toCreate": "doghouse",
        "with": "husky"
      },
      {
        "toCreate": "family",
        "with": "human"
      },
      {
        "toCreate": "farm",
        "with": "farmer"
      },
      {
        "toCreate": "farm",
        "with": "barn"
      },
      {
        "toCreate": "farm",
        "with": "tractor"
      },
      {
        "toCreate": "fireplace",
        "with": "campfire"
      },
      {
        "toCreate": "firestation",
        "with": "firefighter"
      },
      {
        "toCreate": "firestation",
        "with": "firetruck"
      },
      {
        "toCreate": "flood",
        "with": "river"
      },
      {
        "toCreate": "flood",
        "with": "tsunami"
      },
      {
        "toCreate": "garage",
        "with": "car"
      },
      {
        "toCreate": "garage",
        "with": "ambulance"
      },
      {
        "toCreate": "garage",
        "with": "motorcycle"
      },
      {
        "toCreate": "garage",
        "with": "bus"
      },
      {
        "toCreate": "garage",
        "with": "sleigh"
      },
      {
        "toCreate": "garage",
        "with": "electric_car"
      },
      {
        "toCreate": "garage",
        "with": "snowmobile"
      },
      {
        "toCreate": "garage",
        "with": "tractor"
      },
      {
        "toCreate": "garage",
        "with": "rv"
      },
      {
        "toCreate": "garage",
        "with": "ice_cream_truck"
      },
      {
        "toCreate": "garden",
        "with": "plant"
      },
      {
        "toCreate": "garden",
        "with": "flower"
      },
      {
        "toCreate": "gingerbread_house",
        "with": "gingerbread_man"
      },
      {
        "toCreate": "gingerbread_house",
        "with": "dough"
      },
      {
        "toCreate": "gingerbread_house",
        "with": "cookie_dough"
      },
      {
        "toCreate": "gingerbread_house",
        "with": "cookie"
      },
      {
        "toCreate": "gingerbread_house",
        "with": "donut"
      },
      {
        "toCreate": "gingerbread_house",
        "with": "cake"
      },
      {
        "toCreate": "hangar",
        "with": "airplane"
      },
      {
        "toCreate": "hangar",
        "with": "seaplane"
      },
      {
        "toCreate": "hangar",
        "with": "helicopter"
      },
      {
        "toCreate": "hangar",
        "with": "rocket"
      },
      {
        "toCreate": "hangar",
        "with": "spaceship"
      },
      {
        "toCreate": "hospital",
        "with": "sickness"
      },
      {
        "toCreate": "hospital",
        "with": "ambulance"
      },
      {
        "toCreate": "hospital",
        "with": "doctor"
      },
      {
        "toCreate": "igloo",
        "with": "ice"
      },
      {
        "toCreate": "igloo",
        "with": "snow"
      },
      {
        "toCreate": "igloo",
        "with": "cold"
      },
      {
        "toCreate": "igloo",
        "with": "snowman"
      },
      {
        "toCreate": "lawn",
        "with": "grass"
      },
      {
        "toCreate": "library",
        "with": "book"
      },
      {
        "toCreate": "lighthouse",
        "with": "light"
      },
      {
        "toCreate": "lighthouse",
        "with": "spotlight"
      },
      {
        "toCreate": "log_cabin",
        "with": "wood"
      },
      {
        "toCreate": "mouse",
        "with": "cheese"
      },
      {
        "toCreate": "nest",
        "with": "bird"
      },
      {
        "toCreate": "pirate_ship",
        "with": "pirate"
      },
      {
        "toCreate": "post_office",
        "with": "mailman"
      },
      {
        "toCreate": "post_office",
        "with": "letter"
      },
      {
        "toCreate": "ruins",
        "with": "time"
      },
      {
        "toCreate": "rv",
        "with": "car"
      },
      {
        "toCreate": "rv",
        "with": "bus"
      },
      {
        "toCreate": "silo",
        "with": "wheat"
      },
      {
        "toCreate": "skyscraper",
        "with": "sky"
      },
      {
        "toCreate": "skyscraper",
        "with": "big"
      },
      {
        "toCreate": "skyscraper",
        "with": "cloud"
      },
      {
        "toCreate": "space_station",
        "with": "space"
      },
      {
        "toCreate": "space_station",
        "with": "atmosphere"
      },
      {
        "toCreate": "swimming_pool",
        "with": "lake"
      },
      {
        "toCreate": "swimming_pool",
        "with": "swimmer"
      },
      {
        "toCreate": "tent",
        "with": "fabric"
      },
      {
        "toCreate": "trainyard",
        "with": "train"
      },
      {
        "toCreate": "treehouse",
        "with": "tree"
      },
      {
        "toCreate": "village",
        "with": "house"
      },
      {
        "toCreate": "village",
        "with": "container"
      },
      {
        "toCreate": "village",
        "with": "bakery"
      },
      {
        "toCreate": "wagon",
        "with": "cart"
      },
      {
        "toCreate": "windmill",
        "with": "wind"
      }
    ],
    "name": "House"
  },
  "human": {
    "combine": [
      {
        "toCreate": "alchemist",
        "with": "gold"
      },
      {
        "toCreate": "allergy",
        "with": "dust"
      },
      {
        "toCreate": "allergy",
        "with": "pollen"
      },
      {
        "toCreate": "angel",
        "with": "bird"
      },
      {
        "toCreate": "angel",
        "with": "light"
      },
      {
        "toCreate": "angler",
        "with": "fishing_rod"
      },
      {
        "toCreate": "archeologist",
        "with": "ruins"
      },
      {
        "toCreate": "astronaut",
        "with": "space"
      },
      {
        "toCreate": "astronaut",
        "with": "spaceship"
      },
      {
        "toCreate": "astronaut",
        "with": "space_station"
      },
      {
        "toCreate": "astronaut",
        "with": "rocket"
      },
      {
        "toCreate": "astronaut",
        "with": "moon"
      },
      {
        "toCreate": "astronaut",
        "with": "mars"
      },
      {
        "toCreate": "astronaut",
        "with": "solar_system"
      },
      {
        "toCreate": "astronaut",
        "with": "mercury"
      },
      {
        "toCreate": "astronaut",
        "with": "jupiter"
      },
      {
        "toCreate": "astronaut",
        "with": "saturn"
      },
      {
        "toCreate": "astronaut",
        "with": "venus"
      },
      {
        "toCreate": "astronaut",
        "with": "moon_rover"
      },
      {
        "toCreate": "baker",
        "with": "bakery"
      },
      {
        "toCreate": "baker",
        "with": "bread"
      },
      {
        "toCreate": "baker",
        "with": "banana_bread"
      },
      {
        "toCreate": "baker",
        "with": "pizza"
      },
      {
        "toCreate": "baker",
        "with": "toast"
      },
      {
        "toCreate": "baker",
        "with": "batter"
      },
      {
        "toCreate": "baker",
        "with": "pie"
      },
      {
        "toCreate": "baker",
        "with": "donut"
      },
      {
        "toCreate": "baker",
        "with": "cookie"
      },
      {
        "toCreate": "baker",
        "with": "cookie_dough"
      },
      {
        "toCreate": "baker",
        "with": "dough"
      },
      {
        "toCreate": "beekeeper",
        "with": "beehive"
      },
      {
        "toCreate": "beekeeper",
        "with": "bee"
      },
      {
        "toCreate": "blood",
        "with": "blade"
      },
      {
        "toCreate": "butcher",
        "with": "meat"
      },
      {
        "toCreate": "butcher",
        "with": "ham"
      },
      {
        "toCreate": "carbon_dioxide",
        "with": "oxygen"
      },
      {
        "toCreate": "centaur",
        "with": "horse"
      },
      {
        "toCreate": "chill",
        "with": "cold"
      },
      {
        "toCreate": "chill",
        "with": "ice"
      },
      {
        "toCreate": "cold",
        "with": "rain"
      },
      {
        "toCreate": "cold",
        "with": "mountain_range"
      },
      {
        "toCreate": "cold",
        "with": "mountain"
      },
      {
        "toCreate": "cold",
        "with": "snow"
      },
      {
        "toCreate": "cold",
        "with": "wind"
      },
      {
        "toCreate": "cold",
        "with": "storm"
      },
      {
        "toCreate": "cook",
        "with": "fruit"
      },
      {
        "toCreate": "cook",
        "with": "campfire"
      },
      {
        "toCreate": "cook",
        "with": "vegetable"
      },
      {
        "toCreate": "cook",
        "with": "nuts"
      },
      {
        "toCreate": "corpse",
        "with": "bullet"
      },
      {
        "toCreate": "corpse",
        "with": "blade"
      },
      {
        "toCreate": "corpse",
        "with": "death"
      },
      {
        "toCreate": "corpse",
        "with": "grim_reaper"
      },
      {
        "toCreate": "corpse",
        "with": "time"
      },
      {
        "toCreate": "corpse",
        "with": "explosion"
      },
      {
        "toCreate": "corpse",
        "with": "arrow"
      },
      {
        "toCreate": "cyborg",
        "with": "robot"
      },
      {
        "toCreate": "cyclist",
        "with": "bicycle"
      },
      {
        "toCreate": "cyclist",
        "with": "wheel"
      },
      {
        "toCreate": "diver",
        "with": "scuba_tank"
      },
      {
        "toCreate": "doctor",
        "with": "hospital"
      },
      {
        "toCreate": "doctor",
        "with": "stethoscope"
      },
      {
        "toCreate": "domestication",
        "with": "animal"
      },
      {
        "toCreate": "drunk",
        "with": "alcohol"
      },
      {
        "toCreate": "drunk",
        "with": "wine"
      },
      {
        "toCreate": "drunk",
        "with": "beer"
      },
      {
        "toCreate": "electrician",
        "with": "electricity"
      },
      {
        "toCreate": "electrician",
        "with": "wire"
      },
      {
        "toCreate": "engineer",
        "with": "machine"
      },
      {
        "toCreate": "engineer",
        "with": "steam_engine"
      },
      {
        "toCreate": "family",
        "with": "human"
      },
      {
        "toCreate": "family",
        "with": "house"
      },
      {
        "toCreate": "farmer",
        "with": "plant"
      },
      {
        "toCreate": "farmer",
        "with": "grass"
      },
      {
        "toCreate": "farmer",
        "with": "pitchfork"
      },
      {
        "toCreate": "farmer",
        "with": "field"
      },
      {
        "toCreate": "farmer",
        "with": "barn"
      },
      {
        "toCreate": "farmer",
        "with": "plow"
      },
      {
        "toCreate": "faun",
        "with": "goat"
      },
      {
        "toCreate": "faun",
        "with": "mountain_goat"
      },
      {
        "toCreate": "firefighter",
        "with": "fire"
      },
      {
        "toCreate": "firefighter",
        "with": "firetruck"
      },
      {
        "toCreate": "firefighter",
        "with": "fire_extinguisher"
      },
      {
        "toCreate": "flashlight",
        "with": "lamp"
      },
      {
        "toCreate": "force_knight",
        "with": "light_sword"
      },
      {
        "toCreate": "gardener",
        "with": "garden"
      },
      {
        "toCreate": "glasses",
        "with": "glass"
      },
      {
        "toCreate": "glasses",
        "with": "lens"
      },
      {
        "toCreate": "grim_reaper",
        "with": "scythe"
      },
      {
        "toCreate": "hacker",
        "with": "computer"
      },
      {
        "toCreate": "hacker",
        "with": "computer_mouse"
      },
      {
        "toCreate": "hacker",
        "with": "glasses"
      },
      {
        "toCreate": "hacker",
        "with": "internet"
      },
      {
        "toCreate": "hero",
        "with": "legend"
      },
      {
        "toCreate": "hero",
        "with": "story"
      },
      {
        "toCreate": "hero",
        "with": "lightning"
      },
      {
        "toCreate": "house",
        "with": "wall"
      },
      {
        "toCreate": "idea",
        "with": "light_bulb"
      },
      {
        "toCreate": "knight",
        "with": "armor"
      },
      {
        "toCreate": "librarian",
        "with": "library"
      },
      {
        "toCreate": "librarian",
        "with": "book"
      },
      {
        "toCreate": "love",
        "with": "human"
      },
      {
        "toCreate": "lumberjack",
        "with": "axe"
      },
      {
        "toCreate": "lumberjack",
        "with": "chainsaw"
      },
      {
        "toCreate": "lumberjack",
        "with": "wood"
      },
      {
        "toCreate": "lumberjack",
        "with": "tree"
      },
      {
        "toCreate": "mailman",
        "with": "letter"
      },
      {
        "toCreate": "mailman",
        "with": "newspaper"
      },
      {
        "toCreate": "medusa",
        "with": "snake"
      },
      {
        "toCreate": "mermaid",
        "with": "fish"
      },
      {
        "toCreate": "minotaur",
        "with": "cow"
      },
      {
        "toCreate": "monarch",
        "with": "castle"
      },
      {
        "toCreate": "monarch",
        "with": "excalibur"
      },
      {
        "toCreate": "mummy",
        "with": "pyramid"
      },
      {
        "toCreate": "musician",
        "with": "flute"
      },
      {
        "toCreate": "musician",
        "with": "music"
      },
      {
        "toCreate": "musician",
        "with": "sheet_music"
      },
      {
        "toCreate": "musician",
        "with": "drum"
      },
      {
        "toCreate": "musician",
        "with": "pan_flute"
      },
      {
        "toCreate": "ninja",
        "with": "shuriken"
      },
      {
        "toCreate": "ninja",
        "with": "katana"
      },
      {
        "toCreate": "painter",
        "with": "paint"
      },
      {
        "toCreate": "painter",
        "with": "canvas"
      },
      {
        "toCreate": "painter",
        "with": "painting"
      },
      {
        "toCreate": "paleontologist",
        "with": "dinosaur"
      },
      {
        "toCreate": "paleontologist",
        "with": "fossil"
      },
      {
        "toCreate": "philosophy",
        "with": "idea"
      },
      {
        "toCreate": "philosophy",
        "with": "story"
      },
      {
        "toCreate": "pilot",
        "with": "airplane"
      },
      {
        "toCreate": "pilot",
        "with": "seaplane"
      },
      {
        "toCreate": "potter",
        "with": "pottery"
      },
      {
        "toCreate": "potter",
        "with": "clay"
      },
      {
        "toCreate": "sailor",
        "with": "boat"
      },
      {
        "toCreate": "sailor",
        "with": "sailboat"
      },
      {
        "toCreate": "sailor",
        "with": "steamboat"
      },
      {
        "toCreate": "sailor",
        "with": "ocean"
      },
      {
        "toCreate": "sailor",
        "with": "sea"
      },
      {
        "toCreate": "sailor",
        "with": "lake"
      },
      {
        "toCreate": "santa",
        "with": "christmas_tree"
      },
      {
        "toCreate": "santa",
        "with": "christmas_stocking"
      },
      {
        "toCreate": "scarecrow",
        "with": "hay"
      },
      {
        "toCreate": "science",
        "with": "telescope"
      },
      {
        "toCreate": "science",
        "with": "microscope"
      },
      {
        "toCreate": "science",
        "with": "universe"
      },
      {
        "toCreate": "sickness",
        "with": "bacteria"
      },
      {
        "toCreate": "sickness",
        "with": "swamp"
      },
      {
        "toCreate": "sickness",
        "with": "sickness"
      },
      {
        "toCreate": "skier",
        "with": "ski_goggles"
      },
      {
        "toCreate": "skier",
        "with": "mountain"
      },
      {
        "toCreate": "skier",
        "with": "mountain_range"
      },
      {
        "toCreate": "snowball",
        "with": "snow"
      },
      {
        "toCreate": "snowman",
        "with": "snow"
      },
      {
        "toCreate": "sound",
        "with": "wave"
      },
      {
        "toCreate": "statue",
        "with": "medusa"
      },
      {
        "toCreate": "story",
        "with": "campfire"
      },
      {
        "toCreate": "story",
        "with": "hero"
      },
      {
        "toCreate": "surfer",
        "with": "wave"
      },
      {
        "toCreate": "surfer",
        "with": "beach"
      },
      {
        "toCreate": "sweater",
        "with": "wool"
      },
      {
        "toCreate": "swimmer",
        "with": "water"
      },
      {
        "toCreate": "swimmer",
        "with": "swim_goggles"
      },
      {
        "toCreate": "swimmer",
        "with": "pond"
      },
      {
        "toCreate": "swimmer",
        "with": "swimming_pool"
      },
      {
        "toCreate": "tea",
        "with": "leaf"
      },
      {
        "toCreate": "tool",
        "with": "metal"
      },
      {
        "toCreate": "tool",
        "with": "steel"
      },
      {
        "toCreate": "tool",
        "with": "wood"
      },
      {
        "toCreate": "tool",
        "with": "stone"
      },
      {
        "toCreate": "tool",
        "with": "rock"
      },
      {
        "toCreate": "vampire",
        "with": "blood"
      },
      {
        "toCreate": "vampire",
        "with": "vampire"
      },
      {
        "toCreate": "vampire",
        "with": "bat"
      },
      {
        "toCreate": "warmth",
        "with": "heat"
      },
      {
        "toCreate": "warrior",
        "with": "sword"
      },
      {
        "toCreate": "warrior",
        "with": "bow"
      },
      {
        "toCreate": "watch",
        "with": "clock"
      },
      {
        "toCreate": "werewolf",
        "with": "wolf"
      },
      {
        "toCreate": "werewolf",
        "with": "werewolf"
      },
      {
        "toCreate": "witch",
        "with": "broom"
      },
      {
        "toCreate": "wizard",
        "with": "magic"
      },
      {
        "toCreate": "wizard",
        "with": "rainbow"
      },
      {
        "toCreate": "wizard",
        "with": "double_rainbow!"
      },
      {
        "toCreate": "wizard",
        "with": "unicorn"
      },
      {
        "toCreate": "zombie",
        "with": "zombie"
      }
    ],
    "name": "Human"
  },
  "hummingbird": {
    "combine": [
      {
        "toCreate": "airplane",
        "with": "metal"
      },
      {
        "toCreate": "airplane",
        "with": "steel"
      },
      {
        "toCreate": "airplane",
        "with": "machine"
      },
      {
        "toCreate": "bat",
        "with": "mouse"
      },
      {
        "toCreate": "crow",
        "with": "scarecrow"
      },
      {
        "toCreate": "crow",
        "with": "field"
      },
      {
        "toCreate": "cuckoo",
        "with": "clock"
      },
      {
        "toCreate": "cuckoo",
        "with": "alarm_clock"
      },
      {
        "toCreate": "egg",
        "with": "hummingbird"
      },
      {
        "toCreate": "flying_squirrel",
        "with": "squirrel"
      },
      {
        "toCreate": "origami",
        "with": "paper"
      },
      {
        "toCreate": "vulture",
        "with": "corpse"
      },
      {
        "toCreate": "vulture",
        "with": "desert"
      }
    ],
    "name": "Hummingbird"
  },
  "hurricane": {
    "combine": [
      {
        "toCreate": "blizzard",
        "with": "snow"
      },
      {
        "toCreate": "wave",
        "with": "ocean"
      },
      {
        "toCreate": "wave",
        "with": "sea"
      },
      {
        "toCreate": "wave",
        "with": "lake"
      }
    ],
    "name": "Hurricane"
  },
  "husky": {
    "combine": [
      {
        "toCreate": "doghouse",
        "with": "house"
      },
      {
        "toCreate": "doghouse",
        "with": "wall"
      },
      {
        "toCreate": "doghouse",
        "with": "container"
      }
    ],
    "name": "Husky"
  },
  "ice": {
    "combine": [
      {
        "toCreate": "antarctica",
        "with": "desert"
      },
      {
        "toCreate": "antarctica",
        "with": "continent"
      },
      {
        "toCreate": "chill",
        "with": "human"
      },
      {
        "toCreate": "dry_ice",
        "with": "carbon_dioxide"
      },
      {
        "toCreate": "fridge",
        "with": "metal"
      },
      {
        "toCreate": "fridge",
        "with": "steel"
      },
      {
        "toCreate": "fridge",
        "with": "container"
      },
      {
        "toCreate": "frozen_yogurt",
        "with": "yogurt"
      },
      {
        "toCreate": "glacier",
        "with": "mountain"
      },
      {
        "toCreate": "glacier",
        "with": "mountain_range"
      },
      {
        "toCreate": "glacier",
        "with": "time"
      },
      {
        "toCreate": "hail",
        "with": "cloud"
      },
      {
        "toCreate": "hail",
        "with": "rain"
      },
      {
        "toCreate": "hail",
        "with": "storm"
      },
      {
        "toCreate": "hail",
        "with": "wind"
      },
      {
        "toCreate": "hail",
        "with": "steam"
      },
      {
        "toCreate": "hail",
        "with": "sky"
      },
      {
        "toCreate": "husky",
        "with": "dog"
      },
      {
        "toCreate": "ice_cream",
        "with": "milk"
      },
      {
        "toCreate": "ice_sculpture",
        "with": "statue"
      },
      {
        "toCreate": "iceberg",
        "with": "sea"
      },
      {
        "toCreate": "iceberg",
        "with": "ocean"
      },
      {
        "toCreate": "iced_tea",
        "with": "tea"
      },
      {
        "toCreate": "igloo",
        "with": "house"
      },
      {
        "toCreate": "penguin",
        "with": "bird"
      },
      {
        "toCreate": "polar_bear",
        "with": "animal"
      },
      {
        "toCreate": "popsicle",
        "with": "juice"
      },
      {
        "toCreate": "sleigh",
        "with": "cart"
      },
      {
        "toCreate": "sleigh",
        "with": "wagon"
      },
      {
        "toCreate": "smoothie",
        "with": "fruit"
      },
      {
        "toCreate": "snow_globe",
        "with": "crystal_ball"
      },
      {
        "toCreate": "snowboard",
        "with": "wood"
      },
      {
        "toCreate": "snowboard",
        "with": "surfer"
      },
      {
        "toCreate": "snowmobile",
        "with": "motorcycle"
      },
      {
        "toCreate": "snowmobile",
        "with": "car"
      },
      {
        "toCreate": "water",
        "with": "heat"
      }
    ],
    "name": "Ice"
  },
  "ice_cream": {
    "combine": [
      {
        "toCreate": "fridge",
        "with": "container"
      },
      {
        "toCreate": "ice_cream_truck",
        "with": "car"
      },
      {
        "toCreate": "ice_cream_truck",
        "with": "bus"
      },
      {
        "toCreate": "ice_cream_truck",
        "with": "wagon"
      },
      {
        "toCreate": "milk_shake",
        "with": "milk"
      },
      {
        "toCreate": "milk_shake",
        "with": "yogurt"
      },
      {
        "toCreate": "milk_shake",
        "with": "water"
      },
      {
        "toCreate": "yogurt",
        "with": "bacteria"
      }
    ],
    "name": "Ice Cream"
  },
  "ice_cream_truck": {
    "combine": [
      {
        "toCreate": "garage",
        "with": "house"
      },
      {
        "toCreate": "garage",
        "with": "container"
      },
      {
        "toCreate": "garage",
        "with": "wall"
      },
      {
        "toCreate": "garage",
        "with": "barn"
      }
    ],
    "name": "Ice Cream Truck"
  },
  "ice_sculpture": {
    "combine": [],
    "name": "Ice Sculpture"
  },
  "iceberg": {
    "combine": [
      {
        "toCreate": "titanic",
        "with": "steamboat"
      },
      {
        "toCreate": "titanic",
        "with": "boat"
      },
      {
        "toCreate": "titanic",
        "with": "legend"
      }
    ],
    "name": "Iceberg"
  },
  "iced_tea": {
    "combine": [],
    "name": "Iced Tea"
  },
  "idea": {
    "combine": [
      {
        "toCreate": "book",
        "with": "container"
      },
      {
        "toCreate": "gas",
        "with": "air"
      },
      {
        "toCreate": "heat",
        "with": "fire"
      },
      {
        "toCreate": "liquid",
        "with": "water"
      },
      {
        "toCreate": "motion",
        "with": "wind"
      },
      {
        "toCreate": "penicillin",
        "with": "mold"
      },
      {
        "toCreate": "philosophy",
        "with": "human"
      },
      {
        "toCreate": "rain",
        "with": "fire"
      },
      {
        "toCreate": "solid",
        "with": "earth"
      }
    ],
    "name": "Idea"
  },
  "igloo": {
    "combine": [],
    "name": "Igloo"
  },
  "internet": {
    "combine": [
      {
        "toCreate": "doge",
        "with": "dog"
      },
      {
        "toCreate": "email",
        "with": "letter"
      },
      {
        "toCreate": "hacker",
        "with": "human"
      },
      {
        "toCreate": "optical_fiber",
        "with": "light"
      }
    ],
    "name": "Internet"
  },
  "island": {
    "combine": [
      {
        "toCreate": "archipelago",
        "with": "island"
      },
      {
        "toCreate": "archipelago",
        "with": "sea"
      },
      {
        "toCreate": "archipelago",
        "with": "ocean"
      },
      {
        "toCreate": "palm",
        "with": "tree"
      },
      {
        "toCreate": "treasure",
        "with": "treasure_map"
      },
      {
        "toCreate": "treasure_map",
        "with": "map"
      }
    ],
    "name": "Island"
  },
  "ivy": {
    "combine": [],
    "name": "Ivy"
  },
  "jack-o-lantern": {
    "combine": [
      {
        "toCreate": "pumpkin",
        "with": "vegetable"
      },
      {
        "toCreate": "pumpkin",
        "with": "plant"
      },
      {
        "toCreate": "scarecrow",
        "with": "sack"
      }
    ],
    "name": "Jack-O-Lantern"
  },
  "jam": {
    "combine": [
      {
        "toCreate": "jar",
        "with": "container"
      },
      {
        "toCreate": "jar",
        "with": "bottle"
      },
      {
        "toCreate": "jar",
        "with": "glass"
      },
      {
        "toCreate": "jar",
        "with": "bucket"
      }
    ],
    "name": "Jam"
  },
  "jar": {
    "combine": [
      {
        "toCreate": "ant_farm",
        "with": "ant"
      },
      {
        "toCreate": "ant_farm",
        "with": "anthill"
      }
    ],
    "name": "Jar"
  },
  "jerky": {
    "combine": [],
    "name": "Jerky"
  },
  "juice": {
    "combine": [
      {
        "toCreate": "alcohol",
        "with": "time"
      },
      {
        "toCreate": "alcohol",
        "with": "wheat"
      },
      {
        "toCreate": "bottle",
        "with": "container"
      },
      {
        "toCreate": "jam",
        "with": "sugar"
      },
      {
        "toCreate": "jam",
        "with": "fruit"
      },
      {
        "toCreate": "jam",
        "with": "machine"
      },
      {
        "toCreate": "popsicle",
        "with": "ice"
      },
      {
        "toCreate": "popsicle",
        "with": "cold"
      },
      {
        "toCreate": "popsicle",
        "with": "wood"
      },
      {
        "toCreate": "popsicle",
        "with": "snow"
      },
      {
        "toCreate": "soda",
        "with": "carbon_dioxide"
      },
      {
        "toCreate": "sugar",
        "with": "fire"
      },
      {
        "toCreate": "sugar",
        "with": "energy"
      }
    ],
    "name": "Juice"
  },
  "jupiter": {
    "combine": [
      {
        "toCreate": "alien",
        "with": "life"
      },
      {
        "toCreate": "astronaut",
        "with": "human"
      },
      {
        "toCreate": "saturn",
        "with": "small"
      },
      {
        "toCreate": "solar_system",
        "with": "container"
      },
      {
        "toCreate": "telescope",
        "with": "glass"
      }
    ],
    "name": "Jupiter"
  },
  "kaiju": {
    "combine": [],
    "name": "Kaiju"
  },
  "katana": {
    "combine": [
      {
        "toCreate": "ninja",
        "with": "human"
      },
      {
        "toCreate": "ninja_turtle",
        "with": "turtle"
      }
    ],
    "name": "Katana"
  },
  "kite": {
    "combine": [],
    "name": "Kite"
  },
  "knife": {
    "combine": [],
    "name": "Knife"
  },
  "knight": {
    "combine": [
      {
        "toCreate": "castle",
        "with": "stone"
      },
      {
        "toCreate": "castle",
        "with": "wall"
      },
      {
        "toCreate": "castle",
        "with": "house"
      },
      {
        "toCreate": "castle",
        "with": "container"
      },
      {
        "toCreate": "don_quixote",
        "with": "windmill"
      },
      {
        "toCreate": "fairy_tale",
        "with": "story"
      },
      {
        "toCreate": "force_knight",
        "with": "light_sword"
      },
      {
        "toCreate": "hero",
        "with": "dragon"
      },
      {
        "toCreate": "idea",
        "with": "knight"
      }
    ],
    "name": "Knight"
  },
  "lake": {
    "combine": [
      {
        "toCreate": "algae",
        "with": "grass"
      },
      {
        "toCreate": "alligator",
        "with": "lizard"
      },
      {
        "toCreate": "beach",
        "with": "sand"
      },
      {
        "toCreate": "boat",
        "with": "wood"
      },
      {
        "toCreate": "dam",
        "with": "beaver"
      },
      {
        "toCreate": "diver",
        "with": "scuba_tank"
      },
      {
        "toCreate": "duck",
        "with": "bird"
      },
      {
        "toCreate": "duck",
        "with": "owl"
      },
      {
        "toCreate": "duck",
        "with": "pigeon"
      },
      {
        "toCreate": "duck",
        "with": "chicken"
      },
      {
        "toCreate": "excalibur",
        "with": "sword"
      },
      {
        "toCreate": "fish",
        "with": "animal"
      },
      {
        "toCreate": "fish",
        "with": "egg"
      },
      {
        "toCreate": "flood",
        "with": "rain"
      },
      {
        "toCreate": "horizon",
        "with": "sky"
      },
      {
        "toCreate": "life",
        "with": "electricity"
      },
      {
        "toCreate": "life",
        "with": "lightning"
      },
      {
        "toCreate": "manatee",
        "with": "cow"
      },
      {
        "toCreate": "map",
        "with": "paper"
      },
      {
        "toCreate": "nessie",
        "with": "legend"
      },
      {
        "toCreate": "nessie",
        "with": "story"
      },
      {
        "toCreate": "oasis",
        "with": "desert"
      },
      {
        "toCreate": "pond",
        "with": "small"
      },
      {
        "toCreate": "river",
        "with": "motion"
      },
      {
        "toCreate": "roe",
        "with": "egg"
      },
      {
        "toCreate": "sailor",
        "with": "human"
      },
      {
        "toCreate": "sea",
        "with": "water"
      },
      {
        "toCreate": "sea",
        "with": "big"
      },
      {
        "toCreate": "sea",
        "with": "lake"
      },
      {
        "toCreate": "seahorse",
        "with": "horse"
      },
      {
        "toCreate": "seal",
        "with": "dog"
      },
      {
        "toCreate": "seaplane",
        "with": "airplane"
      },
      {
        "toCreate": "seasickness",
        "with": "sickness"
      },
      {
        "toCreate": "seaweed",
        "with": "plant"
      },
      {
        "toCreate": "swamp",
        "with": "algae"
      },
      {
        "toCreate": "swamp",
        "with": "reed"
      },
      {
        "toCreate": "swamp",
        "with": "tree"
      },
      {
        "toCreate": "swim_goggles",
        "with": "glasses"
      },
      {
        "toCreate": "swim_goggles",
        "with": "sunglasses"
      },
      {
        "toCreate": "swimming_pool",
        "with": "house"
      },
      {
        "toCreate": "water_lily",
        "with": "flower"
      },
      {
        "toCreate": "waterfall",
        "with": "mountain"
      },
      {
        "toCreate": "waterfall",
        "with": "mountain_range"
      },
      {
        "toCreate": "waterfall",
        "with": "hill"
      },
      {
        "toCreate": "wave",
        "with": "wind"
      },
      {
        "toCreate": "wave",
        "with": "storm"
      },
      {
        "toCreate": "wave",
        "with": "hurricane"
      }
    ],
    "name": "Lake"
  },
  "lamp": {
    "combine": [
      {
        "toCreate": "candle",
        "with": "wax"
      },
      {
        "toCreate": "flashlight",
        "with": "human"
      },
      {
        "toCreate": "flashlight",
        "with": "tool"
      },
      {
        "toCreate": "lava_lamp",
        "with": "lava"
      },
      {
        "toCreate": "lava_lamp",
        "with": "fire"
      },
      {
        "toCreate": "lava_lamp",
        "with": "volcano"
      },
      {
        "toCreate": "moth",
        "with": "butterfly"
      }
    ],
    "name": "Lamp"
  },
  "land": {
    "combine": [
      {
        "toCreate": "animal",
        "with": "life"
      },
      {
        "toCreate": "anthill",
        "with": "ant"
      },
      {
        "toCreate": "continent",
        "with": "big"
      },
      {
        "toCreate": "continent",
        "with": "land"
      },
      {
        "toCreate": "continent",
        "with": "earth"
      },
      {
        "toCreate": "desert",
        "with": "sand"
      },
      {
        "toCreate": "dust",
        "with": "air"
      },
      {
        "toCreate": "field",
        "with": "farmer"
      },
      {
        "toCreate": "field",
        "with": "tool"
      },
      {
        "toCreate": "field",
        "with": "plow"
      },
      {
        "toCreate": "firestation",
        "with": "firefighter"
      },
      {
        "toCreate": "forest",
        "with": "tree"
      },
      {
        "toCreate": "grass",
        "with": "plant"
      },
      {
        "toCreate": "graveyard",
        "with": "grave"
      },
      {
        "toCreate": "graveyard",
        "with": "gravestone"
      },
      {
        "toCreate": "horizon",
        "with": "sky"
      },
      {
        "toCreate": "horse",
        "with": "animal"
      },
      {
        "toCreate": "lightning",
        "with": "storm"
      },
      {
        "toCreate": "map",
        "with": "paper"
      },
      {
        "toCreate": "plant",
        "with": "seed"
      },
      {
        "toCreate": "plant",
        "with": "algae"
      },
      {
        "toCreate": "sheep",
        "with": "livestock"
      },
      {
        "toCreate": "snake",
        "with": "electric_eel"
      },
      {
        "toCreate": "soil",
        "with": "life"
      },
      {
        "toCreate": "soil",
        "with": "organic_matter"
      }
    ],
    "name": "Land"
  },
  "lasso": {
    "combine": [],
    "name": "Lasso"
  },
  "lava": {
    "combine": [
      {
        "toCreate": "eruption",
        "with": "pressure"
      },
      {
        "toCreate": "explosion",
        "with": "petroleum"
      },
      {
        "toCreate": "granite",
        "with": "pressure"
      },
      {
        "toCreate": "heat",
        "with": "philosophy"
      },
      {
        "toCreate": "lava_lamp",
        "with": "lamp"
      },
      {
        "toCreate": "magma",
        "with": "science"
      },
      {
        "toCreate": "obsidian",
        "with": "water"
      },
      {
        "toCreate": "obsidian",
        "with": "cold"
      },
      {
        "toCreate": "obsidian",
        "with": "glass"
      },
      {
        "toCreate": "primordial_soup",
        "with": "ocean"
      },
      {
        "toCreate": "primordial_soup",
        "with": "sea"
      },
      {
        "toCreate": "steam",
        "with": "water"
      },
      {
        "toCreate": "stone",
        "with": "air"
      },
      {
        "toCreate": "volcano",
        "with": "mountain"
      },
      {
        "toCreate": "volcano",
        "with": "earth"
      },
      {
        "toCreate": "volcano",
        "with": "hill"
      },
      {
        "toCreate": "volcano",
        "with": "container"
      }
    ],
    "name": "Lava"
  },
  "lava_lamp": {
    "combine": [],
    "name": "Lava Lamp"
  },
  "lawn": {
    "combine": [
      {
        "toCreate": "garden",
        "with": "flower"
      },
      {
        "toCreate": "garden",
        "with": "plant"
      },
      {
        "toCreate": "picnic",
        "with": "sandwich"
      }
    ],
    "name": "Lawn"
  },
  "lawn_mower": {
    "combine": [
      {
        "toCreate": "lawn",
        "with": "field"
      },
      {
        "toCreate": "lawn",
        "with": "grass"
      }
    ],
    "name": "Lawn Mower"
  },
  "leaf": {
    "combine": [
      {
        "toCreate": "hedge",
        "with": "fence"
      },
      {
        "toCreate": "hedge",
        "with": "wall"
      },
      {
        "toCreate": "peacock",
        "with": "bird"
      },
      {
        "toCreate": "tea",
        "with": "water"
      },
      {
        "toCreate": "tea",
        "with": "human"
      },
      {
        "toCreate": "tea",
        "with": "heat"
      }
    ],
    "name": "Leaf"
  },
  "leather": {
    "combine": [
      {
        "toCreate": "drum",
        "with": "wood"
      },
      {
        "toCreate": "saddle",
        "with": "horse"
      }
    ],
    "name": "Leather"
  },
  "legend": {
    "combine": [
      {
        "toCreate": "book",
        "with": "paper"
      },
      {
        "toCreate": "book",
        "with": "container"
      },
      {
        "toCreate": "don_quixote",
        "with": "windmill"
      },
      {
        "toCreate": "dragon",
        "with": "lizard"
      },
      {
        "toCreate": "excalibur",
        "with": "sword"
      },
      {
        "toCreate": "faun",
        "with": "goat"
      },
      {
        "toCreate": "faun",
        "with": "mountain_goat"
      },
      {
        "toCreate": "frankenstein’s_monster",
        "with": "corpse"
      },
      {
        "toCreate": "ghost",
        "with": "graveyard"
      },
      {
        "toCreate": "ghost",
        "with": "grave"
      },
      {
        "toCreate": "ghost",
        "with": "gravestone"
      },
      {
        "toCreate": "ghost",
        "with": "night"
      },
      {
        "toCreate": "gnome",
        "with": "garden"
      },
      {
        "toCreate": "golem",
        "with": "clay"
      },
      {
        "toCreate": "golem",
        "with": "statue"
      },
      {
        "toCreate": "hero",
        "with": "human"
      },
      {
        "toCreate": "kaiju",
        "with": "dinosaur"
      },
      {
        "toCreate": "medusa",
        "with": "snake"
      },
      {
        "toCreate": "mermaid",
        "with": "fish"
      },
      {
        "toCreate": "minotaur",
        "with": "cow"
      },
      {
        "toCreate": "nessie",
        "with": "lake"
      },
      {
        "toCreate": "nessie",
        "with": "dinosaur"
      },
      {
        "toCreate": "santa",
        "with": "christmas_tree"
      },
      {
        "toCreate": "santa",
        "with": "christmas_stocking"
      },
      {
        "toCreate": "titanic",
        "with": "iceberg"
      },
      {
        "toCreate": "titanic",
        "with": "steamboat"
      },
      {
        "toCreate": "unicorn",
        "with": "horse"
      },
      {
        "toCreate": "witch",
        "with": "broom"
      },
      {
        "toCreate": "witch",
        "with": "cauldron"
      },
      {
        "toCreate": "yeti",
        "with": "mountain"
      },
      {
        "toCreate": "yeti",
        "with": "mountain_range"
      },
      {
        "toCreate": "yeti",
        "with": "glacier"
      },
      {
        "toCreate": "yeti",
        "with": "antarctica"
      }
    ],
    "name": "Legend"
  },
  "lens": {
    "combine": [
      {
        "toCreate": "glasses",
        "with": "human"
      },
      {
        "toCreate": "glasses",
        "with": "lens"
      },
      {
        "toCreate": "microscope",
        "with": "tool"
      },
      {
        "toCreate": "microscope",
        "with": "bacteria"
      }
    ],
    "name": "Lens"
  },
  "letter": {
    "combine": [
      {
        "toCreate": "email",
        "with": "computer"
      },
      {
        "toCreate": "email",
        "with": "internet"
      },
      {
        "toCreate": "email",
        "with": "computer_mouse"
      },
      {
        "toCreate": "email",
        "with": "electricity"
      },
      {
        "toCreate": "mailbox",
        "with": "box"
      },
      {
        "toCreate": "mailbox",
        "with": "metal"
      },
      {
        "toCreate": "mailbox",
        "with": "steel"
      },
      {
        "toCreate": "mailbox",
        "with": "wood"
      },
      {
        "toCreate": "mailman",
        "with": "human"
      },
      {
        "toCreate": "mailman",
        "with": "mailbox"
      },
      {
        "toCreate": "newspaper",
        "with": "letter"
      },
      {
        "toCreate": "pigeon",
        "with": "bird"
      },
      {
        "toCreate": "post_office",
        "with": "house"
      },
      {
        "toCreate": "post_office",
        "with": "wall"
      },
      {
        "toCreate": "post_office",
        "with": "mailman"
      },
      {
        "toCreate": "post_office",
        "with": "letter"
      },
      {
        "toCreate": "sack",
        "with": "container"
      },
      {
        "toCreate": "smoke_signal",
        "with": "smoke"
      }
    ],
    "name": "Letter"
  },
  "librarian": {
    "combine": [
      {
        "toCreate": "idea",
        "with": "librarian"
      }
    ],
    "name": "Librarian"
  },
  "library": {
    "combine": [
      {
        "toCreate": "librarian",
        "with": "human"
      }
    ],
    "name": "Library"
  },
  "life": {
    "combine": [
      {
        "toCreate": "alien",
        "with": "space"
      },
      {
        "toCreate": "alien",
        "with": "galaxy"
      },
      {
        "toCreate": "alien",
        "with": "galaxy_cluster"
      },
      {
        "toCreate": "alien",
        "with": "solar_system"
      },
      {
        "toCreate": "alien",
        "with": "mars"
      },
      {
        "toCreate": "alien",
        "with": "venus"
      },
      {
        "toCreate": "alien",
        "with": "mercury"
      },
      {
        "toCreate": "alien",
        "with": "jupiter"
      },
      {
        "toCreate": "alien",
        "with": "saturn"
      },
      {
        "toCreate": "animal",
        "with": "land"
      },
      {
        "toCreate": "animal",
        "with": "forest"
      },
      {
        "toCreate": "animal",
        "with": "mountain"
      },
      {
        "toCreate": "animal",
        "with": "mountain_range"
      },
      {
        "toCreate": "animal",
        "with": "beach"
      },
      {
        "toCreate": "animal",
        "with": "desert"
      },
      {
        "toCreate": "bacteria",
        "with": "primordial_soup"
      },
      {
        "toCreate": "bacteria",
        "with": "small"
      },
      {
        "toCreate": "bacteria",
        "with": "mud"
      },
      {
        "toCreate": "death",
        "with": "time"
      },
      {
        "toCreate": "egg",
        "with": "container"
      },
      {
        "toCreate": "gingerbread_man",
        "with": "dough"
      },
      {
        "toCreate": "gingerbread_man",
        "with": "cookie"
      },
      {
        "toCreate": "gingerbread_man",
        "with": "cookie_dough"
      },
      {
        "toCreate": "golem",
        "with": "statue"
      },
      {
        "toCreate": "human",
        "with": "clay"
      },
      {
        "toCreate": "magic",
        "with": "rainbow"
      },
      {
        "toCreate": "magic",
        "with": "double_rainbow!"
      },
      {
        "toCreate": "organic_matter",
        "with": "death"
      },
      {
        "toCreate": "organic_matter",
        "with": "science"
      },
      {
        "toCreate": "phoenix",
        "with": "fire"
      },
      {
        "toCreate": "pinocchio",
        "with": "wood"
      },
      {
        "toCreate": "plankton",
        "with": "water"
      },
      {
        "toCreate": "plankton",
        "with": "ocean"
      },
      {
        "toCreate": "plankton",
        "with": "sea"
      },
      {
        "toCreate": "plant",
        "with": "soil"
      },
      {
        "toCreate": "robot",
        "with": "armor"
      },
      {
        "toCreate": "robot",
        "with": "metal"
      },
      {
        "toCreate": "robot",
        "with": "steel"
      },
      {
        "toCreate": "soil",
        "with": "earth"
      },
      {
        "toCreate": "soil",
        "with": "land"
      },
      {
        "toCreate": "zombie",
        "with": "corpse"
      }
    ],
    "name": "Life"
  },
  "light": {
    "combine": [
      {
        "toCreate": "angel",
        "with": "human"
      },
      {
        "toCreate": "candle",
        "with": "wax"
      },
      {
        "toCreate": "christmas_tree",
        "with": "tree"
      },
      {
        "toCreate": "electricity",
        "with": "solar_cell"
      },
      {
        "toCreate": "flashlight",
        "with": "tool"
      },
      {
        "toCreate": "gold",
        "with": "metal"
      },
      {
        "toCreate": "gold",
        "with": "steel"
      },
      {
        "toCreate": "jack-o-lantern",
        "with": "pumpkin"
      },
      {
        "toCreate": "light_bulb",
        "with": "glass"
      },
      {
        "toCreate": "light_bulb",
        "with": "container"
      },
      {
        "toCreate": "light_sword",
        "with": "sword"
      },
      {
        "toCreate": "lighthouse",
        "with": "ocean"
      },
      {
        "toCreate": "lighthouse",
        "with": "sea"
      },
      {
        "toCreate": "lighthouse",
        "with": "house"
      },
      {
        "toCreate": "lighthouse",
        "with": "beach"
      },
      {
        "toCreate": "moth",
        "with": "butterfly"
      },
      {
        "toCreate": "optical_fiber",
        "with": "wire"
      },
      {
        "toCreate": "optical_fiber",
        "with": "rope"
      },
      {
        "toCreate": "optical_fiber",
        "with": "internet"
      },
      {
        "toCreate": "rainbow",
        "with": "rain"
      },
      {
        "toCreate": "rainbow",
        "with": "water"
      },
      {
        "toCreate": "rainbow",
        "with": "cloud"
      },
      {
        "toCreate": "rainbow",
        "with": "prism"
      },
      {
        "toCreate": "sky",
        "with": "atmosphere"
      },
      {
        "toCreate": "solar_cell",
        "with": "tool"
      },
      {
        "toCreate": "spotlight",
        "with": "machine"
      },
      {
        "toCreate": "spotlight",
        "with": "metal"
      },
      {
        "toCreate": "spotlight",
        "with": "steel"
      },
      {
        "toCreate": "sun",
        "with": "sky"
      },
      {
        "toCreate": "sun",
        "with": "planet"
      },
      {
        "toCreate": "sun",
        "with": "day"
      },
      {
        "toCreate": "sundial",
        "with": "wheel"
      },
      {
        "toCreate": "sundial",
        "with": "tool"
      },
      {
        "toCreate": "sundial",
        "with": "watch"
      },
      {
        "toCreate": "sundial",
        "with": "clock"
      },
      {
        "toCreate": "sunglasses",
        "with": "glasses"
      }
    ],
    "name": "Light"
  },
  "light_bulb": {
    "combine": [
      {
        "toCreate": "christmas_tree",
        "with": "tree"
      },
      {
        "toCreate": "flashlight",
        "with": "tool"
      },
      {
        "toCreate": "idea",
        "with": "human"
      },
      {
        "toCreate": "idea",
        "with": "engineer"
      },
      {
        "toCreate": "idea",
        "with": "science"
      },
      {
        "toCreate": "lamp",
        "with": "metal"
      },
      {
        "toCreate": "lamp",
        "with": "steel"
      },
      {
        "toCreate": "light",
        "with": "electricity"
      }
    ],
    "name": "Light Bulb"
  },
  "light_sword": {
    "combine": [
      {
        "toCreate": "force_knight",
        "with": "human"
      },
      {
        "toCreate": "force_knight",
        "with": "warrior"
      },
      {
        "toCreate": "force_knight",
        "with": "knight"
      }
    ],
    "name": "Light Sword"
  },
  "lighthouse": {
    "combine": [],
    "name": "Lighthouse"
  },
  "lightning": {
    "combine": [
      {
        "toCreate": "electricity",
        "with": "metal"
      },
      {
        "toCreate": "electricity",
        "with": "steel"
      },
      {
        "toCreate": "explosion",
        "with": "gunpowder"
      },
      {
        "toCreate": "frankenstein’s_monster",
        "with": "corpse"
      },
      {
        "toCreate": "frankenstein’s_monster",
        "with": "zombie"
      },
      {
        "toCreate": "glass",
        "with": "sand"
      },
      {
        "toCreate": "hero",
        "with": "human"
      },
      {
        "toCreate": "life",
        "with": "ocean"
      },
      {
        "toCreate": "life",
        "with": "sea"
      },
      {
        "toCreate": "life",
        "with": "lake"
      },
      {
        "toCreate": "life",
        "with": "primordial_soup"
      }
    ],
    "name": "Lightning"
  },
  "lion": {
    "combine": [
      {
        "toCreate": "cage",
        "with": "metal"
      },
      {
        "toCreate": "cage",
        "with": "steel"
      },
      {
        "toCreate": "cage",
        "with": "wall"
      },
      {
        "toCreate": "cave",
        "with": "house"
      },
      {
        "toCreate": "cave",
        "with": "container"
      },
      {
        "toCreate": "origami",
        "with": "paper"
      },
      {
        "toCreate": "sphinx",
        "with": "statue"
      },
      {
        "toCreate": "sphinx",
        "with": "stone"
      }
    ],
    "name": "Lion"
  },
  "liquid": {
    "combine": [
      {
        "toCreate": "bottle",
        "with": "container"
      },
      {
        "toCreate": "chicken_soup",
        "with": "chicken"
      },
      {
        "toCreate": "chicken_soup",
        "with": "chicken_wing"
      },
      {
        "toCreate": "clay",
        "with": "stone"
      },
      {
        "toCreate": "clay",
        "with": "rock"
      },
      {
        "toCreate": "gas",
        "with": "energy"
      },
      {
        "toCreate": "lava",
        "with": "earth"
      },
      {
        "toCreate": "milk",
        "with": "cow"
      },
      {
        "toCreate": "milk",
        "with": "goat"
      },
      {
        "toCreate": "paint",
        "with": "rainbow"
      },
      {
        "toCreate": "paint",
        "with": "double_rainbow!"
      },
      {
        "toCreate": "paint",
        "with": "pencil"
      },
      {
        "toCreate": "petroleum",
        "with": "fossil"
      },
      {
        "toCreate": "quicksilver",
        "with": "metal"
      }
    ],
    "name": "Liquid"
  },
  "little_alchemy": {
    "combine": [],
    "name": "Little Alchemy"
  },
  "livestock": {
    "combine": [
      {
        "toCreate": "barn",
        "with": "house"
      },
      {
        "toCreate": "barn",
        "with": "container"
      },
      {
        "toCreate": "camel",
        "with": "desert"
      },
      {
        "toCreate": "camel",
        "with": "sand"
      },
      {
        "toCreate": "camel",
        "with": "dune"
      },
      {
        "toCreate": "chicken",
        "with": "bird"
      },
      {
        "toCreate": "chicken",
        "with": "egg"
      },
      {
        "toCreate": "cow",
        "with": "barn"
      },
      {
        "toCreate": "cow",
        "with": "field"
      },
      {
        "toCreate": "cow",
        "with": "grass"
      },
      {
        "toCreate": "cow",
        "with": "farmer"
      },
      {
        "toCreate": "goat",
        "with": "mountain"
      },
      {
        "toCreate": "goat",
        "with": "hill"
      },
      {
        "toCreate": "goat",
        "with": "mountain_range"
      },
      {
        "toCreate": "horse",
        "with": "hay"
      },
      {
        "toCreate": "horse",
        "with": "barn"
      },
      {
        "toCreate": "horse",
        "with": "horseshoe"
      },
      {
        "toCreate": "horse",
        "with": "saddle"
      },
      {
        "toCreate": "meat",
        "with": "tool"
      },
      {
        "toCreate": "meat",
        "with": "axe"
      },
      {
        "toCreate": "meat",
        "with": "sword"
      },
      {
        "toCreate": "meat",
        "with": "butcher"
      },
      {
        "toCreate": "pig",
        "with": "mud"
      },
      {
        "toCreate": "reindeer",
        "with": "santa"
      },
      {
        "toCreate": "reindeer",
        "with": "christmas_tree"
      },
      {
        "toCreate": "sheep",
        "with": "wool"
      },
      {
        "toCreate": "sheep",
        "with": "hill"
      },
      {
        "toCreate": "sheep",
        "with": "land"
      },
      {
        "toCreate": "sheep",
        "with": "cloud"
      }
    ],
    "name": "Livestock"
  },
  "lizard": {
    "combine": [
      {
        "toCreate": "alligator",
        "with": "river"
      },
      {
        "toCreate": "alligator",
        "with": "swamp"
      },
      {
        "toCreate": "alligator",
        "with": "lake"
      },
      {
        "toCreate": "chameleon",
        "with": "rainbow"
      },
      {
        "toCreate": "chameleon",
        "with": "double_rainbow!"
      },
      {
        "toCreate": "desert",
        "with": "sand"
      },
      {
        "toCreate": "dinosaur",
        "with": "time"
      },
      {
        "toCreate": "dinosaur",
        "with": "big"
      },
      {
        "toCreate": "dragon",
        "with": "fire"
      },
      {
        "toCreate": "dragon",
        "with": "air"
      },
      {
        "toCreate": "dragon",
        "with": "sky"
      },
      {
        "toCreate": "dragon",
        "with": "airplane"
      },
      {
        "toCreate": "dragon",
        "with": "legend"
      },
      {
        "toCreate": "dragon",
        "with": "story"
      },
      {
        "toCreate": "egg",
        "with": "lizard"
      },
      {
        "toCreate": "turtle",
        "with": "beach"
      }
    ],
    "name": "Lizard"
  },
  "log_cabin": {
    "combine": [],
    "name": "Log cabin"
  },
  "love": {
    "combine": [
      {
        "toCreate": "family",
        "with": "container"
      },
      {
        "toCreate": "ring",
        "with": "gold"
      },
      {
        "toCreate": "ring",
        "with": "diamond"
      },
      {
        "toCreate": "ring",
        "with": "metal"
      },
      {
        "toCreate": "ring",
        "with": "steel"
      },
      {
        "toCreate": "ring",
        "with": "gold"
      },
      {
        "toCreate": "rose",
        "with": "plant"
      },
      {
        "toCreate": "rose",
        "with": "flower"
      }
    ],
    "name": "Love"
  },
  "lumberjack": {
    "combine": [
      {
        "toCreate": "axe",
        "with": "tool"
      },
      {
        "toCreate": "chainsaw",
        "with": "machine"
      },
      {
        "toCreate": "chainsaw",
        "with": "electricity"
      },
      {
        "toCreate": "cyborg",
        "with": "robot"
      },
      {
        "toCreate": "idea",
        "with": "lumberjack"
      },
      {
        "toCreate": "statue",
        "with": "medusa"
      },
      {
        "toCreate": "wood",
        "with": "tree"
      },
      {
        "toCreate": "wood",
        "with": "forest"
      }
    ],
    "name": "Lumberjack"
  },
  "mac_and_cheese": {
    "combine": [],
    "name": "Mac and Cheese"
  },
  "machine": {
    "combine": [
      {
        "toCreate": "airplane",
        "with": "bird"
      },
      {
        "toCreate": "airplane",
        "with": "owl"
      },
      {
        "toCreate": "airplane",
        "with": "vulture"
      },
      {
        "toCreate": "airplane",
        "with": "duck"
      },
      {
        "toCreate": "airplane",
        "with": "seagull"
      },
      {
        "toCreate": "airplane",
        "with": "bat"
      },
      {
        "toCreate": "airplane",
        "with": "eagle"
      },
      {
        "toCreate": "airplane",
        "with": "pigeon"
      },
      {
        "toCreate": "airplane",
        "with": "hummingbird"
      },
      {
        "toCreate": "airplane",
        "with": "crow"
      },
      {
        "toCreate": "bicycle",
        "with": "wheel"
      },
      {
        "toCreate": "car",
        "with": "motorcycle"
      },
      {
        "toCreate": "chainsaw",
        "with": "axe"
      },
      {
        "toCreate": "chainsaw",
        "with": "lumberjack"
      },
      {
        "toCreate": "clock",
        "with": "time"
      },
      {
        "toCreate": "clock",
        "with": "sundial"
      },
      {
        "toCreate": "combustion_engine",
        "with": "petroleum"
      },
      {
        "toCreate": "combustion_engine",
        "with": "explosion"
      },
      {
        "toCreate": "computer",
        "with": "hacker"
      },
      {
        "toCreate": "engineer",
        "with": "human"
      },
      {
        "toCreate": "fabric",
        "with": "thread"
      },
      {
        "toCreate": "fridge",
        "with": "cold"
      },
      {
        "toCreate": "hay_bale",
        "with": "hay"
      },
      {
        "toCreate": "jam",
        "with": "fruit"
      },
      {
        "toCreate": "jam",
        "with": "juice"
      },
      {
        "toCreate": "lawn_mower",
        "with": "scythe"
      },
      {
        "toCreate": "lawn_mower",
        "with": "grass"
      },
      {
        "toCreate": "motorcycle",
        "with": "bicycle"
      },
      {
        "toCreate": "paper",
        "with": "wood"
      },
      {
        "toCreate": "rocket",
        "with": "atmosphere"
      },
      {
        "toCreate": "sewing_machine",
        "with": "needle"
      },
      {
        "toCreate": "sewing_machine",
        "with": "thread"
      },
      {
        "toCreate": "solar_cell",
        "with": "sun"
      },
      {
        "toCreate": "spotlight",
        "with": "light"
      },
      {
        "toCreate": "steam_engine",
        "with": "steam"
      },
      {
        "toCreate": "thread",
        "with": "cotton"
      },
      {
        "toCreate": "trojan_horse",
        "with": "horse"
      },
      {
        "toCreate": "vacuum_cleaner",
        "with": "broom"
      },
      {
        "toCreate": "wind_turbine",
        "with": "windmill"
      }
    ],
    "name": "Machine"
  },
  "magic": {
    "combine": [
      {
        "toCreate": "crystal_ball",
        "with": "glass"
      },
      {
        "toCreate": "crystal_ball",
        "with": "snow_globe"
      },
      {
        "toCreate": "faun",
        "with": "goat"
      },
      {
        "toCreate": "faun",
        "with": "mountain_goat"
      },
      {
        "toCreate": "gingerbread_man",
        "with": "dough"
      },
      {
        "toCreate": "gingerbread_man",
        "with": "cookie"
      },
      {
        "toCreate": "gingerbread_man",
        "with": "cookie_dough"
      },
      {
        "toCreate": "mermaid",
        "with": "fish"
      },
      {
        "toCreate": "mermaid",
        "with": "swimmer"
      },
      {
        "toCreate": "the_one_ring",
        "with": "ring"
      },
      {
        "toCreate": "unicorn",
        "with": "horse"
      },
      {
        "toCreate": "witch",
        "with": "broom"
      },
      {
        "toCreate": "wizard",
        "with": "human"
      }
    ],
    "name": "Magic"
  },
  "magma": {
    "combine": [],
    "name": "Magma"
  },
  "mailbox": {
    "combine": [
      {
        "toCreate": "mailman",
        "with": "letter"
      }
    ],
    "name": "Mailbox"
  },
  "mailman": {
    "combine": [
      {
        "toCreate": "idea",
        "with": "mailman"
      },
      {
        "toCreate": "mailbox",
        "with": "box"
      },
      {
        "toCreate": "mailbox",
        "with": "metal"
      },
      {
        "toCreate": "mailbox",
        "with": "steel"
      },
      {
        "toCreate": "mailbox",
        "with": "wood"
      },
      {
        "toCreate": "post_office",
        "with": "house"
      },
      {
        "toCreate": "post_office",
        "with": "wall"
      },
      {
        "toCreate": "post_office",
        "with": "container"
      },
      {
        "toCreate": "post_office",
        "with": "letter"
      }
    ],
    "name": "Mailman"
  },
  "manatee": {
    "combine": [
      {
        "toCreate": "sloth",
        "with": "tree"
      }
    ],
    "name": "Manatee"
  },
  "map": {
    "combine": [
      {
        "toCreate": "treasure_map",
        "with": "pirate"
      },
      {
        "toCreate": "treasure_map",
        "with": "pirate_ship"
      },
      {
        "toCreate": "treasure_map",
        "with": "treasure"
      },
      {
        "toCreate": "treasure_map",
        "with": "island"
      }
    ],
    "name": "Map"
  },
  "maple_syrup": {
    "combine": [],
    "name": "Maple Syrup"
  },
  "mars": {
    "combine": [
      {
        "toCreate": "alien",
        "with": "life"
      },
      {
        "toCreate": "astronaut",
        "with": "human"
      },
      {
        "toCreate": "solar_system",
        "with": "container"
      },
      {
        "toCreate": "telescope",
        "with": "glass"
      }
    ],
    "name": "Mars"
  },
  "marshmallows": {
    "combine": [],
    "name": "Marshmallows"
  },
  "mayonnaise": {
    "combine": [],
    "name": "Mayonnaise"
  },
  "meat": {
    "combine": [
      {
        "toCreate": "bbq",
        "with": "campfire"
      },
      {
        "toCreate": "bone",
        "with": "wolf"
      },
      {
        "toCreate": "bone",
        "with": "vulture"
      },
      {
        "toCreate": "butcher",
        "with": "human"
      },
      {
        "toCreate": "coconut",
        "with": "palm"
      },
      {
        "toCreate": "ham",
        "with": "smoke"
      },
      {
        "toCreate": "hamburger",
        "with": "bread"
      },
      {
        "toCreate": "hamburger",
        "with": "cheese"
      },
      {
        "toCreate": "jerky",
        "with": "sun"
      },
      {
        "toCreate": "jerky",
        "with": "heat"
      },
      {
        "toCreate": "pie",
        "with": "dough"
      },
      {
        "toCreate": "recipe",
        "with": "paper"
      },
      {
        "toCreate": "recipe",
        "with": "newspaper"
      },
      {
        "toCreate": "sandwich",
        "with": "bread"
      },
      {
        "toCreate": "steak",
        "with": "bbq"
      },
      {
        "toCreate": "steak",
        "with": "fire"
      },
      {
        "toCreate": "tyrannosaurus_rex",
        "with": "dinosaur"
      }
    ],
    "name": "Meat"
  },
  "medusa": {
    "combine": [
      {
        "toCreate": "statue",
        "with": "human"
      },
      {
        "toCreate": "statue",
        "with": "hacker"
      },
      {
        "toCreate": "statue",
        "with": "engineer"
      },
      {
        "toCreate": "statue",
        "with": "lumberjack"
      },
      {
        "toCreate": "statue",
        "with": "pilot"
      },
      {
        "toCreate": "statue",
        "with": "sailor"
      },
      {
        "toCreate": "statue",
        "with": "firefighter"
      },
      {
        "toCreate": "statue",
        "with": "farmer"
      },
      {
        "toCreate": "statue",
        "with": "surfer"
      },
      {
        "toCreate": "statue",
        "with": "astronaut"
      },
      {
        "toCreate": "statue",
        "with": "butcher"
      },
      {
        "toCreate": "statue",
        "with": "doctor"
      },
      {
        "toCreate": "statue",
        "with": "baker"
      },
      {
        "toCreate": "statue",
        "with": "drunk"
      },
      {
        "toCreate": "statue",
        "with": "mirror"
      }
    ],
    "name": "Medusa"
  },
  "mercury": {
    "combine": [
      {
        "toCreate": "alien",
        "with": "life"
      },
      {
        "toCreate": "astronaut",
        "with": "human"
      },
      {
        "toCreate": "solar_system",
        "with": "container"
      },
      {
        "toCreate": "telescope",
        "with": "glass"
      }
    ],
    "name": "Mercury"
  },
  "mermaid": {
    "combine": [],
    "name": "Mermaid"
  },
  "metal": {
    "combine": [
      {
        "toCreate": "airplane",
        "with": "bird"
      },
      {
        "toCreate": "airplane",
        "with": "owl"
      },
      {
        "toCreate": "airplane",
        "with": "vulture"
      },
      {
        "toCreate": "airplane",
        "with": "duck"
      },
      {
        "toCreate": "airplane",
        "with": "seagull"
      },
      {
        "toCreate": "airplane",
        "with": "bat"
      },
      {
        "toCreate": "airplane",
        "with": "eagle"
      },
      {
        "toCreate": "airplane",
        "with": "pigeon"
      },
      {
        "toCreate": "airplane",
        "with": "hummingbird"
      },
      {
        "toCreate": "airplane",
        "with": "crow"
      },
      {
        "toCreate": "armor",
        "with": "fabric"
      },
      {
        "toCreate": "bbq",
        "with": "campfire"
      },
      {
        "toCreate": "bell",
        "with": "sound"
      },
      {
        "toCreate": "bell",
        "with": "hammer"
      },
      {
        "toCreate": "blade",
        "with": "stone"
      },
      {
        "toCreate": "blade",
        "with": "rock"
      },
      {
        "toCreate": "boiler",
        "with": "pressure"
      },
      {
        "toCreate": "boiler",
        "with": "steam"
      },
      {
        "toCreate": "bridge",
        "with": "river"
      },
      {
        "toCreate": "bridge",
        "with": "stream"
      },
      {
        "toCreate": "bullet",
        "with": "gunpowder"
      },
      {
        "toCreate": "cage",
        "with": "wolf"
      },
      {
        "toCreate": "cage",
        "with": "fox"
      },
      {
        "toCreate": "cage",
        "with": "lion"
      },
      {
        "toCreate": "car",
        "with": "wheel"
      },
      {
        "toCreate": "cauldron",
        "with": "witch"
      },
      {
        "toCreate": "cauldron",
        "with": "pottery"
      },
      {
        "toCreate": "chain",
        "with": "rope"
      },
      {
        "toCreate": "chain",
        "with": "wire"
      },
      {
        "toCreate": "electricity",
        "with": "lightning"
      },
      {
        "toCreate": "fire_extinguisher",
        "with": "carbon_dioxide"
      },
      {
        "toCreate": "fridge",
        "with": "cold"
      },
      {
        "toCreate": "fridge",
        "with": "ice"
      },
      {
        "toCreate": "glasses",
        "with": "glass"
      },
      {
        "toCreate": "gold",
        "with": "sun"
      },
      {
        "toCreate": "gold",
        "with": "rainbow"
      },
      {
        "toCreate": "gold",
        "with": "butter"
      },
      {
        "toCreate": "gold",
        "with": "sand"
      },
      {
        "toCreate": "gold",
        "with": "light"
      },
      {
        "toCreate": "gold",
        "with": "alchemist"
      },
      {
        "toCreate": "grenade",
        "with": "explosion"
      },
      {
        "toCreate": "gun",
        "with": "bullet"
      },
      {
        "toCreate": "gun",
        "with": "bow"
      },
      {
        "toCreate": "hammer",
        "with": "tool"
      },
      {
        "toCreate": "horseshoe",
        "with": "horse"
      },
      {
        "toCreate": "lamp",
        "with": "light_bulb"
      },
      {
        "toCreate": "mailbox",
        "with": "letter"
      },
      {
        "toCreate": "mailbox",
        "with": "mailman"
      },
      {
        "toCreate": "mirror",
        "with": "glass"
      },
      {
        "toCreate": "motorcycle",
        "with": "bicycle"
      },
      {
        "toCreate": "mousetrap",
        "with": "cheese"
      },
      {
        "toCreate": "needle",
        "with": "thread"
      },
      {
        "toCreate": "pitchfork",
        "with": "hay"
      },
      {
        "toCreate": "plow",
        "with": "earth"
      },
      {
        "toCreate": "plow",
        "with": "field"
      },
      {
        "toCreate": "quicksilver",
        "with": "liquid"
      },
      {
        "toCreate": "ring",
        "with": "diamond"
      },
      {
        "toCreate": "ring",
        "with": "love"
      },
      {
        "toCreate": "robot",
        "with": "life"
      },
      {
        "toCreate": "robot",
        "with": "golem"
      },
      {
        "toCreate": "rocket",
        "with": "atmosphere"
      },
      {
        "toCreate": "rust",
        "with": "air"
      },
      {
        "toCreate": "rust",
        "with": "wind"
      },
      {
        "toCreate": "rust",
        "with": "oxygen"
      },
      {
        "toCreate": "safe",
        "with": "money"
      },
      {
        "toCreate": "safe",
        "with": "gold"
      },
      {
        "toCreate": "shuriken",
        "with": "star"
      },
      {
        "toCreate": "spaceship",
        "with": "space"
      },
      {
        "toCreate": "spotlight",
        "with": "light"
      },
      {
        "toCreate": "steel",
        "with": "charcoal"
      },
      {
        "toCreate": "steel",
        "with": "coal"
      },
      {
        "toCreate": "steel",
        "with": "mineral"
      },
      {
        "toCreate": "steel",
        "with": "ash"
      },
      {
        "toCreate": "steel_wool",
        "with": "wool"
      },
      {
        "toCreate": "sword",
        "with": "blade"
      },
      {
        "toCreate": "tank",
        "with": "car"
      },
      {
        "toCreate": "tool",
        "with": "human"
      },
      {
        "toCreate": "tool",
        "with": "wood"
      },
      {
        "toCreate": "train",
        "with": "steam_engine"
      },
      {
        "toCreate": "wheel",
        "with": "motion"
      },
      {
        "toCreate": "wire",
        "with": "electricity"
      },
      {
        "toCreate": "wire",
        "with": "rope"
      }
    ],
    "name": "Metal"
  },
  "meteor": {
    "combine": [
      {
        "toCreate": "tsunami",
        "with": "ocean"
      },
      {
        "toCreate": "tsunami",
        "with": "sea"
      }
    ],
    "name": "Meteor"
  },
  "meteoroid": {
    "combine": [
      {
        "toCreate": "meteor",
        "with": "atmosphere"
      },
      {
        "toCreate": "meteor",
        "with": "sky"
      },
      {
        "toCreate": "meteor",
        "with": "air"
      },
      {
        "toCreate": "meteor",
        "with": "night"
      },
      {
        "toCreate": "meteor",
        "with": "day"
      }
    ],
    "name": "Meteoroid"
  },
  "microscope": {
    "combine": [
      {
        "toCreate": "science",
        "with": "human"
      }
    ],
    "name": "Microscope"
  },
  "milk": {
    "combine": [
      {
        "toCreate": "batter",
        "with": "flour"
      },
      {
        "toCreate": "bottle",
        "with": "container"
      },
      {
        "toCreate": "butter",
        "with": "pressure"
      },
      {
        "toCreate": "butter",
        "with": "energy"
      },
      {
        "toCreate": "butter",
        "with": "motion"
      },
      {
        "toCreate": "butter",
        "with": "tool"
      },
      {
        "toCreate": "cat",
        "with": "animal"
      },
      {
        "toCreate": "cereal",
        "with": "wheat"
      },
      {
        "toCreate": "cheese",
        "with": "tool"
      },
      {
        "toCreate": "cheese",
        "with": "time"
      },
      {
        "toCreate": "cheese",
        "with": "cook"
      },
      {
        "toCreate": "cheese",
        "with": "bacteria"
      },
      {
        "toCreate": "chocolate",
        "with": "sugar"
      },
      {
        "toCreate": "chocolate_milk",
        "with": "chocolate"
      },
      {
        "toCreate": "coconut_milk",
        "with": "coconut"
      },
      {
        "toCreate": "gift",
        "with": "santa"
      },
      {
        "toCreate": "ice_cream",
        "with": "cold"
      },
      {
        "toCreate": "ice_cream",
        "with": "ice"
      },
      {
        "toCreate": "ice_cream",
        "with": "snow"
      },
      {
        "toCreate": "milk_shake",
        "with": "ice_cream"
      },
      {
        "toCreate": "yogurt",
        "with": "bacteria"
      }
    ],
    "name": "Milk"
  },
  "milk_shake": {
    "combine": [
      {
        "toCreate": "cup",
        "with": "bottle"
      },
      {
        "toCreate": "cup",
        "with": "container"
      }
    ],
    "name": "Milk Shake"
  },
  "mineral": {
    "combine": [
      {
        "toCreate": "ash",
        "with": "fire"
      },
      {
        "toCreate": "battery",
        "with": "electricity"
      },
      {
        "toCreate": "clay",
        "with": "sand"
      },
      {
        "toCreate": "clay",
        "with": "stone"
      },
      {
        "toCreate": "clay",
        "with": "rock"
      },
      {
        "toCreate": "gunpowder",
        "with": "charcoal"
      },
      {
        "toCreate": "gunpowder",
        "with": "energy"
      },
      {
        "toCreate": "salt",
        "with": "ocean"
      },
      {
        "toCreate": "salt",
        "with": "sea"
      },
      {
        "toCreate": "steel",
        "with": "metal"
      }
    ],
    "name": "Mineral"
  },
  "minotaur": {
    "combine": [],
    "name": "Minotaur"
  },
  "mirror": {
    "combine": [
      {
        "toCreate": "statue",
        "with": "medusa"
      }
    ],
    "name": "Mirror"
  },
  "mist": {
    "combine": [
      {
        "toCreate": "cloud",
        "with": "atmosphere"
      },
      {
        "toCreate": "cloud",
        "with": "sky"
      }
    ],
    "name": "Mist"
  },
  "mold": {
    "combine": [
      {
        "toCreate": "penicillin",
        "with": "doctor"
      },
      {
        "toCreate": "penicillin",
        "with": "science"
      },
      {
        "toCreate": "penicillin",
        "with": "idea"
      },
      {
        "toCreate": "penicillin",
        "with": "hospital"
      }
    ],
    "name": "Mold"
  },
  "monarch": {
    "combine": [
      {
        "toCreate": "castle",
        "with": "container"
      },
      {
        "toCreate": "castle",
        "with": "house"
      },
      {
        "toCreate": "castle",
        "with": "wall"
      },
      {
        "toCreate": "excalibur",
        "with": "sword"
      },
      {
        "toCreate": "fairy_tale",
        "with": "story"
      },
      {
        "toCreate": "idea",
        "with": "monarch"
      },
      {
        "toCreate": "lion",
        "with": "cat"
      },
      {
        "toCreate": "tyrannosaurus_rex",
        "with": "dinosaur"
      }
    ],
    "name": "Monarch"
  },
  "money": {
    "combine": [
      {
        "toCreate": "bank",
        "with": "house"
      },
      {
        "toCreate": "bank",
        "with": "skyscraper"
      },
      {
        "toCreate": "bank",
        "with": "city"
      },
      {
        "toCreate": "bank",
        "with": "container"
      },
      {
        "toCreate": "piggy_bank",
        "with": "pig"
      },
      {
        "toCreate": "safe",
        "with": "metal"
      },
      {
        "toCreate": "safe",
        "with": "steel"
      }
    ],
    "name": "Money"
  },
  "monkey": {
    "combine": [
      {
        "toCreate": "banana",
        "with": "fruit"
      },
      {
        "toCreate": "human",
        "with": "time"
      },
      {
        "toCreate": "human",
        "with": "tool"
      }
    ],
    "name": "Monkey"
  },
  "moon": {
    "combine": [
      {
        "toCreate": "astronaut",
        "with": "human"
      },
      {
        "toCreate": "eclipse",
        "with": "sun"
      },
      {
        "toCreate": "moon_rover",
        "with": "car"
      },
      {
        "toCreate": "moon_rover",
        "with": "electric_car"
      },
      {
        "toCreate": "moth",
        "with": "butterfly"
      },
      {
        "toCreate": "night",
        "with": "sky"
      },
      {
        "toCreate": "night",
        "with": "time"
      },
      {
        "toCreate": "owl",
        "with": "bird"
      },
      {
        "toCreate": "sky",
        "with": "sun"
      },
      {
        "toCreate": "space",
        "with": "star"
      },
      {
        "toCreate": "telescope",
        "with": "glass"
      },
      {
        "toCreate": "tide",
        "with": "ocean"
      },
      {
        "toCreate": "tide",
        "with": "sea"
      },
      {
        "toCreate": "wolf",
        "with": "animal"
      }
    ],
    "name": "Moon"
  },
  "moon_rover": {
    "combine": [
      {
        "toCreate": "astronaut",
        "with": "human"
      }
    ],
    "name": "Moon Rover"
  },
  "moss": {
    "combine": [],
    "name": "Moss"
  },
  "moth": {
    "combine": [
      {
        "toCreate": "egg",
        "with": "moth"
      }
    ],
    "name": "Moth"
  },
  "motion": {
    "combine": [
      {
        "toCreate": "blender",
        "with": "blade"
      },
      {
        "toCreate": "butter",
        "with": "milk"
      },
      {
        "toCreate": "chainsaw",
        "with": "axe"
      },
      {
        "toCreate": "current",
        "with": "ocean"
      },
      {
        "toCreate": "current",
        "with": "sea"
      },
      {
        "toCreate": "earthquake",
        "with": "earth"
      },
      {
        "toCreate": "earthquake",
        "with": "continent"
      },
      {
        "toCreate": "electricity",
        "with": "wind_turbine"
      },
      {
        "toCreate": "lawn_mower",
        "with": "scythe"
      },
      {
        "toCreate": "river",
        "with": "lake"
      },
      {
        "toCreate": "rivulet",
        "with": "puddle"
      },
      {
        "toCreate": "sandstorm",
        "with": "sand"
      },
      {
        "toCreate": "sandstorm",
        "with": "desert"
      },
      {
        "toCreate": "stream",
        "with": "pond"
      },
      {
        "toCreate": "tornado",
        "with": "storm"
      },
      {
        "toCreate": "tornado",
        "with": "wind"
      },
      {
        "toCreate": "wheel",
        "with": "tool"
      },
      {
        "toCreate": "wheel",
        "with": "stone"
      },
      {
        "toCreate": "wheel",
        "with": "metal"
      },
      {
        "toCreate": "wheel",
        "with": "steel"
      },
      {
        "toCreate": "wind",
        "with": "air"
      },
      {
        "toCreate": "wind",
        "with": "atmosphere"
      }
    ],
    "name": "Motion"
  },
  "motorcycle": {
    "combine": [
      {
        "toCreate": "car",
        "with": "motorcycle"
      },
      {
        "toCreate": "car",
        "with": "wheel"
      },
      {
        "toCreate": "car",
        "with": "machine"
      },
      {
        "toCreate": "garage",
        "with": "house"
      },
      {
        "toCreate": "garage",
        "with": "container"
      },
      {
        "toCreate": "garage",
        "with": "wall"
      },
      {
        "toCreate": "garage",
        "with": "barn"
      },
      {
        "toCreate": "snowmobile",
        "with": "snow"
      },
      {
        "toCreate": "snowmobile",
        "with": "glacier"
      },
      {
        "toCreate": "snowmobile",
        "with": "ice"
      }
    ],
    "name": "Motorcycle"
  },
  "mountain": {
    "combine": [
      {
        "toCreate": "alpaca",
        "with": "sheep"
      },
      {
        "toCreate": "animal",
        "with": "life"
      },
      {
        "toCreate": "avalanche",
        "with": "earthquake"
      },
      {
        "toCreate": "avalanche",
        "with": "wave"
      },
      {
        "toCreate": "avalanche",
        "with": "sound"
      },
      {
        "toCreate": "avalanche",
        "with": "gun"
      },
      {
        "toCreate": "cable_car",
        "with": "car"
      },
      {
        "toCreate": "cable_car",
        "with": "wire"
      },
      {
        "toCreate": "cable_car",
        "with": "rope"
      },
      {
        "toCreate": "cold",
        "with": "human"
      },
      {
        "toCreate": "eagle",
        "with": "bird"
      },
      {
        "toCreate": "fog",
        "with": "cloud"
      },
      {
        "toCreate": "geyser",
        "with": "steam"
      },
      {
        "toCreate": "glacier",
        "with": "ice"
      },
      {
        "toCreate": "glacier",
        "with": "snow"
      },
      {
        "toCreate": "goat",
        "with": "livestock"
      },
      {
        "toCreate": "goat",
        "with": "cow"
      },
      {
        "toCreate": "hill",
        "with": "small"
      },
      {
        "toCreate": "map",
        "with": "paper"
      },
      {
        "toCreate": "mineral",
        "with": "organic_matter"
      },
      {
        "toCreate": "mountain_goat",
        "with": "goat"
      },
      {
        "toCreate": "mountain_range",
        "with": "mountain"
      },
      {
        "toCreate": "mountain_range",
        "with": "continent"
      },
      {
        "toCreate": "ore",
        "with": "hammer"
      },
      {
        "toCreate": "pyramid",
        "with": "desert"
      },
      {
        "toCreate": "river",
        "with": "water"
      },
      {
        "toCreate": "river",
        "with": "rain"
      },
      {
        "toCreate": "skier",
        "with": "human"
      },
      {
        "toCreate": "snow",
        "with": "rain"
      },
      {
        "toCreate": "snowboard",
        "with": "surfer"
      },
      {
        "toCreate": "tunnel",
        "with": "engineer"
      },
      {
        "toCreate": "tunnel",
        "with": "train"
      },
      {
        "toCreate": "tunnel",
        "with": "cave"
      },
      {
        "toCreate": "volcano",
        "with": "lava"
      },
      {
        "toCreate": "volcano",
        "with": "fire"
      },
      {
        "toCreate": "volcano",
        "with": "pressure"
      },
      {
        "toCreate": "waterfall",
        "with": "river"
      },
      {
        "toCreate": "waterfall",
        "with": "lake"
      },
      {
        "toCreate": "yeti",
        "with": "story"
      },
      {
        "toCreate": "yeti",
        "with": "legend"
      }
    ],
    "name": "Mountain"
  },
  "mountain_goat": {
    "combine": [
      {
        "toCreate": "alpaca",
        "with": "sheep"
      },
      {
        "toCreate": "cashmere",
        "with": "tool"
      },
      {
        "toCreate": "cashmere",
        "with": "thread"
      },
      {
        "toCreate": "cashmere",
        "with": "fabric"
      },
      {
        "toCreate": "cashmere",
        "with": "scissors"
      },
      {
        "toCreate": "cashmere",
        "with": "wool"
      },
      {
        "toCreate": "faun",
        "with": "human"
      },
      {
        "toCreate": "faun",
        "with": "legend"
      },
      {
        "toCreate": "faun",
        "with": "wizard"
      },
      {
        "toCreate": "faun",
        "with": "magic"
      },
      {
        "toCreate": "sheep",
        "with": "domestication"
      }
    ],
    "name": "Mountain Goat"
  },
  "mountain_range": {
    "combine": [
      {
        "toCreate": "alpaca",
        "with": "sheep"
      },
      {
        "toCreate": "animal",
        "with": "life"
      },
      {
        "toCreate": "avalanche",
        "with": "earthquake"
      },
      {
        "toCreate": "avalanche",
        "with": "wave"
      },
      {
        "toCreate": "avalanche",
        "with": "gun"
      },
      {
        "toCreate": "cable_car",
        "with": "car"
      },
      {
        "toCreate": "cable_car",
        "with": "wire"
      },
      {
        "toCreate": "cable_car",
        "with": "rope"
      },
      {
        "toCreate": "cold",
        "with": "human"
      },
      {
        "toCreate": "continent",
        "with": "mountain_range"
      },
      {
        "toCreate": "eagle",
        "with": "bird"
      },
      {
        "toCreate": "glacier",
        "with": "ice"
      },
      {
        "toCreate": "glacier",
        "with": "snow"
      },
      {
        "toCreate": "goat",
        "with": "livestock"
      },
      {
        "toCreate": "goat",
        "with": "cow"
      },
      {
        "toCreate": "map",
        "with": "paper"
      },
      {
        "toCreate": "mountain_goat",
        "with": "goat"
      },
      {
        "toCreate": "skier",
        "with": "human"
      },
      {
        "toCreate": "snow",
        "with": "rain"
      },
      {
        "toCreate": "snowboard",
        "with": "surfer"
      },
      {
        "toCreate": "tunnel",
        "with": "engineer"
      },
      {
        "toCreate": "tunnel",
        "with": "train"
      },
      {
        "toCreate": "tunnel",
        "with": "cave"
      },
      {
        "toCreate": "waterfall",
        "with": "river"
      },
      {
        "toCreate": "waterfall",
        "with": "lake"
      },
      {
        "toCreate": "yeti",
        "with": "story"
      },
      {
        "toCreate": "yeti",
        "with": "legend"
      }
    ],
    "name": "Mountain Range"
  },
  "mouse": {
    "combine": [
      {
        "toCreate": "bat",
        "with": "sky"
      },
      {
        "toCreate": "bat",
        "with": "air"
      },
      {
        "toCreate": "bat",
        "with": "atmosphere"
      },
      {
        "toCreate": "bat",
        "with": "bird"
      },
      {
        "toCreate": "bat",
        "with": "hummingbird"
      },
      {
        "toCreate": "computer_mouse",
        "with": "computer"
      },
      {
        "toCreate": "hamster",
        "with": "wheel"
      },
      {
        "toCreate": "hamster",
        "with": "domestication"
      },
      {
        "toCreate": "hedgehog",
        "with": "needle"
      },
      {
        "toCreate": "mousetrap",
        "with": "blade"
      },
      {
        "toCreate": "rat",
        "with": "pirate_ship"
      },
      {
        "toCreate": "rat",
        "with": "sailboat"
      },
      {
        "toCreate": "rat",
        "with": "city"
      },
      {
        "toCreate": "rat",
        "with": "skyscraper"
      },
      {
        "toCreate": "rat",
        "with": "village"
      },
      {
        "toCreate": "rat",
        "with": "big"
      },
      {
        "toCreate": "squirrel",
        "with": "tree"
      },
      {
        "toCreate": "squirrel",
        "with": "plant"
      },
      {
        "toCreate": "squirrel",
        "with": "nuts"
      }
    ],
    "name": "Mouse"
  },
  "mousetrap": {
    "combine": [],
    "name": "Mousetrap"
  },
  "mud": {
    "combine": [
      {
        "toCreate": "bacteria",
        "with": "life"
      },
      {
        "toCreate": "brick",
        "with": "fire"
      },
      {
        "toCreate": "brick",
        "with": "sun"
      },
      {
        "toCreate": "clay",
        "with": "sand"
      },
      {
        "toCreate": "clay",
        "with": "stone"
      },
      {
        "toCreate": "pig",
        "with": "livestock"
      },
      {
        "toCreate": "pig",
        "with": "animal"
      },
      {
        "toCreate": "pig",
        "with": "cow"
      },
      {
        "toCreate": "swamp",
        "with": "grass"
      },
      {
        "toCreate": "swamp",
        "with": "tree"
      }
    ],
    "name": "Mud"
  },
  "mummy": {
    "combine": [
      {
        "toCreate": "pyramid",
        "with": "container"
      }
    ],
    "name": "Mummy"
  },
  "music": {
    "combine": [
      {
        "toCreate": "drum",
        "with": "wood"
      },
      {
        "toCreate": "harp",
        "with": "angel"
      },
      {
        "toCreate": "harp",
        "with": "wire"
      },
      {
        "toCreate": "musician",
        "with": "human"
      },
      {
        "toCreate": "sheet_music",
        "with": "paper"
      },
      {
        "toCreate": "sheet_music",
        "with": "book"
      }
    ],
    "name": "Music"
  },
  "musician": {
    "combine": [
      {
        "toCreate": "harp",
        "with": "angel"
      },
      {
        "toCreate": "music",
        "with": "flute"
      },
      {
        "toCreate": "music",
        "with": "sheet_music"
      },
      {
        "toCreate": "music",
        "with": "drum"
      },
      {
        "toCreate": "music",
        "with": "pan_flute"
      }
    ],
    "name": "Musician"
  },
  "narwhal": {
    "combine": [],
    "name": "Narwhal"
  },
  "needle": {
    "combine": [
      {
        "toCreate": "hedgehog",
        "with": "animal"
      },
      {
        "toCreate": "hedgehog",
        "with": "mouse"
      },
      {
        "toCreate": "hedgehog",
        "with": "rat"
      },
      {
        "toCreate": "sewing_machine",
        "with": "machine"
      },
      {
        "toCreate": "sewing_machine",
        "with": "electricity"
      },
      {
        "toCreate": "sewing_machine",
        "with": "robot"
      },
      {
        "toCreate": "syringe",
        "with": "doctor"
      },
      {
        "toCreate": "syringe",
        "with": "tool"
      }
    ],
    "name": "Needle"
  },
  "nessie": {
    "combine": [],
    "name": "Nessie"
  },
  "nest": {
    "combine": [
      {
        "toCreate": "tree",
        "with": "container"
      }
    ],
    "name": "Nest"
  },
  "net": {
    "combine": [
      {
        "toCreate": "butterfly_net",
        "with": "butterfly"
      },
      {
        "toCreate": "butterfly_net",
        "with": "bee"
      },
      {
        "toCreate": "internet",
        "with": "computer"
      },
      {
        "toCreate": "meat",
        "with": "fish"
      },
      {
        "toCreate": "meat",
        "with": "swordfish"
      },
      {
        "toCreate": "meat",
        "with": "shark"
      },
      {
        "toCreate": "meat",
        "with": "piranha"
      },
      {
        "toCreate": "meat",
        "with": "flying_fish"
      },
      {
        "toCreate": "spider",
        "with": "animal"
      },
      {
        "toCreate": "web",
        "with": "spider"
      }
    ],
    "name": "Net"
  },
  "newspaper": {
    "combine": [
      {
        "toCreate": "book",
        "with": "newspaper"
      },
      {
        "toCreate": "mailman",
        "with": "human"
      },
      {
        "toCreate": "printer",
        "with": "computer"
      },
      {
        "toCreate": "recipe",
        "with": "flour"
      },
      {
        "toCreate": "recipe",
        "with": "meat"
      },
      {
        "toCreate": "recipe",
        "with": "baker"
      },
      {
        "toCreate": "recipe",
        "with": "vegetable"
      },
      {
        "toCreate": "recipe",
        "with": "bakery"
      },
      {
        "toCreate": "recipe",
        "with": "vegetable"
      },
      {
        "toCreate": "recipe",
        "with": "fruit"
      }
    ],
    "name": "Newspaper"
  },
  "night": {
    "combine": [
      {
        "toCreate": "carbon_dioxide",
        "with": "plant"
      },
      {
        "toCreate": "carbon_dioxide",
        "with": "tree"
      },
      {
        "toCreate": "carbon_dioxide",
        "with": "grass"
      },
      {
        "toCreate": "cat",
        "with": "animal"
      },
      {
        "toCreate": "darkness",
        "with": "sky"
      },
      {
        "toCreate": "dawn",
        "with": "time"
      },
      {
        "toCreate": "dawn",
        "with": "day"
      },
      {
        "toCreate": "day",
        "with": "sun"
      },
      {
        "toCreate": "ghost",
        "with": "graveyard"
      },
      {
        "toCreate": "ghost",
        "with": "grave"
      },
      {
        "toCreate": "ghost",
        "with": "gravestone"
      },
      {
        "toCreate": "ghost",
        "with": "castle"
      },
      {
        "toCreate": "ghost",
        "with": "story"
      },
      {
        "toCreate": "ghost",
        "with": "legend"
      },
      {
        "toCreate": "jack-o-lantern",
        "with": "pumpkin"
      },
      {
        "toCreate": "jack-o-lantern",
        "with": "vegetable"
      },
      {
        "toCreate": "meteor",
        "with": "meteoroid"
      },
      {
        "toCreate": "moon",
        "with": "planet"
      },
      {
        "toCreate": "moon",
        "with": "stone"
      },
      {
        "toCreate": "moon",
        "with": "earth"
      },
      {
        "toCreate": "moon",
        "with": "cheese"
      },
      {
        "toCreate": "moth",
        "with": "butterfly"
      },
      {
        "toCreate": "owl",
        "with": "bird"
      },
      {
        "toCreate": "space",
        "with": "solar_system"
      },
      {
        "toCreate": "star",
        "with": "sky"
      },
      {
        "toCreate": "star",
        "with": "telescope"
      },
      {
        "toCreate": "star",
        "with": "space"
      },
      {
        "toCreate": "twilight",
        "with": "day"
      }
    ],
    "name": "Night"
  },
  "ninja": {
    "combine": [
      {
        "toCreate": "katana",
        "with": "blade"
      },
      {
        "toCreate": "katana",
        "with": "sword"
      },
      {
        "toCreate": "ninja_turtle",
        "with": "turtle"
      },
      {
        "toCreate": "shuriken",
        "with": "blade"
      }
    ],
    "name": "Ninja"
  },
  "ninja_turtle": {
    "combine": [],
    "name": "Ninja Turtle"
  },
  "nuts": {
    "combine": [
      {
        "toCreate": "coconut",
        "with": "palm"
      },
      {
        "toCreate": "cook",
        "with": "human"
      },
      {
        "toCreate": "peanut_butter",
        "with": "butter"
      },
      {
        "toCreate": "peanut_butter",
        "with": "pressure"
      },
      {
        "toCreate": "squirrel",
        "with": "mouse"
      },
      {
        "toCreate": "squirrel",
        "with": "animal"
      }
    ],
    "name": "Nuts"
  },
  "oasis": {
    "combine": [],
    "name": "Oasis"
  },
  "obsidian": {
    "combine": [],
    "name": "Obsidian"
  },
  "ocean": {
    "combine": [
      {
        "toCreate": "archipelago",
        "with": "island"
      },
      {
        "toCreate": "arctic",
        "with": "cold"
      },
      {
        "toCreate": "beach",
        "with": "sand"
      },
      {
        "toCreate": "boat",
        "with": "wood"
      },
      {
        "toCreate": "coral",
        "with": "tree"
      },
      {
        "toCreate": "coral",
        "with": "fossil"
      },
      {
        "toCreate": "coral",
        "with": "bone"
      },
      {
        "toCreate": "current",
        "with": "motion"
      },
      {
        "toCreate": "current",
        "with": "heat"
      },
      {
        "toCreate": "current",
        "with": "cold"
      },
      {
        "toCreate": "current",
        "with": "science"
      },
      {
        "toCreate": "diver",
        "with": "scuba_tank"
      },
      {
        "toCreate": "fish",
        "with": "animal"
      },
      {
        "toCreate": "fish",
        "with": "egg"
      },
      {
        "toCreate": "horizon",
        "with": "sky"
      },
      {
        "toCreate": "hurricane",
        "with": "tornado"
      },
      {
        "toCreate": "hurricane",
        "with": "storm"
      },
      {
        "toCreate": "iceberg",
        "with": "ice"
      },
      {
        "toCreate": "iceberg",
        "with": "antarctica"
      },
      {
        "toCreate": "iceberg",
        "with": "arctic"
      },
      {
        "toCreate": "island",
        "with": "volcano"
      },
      {
        "toCreate": "life",
        "with": "electricity"
      },
      {
        "toCreate": "life",
        "with": "lightning"
      },
      {
        "toCreate": "lighthouse",
        "with": "light"
      },
      {
        "toCreate": "lighthouse",
        "with": "spotlight"
      },
      {
        "toCreate": "manatee",
        "with": "cow"
      },
      {
        "toCreate": "map",
        "with": "paper"
      },
      {
        "toCreate": "narwhal",
        "with": "unicorn"
      },
      {
        "toCreate": "planet",
        "with": "continent"
      },
      {
        "toCreate": "plankton",
        "with": "life"
      },
      {
        "toCreate": "plankton",
        "with": "bacteria"
      },
      {
        "toCreate": "pressure",
        "with": "ocean"
      },
      {
        "toCreate": "primordial_soup",
        "with": "lava"
      },
      {
        "toCreate": "primordial_soup",
        "with": "planet"
      },
      {
        "toCreate": "primordial_soup",
        "with": "earth"
      },
      {
        "toCreate": "roe",
        "with": "egg"
      },
      {
        "toCreate": "sailor",
        "with": "human"
      },
      {
        "toCreate": "salt",
        "with": "sun"
      },
      {
        "toCreate": "salt",
        "with": "fire"
      },
      {
        "toCreate": "salt",
        "with": "mineral"
      },
      {
        "toCreate": "sea",
        "with": "small"
      },
      {
        "toCreate": "seagull",
        "with": "bird"
      },
      {
        "toCreate": "seagull",
        "with": "pigeon"
      },
      {
        "toCreate": "seagull",
        "with": "rat"
      },
      {
        "toCreate": "seahorse",
        "with": "horse"
      },
      {
        "toCreate": "seal",
        "with": "dog"
      },
      {
        "toCreate": "seaplane",
        "with": "airplane"
      },
      {
        "toCreate": "seasickness",
        "with": "sickness"
      },
      {
        "toCreate": "seaweed",
        "with": "plant"
      },
      {
        "toCreate": "seaweed",
        "with": "grass"
      },
      {
        "toCreate": "shark",
        "with": "blood"
      },
      {
        "toCreate": "shark",
        "with": "wolf"
      },
      {
        "toCreate": "starfish",
        "with": "star"
      },
      {
        "toCreate": "steamboat",
        "with": "steam_engine"
      },
      {
        "toCreate": "swim_goggles",
        "with": "glasses"
      },
      {
        "toCreate": "swim_goggles",
        "with": "sunglasses"
      },
      {
        "toCreate": "tide",
        "with": "moon"
      },
      {
        "toCreate": "tide",
        "with": "time"
      },
      {
        "toCreate": "tsunami",
        "with": "earthquake"
      },
      {
        "toCreate": "tsunami",
        "with": "explosion"
      },
      {
        "toCreate": "tsunami",
        "with": "meteor"
      },
      {
        "toCreate": "wave",
        "with": "wind"
      },
      {
        "toCreate": "wave",
        "with": "storm"
      },
      {
        "toCreate": "wave",
        "with": "hurricane"
      }
    ],
    "name": "Ocean"
  },
  "oil": {
    "combine": [
      {
        "toCreate": "bottle",
        "with": "container"
      },
      {
        "toCreate": "chocolate",
        "with": "sugar"
      },
      {
        "toCreate": "donut",
        "with": "dough"
      },
      {
        "toCreate": "donut",
        "with": "cookie_dough"
      },
      {
        "toCreate": "french_fries",
        "with": "vegetable"
      },
      {
        "toCreate": "french_fries",
        "with": "potato"
      },
      {
        "toCreate": "mayonnaise",
        "with": "egg"
      },
      {
        "toCreate": "soap",
        "with": "ash"
      },
      {
        "toCreate": "soap",
        "with": "wax"
      },
      {
        "toCreate": "soap",
        "with": "clay"
      }
    ],
    "name": "Oil"
  },
  "omelette": {
    "combine": [],
    "name": "Omelette"
  },
  "optical_fiber": {
    "combine": [],
    "name": "Optical Fiber"
  },
  "orchard": {
    "combine": [
      {
        "toCreate": "fruit",
        "with": "farmer"
      },
      {
        "toCreate": "leaf",
        "with": "wind"
      }
    ],
    "name": "Orchard"
  },
  "ore": {
    "combine": [
      {
        "toCreate": "battery",
        "with": "electricity"
      },
      {
        "toCreate": "metal",
        "with": "fire"
      },
      {
        "toCreate": "metal",
        "with": "heat"
      },
      {
        "toCreate": "metal",
        "with": "tool"
      }
    ],
    "name": "Ore"
  },
  "organic_matter": {
    "combine": [
      {
        "toCreate": "charcoal",
        "with": "fire"
      },
      {
        "toCreate": "coal",
        "with": "pressure"
      },
      {
        "toCreate": "mineral",
        "with": "stone"
      },
      {
        "toCreate": "mineral",
        "with": "rock"
      },
      {
        "toCreate": "mineral",
        "with": "earth"
      },
      {
        "toCreate": "mineral",
        "with": "boulder"
      },
      {
        "toCreate": "mineral",
        "with": "hill"
      },
      {
        "toCreate": "mineral",
        "with": "mountain"
      },
      {
        "toCreate": "soil",
        "with": "earth"
      },
      {
        "toCreate": "soil",
        "with": "land"
      }
    ],
    "name": "Organic Matter"
  },
  "origami": {
    "combine": [],
    "name": "Origami"
  },
  "ostrich": {
    "combine": [
      {
        "toCreate": "egg",
        "with": "ostrich"
      }
    ],
    "name": "Ostrich"
  },
  "owl": {
    "combine": [
      {
        "toCreate": "airplane",
        "with": "metal"
      },
      {
        "toCreate": "airplane",
        "with": "steel"
      },
      {
        "toCreate": "airplane",
        "with": "steam_engine"
      },
      {
        "toCreate": "airplane",
        "with": "machine"
      },
      {
        "toCreate": "crow",
        "with": "scarecrow"
      },
      {
        "toCreate": "crow",
        "with": "field"
      },
      {
        "toCreate": "cuckoo",
        "with": "clock"
      },
      {
        "toCreate": "cuckoo",
        "with": "alarm_clock"
      },
      {
        "toCreate": "duck",
        "with": "water"
      },
      {
        "toCreate": "duck",
        "with": "pond"
      },
      {
        "toCreate": "duck",
        "with": "lake"
      },
      {
        "toCreate": "egg",
        "with": "owl"
      },
      {
        "toCreate": "hummingbird",
        "with": "flower"
      },
      {
        "toCreate": "hummingbird",
        "with": "garden"
      },
      {
        "toCreate": "origami",
        "with": "paper"
      },
      {
        "toCreate": "toucan",
        "with": "palm"
      },
      {
        "toCreate": "vulture",
        "with": "corpse"
      },
      {
        "toCreate": "vulture",
        "with": "desert"
      }
    ],
    "name": "Owl"
  },
  "oxygen": {
    "combine": [
      {
        "toCreate": "carbon_dioxide",
        "with": "human"
      },
      {
        "toCreate": "carbon_dioxide",
        "with": "animal"
      },
      {
        "toCreate": "carbon_dioxide",
        "with": "fish"
      },
      {
        "toCreate": "carbon_dioxide",
        "with": "bird"
      },
      {
        "toCreate": "ozone",
        "with": "oxygen"
      },
      {
        "toCreate": "ozone",
        "with": "electricity"
      },
      {
        "toCreate": "rust",
        "with": "metal"
      },
      {
        "toCreate": "rust",
        "with": "steel"
      },
      {
        "toCreate": "scuba_tank",
        "with": "container"
      },
      {
        "toCreate": "small",
        "with": "philosophy"
      },
      {
        "toCreate": "vinegar",
        "with": "wine"
      }
    ],
    "name": "Oxygen"
  },
  "ozone": {
    "combine": [
      {
        "toCreate": "small",
        "with": "philosophy"
      }
    ],
    "name": "Ozone"
  },
  "paint": {
    "combine": [
      {
        "toCreate": "bucket",
        "with": "container"
      },
      {
        "toCreate": "bucket",
        "with": "bottle"
      },
      {
        "toCreate": "canvas",
        "with": "fabric"
      },
      {
        "toCreate": "crayon",
        "with": "pencil"
      },
      {
        "toCreate": "crayon",
        "with": "wax"
      },
      {
        "toCreate": "painter",
        "with": "human"
      },
      {
        "toCreate": "painting",
        "with": "canvas"
      },
      {
        "toCreate": "sprinkles",
        "with": "sugar"
      }
    ],
    "name": "Paint"
  },
  "painter": {
    "combine": [
      {
        "toCreate": "painting",
        "with": "canvas"
      }
    ],
    "name": "Painter"
  },
  "painting": {
    "combine": [
      {
        "toCreate": "painter",
        "with": "human"
      }
    ],
    "name": "Painting"
  },
  "paleontologist": {
    "combine": [],
    "name": "Paleontologist"
  },
  "palm": {
    "combine": [
      {
        "toCreate": "coconut",
        "with": "fruit"
      },
      {
        "toCreate": "coconut",
        "with": "nuts"
      },
      {
        "toCreate": "coconut",
        "with": "vegetable"
      },
      {
        "toCreate": "coconut",
        "with": "meat"
      },
      {
        "toCreate": "oasis",
        "with": "desert"
      },
      {
        "toCreate": "toucan",
        "with": "bird"
      },
      {
        "toCreate": "toucan",
        "with": "seagull"
      },
      {
        "toCreate": "toucan",
        "with": "pigeon"
      },
      {
        "toCreate": "toucan",
        "with": "owl"
      }
    ],
    "name": "Palm"
  },
  "pan_flute": {
    "combine": [
      {
        "toCreate": "music",
        "with": "musician"
      },
      {
        "toCreate": "musician",
        "with": "human"
      }
    ],
    "name": "Pan Flute"
  },
  "paper": {
    "combine": [
      {
        "toCreate": "ash",
        "with": "phoenix"
      },
      {
        "toCreate": "ash",
        "with": "campfire"
      },
      {
        "toCreate": "ash",
        "with": "fire"
      },
      {
        "toCreate": "book",
        "with": "legend"
      },
      {
        "toCreate": "book",
        "with": "wood"
      },
      {
        "toCreate": "cigarette",
        "with": "tobacco"
      },
      {
        "toCreate": "confetti",
        "with": "scissors"
      },
      {
        "toCreate": "confetti",
        "with": "blade"
      },
      {
        "toCreate": "fortune_cookie",
        "with": "cookie"
      },
      {
        "toCreate": "fortune_cookie",
        "with": "cookie_dough"
      },
      {
        "toCreate": "fortune_cookie",
        "with": "gingerbread_man"
      },
      {
        "toCreate": "kite",
        "with": "wind"
      },
      {
        "toCreate": "kite",
        "with": "sky"
      },
      {
        "toCreate": "kite",
        "with": "air"
      },
      {
        "toCreate": "letter",
        "with": "pencil"
      },
      {
        "toCreate": "map",
        "with": "land"
      },
      {
        "toCreate": "map",
        "with": "hill"
      },
      {
        "toCreate": "map",
        "with": "mountain"
      },
      {
        "toCreate": "map",
        "with": "mountain_range"
      },
      {
        "toCreate": "map",
        "with": "continent"
      },
      {
        "toCreate": "map",
        "with": "ocean"
      },
      {
        "toCreate": "map",
        "with": "sea"
      },
      {
        "toCreate": "map",
        "with": "lake"
      },
      {
        "toCreate": "map",
        "with": "river"
      },
      {
        "toCreate": "map",
        "with": "city"
      },
      {
        "toCreate": "map",
        "with": "village"
      },
      {
        "toCreate": "money",
        "with": "gold"
      },
      {
        "toCreate": "money",
        "with": "diamond"
      },
      {
        "toCreate": "money",
        "with": "bank"
      },
      {
        "toCreate": "newspaper",
        "with": "paper"
      },
      {
        "toCreate": "newspaper",
        "with": "story"
      },
      {
        "toCreate": "origami",
        "with": "bird"
      },
      {
        "toCreate": "origami",
        "with": "animal"
      },
      {
        "toCreate": "origami",
        "with": "eagle"
      },
      {
        "toCreate": "origami",
        "with": "seagull"
      },
      {
        "toCreate": "origami",
        "with": "owl"
      },
      {
        "toCreate": "origami",
        "with": "pigeon"
      },
      {
        "toCreate": "origami",
        "with": "cow"
      },
      {
        "toCreate": "origami",
        "with": "horse"
      },
      {
        "toCreate": "origami",
        "with": "dog"
      },
      {
        "toCreate": "origami",
        "with": "cat"
      },
      {
        "toCreate": "origami",
        "with": "lion"
      },
      {
        "toCreate": "origami",
        "with": "hummingbird"
      },
      {
        "toCreate": "origami",
        "with": "chicken"
      },
      {
        "toCreate": "origami",
        "with": "penguin"
      },
      {
        "toCreate": "origami",
        "with": "duck"
      },
      {
        "toCreate": "origami",
        "with": "vulture"
      },
      {
        "toCreate": "paper_airplane",
        "with": "airplane"
      },
      {
        "toCreate": "paper_cup",
        "with": "cup"
      },
      {
        "toCreate": "printer",
        "with": "computer"
      },
      {
        "toCreate": "recipe",
        "with": "flour"
      },
      {
        "toCreate": "recipe",
        "with": "meat"
      },
      {
        "toCreate": "recipe",
        "with": "baker"
      },
      {
        "toCreate": "recipe",
        "with": "bakery"
      },
      {
        "toCreate": "recipe",
        "with": "vegetable"
      },
      {
        "toCreate": "recipe",
        "with": "fruit"
      },
      {
        "toCreate": "recipe",
        "with": "cook"
      },
      {
        "toCreate": "sandpaper",
        "with": "sand"
      },
      {
        "toCreate": "scissors",
        "with": "blade"
      },
      {
        "toCreate": "scissors",
        "with": "sword"
      },
      {
        "toCreate": "sheet_music",
        "with": "music"
      },
      {
        "toCreate": "wrapping_paper",
        "with": "gift"
      },
      {
        "toCreate": "wrapping_paper",
        "with": "christmas_tree"
      },
      {
        "toCreate": "wrapping_paper",
        "with": "christmas_stocking"
      },
      {
        "toCreate": "wrapping_paper",
        "with": "santa"
      }
    ],
    "name": "Paper"
  },
  "paper_airplane": {
    "combine": [
      {
        "toCreate": "drone",
        "with": "robot"
      }
    ],
    "name": "Paper Airplane"
  },
  "paper_cup": {
    "combine": [
      {
        "toCreate": "string_phone",
        "with": "wire"
      },
      {
        "toCreate": "string_phone",
        "with": "thread"
      }
    ],
    "name": "Paper Cup"
  },
  "parachute": {
    "combine": [],
    "name": "Parachute"
  },
  "park": {
    "combine": [
      {
        "toCreate": "roller_coaster",
        "with": "train"
      },
      {
        "toCreate": "roller_coaster",
        "with": "car"
      },
      {
        "toCreate": "roller_coaster",
        "with": "cart"
      },
      {
        "toCreate": "roller_coaster",
        "with": "wagon"
      }
    ],
    "name": "Park"
  },
  "parrot": {
    "combine": [
      {
        "toCreate": "egg",
        "with": "parrot"
      }
    ],
    "name": "Parrot"
  },
  "pasta": {
    "combine": [
      {
        "toCreate": "mac_and_cheese",
        "with": "cheese"
      },
      {
        "toCreate": "spaghetti",
        "with": "thread"
      },
      {
        "toCreate": "spaghetti",
        "with": "wire"
      },
      {
        "toCreate": "spaghetti",
        "with": "rope"
      }
    ],
    "name": "Pasta"
  },
  "peacock": {
    "combine": [
      {
        "toCreate": "egg",
        "with": "peacock"
      }
    ],
    "name": "Peacock"
  },
  "peanut_butter": {
    "combine": [],
    "name": "Peanut Butter"
  },
  "peat": {
    "combine": [
      {
        "toCreate": "coal",
        "with": "earth"
      },
      {
        "toCreate": "coal",
        "with": "stone"
      },
      {
        "toCreate": "coal",
        "with": "rock"
      },
      {
        "toCreate": "coal",
        "with": "time"
      },
      {
        "toCreate": "coal",
        "with": "pressure"
      }
    ],
    "name": "Peat"
  },
  "pebble": {
    "combine": [
      {
        "toCreate": "rock",
        "with": "big"
      },
      {
        "toCreate": "rock",
        "with": "pebble"
      },
      {
        "toCreate": "rock",
        "with": "stone"
      },
      {
        "toCreate": "rock",
        "with": "earth"
      },
      {
        "toCreate": "sand",
        "with": "air"
      },
      {
        "toCreate": "sand",
        "with": "wind"
      },
      {
        "toCreate": "small",
        "with": "philosophy"
      }
    ],
    "name": "Pebble"
  },
  "pegasus": {
    "combine": [],
    "name": "Pegasus"
  },
  "pencil": {
    "combine": [
      {
        "toCreate": "box",
        "with": "container"
      },
      {
        "toCreate": "crayon",
        "with": "rainbow"
      },
      {
        "toCreate": "crayon",
        "with": "double_rainbow!"
      },
      {
        "toCreate": "crayon",
        "with": "paint"
      },
      {
        "toCreate": "letter",
        "with": "paper"
      },
      {
        "toCreate": "paint",
        "with": "water"
      },
      {
        "toCreate": "paint",
        "with": "liquid"
      },
      {
        "toCreate": "pencil_sharpener",
        "with": "blade"
      },
      {
        "toCreate": "pencil_sharpener",
        "with": "blender"
      },
      {
        "toCreate": "pencil_sharpener",
        "with": "sword"
      },
      {
        "toCreate": "ruler",
        "with": "wood"
      },
      {
        "toCreate": "wand",
        "with": "wizard"
      }
    ],
    "name": "Pencil"
  },
  "pencil_sharpener": {
    "combine": [],
    "name": "Pencil Sharpener"
  },
  "penguin": {
    "combine": [
      {
        "toCreate": "egg",
        "with": "penguin"
      },
      {
        "toCreate": "origami",
        "with": "paper"
      },
      {
        "toCreate": "vulture",
        "with": "corpse"
      },
      {
        "toCreate": "vulture",
        "with": "desert"
      }
    ],
    "name": "Penguin"
  },
  "penicillin": {
    "combine": [],
    "name": "Penicillin"
  },
  "perfume": {
    "combine": [
      {
        "toCreate": "bottle",
        "with": "container"
      }
    ],
    "name": "Perfume"
  },
  "petroleum": {
    "combine": [
      {
        "toCreate": "combustion_engine",
        "with": "machine"
      },
      {
        "toCreate": "combustion_engine",
        "with": "steam_engine"
      },
      {
        "toCreate": "combustion_engine",
        "with": "wheel"
      },
      {
        "toCreate": "explosion",
        "with": "fire"
      },
      {
        "toCreate": "explosion",
        "with": "explosion"
      },
      {
        "toCreate": "explosion",
        "with": "pressure"
      },
      {
        "toCreate": "explosion",
        "with": "heat"
      },
      {
        "toCreate": "explosion",
        "with": "lava"
      },
      {
        "toCreate": "explosion",
        "with": "volcano"
      }
    ],
    "name": "Petroleum"
  },
  "philosophy": {
    "combine": [
      {
        "toCreate": "alchemist",
        "with": "gold"
      },
      {
        "toCreate": "big",
        "with": "universe"
      },
      {
        "toCreate": "big",
        "with": "galaxy"
      },
      {
        "toCreate": "big",
        "with": "galaxy_cluster"
      },
      {
        "toCreate": "big",
        "with": "solar_system"
      },
      {
        "toCreate": "big",
        "with": "sun"
      },
      {
        "toCreate": "big",
        "with": "planet"
      },
      {
        "toCreate": "container",
        "with": "safe"
      },
      {
        "toCreate": "container",
        "with": "pottery"
      },
      {
        "toCreate": "container",
        "with": "house"
      },
      {
        "toCreate": "container",
        "with": "box"
      },
      {
        "toCreate": "container",
        "with": "bottle"
      },
      {
        "toCreate": "container",
        "with": "bucket"
      },
      {
        "toCreate": "death",
        "with": "corpse"
      },
      {
        "toCreate": "death",
        "with": "skeleton"
      },
      {
        "toCreate": "death",
        "with": "grave"
      },
      {
        "toCreate": "death",
        "with": "graveyard"
      },
      {
        "toCreate": "heat",
        "with": "lava"
      },
      {
        "toCreate": "idea",
        "with": "philosophy"
      },
      {
        "toCreate": "idea",
        "with": "science"
      },
      {
        "toCreate": "motion",
        "with": "wind"
      },
      {
        "toCreate": "motion",
        "with": "tornado"
      },
      {
        "toCreate": "motion",
        "with": "stream"
      },
      {
        "toCreate": "motion",
        "with": "river"
      },
      {
        "toCreate": "small",
        "with": "bacteria"
      },
      {
        "toCreate": "small",
        "with": "oxygen"
      },
      {
        "toCreate": "small",
        "with": "carbon_dioxide"
      },
      {
        "toCreate": "small",
        "with": "ozone"
      },
      {
        "toCreate": "small",
        "with": "pebble"
      },
      {
        "toCreate": "small",
        "with": "rivulet"
      },
      {
        "toCreate": "small",
        "with": "ant"
      },
      {
        "toCreate": "small",
        "with": "spider"
      },
      {
        "toCreate": "small",
        "with": "bee"
      },
      {
        "toCreate": "small",
        "with": "confetti"
      },
      {
        "toCreate": "small",
        "with": "scorpion"
      },
      {
        "toCreate": "small",
        "with": "seahorse"
      }
    ],
    "name": "Philosophy"
  },
  "phoenix": {
    "combine": [
      {
        "toCreate": "ash",
        "with": "paper"
      },
      {
        "toCreate": "egg",
        "with": "phoenix"
      }
    ],
    "name": "Phoenix"
  },
  "picnic": {
    "combine": [],
    "name": "Picnic"
  },
  "pie": {
    "combine": [
      {
        "toCreate": "baker",
        "with": "human"
      }
    ],
    "name": "Pie"
  },
  "pig": {
    "combine": [
      {
        "toCreate": "bacon",
        "with": "fire"
      },
      {
        "toCreate": "bacon",
        "with": "campfire"
      },
      {
        "toCreate": "barn",
        "with": "container"
      },
      {
        "toCreate": "ham",
        "with": "smoke"
      },
      {
        "toCreate": "lasso",
        "with": "rope"
      },
      {
        "toCreate": "leather",
        "with": "blade"
      },
      {
        "toCreate": "leather",
        "with": "sword"
      },
      {
        "toCreate": "leather",
        "with": "tool"
      },
      {
        "toCreate": "meat",
        "with": "tool"
      },
      {
        "toCreate": "meat",
        "with": "axe"
      },
      {
        "toCreate": "meat",
        "with": "sword"
      },
      {
        "toCreate": "meat",
        "with": "butcher"
      },
      {
        "toCreate": "piggy_bank",
        "with": "money"
      },
      {
        "toCreate": "piggy_bank",
        "with": "pottery"
      },
      {
        "toCreate": "piggy_bank",
        "with": "gold"
      },
      {
        "toCreate": "wild_boar",
        "with": "animal"
      },
      {
        "toCreate": "wild_boar",
        "with": "forest"
      },
      {
        "toCreate": "wild_boar",
        "with": "hill"
      }
    ],
    "name": "Pig"
  },
  "pigeon": {
    "combine": [
      {
        "toCreate": "airplane",
        "with": "steel"
      },
      {
        "toCreate": "airplane",
        "with": "metal"
      },
      {
        "toCreate": "airplane",
        "with": "machine"
      },
      {
        "toCreate": "crow",
        "with": "scarecrow"
      },
      {
        "toCreate": "crow",
        "with": "field"
      },
      {
        "toCreate": "duck",
        "with": "water"
      },
      {
        "toCreate": "duck",
        "with": "pond"
      },
      {
        "toCreate": "duck",
        "with": "lake"
      },
      {
        "toCreate": "egg",
        "with": "pigeon"
      },
      {
        "toCreate": "flying_squirrel",
        "with": "squirrel"
      },
      {
        "toCreate": "hummingbird",
        "with": "flower"
      },
      {
        "toCreate": "hummingbird",
        "with": "garden"
      },
      {
        "toCreate": "origami",
        "with": "paper"
      },
      {
        "toCreate": "parrot",
        "with": "pirate"
      },
      {
        "toCreate": "parrot",
        "with": "pirate_ship"
      },
      {
        "toCreate": "seagull",
        "with": "sea"
      },
      {
        "toCreate": "seagull",
        "with": "ocean"
      },
      {
        "toCreate": "seagull",
        "with": "beach"
      },
      {
        "toCreate": "toucan",
        "with": "palm"
      },
      {
        "toCreate": "vulture",
        "with": "corpse"
      },
      {
        "toCreate": "vulture",
        "with": "desert"
      }
    ],
    "name": "Pigeon"
  },
  "piggy_bank": {
    "combine": [],
    "name": "Piggy Bank"
  },
  "pilot": {
    "combine": [
      {
        "toCreate": "airplane",
        "with": "container"
      },
      {
        "toCreate": "cyborg",
        "with": "robot"
      },
      {
        "toCreate": "idea",
        "with": "pilot"
      },
      {
        "toCreate": "parachute",
        "with": "fabric"
      },
      {
        "toCreate": "parachute",
        "with": "umbrella"
      },
      {
        "toCreate": "statue",
        "with": "medusa"
      }
    ],
    "name": "Pilot"
  },
  "pinocchio": {
    "combine": [],
    "name": "Pinocchio"
  },
  "pipe": {
    "combine": [
      {
        "toCreate": "dynamite",
        "with": "gunpowder"
      },
      {
        "toCreate": "water_pipe",
        "with": "water"
      }
    ],
    "name": "Pipe"
  },
  "piranha": {
    "combine": [
      {
        "toCreate": "egg",
        "with": "piranha"
      },
      {
        "toCreate": "fishing_rod",
        "with": "tool"
      },
      {
        "toCreate": "fishing_rod",
        "with": "wood"
      },
      {
        "toCreate": "fishing_rod",
        "with": "wire"
      },
      {
        "toCreate": "fishing_rod",
        "with": "thread"
      },
      {
        "toCreate": "meat",
        "with": "net"
      }
    ],
    "name": "Piranha"
  },
  "pirate": {
    "combine": [
      {
        "toCreate": "parrot",
        "with": "bird"
      },
      {
        "toCreate": "parrot",
        "with": "pigeon"
      },
      {
        "toCreate": "pirate_ship",
        "with": "sailboat"
      },
      {
        "toCreate": "pirate_ship",
        "with": "boat"
      },
      {
        "toCreate": "pirate_ship",
        "with": "steam_engine"
      },
      {
        "toCreate": "pirate_ship",
        "with": "container"
      },
      {
        "toCreate": "pirate_ship",
        "with": "house"
      },
      {
        "toCreate": "treasure",
        "with": "treasure_map"
      },
      {
        "toCreate": "treasure_map",
        "with": "map"
      }
    ],
    "name": "Pirate"
  },
  "pirate_ship": {
    "combine": [
      {
        "toCreate": "cannon",
        "with": "gunpowder"
      },
      {
        "toCreate": "cannon",
        "with": "gun"
      },
      {
        "toCreate": "parrot",
        "with": "bird"
      },
      {
        "toCreate": "parrot",
        "with": "pigeon"
      },
      {
        "toCreate": "pirate",
        "with": "sailor"
      },
      {
        "toCreate": "rat",
        "with": "animal"
      },
      {
        "toCreate": "rat",
        "with": "mouse"
      },
      {
        "toCreate": "rocket",
        "with": "atmosphere"
      },
      {
        "toCreate": "rope",
        "with": "tool"
      },
      {
        "toCreate": "spaceship",
        "with": "space"
      },
      {
        "toCreate": "steamboat",
        "with": "steam_engine"
      },
      {
        "toCreate": "treasure",
        "with": "treasure_map"
      },
      {
        "toCreate": "treasure_map",
        "with": "map"
      }
    ],
    "name": "Pirate Ship"
  },
  "pitchfork": {
    "combine": [
      {
        "toCreate": "farmer",
        "with": "human"
      },
      {
        "toCreate": "hay",
        "with": "grass"
      },
      {
        "toCreate": "hay",
        "with": "barn"
      },
      {
        "toCreate": "hay",
        "with": "farm"
      },
      {
        "toCreate": "hay",
        "with": "farmer"
      }
    ],
    "name": "Pitchfork"
  },
  "pizza": {
    "combine": [
      {
        "toCreate": "baker",
        "with": "human"
      },
      {
        "toCreate": "box",
        "with": "container"
      }
    ],
    "name": "Pizza"
  },
  "planet": {
    "combine": [
      {
        "toCreate": "atmosphere",
        "with": "air"
      },
      {
        "toCreate": "big",
        "with": "philosophy"
      },
      {
        "toCreate": "jupiter",
        "with": "cloud"
      },
      {
        "toCreate": "jupiter",
        "with": "storm"
      },
      {
        "toCreate": "jupiter",
        "with": "big"
      },
      {
        "toCreate": "jupiter",
        "with": "gas"
      },
      {
        "toCreate": "mars",
        "with": "rust"
      },
      {
        "toCreate": "mars",
        "with": "desert"
      },
      {
        "toCreate": "mercury",
        "with": "small"
      },
      {
        "toCreate": "mercury",
        "with": "heat"
      },
      {
        "toCreate": "moon",
        "with": "sky"
      },
      {
        "toCreate": "moon",
        "with": "night"
      },
      {
        "toCreate": "primordial_soup",
        "with": "ocean"
      },
      {
        "toCreate": "primordial_soup",
        "with": "sea"
      },
      {
        "toCreate": "saturn",
        "with": "ring"
      },
      {
        "toCreate": "solar_system",
        "with": "planet"
      },
      {
        "toCreate": "solar_system",
        "with": "sun"
      },
      {
        "toCreate": "solar_system",
        "with": "container"
      },
      {
        "toCreate": "sun",
        "with": "fire"
      },
      {
        "toCreate": "sun",
        "with": "light"
      },
      {
        "toCreate": "telescope",
        "with": "glass"
      },
      {
        "toCreate": "venus",
        "with": "acid_rain"
      },
      {
        "toCreate": "venus",
        "with": "smog"
      },
      {
        "toCreate": "venus",
        "with": "volcano"
      }
    ],
    "name": "Planet"
  },
  "plankton": {
    "combine": [],
    "name": "Plankton"
  },
  "plant": {
    "combine": [
      {
        "toCreate": "algae",
        "with": "water"
      },
      {
        "toCreate": "ash",
        "with": "fire"
      },
      {
        "toCreate": "cactus",
        "with": "sand"
      },
      {
        "toCreate": "cactus",
        "with": "desert"
      },
      {
        "toCreate": "carbon_dioxide",
        "with": "night"
      },
      {
        "toCreate": "catnip",
        "with": "cat"
      },
      {
        "toCreate": "cotton",
        "with": "cloud"
      },
      {
        "toCreate": "cotton",
        "with": "fabric"
      },
      {
        "toCreate": "cotton",
        "with": "thread"
      },
      {
        "toCreate": "cotton",
        "with": "wool"
      },
      {
        "toCreate": "cotton",
        "with": "sheep"
      },
      {
        "toCreate": "dew",
        "with": "fog"
      },
      {
        "toCreate": "dew",
        "with": "dawn"
      },
      {
        "toCreate": "farmer",
        "with": "human"
      },
      {
        "toCreate": "flower",
        "with": "garden"
      },
      {
        "toCreate": "flower",
        "with": "rainbow"
      },
      {
        "toCreate": "flower",
        "with": "double_rainbow!"
      },
      {
        "toCreate": "forest",
        "with": "tree"
      },
      {
        "toCreate": "fruit_tree",
        "with": "fruit"
      },
      {
        "toCreate": "garden",
        "with": "grass"
      },
      {
        "toCreate": "garden",
        "with": "house"
      },
      {
        "toCreate": "garden",
        "with": "flower"
      },
      {
        "toCreate": "garden",
        "with": "lawn"
      },
      {
        "toCreate": "grass",
        "with": "earth"
      },
      {
        "toCreate": "grass",
        "with": "land"
      },
      {
        "toCreate": "greenhouse",
        "with": "glass"
      },
      {
        "toCreate": "greenhouse",
        "with": "aquarium"
      },
      {
        "toCreate": "greenhouse",
        "with": "container"
      },
      {
        "toCreate": "hedge",
        "with": "fence"
      },
      {
        "toCreate": "hedge",
        "with": "wall"
      },
      {
        "toCreate": "ivy",
        "with": "wall"
      },
      {
        "toCreate": "moss",
        "with": "stone"
      },
      {
        "toCreate": "moss",
        "with": "rock"
      },
      {
        "toCreate": "moss",
        "with": "boulder"
      },
      {
        "toCreate": "oxygen",
        "with": "sun"
      },
      {
        "toCreate": "oxygen",
        "with": "carbon_dioxide"
      },
      {
        "toCreate": "peat",
        "with": "swamp"
      },
      {
        "toCreate": "pollen",
        "with": "dust"
      },
      {
        "toCreate": "pollen",
        "with": "wind"
      },
      {
        "toCreate": "pumpkin",
        "with": "jack-o-lantern"
      },
      {
        "toCreate": "reed",
        "with": "pond"
      },
      {
        "toCreate": "reed",
        "with": "puddle"
      },
      {
        "toCreate": "reed",
        "with": "swamp"
      },
      {
        "toCreate": "reed",
        "with": "river"
      },
      {
        "toCreate": "rose",
        "with": "love"
      },
      {
        "toCreate": "rose",
        "with": "blade"
      },
      {
        "toCreate": "seaweed",
        "with": "ocean"
      },
      {
        "toCreate": "seaweed",
        "with": "sea"
      },
      {
        "toCreate": "seaweed",
        "with": "lake"
      },
      {
        "toCreate": "seed",
        "with": "pollen"
      },
      {
        "toCreate": "smoke",
        "with": "fire"
      },
      {
        "toCreate": "squirrel",
        "with": "mouse"
      },
      {
        "toCreate": "sunflower",
        "with": "sun"
      },
      {
        "toCreate": "tobacco",
        "with": "smoke"
      },
      {
        "toCreate": "tobacco",
        "with": "fire"
      },
      {
        "toCreate": "tree",
        "with": "big"
      },
      {
        "toCreate": "tree",
        "with": "wood"
      },
      {
        "toCreate": "vase",
        "with": "pottery"
      },
      {
        "toCreate": "vase",
        "with": "bottle"
      },
      {
        "toCreate": "vegetable",
        "with": "farmer"
      },
      {
        "toCreate": "vegetable",
        "with": "field"
      },
      {
        "toCreate": "vegetable",
        "with": "domestication"
      }
    ],
    "name": "Plant"
  },
  "plasma": {
    "combine": [],
    "name": "Plasma"
  },
  "platypus": {
    "combine": [],
    "name": "Platypus"
  },
  "plow": {
    "combine": [
      {
        "toCreate": "farmer",
        "with": "human"
      },
      {
        "toCreate": "field",
        "with": "earth"
      },
      {
        "toCreate": "field",
        "with": "land"
      },
      {
        "toCreate": "field",
        "with": "soil"
      }
    ],
    "name": "Plow"
  },
  "polar_bear": {
    "combine": [],
    "name": "Polar Bear"
  },
  "pollen": {
    "combine": [
      {
        "toCreate": "allergy",
        "with": "human"
      },
      {
        "toCreate": "seed",
        "with": "plant"
      },
      {
        "toCreate": "seed",
        "with": "bee"
      }
    ],
    "name": "Pollen"
  },
  "pond": {
    "combine": [
      {
        "toCreate": "algae",
        "with": "grass"
      },
      {
        "toCreate": "aquarium",
        "with": "glass"
      },
      {
        "toCreate": "duck",
        "with": "bird"
      },
      {
        "toCreate": "duck",
        "with": "owl"
      },
      {
        "toCreate": "duck",
        "with": "pigeon"
      },
      {
        "toCreate": "duck",
        "with": "chicken"
      },
      {
        "toCreate": "frog",
        "with": "animal"
      },
      {
        "toCreate": "frog",
        "with": "egg"
      },
      {
        "toCreate": "lake",
        "with": "water"
      },
      {
        "toCreate": "lake",
        "with": "big"
      },
      {
        "toCreate": "lake",
        "with": "pond"
      },
      {
        "toCreate": "oasis",
        "with": "desert"
      },
      {
        "toCreate": "puddle",
        "with": "small"
      },
      {
        "toCreate": "reed",
        "with": "plant"
      },
      {
        "toCreate": "reed",
        "with": "grass"
      },
      {
        "toCreate": "stream",
        "with": "motion"
      },
      {
        "toCreate": "swamp",
        "with": "algae"
      },
      {
        "toCreate": "swamp",
        "with": "reed"
      },
      {
        "toCreate": "swimmer",
        "with": "human"
      },
      {
        "toCreate": "water_lily",
        "with": "flower"
      }
    ],
    "name": "Pond"
  },
  "popsicle": {
    "combine": [],
    "name": "Popsicle"
  },
  "post_office": {
    "combine": [
      {
        "toCreate": "city",
        "with": "bank"
      },
      {
        "toCreate": "city",
        "with": "skyscraper"
      }
    ],
    "name": "Post Office"
  },
  "potato": {
    "combine": [
      {
        "toCreate": "french_fries",
        "with": "oil"
      },
      {
        "toCreate": "french_fries",
        "with": "fire"
      }
    ],
    "name": "Potato"
  },
  "potter": {
    "combine": [],
    "name": "Potter"
  },
  "pottery": {
    "combine": [
      {
        "toCreate": "bonsai_tree",
        "with": "tree"
      },
      {
        "toCreate": "cauldron",
        "with": "metal"
      },
      {
        "toCreate": "cauldron",
        "with": "steel"
      },
      {
        "toCreate": "container",
        "with": "philosophy"
      },
      {
        "toCreate": "paint",
        "with": "rainbow"
      },
      {
        "toCreate": "paint",
        "with": "double_rainbow!"
      },
      {
        "toCreate": "piggy_bank",
        "with": "pig"
      },
      {
        "toCreate": "potter",
        "with": "human"
      },
      {
        "toCreate": "vase",
        "with": "plant"
      },
      {
        "toCreate": "vase",
        "with": "flower"
      },
      {
        "toCreate": "vase",
        "with": "rose"
      }
    ],
    "name": "Pottery"
  },
  "pressure": {
    "combine": [
      {
        "toCreate": "black_hole",
        "with": "star"
      },
      {
        "toCreate": "black_hole",
        "with": "sun"
      },
      {
        "toCreate": "boiler",
        "with": "metal"
      },
      {
        "toCreate": "boiler",
        "with": "tool"
      },
      {
        "toCreate": "boiler",
        "with": "container"
      },
      {
        "toCreate": "butter",
        "with": "milk"
      },
      {
        "toCreate": "coal",
        "with": "organic_matter"
      },
      {
        "toCreate": "coal",
        "with": "peat"
      },
      {
        "toCreate": "diamond",
        "with": "coal"
      },
      {
        "toCreate": "eruption",
        "with": "volcano"
      },
      {
        "toCreate": "eruption",
        "with": "lava"
      },
      {
        "toCreate": "explosion",
        "with": "volcano"
      },
      {
        "toCreate": "explosion",
        "with": "petroleum"
      },
      {
        "toCreate": "fire_extinguisher",
        "with": "carbon_dioxide"
      },
      {
        "toCreate": "geyser",
        "with": "steam"
      },
      {
        "toCreate": "granite",
        "with": "lava"
      },
      {
        "toCreate": "granite",
        "with": "stone"
      },
      {
        "toCreate": "granite",
        "with": "rock"
      },
      {
        "toCreate": "juice",
        "with": "fruit"
      },
      {
        "toCreate": "juice",
        "with": "vegetable"
      },
      {
        "toCreate": "oil",
        "with": "sunflower"
      },
      {
        "toCreate": "paper",
        "with": "wood"
      },
      {
        "toCreate": "peanut_butter",
        "with": "nuts"
      },
      {
        "toCreate": "peat",
        "with": "swamp"
      },
      {
        "toCreate": "petroleum",
        "with": "fossil"
      },
      {
        "toCreate": "plasma",
        "with": "heat"
      },
      {
        "toCreate": "stone",
        "with": "earth"
      },
      {
        "toCreate": "volcano",
        "with": "mountain"
      },
      {
        "toCreate": "volcano",
        "with": "hill"
      },
      {
        "toCreate": "wind",
        "with": "air"
      }
    ],
    "name": "Pressure"
  },
  "primordial_soup": {
    "combine": [
      {
        "toCreate": "bacteria",
        "with": "life"
      },
      {
        "toCreate": "life",
        "with": "electricity"
      },
      {
        "toCreate": "life",
        "with": "time"
      },
      {
        "toCreate": "life",
        "with": "storm"
      },
      {
        "toCreate": "life",
        "with": "volcano"
      },
      {
        "toCreate": "life",
        "with": "lightning"
      }
    ],
    "name": "Primordial Soup"
  },
  "printer": {
    "combine": [],
    "name": "Printer"
  },
  "prism": {
    "combine": [
      {
        "toCreate": "rainbow",
        "with": "sun"
      },
      {
        "toCreate": "rainbow",
        "with": "light"
      }
    ],
    "name": "Prism"
  },
  "pterodactyl": {
    "combine": [
      {
        "toCreate": "bird",
        "with": "time"
      },
      {
        "toCreate": "egg",
        "with": "pterodactyl"
      }
    ],
    "name": "Pterodactyl"
  },
  "puddle": {
    "combine": [
      {
        "toCreate": "aquarium",
        "with": "glass"
      },
      {
        "toCreate": "dough",
        "with": "flour"
      },
      {
        "toCreate": "frog",
        "with": "egg"
      },
      {
        "toCreate": "frog",
        "with": "animal"
      },
      {
        "toCreate": "ice",
        "with": "cold"
      },
      {
        "toCreate": "ice",
        "with": "solid"
      },
      {
        "toCreate": "oasis",
        "with": "desert"
      },
      {
        "toCreate": "pond",
        "with": "water"
      },
      {
        "toCreate": "pond",
        "with": "big"
      },
      {
        "toCreate": "pond",
        "with": "puddle"
      },
      {
        "toCreate": "reed",
        "with": "plant"
      },
      {
        "toCreate": "reed",
        "with": "grass"
      },
      {
        "toCreate": "rivulet",
        "with": "motion"
      },
      {
        "toCreate": "water_gun",
        "with": "gun"
      },
      {
        "toCreate": "water_lily",
        "with": "flower"
      }
    ],
    "name": "Puddle"
  },
  "pumpkin": {
    "combine": [
      {
        "toCreate": "jack-o-lantern",
        "with": "fire"
      },
      {
        "toCreate": "jack-o-lantern",
        "with": "candle"
      },
      {
        "toCreate": "jack-o-lantern",
        "with": "light"
      },
      {
        "toCreate": "jack-o-lantern",
        "with": "night"
      },
      {
        "toCreate": "jack-o-lantern",
        "with": "ghost"
      },
      {
        "toCreate": "jack-o-lantern",
        "with": "skeleton"
      },
      {
        "toCreate": "scarecrow",
        "with": "sack"
      }
    ],
    "name": "Pumpkin"
  },
  "pyramid": {
    "combine": [
      {
        "toCreate": "mummy",
        "with": "corpse"
      },
      {
        "toCreate": "mummy",
        "with": "fabric"
      },
      {
        "toCreate": "mummy",
        "with": "human"
      },
      {
        "toCreate": "sphinx",
        "with": "statue"
      }
    ],
    "name": "Pyramid"
  },
  "quicksand": {
    "combine": [],
    "name": "Quicksand"
  },
  "quicksilver": {
    "combine": [
      {
        "toCreate": "thermometer",
        "with": "glass"
      },
      {
        "toCreate": "thermometer",
        "with": "tool"
      },
      {
        "toCreate": "thermometer",
        "with": "engineer"
      }
    ],
    "name": "Quicksilver"
  },
  "rabbit": {
    "combine": [
      {
        "toCreate": "rabbit",
        "with": "rabbit"
      }
    ],
    "name": "Rabbit"
  },
  "rain": {
    "combine": [
      {
        "toCreate": "acid_rain",
        "with": "smoke"
      },
      {
        "toCreate": "acid_rain",
        "with": "smog"
      },
      {
        "toCreate": "acid_rain",
        "with": "sickness"
      },
      {
        "toCreate": "acid_rain",
        "with": "city"
      },
      {
        "toCreate": "ash",
        "with": "campfire"
      },
      {
        "toCreate": "cold",
        "with": "human"
      },
      {
        "toCreate": "dough",
        "with": "flour"
      },
      {
        "toCreate": "flood",
        "with": "rain"
      },
      {
        "toCreate": "flood",
        "with": "river"
      },
      {
        "toCreate": "flood",
        "with": "lake"
      },
      {
        "toCreate": "flood",
        "with": "big"
      },
      {
        "toCreate": "flood",
        "with": "time"
      },
      {
        "toCreate": "fruit",
        "with": "flower"
      },
      {
        "toCreate": "hail",
        "with": "ice"
      },
      {
        "toCreate": "lightning",
        "with": "electricity"
      },
      {
        "toCreate": "lightning",
        "with": "energy"
      },
      {
        "toCreate": "mist",
        "with": "air"
      },
      {
        "toCreate": "plant",
        "with": "soil"
      },
      {
        "toCreate": "rainbow",
        "with": "sun"
      },
      {
        "toCreate": "rainbow",
        "with": "light"
      },
      {
        "toCreate": "rainforest",
        "with": "forest"
      },
      {
        "toCreate": "river",
        "with": "mountain"
      },
      {
        "toCreate": "river",
        "with": "hill"
      },
      {
        "toCreate": "snow",
        "with": "cold"
      },
      {
        "toCreate": "snow",
        "with": "mountain"
      },
      {
        "toCreate": "snow",
        "with": "mountain_range"
      },
      {
        "toCreate": "storm",
        "with": "wind"
      },
      {
        "toCreate": "umbrella",
        "with": "tool"
      },
      {
        "toCreate": "umbrella",
        "with": "fabric"
      }
    ],
    "name": "Rain"
  },
  "rainbow": {
    "combine": [
      {
        "toCreate": "butterfly",
        "with": "animal"
      },
      {
        "toCreate": "chameleon",
        "with": "lizard"
      },
      {
        "toCreate": "chameleon",
        "with": "snake"
      },
      {
        "toCreate": "chameleon",
        "with": "turtle"
      },
      {
        "toCreate": "crayon",
        "with": "pencil"
      },
      {
        "toCreate": "crayon",
        "with": "wax"
      },
      {
        "toCreate": "double_rainbow!",
        "with": "rainbow"
      },
      {
        "toCreate": "flower",
        "with": "plant"
      },
      {
        "toCreate": "flower",
        "with": "grass"
      },
      {
        "toCreate": "gold",
        "with": "metal"
      },
      {
        "toCreate": "gold",
        "with": "steel"
      },
      {
        "toCreate": "magic",
        "with": "life"
      },
      {
        "toCreate": "paint",
        "with": "water"
      },
      {
        "toCreate": "paint",
        "with": "pottery"
      },
      {
        "toCreate": "paint",
        "with": "tool"
      },
      {
        "toCreate": "paint",
        "with": "liquid"
      },
      {
        "toCreate": "peacock",
        "with": "bird"
      },
      {
        "toCreate": "prism",
        "with": "glass"
      },
      {
        "toCreate": "prism",
        "with": "crystal_ball"
      },
      {
        "toCreate": "sprinkles",
        "with": "sugar"
      },
      {
        "toCreate": "toucan",
        "with": "bird"
      },
      {
        "toCreate": "unicorn",
        "with": "horse"
      },
      {
        "toCreate": "wizard",
        "with": "human"
      }
    ],
    "name": "Rainbow"
  },
  "rainforest": {
    "combine": [
      {
        "toCreate": "vine",
        "with": "rope"
      },
      {
        "toCreate": "vine",
        "with": "thread"
      },
      {
        "toCreate": "vine",
        "with": "wire"
      }
    ],
    "name": "Rainforest"
  },
  "rat": {
    "combine": [
      {
        "toCreate": "hamster",
        "with": "domestication"
      },
      {
        "toCreate": "hamster",
        "with": "wheel"
      },
      {
        "toCreate": "hedgehog",
        "with": "needle"
      },
      {
        "toCreate": "seagull",
        "with": "sea"
      },
      {
        "toCreate": "seagull",
        "with": "ocean"
      },
      {
        "toCreate": "seagull",
        "with": "beach"
      }
    ],
    "name": "Rat"
  },
  "recipe": {
    "combine": [
      {
        "toCreate": "cookbook",
        "with": "recipe"
      }
    ],
    "name": "Recipe"
  },
  "reed": {
    "combine": [
      {
        "toCreate": "swamp",
        "with": "lake"
      },
      {
        "toCreate": "swamp",
        "with": "pond"
      }
    ],
    "name": "Reed"
  },
  "reindeer": {
    "combine": [
      {
        "toCreate": "candy_cane",
        "with": "sugar"
      },
      {
        "toCreate": "christmas_stocking",
        "with": "wool"
      }
    ],
    "name": "Reindeer"
  },
  "ring": {
    "combine": [
      {
        "toCreate": "saturn",
        "with": "planet"
      },
      {
        "toCreate": "the_one_ring",
        "with": "volcano"
      },
      {
        "toCreate": "the_one_ring",
        "with": "magic"
      }
    ],
    "name": "Ring"
  },
  "river": {
    "combine": [
      {
        "toCreate": "alligator",
        "with": "lizard"
      },
      {
        "toCreate": "beaver",
        "with": "animal"
      },
      {
        "toCreate": "boat",
        "with": "wood"
      },
      {
        "toCreate": "bridge",
        "with": "wood"
      },
      {
        "toCreate": "bridge",
        "with": "metal"
      },
      {
        "toCreate": "bridge",
        "with": "steel"
      },
      {
        "toCreate": "bucket",
        "with": "bottle"
      },
      {
        "toCreate": "dam",
        "with": "beaver"
      },
      {
        "toCreate": "dam",
        "with": "wall"
      },
      {
        "toCreate": "flood",
        "with": "rain"
      },
      {
        "toCreate": "flood",
        "with": "house"
      },
      {
        "toCreate": "flood",
        "with": "city"
      },
      {
        "toCreate": "grim_reaper",
        "with": "scythe"
      },
      {
        "toCreate": "hippo",
        "with": "horse"
      },
      {
        "toCreate": "hippo",
        "with": "cow"
      },
      {
        "toCreate": "manatee",
        "with": "cow"
      },
      {
        "toCreate": "map",
        "with": "paper"
      },
      {
        "toCreate": "motion",
        "with": "philosophy"
      },
      {
        "toCreate": "motion",
        "with": "science"
      },
      {
        "toCreate": "oasis",
        "with": "desert"
      },
      {
        "toCreate": "reed",
        "with": "plant"
      },
      {
        "toCreate": "reed",
        "with": "grass"
      },
      {
        "toCreate": "stream",
        "with": "small"
      },
      {
        "toCreate": "swim_goggles",
        "with": "glasses"
      },
      {
        "toCreate": "swim_goggles",
        "with": "sunglasses"
      },
      {
        "toCreate": "waterfall",
        "with": "mountain"
      },
      {
        "toCreate": "waterfall",
        "with": "mountain_range"
      },
      {
        "toCreate": "waterfall",
        "with": "hill"
      },
      {
        "toCreate": "wheel",
        "with": "tool"
      }
    ],
    "name": "River"
  },
  "rivulet": {
    "combine": [
      {
        "toCreate": "small",
        "with": "philosophy"
      },
      {
        "toCreate": "stream",
        "with": "big"
      }
    ],
    "name": "Rivulet"
  },
  "robot": {
    "combine": [
      {
        "toCreate": "cyborg",
        "with": "human"
      },
      {
        "toCreate": "cyborg",
        "with": "engineer"
      },
      {
        "toCreate": "cyborg",
        "with": "lumberjack"
      },
      {
        "toCreate": "cyborg",
        "with": "sailor"
      },
      {
        "toCreate": "cyborg",
        "with": "baker"
      },
      {
        "toCreate": "cyborg",
        "with": "cook"
      },
      {
        "toCreate": "cyborg",
        "with": "doctor"
      },
      {
        "toCreate": "cyborg",
        "with": "farmer"
      },
      {
        "toCreate": "cyborg",
        "with": "firefighter"
      },
      {
        "toCreate": "cyborg",
        "with": "pilot"
      },
      {
        "toCreate": "cyborg",
        "with": "astronaut"
      },
      {
        "toCreate": "cyborg",
        "with": "electrician"
      },
      {
        "toCreate": "drone",
        "with": "airplane"
      },
      {
        "toCreate": "drone",
        "with": "helicopter"
      },
      {
        "toCreate": "drone",
        "with": "paper_airplane"
      },
      {
        "toCreate": "drone",
        "with": "seaplane"
      },
      {
        "toCreate": "robot_vacuum",
        "with": "vacuum_cleaner"
      },
      {
        "toCreate": "robot_vacuum",
        "with": "broom"
      },
      {
        "toCreate": "sewing_machine",
        "with": "needle"
      },
      {
        "toCreate": "sewing_machine",
        "with": "thread"
      }
    ],
    "name": "Robot"
  },
  "robot_vacuum": {
    "combine": [
      {
        "toCreate": "closet",
        "with": "container"
      }
    ],
    "name": "Robot Vacuum"
  },
  "rock": {
    "combine": [
      {
        "toCreate": "blade",
        "with": "metal"
      },
      {
        "toCreate": "blade",
        "with": "steel"
      },
      {
        "toCreate": "boulder",
        "with": "big"
      },
      {
        "toCreate": "boulder",
        "with": "rock"
      },
      {
        "toCreate": "boulder",
        "with": "stone"
      },
      {
        "toCreate": "boulder",
        "with": "earth"
      },
      {
        "toCreate": "clay",
        "with": "liquid"
      },
      {
        "toCreate": "clay",
        "with": "mineral"
      },
      {
        "toCreate": "coal",
        "with": "peat"
      },
      {
        "toCreate": "flour",
        "with": "wheat"
      },
      {
        "toCreate": "fossil",
        "with": "dinosaur"
      },
      {
        "toCreate": "fossil",
        "with": "bone"
      },
      {
        "toCreate": "fossil",
        "with": "corpse"
      },
      {
        "toCreate": "fossil",
        "with": "skeleton"
      },
      {
        "toCreate": "granite",
        "with": "pressure"
      },
      {
        "toCreate": "gravestone",
        "with": "grave"
      },
      {
        "toCreate": "gravestone",
        "with": "death"
      },
      {
        "toCreate": "gravestone",
        "with": "graveyard"
      },
      {
        "toCreate": "hammer",
        "with": "tool"
      },
      {
        "toCreate": "juice",
        "with": "fruit"
      },
      {
        "toCreate": "juice",
        "with": "vegetable"
      },
      {
        "toCreate": "lizard",
        "with": "animal"
      },
      {
        "toCreate": "lizard",
        "with": "egg"
      },
      {
        "toCreate": "meteoroid",
        "with": "space"
      },
      {
        "toCreate": "meteoroid",
        "with": "solar_system"
      },
      {
        "toCreate": "meteoroid",
        "with": "sun"
      },
      {
        "toCreate": "mineral",
        "with": "organic_matter"
      },
      {
        "toCreate": "moss",
        "with": "plant"
      },
      {
        "toCreate": "moss",
        "with": "grass"
      },
      {
        "toCreate": "moss",
        "with": "algae"
      },
      {
        "toCreate": "ore",
        "with": "hammer"
      },
      {
        "toCreate": "pebble",
        "with": "small"
      },
      {
        "toCreate": "sand",
        "with": "air"
      },
      {
        "toCreate": "sand",
        "with": "wind"
      },
      {
        "toCreate": "sandstone",
        "with": "sand"
      },
      {
        "toCreate": "snowball",
        "with": "snow"
      },
      {
        "toCreate": "tool",
        "with": "human"
      },
      {
        "toCreate": "tool",
        "with": "wood"
      }
    ],
    "name": "Rock"
  },
  "rocket": {
    "combine": [
      {
        "toCreate": "astronaut",
        "with": "human"
      },
      {
        "toCreate": "hangar",
        "with": "container"
      },
      {
        "toCreate": "hangar",
        "with": "house"
      },
      {
        "toCreate": "hangar",
        "with": "wall"
      },
      {
        "toCreate": "hangar",
        "with": "barn"
      },
      {
        "toCreate": "ufo",
        "with": "alien"
      }
    ],
    "name": "Rocket"
  },
  "roe": {
    "combine": [
      {
        "toCreate": "caviar",
        "with": "salt"
      },
      {
        "toCreate": "caviar",
        "with": "cook"
      }
    ],
    "name": "Roe"
  },
  "roller_coaster": {
    "combine": [],
    "name": "Roller Coaster"
  },
  "rope": {
    "combine": [
      {
        "toCreate": "bow",
        "with": "wood"
      },
      {
        "toCreate": "cable_car",
        "with": "mountain"
      },
      {
        "toCreate": "cable_car",
        "with": "mountain_range"
      },
      {
        "toCreate": "chain",
        "with": "metal"
      },
      {
        "toCreate": "chain",
        "with": "steel"
      },
      {
        "toCreate": "lasso",
        "with": "cow"
      },
      {
        "toCreate": "lasso",
        "with": "horse"
      },
      {
        "toCreate": "lasso",
        "with": "pig"
      },
      {
        "toCreate": "lasso",
        "with": "wild_boar"
      },
      {
        "toCreate": "lasso",
        "with": "sheep"
      },
      {
        "toCreate": "lasso",
        "with": "goat"
      },
      {
        "toCreate": "net",
        "with": "angler"
      },
      {
        "toCreate": "net",
        "with": "fish"
      },
      {
        "toCreate": "net",
        "with": "swordfish"
      },
      {
        "toCreate": "net",
        "with": "fishing_rod"
      },
      {
        "toCreate": "net",
        "with": "flying_fish"
      },
      {
        "toCreate": "optical_fiber",
        "with": "light"
      },
      {
        "toCreate": "snake",
        "with": "animal"
      },
      {
        "toCreate": "spaghetti",
        "with": "pasta"
      },
      {
        "toCreate": "vine",
        "with": "rainforest"
      },
      {
        "toCreate": "wire",
        "with": "electricity"
      },
      {
        "toCreate": "wire",
        "with": "metal"
      },
      {
        "toCreate": "wire",
        "with": "steel"
      }
    ],
    "name": "Rope"
  },
  "rose": {
    "combine": [
      {
        "toCreate": "perfume",
        "with": "water"
      },
      {
        "toCreate": "perfume",
        "with": "steam"
      },
      {
        "toCreate": "perfume",
        "with": "alcohol"
      },
      {
        "toCreate": "vase",
        "with": "container"
      },
      {
        "toCreate": "vase",
        "with": "pottery"
      },
      {
        "toCreate": "vase",
        "with": "bottle"
      }
    ],
    "name": "Rose"
  },
  "ruins": {
    "combine": [
      {
        "toCreate": "archeologist",
        "with": "human"
      },
      {
        "toCreate": "archeologist",
        "with": "science"
      }
    ],
    "name": "Ruins"
  },
  "ruler": {
    "combine": [
      {
        "toCreate": "toolbox",
        "with": "container"
      },
      {
        "toCreate": "toolbox",
        "with": "box"
      }
    ],
    "name": "Ruler"
  },
  "rust": {
    "combine": [
      {
        "toCreate": "mars",
        "with": "planet"
      }
    ],
    "name": "Rust"
  },
  "rv": {
    "combine": [
      {
        "toCreate": "garage",
        "with": "house"
      },
      {
        "toCreate": "garage",
        "with": "container"
      },
      {
        "toCreate": "garage",
        "with": "wall"
      },
      {
        "toCreate": "garage",
        "with": "barn"
      }
    ],
    "name": "RV"
  },
  "sack": {
    "combine": [
      {
        "toCreate": "blood_bag",
        "with": "blood"
      },
      {
        "toCreate": "scarecrow",
        "with": "pumpkin"
      },
      {
        "toCreate": "scarecrow",
        "with": "jack-o-lantern"
      },
      {
        "toCreate": "scarecrow",
        "with": "hay"
      }
    ],
    "name": "Sack"
  },
  "saddle": {
    "combine": [
      {
        "toCreate": "camel",
        "with": "desert"
      },
      {
        "toCreate": "horse",
        "with": "animal"
      },
      {
        "toCreate": "horse",
        "with": "livestock"
      }
    ],
    "name": "Saddle"
  },
  "safe": {
    "combine": [
      {
        "toCreate": "bank",
        "with": "house"
      },
      {
        "toCreate": "bank",
        "with": "skyscraper"
      },
      {
        "toCreate": "bank",
        "with": "city"
      },
      {
        "toCreate": "birdcage",
        "with": "bird"
      },
      {
        "toCreate": "container",
        "with": "philosophy"
      },
      {
        "toCreate": "silo",
        "with": "wheat"
      },
      {
        "toCreate": "toolbox",
        "with": "tool"
      },
      {
        "toCreate": "vault",
        "with": "big"
      }
    ],
    "name": "Safe"
  },
  "safety_glasses": {
    "combine": [
      {
        "toCreate": "microscope",
        "with": "bacteria"
      }
    ],
    "name": "Safety Glasses"
  },
  "sailboat": {
    "combine": [
      {
        "toCreate": "airplane",
        "with": "bird"
      },
      {
        "toCreate": "pirate_ship",
        "with": "pirate"
      },
      {
        "toCreate": "rat",
        "with": "animal"
      },
      {
        "toCreate": "rat",
        "with": "mouse"
      },
      {
        "toCreate": "rope",
        "with": "tool"
      },
      {
        "toCreate": "sailor",
        "with": "human"
      },
      {
        "toCreate": "seasickness",
        "with": "sickness"
      },
      {
        "toCreate": "steamboat",
        "with": "steam_engine"
      }
    ],
    "name": "Sailboat"
  },
  "sailor": {
    "combine": [
      {
        "toCreate": "angler",
        "with": "fishing_rod"
      },
      {
        "toCreate": "cyborg",
        "with": "robot"
      },
      {
        "toCreate": "idea",
        "with": "sailor"
      },
      {
        "toCreate": "pirate",
        "with": "sword"
      },
      {
        "toCreate": "pirate",
        "with": "pirate_ship"
      },
      {
        "toCreate": "pirate",
        "with": "gun"
      },
      {
        "toCreate": "pirate",
        "with": "bayonet"
      },
      {
        "toCreate": "statue",
        "with": "medusa"
      },
      {
        "toCreate": "treasure",
        "with": "treasure_map"
      }
    ],
    "name": "Sailor"
  },
  "salt": {
    "combine": [
      {
        "toCreate": "caviar",
        "with": "roe"
      },
      {
        "toCreate": "sack",
        "with": "container"
      }
    ],
    "name": "Salt"
  },
  "sand": {
    "combine": [
      {
        "toCreate": "beach",
        "with": "ocean"
      },
      {
        "toCreate": "beach",
        "with": "sea"
      },
      {
        "toCreate": "beach",
        "with": "lake"
      },
      {
        "toCreate": "beach",
        "with": "water"
      },
      {
        "toCreate": "beach",
        "with": "wave"
      },
      {
        "toCreate": "cactus",
        "with": "plant"
      },
      {
        "toCreate": "cactus",
        "with": "tree"
      },
      {
        "toCreate": "camel",
        "with": "horse"
      },
      {
        "toCreate": "camel",
        "with": "livestock"
      },
      {
        "toCreate": "camel",
        "with": "cow"
      },
      {
        "toCreate": "clay",
        "with": "mud"
      },
      {
        "toCreate": "clay",
        "with": "mineral"
      },
      {
        "toCreate": "desert",
        "with": "sand"
      },
      {
        "toCreate": "desert",
        "with": "land"
      },
      {
        "toCreate": "desert",
        "with": "cactus"
      },
      {
        "toCreate": "desert",
        "with": "lizard"
      },
      {
        "toCreate": "desert",
        "with": "vulture"
      },
      {
        "toCreate": "dune",
        "with": "wind"
      },
      {
        "toCreate": "dune",
        "with": "desert"
      },
      {
        "toCreate": "glass",
        "with": "fire"
      },
      {
        "toCreate": "glass",
        "with": "heat"
      },
      {
        "toCreate": "glass",
        "with": "electricity"
      },
      {
        "toCreate": "glass",
        "with": "lightning"
      },
      {
        "toCreate": "gold",
        "with": "metal"
      },
      {
        "toCreate": "gold",
        "with": "steel"
      },
      {
        "toCreate": "hourglass",
        "with": "glass"
      },
      {
        "toCreate": "hourglass",
        "with": "time"
      },
      {
        "toCreate": "hourglass",
        "with": "container"
      },
      {
        "toCreate": "ostrich",
        "with": "bird"
      },
      {
        "toCreate": "ostrich",
        "with": "eagle"
      },
      {
        "toCreate": "ostrich",
        "with": "chicken"
      },
      {
        "toCreate": "palm",
        "with": "tree"
      },
      {
        "toCreate": "quicksand",
        "with": "swamp"
      },
      {
        "toCreate": "sand_castle",
        "with": "castle"
      },
      {
        "toCreate": "sandpaper",
        "with": "paper"
      },
      {
        "toCreate": "sandpaper",
        "with": "fabric"
      },
      {
        "toCreate": "sandstone",
        "with": "stone"
      },
      {
        "toCreate": "sandstone",
        "with": "rock"
      },
      {
        "toCreate": "sandstone",
        "with": "earth"
      },
      {
        "toCreate": "sandstorm",
        "with": "storm"
      },
      {
        "toCreate": "sandstorm",
        "with": "tornado"
      },
      {
        "toCreate": "sandstorm",
        "with": "motion"
      },
      {
        "toCreate": "scorpion",
        "with": "animal"
      },
      {
        "toCreate": "scorpion",
        "with": "spider"
      },
      {
        "toCreate": "turtle",
        "with": "egg"
      }
    ],
    "name": "Sand"
  },
  "sand_castle": {
    "combine": [],
    "name": "Sand Castle"
  },
  "sandpaper": {
    "combine": [],
    "name": "Sandpaper"
  },
  "sandstone": {
    "combine": [],
    "name": "Sandstone"
  },
  "sandstorm": {
    "combine": [
      {
        "toCreate": "dune",
        "with": "desert"
      },
      {
        "toCreate": "electricity",
        "with": "wind_turbine"
      }
    ],
    "name": "Sandstorm"
  },
  "sandwich": {
    "combine": [
      {
        "toCreate": "cheeseburger",
        "with": "cheese"
      },
      {
        "toCreate": "picnic",
        "with": "grass"
      },
      {
        "toCreate": "picnic",
        "with": "beach"
      },
      {
        "toCreate": "picnic",
        "with": "fabric"
      },
      {
        "toCreate": "picnic",
        "with": "lawn"
      },
      {
        "toCreate": "picnic",
        "with": "garden"
      },
      {
        "toCreate": "toast",
        "with": "fire"
      },
      {
        "toCreate": "toast",
        "with": "warmth"
      }
    ],
    "name": "Sandwich"
  },
  "santa": {
    "combine": [
      {
        "toCreate": "candy_cane",
        "with": "sugar"
      },
      {
        "toCreate": "christmas_stocking",
        "with": "wool"
      },
      {
        "toCreate": "gift",
        "with": "christmas_tree"
      },
      {
        "toCreate": "gift",
        "with": "christmas_stocking"
      },
      {
        "toCreate": "gift",
        "with": "chimney"
      },
      {
        "toCreate": "gift",
        "with": "fireplace"
      },
      {
        "toCreate": "gift",
        "with": "cookie"
      },
      {
        "toCreate": "gift",
        "with": "milk"
      },
      {
        "toCreate": "gift",
        "with": "wrapping_paper"
      },
      {
        "toCreate": "reindeer",
        "with": "animal"
      },
      {
        "toCreate": "reindeer",
        "with": "livestock"
      },
      {
        "toCreate": "snow_globe",
        "with": "crystal_ball"
      },
      {
        "toCreate": "snow_globe",
        "with": "glass"
      },
      {
        "toCreate": "wrapping_paper",
        "with": "paper"
      }
    ],
    "name": "Santa"
  },
  "sap": {
    "combine": [
      {
        "toCreate": "maple_syrup",
        "with": "heat"
      },
      {
        "toCreate": "maple_syrup",
        "with": "sugar"
      }
    ],
    "name": "Sap"
  },
  "saturn": {
    "combine": [
      {
        "toCreate": "alien",
        "with": "life"
      },
      {
        "toCreate": "astronaut",
        "with": "human"
      },
      {
        "toCreate": "jupiter",
        "with": "big"
      },
      {
        "toCreate": "telescope",
        "with": "glass"
      }
    ],
    "name": "Saturn"
  },
  "scalpel": {
    "combine": [],
    "name": "Scalpel"
  },
  "scarecrow": {
    "combine": [
      {
        "toCreate": "crow",
        "with": "bird"
      },
      {
        "toCreate": "crow",
        "with": "pigeon"
      },
      {
        "toCreate": "crow",
        "with": "owl"
      },
      {
        "toCreate": "crow",
        "with": "hummingbird"
      }
    ],
    "name": "Scarecrow"
  },
  "science": {
    "combine": [
      {
        "toCreate": "archeologist",
        "with": "ruins"
      },
      {
        "toCreate": "current",
        "with": "ocean"
      },
      {
        "toCreate": "current",
        "with": "sea"
      },
      {
        "toCreate": "domestication",
        "with": "animal"
      },
      {
        "toCreate": "domestication",
        "with": "farmer"
      },
      {
        "toCreate": "electricity",
        "with": "storm"
      },
      {
        "toCreate": "energy",
        "with": "fire"
      },
      {
        "toCreate": "energy",
        "with": "heat"
      },
      {
        "toCreate": "gas",
        "with": "air"
      },
      {
        "toCreate": "heat",
        "with": "fire"
      },
      {
        "toCreate": "idea",
        "with": "light_bulb"
      },
      {
        "toCreate": "idea",
        "with": "science"
      },
      {
        "toCreate": "idea",
        "with": "philosophy"
      },
      {
        "toCreate": "liquid",
        "with": "water"
      },
      {
        "toCreate": "magma",
        "with": "lava"
      },
      {
        "toCreate": "magma",
        "with": "volcano"
      },
      {
        "toCreate": "motion",
        "with": "wind"
      },
      {
        "toCreate": "motion",
        "with": "tornado"
      },
      {
        "toCreate": "motion",
        "with": "stream"
      },
      {
        "toCreate": "motion",
        "with": "river"
      },
      {
        "toCreate": "organic_matter",
        "with": "life"
      },
      {
        "toCreate": "organic_matter",
        "with": "corpse"
      },
      {
        "toCreate": "paleontologist",
        "with": "dinosaur"
      },
      {
        "toCreate": "paleontologist",
        "with": "fossil"
      },
      {
        "toCreate": "penicillin",
        "with": "mold"
      },
      {
        "toCreate": "plasma",
        "with": "star"
      },
      {
        "toCreate": "plasma",
        "with": "sun"
      },
      {
        "toCreate": "pressure",
        "with": "geyser"
      },
      {
        "toCreate": "rain",
        "with": "fire"
      },
      {
        "toCreate": "solid",
        "with": "earth"
      }
    ],
    "name": "Science"
  },
  "scissors": {
    "combine": [
      {
        "toCreate": "bonsai_tree",
        "with": "tree"
      },
      {
        "toCreate": "cashmere",
        "with": "mountain_goat"
      },
      {
        "toCreate": "confetti",
        "with": "paper"
      },
      {
        "toCreate": "wool",
        "with": "sheep"
      }
    ],
    "name": "Scissors"
  },
  "scorpion": {
    "combine": [
      {
        "toCreate": "egg",
        "with": "scorpion"
      },
      {
        "toCreate": "small",
        "with": "philosophy"
      }
    ],
    "name": "Scorpion"
  },
  "scuba_tank": {
    "combine": [
      {
        "toCreate": "diver",
        "with": "human"
      },
      {
        "toCreate": "diver",
        "with": "swimmer"
      },
      {
        "toCreate": "diver",
        "with": "ocean"
      },
      {
        "toCreate": "diver",
        "with": "sea"
      },
      {
        "toCreate": "diver",
        "with": "lake"
      }
    ],
    "name": "Scuba Tank"
  },
  "scythe": {
    "combine": [
      {
        "toCreate": "grim_reaper",
        "with": "human"
      },
      {
        "toCreate": "grim_reaper",
        "with": "corpse"
      },
      {
        "toCreate": "grim_reaper",
        "with": "zombie"
      },
      {
        "toCreate": "grim_reaper",
        "with": "death"
      },
      {
        "toCreate": "grim_reaper",
        "with": "boat"
      },
      {
        "toCreate": "grim_reaper",
        "with": "river"
      },
      {
        "toCreate": "hay",
        "with": "grass"
      },
      {
        "toCreate": "lawn_mower",
        "with": "electricity"
      },
      {
        "toCreate": "lawn_mower",
        "with": "helicopter"
      },
      {
        "toCreate": "lawn_mower",
        "with": "windmill"
      },
      {
        "toCreate": "lawn_mower",
        "with": "tool"
      },
      {
        "toCreate": "lawn_mower",
        "with": "machine"
      },
      {
        "toCreate": "lawn_mower",
        "with": "motion"
      }
    ],
    "name": "Scythe"
  },
  "sea": {
    "combine": [
      {
        "toCreate": "archipelago",
        "with": "island"
      },
      {
        "toCreate": "arctic",
        "with": "cold"
      },
      {
        "toCreate": "beach",
        "with": "sand"
      },
      {
        "toCreate": "boat",
        "with": "wood"
      },
      {
        "toCreate": "coral",
        "with": "tree"
      },
      {
        "toCreate": "coral",
        "with": "fossil"
      },
      {
        "toCreate": "coral",
        "with": "bone"
      },
      {
        "toCreate": "current",
        "with": "motion"
      },
      {
        "toCreate": "current",
        "with": "heat"
      },
      {
        "toCreate": "current",
        "with": "cold"
      },
      {
        "toCreate": "current",
        "with": "science"
      },
      {
        "toCreate": "diver",
        "with": "scuba_tank"
      },
      {
        "toCreate": "fish",
        "with": "animal"
      },
      {
        "toCreate": "fish",
        "with": "egg"
      },
      {
        "toCreate": "horizon",
        "with": "sky"
      },
      {
        "toCreate": "hurricane",
        "with": "tornado"
      },
      {
        "toCreate": "hurricane",
        "with": "storm"
      },
      {
        "toCreate": "iceberg",
        "with": "ice"
      },
      {
        "toCreate": "iceberg",
        "with": "antarctica"
      },
      {
        "toCreate": "iceberg",
        "with": "arctic"
      },
      {
        "toCreate": "island",
        "with": "volcano"
      },
      {
        "toCreate": "lake",
        "with": "small"
      },
      {
        "toCreate": "life",
        "with": "electricity"
      },
      {
        "toCreate": "life",
        "with": "lightning"
      },
      {
        "toCreate": "lighthouse",
        "with": "light"
      },
      {
        "toCreate": "lighthouse",
        "with": "spotlight"
      },
      {
        "toCreate": "manatee",
        "with": "cow"
      },
      {
        "toCreate": "map",
        "with": "paper"
      },
      {
        "toCreate": "narwhal",
        "with": "unicorn"
      },
      {
        "toCreate": "ocean",
        "with": "water"
      },
      {
        "toCreate": "ocean",
        "with": "big"
      },
      {
        "toCreate": "ocean",
        "with": "sea"
      },
      {
        "toCreate": "plankton",
        "with": "life"
      },
      {
        "toCreate": "plankton",
        "with": "bacteria"
      },
      {
        "toCreate": "primordial_soup",
        "with": "lava"
      },
      {
        "toCreate": "primordial_soup",
        "with": "planet"
      },
      {
        "toCreate": "primordial_soup",
        "with": "earth"
      },
      {
        "toCreate": "roe",
        "with": "egg"
      },
      {
        "toCreate": "sailor",
        "with": "human"
      },
      {
        "toCreate": "salt",
        "with": "sun"
      },
      {
        "toCreate": "salt",
        "with": "fire"
      },
      {
        "toCreate": "salt",
        "with": "mineral"
      },
      {
        "toCreate": "seagull",
        "with": "bird"
      },
      {
        "toCreate": "seagull",
        "with": "pigeon"
      },
      {
        "toCreate": "seagull",
        "with": "rat"
      },
      {
        "toCreate": "seahorse",
        "with": "horse"
      },
      {
        "toCreate": "seal",
        "with": "dog"
      },
      {
        "toCreate": "seaplane",
        "with": "airplane"
      },
      {
        "toCreate": "seasickness",
        "with": "sickness"
      },
      {
        "toCreate": "seaweed",
        "with": "plant"
      },
      {
        "toCreate": "seaweed",
        "with": "grass"
      },
      {
        "toCreate": "shark",
        "with": "blood"
      },
      {
        "toCreate": "shark",
        "with": "wolf"
      },
      {
        "toCreate": "starfish",
        "with": "star"
      },
      {
        "toCreate": "steamboat",
        "with": "steam_engine"
      },
      {
        "toCreate": "swim_goggles",
        "with": "glasses"
      },
      {
        "toCreate": "swim_goggles",
        "with": "sunglasses"
      },
      {
        "toCreate": "tide",
        "with": "moon"
      },
      {
        "toCreate": "tide",
        "with": "time"
      },
      {
        "toCreate": "tsunami",
        "with": "earthquake"
      },
      {
        "toCreate": "tsunami",
        "with": "explosion"
      },
      {
        "toCreate": "tsunami",
        "with": "meteor"
      },
      {
        "toCreate": "wave",
        "with": "wind"
      },
      {
        "toCreate": "wave",
        "with": "storm"
      },
      {
        "toCreate": "wave",
        "with": "hurricane"
      }
    ],
    "name": "Sea"
  },
  "seagull": {
    "combine": [
      {
        "toCreate": "airplane",
        "with": "metal"
      },
      {
        "toCreate": "airplane",
        "with": "steel"
      },
      {
        "toCreate": "airplane",
        "with": "machine"
      },
      {
        "toCreate": "egg",
        "with": "seagull"
      },
      {
        "toCreate": "flying_squirrel",
        "with": "squirrel"
      },
      {
        "toCreate": "hummingbird",
        "with": "flower"
      },
      {
        "toCreate": "hummingbird",
        "with": "garden"
      },
      {
        "toCreate": "origami",
        "with": "paper"
      },
      {
        "toCreate": "platypus",
        "with": "beaver"
      },
      {
        "toCreate": "toucan",
        "with": "palm"
      }
    ],
    "name": "Seagull"
  },
  "seahorse": {
    "combine": [
      {
        "toCreate": "small",
        "with": "philosophy"
      }
    ],
    "name": "Seahorse"
  },
  "seal": {
    "combine": [],
    "name": "Seal"
  },
  "seaplane": {
    "combine": [
      {
        "toCreate": "drone",
        "with": "robot"
      },
      {
        "toCreate": "flying_squirrel",
        "with": "squirrel"
      },
      {
        "toCreate": "hangar",
        "with": "container"
      },
      {
        "toCreate": "hangar",
        "with": "house"
      },
      {
        "toCreate": "hangar",
        "with": "wall"
      },
      {
        "toCreate": "hangar",
        "with": "barn"
      },
      {
        "toCreate": "helicopter",
        "with": "blade"
      },
      {
        "toCreate": "helicopter",
        "with": "windmill"
      },
      {
        "toCreate": "helicopter",
        "with": "wind_turbine"
      },
      {
        "toCreate": "pilot",
        "with": "human"
      }
    ],
    "name": "Seaplane"
  },
  "seasickness": {
    "combine": [],
    "name": "Seasickness"
  },
  "seaweed": {
    "combine": [
      {
        "toCreate": "sushi",
        "with": "fish"
      },
      {
        "toCreate": "sushi",
        "with": "caviar"
      }
    ],
    "name": "Seaweed"
  },
  "seed": {
    "combine": [
      {
        "toCreate": "chocolate",
        "with": "sugar"
      },
      {
        "toCreate": "flower",
        "with": "grass"
      },
      {
        "toCreate": "flower",
        "with": "garden"
      },
      {
        "toCreate": "plant",
        "with": "water"
      },
      {
        "toCreate": "plant",
        "with": "soil"
      },
      {
        "toCreate": "plant",
        "with": "land"
      },
      {
        "toCreate": "plant",
        "with": "earth"
      }
    ],
    "name": "Seed"
  },
  "sewing_machine": {
    "combine": [],
    "name": "Sewing Machine"
  },
  "shark": {
    "combine": [
      {
        "toCreate": "meat",
        "with": "tool"
      },
      {
        "toCreate": "meat",
        "with": "axe"
      },
      {
        "toCreate": "meat",
        "with": "sword"
      },
      {
        "toCreate": "meat",
        "with": "butcher"
      },
      {
        "toCreate": "meat",
        "with": "net"
      },
      {
        "toCreate": "narwhal",
        "with": "unicorn"
      },
      {
        "toCreate": "swordfish",
        "with": "sword"
      },
      {
        "toCreate": "swordfish",
        "with": "blade"
      }
    ],
    "name": "Shark"
  },
  "sheep": {
    "combine": [
      {
        "toCreate": "alpaca",
        "with": "mountain"
      },
      {
        "toCreate": "alpaca",
        "with": "mountain_range"
      },
      {
        "toCreate": "alpaca",
        "with": "mountain_goat"
      },
      {
        "toCreate": "barn",
        "with": "house"
      },
      {
        "toCreate": "barn",
        "with": "container"
      },
      {
        "toCreate": "cotton",
        "with": "plant"
      },
      {
        "toCreate": "lasso",
        "with": "rope"
      },
      {
        "toCreate": "leather",
        "with": "blade"
      },
      {
        "toCreate": "leather",
        "with": "sword"
      },
      {
        "toCreate": "leather",
        "with": "tool"
      },
      {
        "toCreate": "wool",
        "with": "tool"
      },
      {
        "toCreate": "wool",
        "with": "scissors"
      },
      {
        "toCreate": "wool",
        "with": "blade"
      }
    ],
    "name": "Sheep"
  },
  "sheet_music": {
    "combine": [
      {
        "toCreate": "music",
        "with": "musician"
      },
      {
        "toCreate": "musician",
        "with": "human"
      }
    ],
    "name": "Sheet Music"
  },
  "shovel": {
    "combine": [],
    "name": "Shovel"
  },
  "shuriken": {
    "combine": [
      {
        "toCreate": "katana",
        "with": "blade"
      },
      {
        "toCreate": "katana",
        "with": "sword"
      },
      {
        "toCreate": "ninja",
        "with": "human"
      },
      {
        "toCreate": "ninja_turtle",
        "with": "turtle"
      }
    ],
    "name": "Shuriken"
  },
  "sickness": {
    "combine": [
      {
        "toCreate": "acid_rain",
        "with": "rain"
      },
      {
        "toCreate": "acid_rain",
        "with": "cloud"
      },
      {
        "toCreate": "hospital",
        "with": "house"
      },
      {
        "toCreate": "hospital",
        "with": "wall"
      },
      {
        "toCreate": "seasickness",
        "with": "boat"
      },
      {
        "toCreate": "seasickness",
        "with": "steamboat"
      },
      {
        "toCreate": "seasickness",
        "with": "sailboat"
      },
      {
        "toCreate": "seasickness",
        "with": "ocean"
      },
      {
        "toCreate": "seasickness",
        "with": "sea"
      },
      {
        "toCreate": "seasickness",
        "with": "lake"
      },
      {
        "toCreate": "sickness",
        "with": "human"
      }
    ],
    "name": "Sickness"
  },
  "silo": {
    "combine": [],
    "name": "Silo"
  },
  "skateboard": {
    "combine": [],
    "name": "Skateboard"
  },
  "skeleton": {
    "combine": [
      {
        "toCreate": "death",
        "with": "philosophy"
      },
      {
        "toCreate": "fossil",
        "with": "earth"
      },
      {
        "toCreate": "fossil",
        "with": "stone"
      },
      {
        "toCreate": "fossil",
        "with": "rock"
      },
      {
        "toCreate": "fossil",
        "with": "time"
      },
      {
        "toCreate": "jack-o-lantern",
        "with": "pumpkin"
      },
      {
        "toCreate": "jack-o-lantern",
        "with": "vegetable"
      }
    ],
    "name": "Skeleton"
  },
  "ski_goggles": {
    "combine": [
      {
        "toCreate": "skateboard",
        "with": "wheel"
      },
      {
        "toCreate": "skier",
        "with": "human"
      }
    ],
    "name": "Ski Goggles"
  },
  "skier": {
    "combine": [
      {
        "toCreate": "idea",
        "with": "skier"
      },
      {
        "toCreate": "skateboard",
        "with": "wheel"
      },
      {
        "toCreate": "ski_goggles",
        "with": "glasses"
      },
      {
        "toCreate": "ski_goggles",
        "with": "sunglasses"
      }
    ],
    "name": "Skier"
  },
  "sky": {
    "combine": [
      {
        "toCreate": "atmosphere",
        "with": "air"
      },
      {
        "toCreate": "aurora",
        "with": "antarctica"
      },
      {
        "toCreate": "aurora",
        "with": "electricity"
      },
      {
        "toCreate": "aurora",
        "with": "arctic"
      },
      {
        "toCreate": "bat",
        "with": "mouse"
      },
      {
        "toCreate": "bird",
        "with": "animal"
      },
      {
        "toCreate": "bird",
        "with": "egg"
      },
      {
        "toCreate": "cloud",
        "with": "mist"
      },
      {
        "toCreate": "cloud",
        "with": "water"
      },
      {
        "toCreate": "darkness",
        "with": "night"
      },
      {
        "toCreate": "darkness",
        "with": "twilight"
      },
      {
        "toCreate": "day",
        "with": "sun"
      },
      {
        "toCreate": "dragon",
        "with": "lizard"
      },
      {
        "toCreate": "fireworks",
        "with": "explosion"
      },
      {
        "toCreate": "flying_fish",
        "with": "fish"
      },
      {
        "toCreate": "hail",
        "with": "ice"
      },
      {
        "toCreate": "horizon",
        "with": "earth"
      },
      {
        "toCreate": "horizon",
        "with": "continent"
      },
      {
        "toCreate": "horizon",
        "with": "land"
      },
      {
        "toCreate": "horizon",
        "with": "ocean"
      },
      {
        "toCreate": "horizon",
        "with": "sea"
      },
      {
        "toCreate": "horizon",
        "with": "lake"
      },
      {
        "toCreate": "kite",
        "with": "paper"
      },
      {
        "toCreate": "meteor",
        "with": "meteoroid"
      },
      {
        "toCreate": "moon",
        "with": "cheese"
      },
      {
        "toCreate": "moon",
        "with": "planet"
      },
      {
        "toCreate": "moon",
        "with": "stone"
      },
      {
        "toCreate": "moon",
        "with": "time"
      },
      {
        "toCreate": "night",
        "with": "moon"
      },
      {
        "toCreate": "parachute",
        "with": "umbrella"
      },
      {
        "toCreate": "pegasus",
        "with": "horse"
      },
      {
        "toCreate": "pegasus",
        "with": "unicorn"
      },
      {
        "toCreate": "planet",
        "with": "earth"
      },
      {
        "toCreate": "pterodactyl",
        "with": "dinosaur"
      },
      {
        "toCreate": "skyscraper",
        "with": "house"
      },
      {
        "toCreate": "skyscraper",
        "with": "village"
      },
      {
        "toCreate": "space",
        "with": "star"
      },
      {
        "toCreate": "space",
        "with": "solar_system"
      },
      {
        "toCreate": "star",
        "with": "night"
      },
      {
        "toCreate": "star",
        "with": "space"
      },
      {
        "toCreate": "sun",
        "with": "light"
      },
      {
        "toCreate": "sun",
        "with": "fire"
      },
      {
        "toCreate": "sun",
        "with": "day"
      },
      {
        "toCreate": "sunglasses",
        "with": "glasses"
      },
      {
        "toCreate": "telescope",
        "with": "glass"
      },
      {
        "toCreate": "ufo",
        "with": "alien"
      }
    ],
    "name": "Sky"
  },
  "skyscraper": {
    "combine": [
      {
        "toCreate": "bank",
        "with": "gold"
      },
      {
        "toCreate": "bank",
        "with": "safe"
      },
      {
        "toCreate": "bank",
        "with": "money"
      },
      {
        "toCreate": "city",
        "with": "skyscraper"
      },
      {
        "toCreate": "city",
        "with": "village"
      },
      {
        "toCreate": "city",
        "with": "container"
      },
      {
        "toCreate": "city",
        "with": "bank"
      },
      {
        "toCreate": "city",
        "with": "post_office"
      },
      {
        "toCreate": "kaiju",
        "with": "dinosaur"
      },
      {
        "toCreate": "pigeon",
        "with": "bird"
      },
      {
        "toCreate": "rat",
        "with": "mouse"
      },
      {
        "toCreate": "ruins",
        "with": "time"
      }
    ],
    "name": "Skyscraper"
  },
  "sleigh": {
    "combine": [
      {
        "toCreate": "garage",
        "with": "house"
      },
      {
        "toCreate": "garage",
        "with": "container"
      },
      {
        "toCreate": "garage",
        "with": "wall"
      },
      {
        "toCreate": "garage",
        "with": "barn"
      }
    ],
    "name": "Sleigh"
  },
  "sloth": {
    "combine": [],
    "name": "Sloth"
  },
  "small": {
    "combine": [
      {
        "toCreate": "aquarium",
        "with": "swimming_pool"
      },
      {
        "toCreate": "bacteria",
        "with": "life"
      },
      {
        "toCreate": "bonsai_tree",
        "with": "tree"
      },
      {
        "toCreate": "boulder",
        "with": "hill"
      },
      {
        "toCreate": "gust",
        "with": "wind"
      },
      {
        "toCreate": "gust",
        "with": "air"
      },
      {
        "toCreate": "hill",
        "with": "mountain"
      },
      {
        "toCreate": "hummingbird",
        "with": "bird"
      },
      {
        "toCreate": "lake",
        "with": "sea"
      },
      {
        "toCreate": "land",
        "with": "continent"
      },
      {
        "toCreate": "little_alchemy",
        "with": "alchemist"
      },
      {
        "toCreate": "lizard",
        "with": "dinosaur"
      },
      {
        "toCreate": "mercury",
        "with": "planet"
      },
      {
        "toCreate": "pebble",
        "with": "stone"
      },
      {
        "toCreate": "pebble",
        "with": "earth"
      },
      {
        "toCreate": "pebble",
        "with": "rock"
      },
      {
        "toCreate": "plant",
        "with": "tree"
      },
      {
        "toCreate": "pond",
        "with": "lake"
      },
      {
        "toCreate": "puddle",
        "with": "pond"
      },
      {
        "toCreate": "rivulet",
        "with": "stream"
      },
      {
        "toCreate": "rock",
        "with": "boulder"
      },
      {
        "toCreate": "saturn",
        "with": "jupiter"
      },
      {
        "toCreate": "sea",
        "with": "ocean"
      },
      {
        "toCreate": "stream",
        "with": "river"
      },
      {
        "toCreate": "watch",
        "with": "clock"
      }
    ],
    "name": "Small"
  },
  "smog": {
    "combine": [
      {
        "toCreate": "acid_rain",
        "with": "rain"
      },
      {
        "toCreate": "acid_rain",
        "with": "cloud"
      },
      {
        "toCreate": "venus",
        "with": "planet"
      }
    ],
    "name": "Smog"
  },
  "smoke": {
    "combine": [
      {
        "toCreate": "acid_rain",
        "with": "rain"
      },
      {
        "toCreate": "acid_rain",
        "with": "cloud"
      },
      {
        "toCreate": "chimney",
        "with": "brick"
      },
      {
        "toCreate": "chimney",
        "with": "fireplace"
      },
      {
        "toCreate": "chimney",
        "with": "house"
      },
      {
        "toCreate": "chimney",
        "with": "stone"
      },
      {
        "toCreate": "ham",
        "with": "meat"
      },
      {
        "toCreate": "ham",
        "with": "pig"
      },
      {
        "toCreate": "smog",
        "with": "fog"
      },
      {
        "toCreate": "smog",
        "with": "city"
      },
      {
        "toCreate": "smoke_signal",
        "with": "fabric"
      },
      {
        "toCreate": "smoke_signal",
        "with": "letter"
      },
      {
        "toCreate": "tobacco",
        "with": "plant"
      },
      {
        "toCreate": "tobacco",
        "with": "grass"
      }
    ],
    "name": "Smoke"
  },
  "smoke_signal": {
    "combine": [],
    "name": "Smoke Signal"
  },
  "smoothie": {
    "combine": [
      {
        "toCreate": "cup",
        "with": "container"
      },
      {
        "toCreate": "cup",
        "with": "bottle"
      }
    ],
    "name": "Smoothie"
  },
  "snake": {
    "combine": [
      {
        "toCreate": "chameleon",
        "with": "rainbow"
      },
      {
        "toCreate": "chameleon",
        "with": "double_rainbow!"
      },
      {
        "toCreate": "egg",
        "with": "snake"
      },
      {
        "toCreate": "medusa",
        "with": "human"
      },
      {
        "toCreate": "medusa",
        "with": "legend"
      },
      {
        "toCreate": "medusa",
        "with": "story"
      },
      {
        "toCreate": "medusa",
        "with": "hero"
      }
    ],
    "name": "Snake"
  },
  "snow": {
    "combine": [
      {
        "toCreate": "antarctica",
        "with": "desert"
      },
      {
        "toCreate": "antarctica",
        "with": "continent"
      },
      {
        "toCreate": "avalanche",
        "with": "earthquake"
      },
      {
        "toCreate": "blizzard",
        "with": "storm"
      },
      {
        "toCreate": "blizzard",
        "with": "hurricane"
      },
      {
        "toCreate": "blizzard",
        "with": "tornado"
      },
      {
        "toCreate": "blizzard",
        "with": "wind"
      },
      {
        "toCreate": "blizzard",
        "with": "snow"
      },
      {
        "toCreate": "blizzard",
        "with": "big"
      },
      {
        "toCreate": "cold",
        "with": "human"
      },
      {
        "toCreate": "glacier",
        "with": "mountain"
      },
      {
        "toCreate": "glacier",
        "with": "mountain_range"
      },
      {
        "toCreate": "husky",
        "with": "dog"
      },
      {
        "toCreate": "ice_cream",
        "with": "milk"
      },
      {
        "toCreate": "iced_tea",
        "with": "tea"
      },
      {
        "toCreate": "igloo",
        "with": "house"
      },
      {
        "toCreate": "penguin",
        "with": "bird"
      },
      {
        "toCreate": "popsicle",
        "with": "juice"
      },
      {
        "toCreate": "ski_goggles",
        "with": "glasses"
      },
      {
        "toCreate": "ski_goggles",
        "with": "sunglasses"
      },
      {
        "toCreate": "sleigh",
        "with": "cart"
      },
      {
        "toCreate": "sleigh",
        "with": "wagon"
      },
      {
        "toCreate": "snow_globe",
        "with": "crystal_ball"
      },
      {
        "toCreate": "snow_globe",
        "with": "glass"
      },
      {
        "toCreate": "snowball",
        "with": "stone"
      },
      {
        "toCreate": "snowball",
        "with": "rock"
      },
      {
        "toCreate": "snowball",
        "with": "earth"
      },
      {
        "toCreate": "snowball",
        "with": "human"
      },
      {
        "toCreate": "snowboard",
        "with": "wood"
      },
      {
        "toCreate": "snowboard",
        "with": "surfer"
      },
      {
        "toCreate": "snowman",
        "with": "human"
      },
      {
        "toCreate": "snowman",
        "with": "carrot"
      },
      {
        "toCreate": "snowman",
        "with": "coal"
      },
      {
        "toCreate": "snowmobile",
        "with": "motorcycle"
      },
      {
        "toCreate": "snowmobile",
        "with": "car"
      },
      {
        "toCreate": "water",
        "with": "heat"
      }
    ],
    "name": "Snow"
  },
  "snow_globe": {
    "combine": [
      {
        "toCreate": "crystal_ball",
        "with": "witch"
      },
      {
        "toCreate": "crystal_ball",
        "with": "wizard"
      },
      {
        "toCreate": "crystal_ball",
        "with": "magic"
      }
    ],
    "name": "Snow Globe"
  },
  "snowball": {
    "combine": [
      {
        "toCreate": "snow_globe",
        "with": "crystal_ball"
      },
      {
        "toCreate": "snow_globe",
        "with": "glass"
      },
      {
        "toCreate": "snowman",
        "with": "carrot"
      },
      {
        "toCreate": "snowman",
        "with": "coal"
      },
      {
        "toCreate": "snowman",
        "with": "snowball"
      }
    ],
    "name": "Snowball"
  },
  "snowboard": {
    "combine": [
      {
        "toCreate": "skateboard",
        "with": "wheel"
      }
    ],
    "name": "Snowboard"
  },
  "snowman": {
    "combine": [
      {
        "toCreate": "carrot",
        "with": "vegetable"
      },
      {
        "toCreate": "carrot",
        "with": "sun"
      },
      {
        "toCreate": "igloo",
        "with": "house"
      },
      {
        "toCreate": "snow_globe",
        "with": "crystal_ball"
      },
      {
        "toCreate": "snow_globe",
        "with": "glass"
      }
    ],
    "name": "Snowman"
  },
  "snowmobile": {
    "combine": [
      {
        "toCreate": "garage",
        "with": "house"
      },
      {
        "toCreate": "garage",
        "with": "container"
      },
      {
        "toCreate": "garage",
        "with": "wall"
      },
      {
        "toCreate": "garage",
        "with": "barn"
      },
      {
        "toCreate": "snow_globe",
        "with": "crystal_ball"
      },
      {
        "toCreate": "snow_globe",
        "with": "glass"
      }
    ],
    "name": "Snowmobile"
  },
  "soap": {
    "combine": [],
    "name": "Soap"
  },
  "soda": {
    "combine": [
      {
        "toCreate": "bottle",
        "with": "container"
      }
    ],
    "name": "Soda"
  },
  "soil": {
    "combine": [
      {
        "toCreate": "anthill",
        "with": "ant"
      },
      {
        "toCreate": "dust",
        "with": "air"
      },
      {
        "toCreate": "field",
        "with": "farmer"
      },
      {
        "toCreate": "field",
        "with": "tool"
      },
      {
        "toCreate": "field",
        "with": "plow"
      },
      {
        "toCreate": "land",
        "with": "soil"
      },
      {
        "toCreate": "land",
        "with": "big"
      },
      {
        "toCreate": "mud",
        "with": "water"
      },
      {
        "toCreate": "plant",
        "with": "seed"
      },
      {
        "toCreate": "plant",
        "with": "life"
      },
      {
        "toCreate": "plant",
        "with": "rain"
      }
    ],
    "name": "Soil"
  },
  "solar_cell": {
    "combine": [
      {
        "toCreate": "electric_car",
        "with": "car"
      },
      {
        "toCreate": "electric_car",
        "with": "wagon"
      },
      {
        "toCreate": "electricity",
        "with": "sun"
      },
      {
        "toCreate": "electricity",
        "with": "light"
      },
      {
        "toCreate": "electricity",
        "with": "star"
      }
    ],
    "name": "Solar Cell"
  },
  "solar_system": {
    "combine": [
      {
        "toCreate": "alien",
        "with": "life"
      },
      {
        "toCreate": "astronaut",
        "with": "human"
      },
      {
        "toCreate": "big",
        "with": "philosophy"
      },
      {
        "toCreate": "galaxy",
        "with": "solar_system"
      },
      {
        "toCreate": "galaxy",
        "with": "star"
      },
      {
        "toCreate": "galaxy",
        "with": "space"
      },
      {
        "toCreate": "galaxy",
        "with": "container"
      },
      {
        "toCreate": "meteoroid",
        "with": "stone"
      },
      {
        "toCreate": "meteoroid",
        "with": "rock"
      },
      {
        "toCreate": "meteoroid",
        "with": "boulder"
      },
      {
        "toCreate": "planet",
        "with": "earth"
      },
      {
        "toCreate": "space",
        "with": "night"
      },
      {
        "toCreate": "space",
        "with": "sky"
      }
    ],
    "name": "Solar System"
  },
  "solid": {
    "combine": [
      {
        "toCreate": "ice",
        "with": "water"
      },
      {
        "toCreate": "ice",
        "with": "puddle"
      },
      {
        "toCreate": "liquid",
        "with": "energy"
      },
      {
        "toCreate": "stone",
        "with": "earth"
      }
    ],
    "name": "Solid"
  },
  "sound": {
    "combine": [
      {
        "toCreate": "alarm_clock",
        "with": "clock"
      },
      {
        "toCreate": "alarm_clock",
        "with": "watch"
      },
      {
        "toCreate": "avalanche",
        "with": "glacier"
      },
      {
        "toCreate": "avalanche",
        "with": "mountain"
      },
      {
        "toCreate": "bell",
        "with": "metal"
      },
      {
        "toCreate": "bell",
        "with": "steel"
      },
      {
        "toCreate": "bell",
        "with": "wood"
      },
      {
        "toCreate": "stethoscope",
        "with": "doctor"
      },
      {
        "toCreate": "stethoscope",
        "with": "tool"
      }
    ],
    "name": "Sound"
  },
  "space": {
    "combine": [
      {
        "toCreate": "alien",
        "with": "life"
      },
      {
        "toCreate": "astronaut",
        "with": "human"
      },
      {
        "toCreate": "cold",
        "with": "thermometer"
      },
      {
        "toCreate": "galaxy",
        "with": "space"
      },
      {
        "toCreate": "galaxy",
        "with": "solar_system"
      },
      {
        "toCreate": "galaxy",
        "with": "star"
      },
      {
        "toCreate": "light_sword",
        "with": "sword"
      },
      {
        "toCreate": "meteoroid",
        "with": "stone"
      },
      {
        "toCreate": "meteoroid",
        "with": "rock"
      },
      {
        "toCreate": "meteoroid",
        "with": "boulder"
      },
      {
        "toCreate": "planet",
        "with": "earth"
      },
      {
        "toCreate": "space_station",
        "with": "house"
      },
      {
        "toCreate": "space_station",
        "with": "wall"
      },
      {
        "toCreate": "space_station",
        "with": "village"
      },
      {
        "toCreate": "spaceship",
        "with": "airplane"
      },
      {
        "toCreate": "spaceship",
        "with": "metal"
      },
      {
        "toCreate": "spaceship",
        "with": "steel"
      },
      {
        "toCreate": "spaceship",
        "with": "pirate_ship"
      },
      {
        "toCreate": "spaceship",
        "with": "boat"
      },
      {
        "toCreate": "spaceship",
        "with": "steamboat"
      },
      {
        "toCreate": "spaceship",
        "with": "car"
      },
      {
        "toCreate": "star",
        "with": "telescope"
      },
      {
        "toCreate": "star",
        "with": "sky"
      },
      {
        "toCreate": "star",
        "with": "night"
      },
      {
        "toCreate": "star",
        "with": "sun"
      },
      {
        "toCreate": "sun",
        "with": "day"
      },
      {
        "toCreate": "supernova",
        "with": "explosion"
      },
      {
        "toCreate": "telescope",
        "with": "glass"
      },
      {
        "toCreate": "universe",
        "with": "container"
      }
    ],
    "name": "Space"
  },
  "space_station": {
    "combine": [
      {
        "toCreate": "astronaut",
        "with": "human"
      },
      {
        "toCreate": "ufo",
        "with": "alien"
      }
    ],
    "name": "Space Station"
  },
  "spaceship": {
    "combine": [
      {
        "toCreate": "astronaut",
        "with": "human"
      },
      {
        "toCreate": "hangar",
        "with": "container"
      },
      {
        "toCreate": "hangar",
        "with": "house"
      },
      {
        "toCreate": "hangar",
        "with": "wall"
      },
      {
        "toCreate": "hangar",
        "with": "barn"
      },
      {
        "toCreate": "ufo",
        "with": "alien"
      }
    ],
    "name": "Spaceship"
  },
  "spaghetti": {
    "combine": [],
    "name": "Spaghetti"
  },
  "sphinx": {
    "combine": [],
    "name": "Sphinx"
  },
  "spider": {
    "combine": [
      {
        "toCreate": "ant",
        "with": "grass"
      },
      {
        "toCreate": "egg",
        "with": "spider"
      },
      {
        "toCreate": "scorpion",
        "with": "dune"
      },
      {
        "toCreate": "scorpion",
        "with": "sand"
      },
      {
        "toCreate": "scorpion",
        "with": "desert"
      },
      {
        "toCreate": "small",
        "with": "philosophy"
      },
      {
        "toCreate": "web",
        "with": "thread"
      },
      {
        "toCreate": "web",
        "with": "cotton"
      },
      {
        "toCreate": "web",
        "with": "fabric"
      },
      {
        "toCreate": "web",
        "with": "net"
      }
    ],
    "name": "Spider"
  },
  "spotlight": {
    "combine": [
      {
        "toCreate": "lighthouse",
        "with": "ocean"
      },
      {
        "toCreate": "lighthouse",
        "with": "sea"
      },
      {
        "toCreate": "lighthouse",
        "with": "house"
      },
      {
        "toCreate": "lighthouse",
        "with": "beach"
      }
    ],
    "name": "Spotlight"
  },
  "sprinkles": {
    "combine": [],
    "name": "Sprinkles"
  },
  "squirrel": {
    "combine": [
      {
        "toCreate": "flying_squirrel",
        "with": "bird"
      },
      {
        "toCreate": "flying_squirrel",
        "with": "hummingbird"
      },
      {
        "toCreate": "flying_squirrel",
        "with": "pigeon"
      },
      {
        "toCreate": "flying_squirrel",
        "with": "seagull"
      },
      {
        "toCreate": "flying_squirrel",
        "with": "air"
      },
      {
        "toCreate": "flying_squirrel",
        "with": "airplane"
      },
      {
        "toCreate": "flying_squirrel",
        "with": "seaplane"
      },
      {
        "toCreate": "flying_squirrel",
        "with": "helicopter"
      }
    ],
    "name": "Squirrel"
  },
  "star": {
    "combine": [
      {
        "toCreate": "black_hole",
        "with": "pressure"
      },
      {
        "toCreate": "black_hole",
        "with": "darkness"
      },
      {
        "toCreate": "christmas_tree",
        "with": "tree"
      },
      {
        "toCreate": "constellation",
        "with": "star"
      },
      {
        "toCreate": "electricity",
        "with": "solar_cell"
      },
      {
        "toCreate": "galaxy",
        "with": "star"
      },
      {
        "toCreate": "galaxy",
        "with": "solar_system"
      },
      {
        "toCreate": "galaxy",
        "with": "space"
      },
      {
        "toCreate": "galaxy",
        "with": "container"
      },
      {
        "toCreate": "plasma",
        "with": "science"
      },
      {
        "toCreate": "shuriken",
        "with": "blade"
      },
      {
        "toCreate": "shuriken",
        "with": "sword"
      },
      {
        "toCreate": "shuriken",
        "with": "metal"
      },
      {
        "toCreate": "shuriken",
        "with": "steel"
      },
      {
        "toCreate": "space",
        "with": "sky"
      },
      {
        "toCreate": "space",
        "with": "sun"
      },
      {
        "toCreate": "space",
        "with": "moon"
      },
      {
        "toCreate": "starfish",
        "with": "fish"
      },
      {
        "toCreate": "starfish",
        "with": "sea"
      },
      {
        "toCreate": "starfish",
        "with": "ocean"
      },
      {
        "toCreate": "supernova",
        "with": "explosion"
      },
      {
        "toCreate": "telescope",
        "with": "glass"
      }
    ],
    "name": "Star"
  },
  "starfish": {
    "combine": [],
    "name": "Starfish"
  },
  "statue": {
    "combine": [
      {
        "toCreate": "fountain",
        "with": "water"
      },
      {
        "toCreate": "fountain",
        "with": "stream"
      },
      {
        "toCreate": "gnome",
        "with": "garden"
      },
      {
        "toCreate": "golem",
        "with": "life"
      },
      {
        "toCreate": "golem",
        "with": "story"
      },
      {
        "toCreate": "golem",
        "with": "legend"
      },
      {
        "toCreate": "ice_sculpture",
        "with": "ice"
      },
      {
        "toCreate": "pigeon",
        "with": "bird"
      },
      {
        "toCreate": "scarecrow",
        "with": "hay"
      },
      {
        "toCreate": "scarecrow",
        "with": "farm"
      },
      {
        "toCreate": "scarecrow",
        "with": "barn"
      },
      {
        "toCreate": "sphinx",
        "with": "lion"
      },
      {
        "toCreate": "sphinx",
        "with": "desert"
      },
      {
        "toCreate": "sphinx",
        "with": "pyramid"
      },
      {
        "toCreate": "trojan_horse",
        "with": "horse"
      }
    ],
    "name": "Statue"
  },
  "steak": {
    "combine": [
      {
        "toCreate": "jerky",
        "with": "sun"
      },
      {
        "toCreate": "jerky",
        "with": "heat"
      }
    ],
    "name": "Steak"
  },
  "steam": {
    "combine": [
      {
        "toCreate": "boiler",
        "with": "metal"
      },
      {
        "toCreate": "boiler",
        "with": "tool"
      },
      {
        "toCreate": "geyser",
        "with": "earth"
      },
      {
        "toCreate": "geyser",
        "with": "hill"
      },
      {
        "toCreate": "geyser",
        "with": "mountain"
      },
      {
        "toCreate": "geyser",
        "with": "pressure"
      },
      {
        "toCreate": "hail",
        "with": "ice"
      },
      {
        "toCreate": "mist",
        "with": "air"
      },
      {
        "toCreate": "perfume",
        "with": "flower"
      },
      {
        "toCreate": "perfume",
        "with": "rose"
      },
      {
        "toCreate": "perfume",
        "with": "sunflower"
      },
      {
        "toCreate": "snow",
        "with": "cold"
      },
      {
        "toCreate": "steam_engine",
        "with": "machine"
      },
      {
        "toCreate": "steam_engine",
        "with": "wheel"
      }
    ],
    "name": "Steam"
  },
  "steam_engine": {
    "combine": [
      {
        "toCreate": "airplane",
        "with": "bird"
      },
      {
        "toCreate": "airplane",
        "with": "owl"
      },
      {
        "toCreate": "combustion_engine",
        "with": "petroleum"
      },
      {
        "toCreate": "combustion_engine",
        "with": "explosion"
      },
      {
        "toCreate": "engineer",
        "with": "human"
      },
      {
        "toCreate": "motorcycle",
        "with": "bicycle"
      },
      {
        "toCreate": "pirate_ship",
        "with": "pirate"
      },
      {
        "toCreate": "steamboat",
        "with": "boat"
      },
      {
        "toCreate": "steamboat",
        "with": "sailboat"
      },
      {
        "toCreate": "steamboat",
        "with": "pirate_ship"
      },
      {
        "toCreate": "steamboat",
        "with": "ocean"
      },
      {
        "toCreate": "steamboat",
        "with": "sea"
      },
      {
        "toCreate": "train",
        "with": "steel"
      },
      {
        "toCreate": "train",
        "with": "metal"
      },
      {
        "toCreate": "train",
        "with": "wheel"
      },
      {
        "toCreate": "train",
        "with": "wagon"
      }
    ],
    "name": "Steam Engine"
  },
  "steamboat": {
    "combine": [
      {
        "toCreate": "airplane",
        "with": "bird"
      },
      {
        "toCreate": "rocket",
        "with": "atmosphere"
      },
      {
        "toCreate": "sailboat",
        "with": "wind"
      },
      {
        "toCreate": "sailboat",
        "with": "fabric"
      },
      {
        "toCreate": "sailor",
        "with": "human"
      },
      {
        "toCreate": "seasickness",
        "with": "sickness"
      },
      {
        "toCreate": "spaceship",
        "with": "space"
      },
      {
        "toCreate": "titanic",
        "with": "iceberg"
      },
      {
        "toCreate": "titanic",
        "with": "legend"
      }
    ],
    "name": "Steamboat"
  },
  "steel": {
    "combine": [
      {
        "toCreate": "airplane",
        "with": "bird"
      },
      {
        "toCreate": "airplane",
        "with": "owl"
      },
      {
        "toCreate": "airplane",
        "with": "vulture"
      },
      {
        "toCreate": "airplane",
        "with": "duck"
      },
      {
        "toCreate": "airplane",
        "with": "seagull"
      },
      {
        "toCreate": "airplane",
        "with": "bat"
      },
      {
        "toCreate": "airplane",
        "with": "eagle"
      },
      {
        "toCreate": "airplane",
        "with": "pigeon"
      },
      {
        "toCreate": "airplane",
        "with": "hummingbird"
      },
      {
        "toCreate": "airplane",
        "with": "crow"
      },
      {
        "toCreate": "armor",
        "with": "fabric"
      },
      {
        "toCreate": "bbq",
        "with": "campfire"
      },
      {
        "toCreate": "bell",
        "with": "sound"
      },
      {
        "toCreate": "bell",
        "with": "hammer"
      },
      {
        "toCreate": "blade",
        "with": "stone"
      },
      {
        "toCreate": "blade",
        "with": "rock"
      },
      {
        "toCreate": "bridge",
        "with": "river"
      },
      {
        "toCreate": "bridge",
        "with": "stream"
      },
      {
        "toCreate": "bullet",
        "with": "gunpowder"
      },
      {
        "toCreate": "cage",
        "with": "wolf"
      },
      {
        "toCreate": "cage",
        "with": "fox"
      },
      {
        "toCreate": "cage",
        "with": "lion"
      },
      {
        "toCreate": "car",
        "with": "wheel"
      },
      {
        "toCreate": "cauldron",
        "with": "witch"
      },
      {
        "toCreate": "cauldron",
        "with": "pottery"
      },
      {
        "toCreate": "chain",
        "with": "rope"
      },
      {
        "toCreate": "chain",
        "with": "wire"
      },
      {
        "toCreate": "electricity",
        "with": "lightning"
      },
      {
        "toCreate": "fire_extinguisher",
        "with": "carbon_dioxide"
      },
      {
        "toCreate": "fridge",
        "with": "cold"
      },
      {
        "toCreate": "fridge",
        "with": "ice"
      },
      {
        "toCreate": "glasses",
        "with": "glass"
      },
      {
        "toCreate": "gold",
        "with": "sun"
      },
      {
        "toCreate": "gold",
        "with": "rainbow"
      },
      {
        "toCreate": "gold",
        "with": "butter"
      },
      {
        "toCreate": "gold",
        "with": "sand"
      },
      {
        "toCreate": "gold",
        "with": "light"
      },
      {
        "toCreate": "gold",
        "with": "alchemist"
      },
      {
        "toCreate": "grenade",
        "with": "explosion"
      },
      {
        "toCreate": "gun",
        "with": "bullet"
      },
      {
        "toCreate": "gun",
        "with": "bow"
      },
      {
        "toCreate": "hammer",
        "with": "tool"
      },
      {
        "toCreate": "horseshoe",
        "with": "horse"
      },
      {
        "toCreate": "lamp",
        "with": "light_bulb"
      },
      {
        "toCreate": "mailbox",
        "with": "letter"
      },
      {
        "toCreate": "mailbox",
        "with": "mailman"
      },
      {
        "toCreate": "mirror",
        "with": "glass"
      },
      {
        "toCreate": "motorcycle",
        "with": "bicycle"
      },
      {
        "toCreate": "mousetrap",
        "with": "cheese"
      },
      {
        "toCreate": "needle",
        "with": "thread"
      },
      {
        "toCreate": "pitchfork",
        "with": "hay"
      },
      {
        "toCreate": "plow",
        "with": "earth"
      },
      {
        "toCreate": "plow",
        "with": "field"
      },
      {
        "toCreate": "ring",
        "with": "diamond"
      },
      {
        "toCreate": "ring",
        "with": "love"
      },
      {
        "toCreate": "robot",
        "with": "life"
      },
      {
        "toCreate": "robot",
        "with": "golem"
      },
      {
        "toCreate": "rocket",
        "with": "atmosphere"
      },
      {
        "toCreate": "rust",
        "with": "air"
      },
      {
        "toCreate": "rust",
        "with": "wind"
      },
      {
        "toCreate": "rust",
        "with": "oxygen"
      },
      {
        "toCreate": "safe",
        "with": "money"
      },
      {
        "toCreate": "safe",
        "with": "gold"
      },
      {
        "toCreate": "shuriken",
        "with": "star"
      },
      {
        "toCreate": "spaceship",
        "with": "space"
      },
      {
        "toCreate": "spotlight",
        "with": "light"
      },
      {
        "toCreate": "steel_wool",
        "with": "wool"
      },
      {
        "toCreate": "sword",
        "with": "blade"
      },
      {
        "toCreate": "tank",
        "with": "car"
      },
      {
        "toCreate": "tool",
        "with": "human"
      },
      {
        "toCreate": "tool",
        "with": "wood"
      },
      {
        "toCreate": "train",
        "with": "steam_engine"
      },
      {
        "toCreate": "wheel",
        "with": "motion"
      },
      {
        "toCreate": "wire",
        "with": "electricity"
      },
      {
        "toCreate": "wire",
        "with": "rope"
      }
    ],
    "name": "Steel"
  },
  "steel_wool": {
    "combine": [
      {
        "toCreate": "toolbox",
        "with": "container"
      },
      {
        "toCreate": "toolbox",
        "with": "box"
      }
    ],
    "name": "Steel Wool"
  },
  "stethoscope": {
    "combine": [
      {
        "toCreate": "doctor",
        "with": "human"
      }
    ],
    "name": "Stethoscope"
  },
  "stone": {
    "combine": [
      {
        "toCreate": "axe",
        "with": "wood"
      },
      {
        "toCreate": "blade",
        "with": "metal"
      },
      {
        "toCreate": "blade",
        "with": "steel"
      },
      {
        "toCreate": "boulder",
        "with": "rock"
      },
      {
        "toCreate": "boulder",
        "with": "big"
      },
      {
        "toCreate": "brick",
        "with": "clay"
      },
      {
        "toCreate": "castle",
        "with": "knight"
      },
      {
        "toCreate": "castle",
        "with": "warrior"
      },
      {
        "toCreate": "chimney",
        "with": "smoke"
      },
      {
        "toCreate": "chimney",
        "with": "fireplace"
      },
      {
        "toCreate": "clay",
        "with": "liquid"
      },
      {
        "toCreate": "clay",
        "with": "mineral"
      },
      {
        "toCreate": "clay",
        "with": "mud"
      },
      {
        "toCreate": "coal",
        "with": "peat"
      },
      {
        "toCreate": "excalibur",
        "with": "sword"
      },
      {
        "toCreate": "flour",
        "with": "wheat"
      },
      {
        "toCreate": "fossil",
        "with": "dinosaur"
      },
      {
        "toCreate": "fossil",
        "with": "bone"
      },
      {
        "toCreate": "fossil",
        "with": "corpse"
      },
      {
        "toCreate": "fossil",
        "with": "skeleton"
      },
      {
        "toCreate": "granite",
        "with": "pressure"
      },
      {
        "toCreate": "gravestone",
        "with": "grave"
      },
      {
        "toCreate": "gravestone",
        "with": "death"
      },
      {
        "toCreate": "gravestone",
        "with": "graveyard"
      },
      {
        "toCreate": "hammer",
        "with": "tool"
      },
      {
        "toCreate": "hill",
        "with": "boulder"
      },
      {
        "toCreate": "juice",
        "with": "fruit"
      },
      {
        "toCreate": "juice",
        "with": "vegetable"
      },
      {
        "toCreate": "land",
        "with": "earth"
      },
      {
        "toCreate": "lizard",
        "with": "animal"
      },
      {
        "toCreate": "lizard",
        "with": "egg"
      },
      {
        "toCreate": "meteoroid",
        "with": "space"
      },
      {
        "toCreate": "meteoroid",
        "with": "solar_system"
      },
      {
        "toCreate": "meteoroid",
        "with": "sun"
      },
      {
        "toCreate": "mineral",
        "with": "organic_matter"
      },
      {
        "toCreate": "moon",
        "with": "sky"
      },
      {
        "toCreate": "moon",
        "with": "night"
      },
      {
        "toCreate": "moss",
        "with": "plant"
      },
      {
        "toCreate": "moss",
        "with": "grass"
      },
      {
        "toCreate": "moss",
        "with": "algae"
      },
      {
        "toCreate": "oil",
        "with": "sunflower"
      },
      {
        "toCreate": "ore",
        "with": "hammer"
      },
      {
        "toCreate": "pebble",
        "with": "small"
      },
      {
        "toCreate": "pyramid",
        "with": "desert"
      },
      {
        "toCreate": "rock",
        "with": "pebble"
      },
      {
        "toCreate": "sand",
        "with": "air"
      },
      {
        "toCreate": "sand",
        "with": "wind"
      },
      {
        "toCreate": "sandstone",
        "with": "sand"
      },
      {
        "toCreate": "snowball",
        "with": "snow"
      },
      {
        "toCreate": "sphinx",
        "with": "lion"
      },
      {
        "toCreate": "statue",
        "with": "hammer"
      },
      {
        "toCreate": "tool",
        "with": "human"
      },
      {
        "toCreate": "tool",
        "with": "wood"
      },
      {
        "toCreate": "wall",
        "with": "stone"
      },
      {
        "toCreate": "wheel",
        "with": "motion"
      }
    ],
    "name": "Stone"
  },
  "storm": {
    "combine": [
      {
        "toCreate": "ash",
        "with": "campfire"
      },
      {
        "toCreate": "blizzard",
        "with": "snow"
      },
      {
        "toCreate": "cold",
        "with": "human"
      },
      {
        "toCreate": "electricity",
        "with": "wind_turbine"
      },
      {
        "toCreate": "electricity",
        "with": "science"
      },
      {
        "toCreate": "hail",
        "with": "ice"
      },
      {
        "toCreate": "hurricane",
        "with": "ocean"
      },
      {
        "toCreate": "hurricane",
        "with": "sea"
      },
      {
        "toCreate": "jupiter",
        "with": "planet"
      },
      {
        "toCreate": "life",
        "with": "primordial_soup"
      },
      {
        "toCreate": "lightning",
        "with": "electricity"
      },
      {
        "toCreate": "lightning",
        "with": "energy"
      },
      {
        "toCreate": "lightning",
        "with": "land"
      },
      {
        "toCreate": "sandstorm",
        "with": "sand"
      },
      {
        "toCreate": "sandstorm",
        "with": "desert"
      },
      {
        "toCreate": "smoke",
        "with": "campfire"
      },
      {
        "toCreate": "tornado",
        "with": "storm"
      },
      {
        "toCreate": "tornado",
        "with": "wind"
      },
      {
        "toCreate": "tornado",
        "with": "motion"
      },
      {
        "toCreate": "umbrella",
        "with": "fabric"
      },
      {
        "toCreate": "umbrella",
        "with": "tool"
      },
      {
        "toCreate": "wave",
        "with": "ocean"
      },
      {
        "toCreate": "wave",
        "with": "sea"
      },
      {
        "toCreate": "wave",
        "with": "lake"
      }
    ],
    "name": "Storm"
  },
  "story": {
    "combine": [
      {
        "toCreate": "book",
        "with": "container"
      },
      {
        "toCreate": "centaur",
        "with": "horse"
      },
      {
        "toCreate": "don_quixote",
        "with": "windmill"
      },
      {
        "toCreate": "dragon",
        "with": "lizard"
      },
      {
        "toCreate": "excalibur",
        "with": "sword"
      },
      {
        "toCreate": "fairy_tale",
        "with": "monarch"
      },
      {
        "toCreate": "fairy_tale",
        "with": "castle"
      },
      {
        "toCreate": "fairy_tale",
        "with": "knight"
      },
      {
        "toCreate": "fairy_tale",
        "with": "dragon"
      },
      {
        "toCreate": "fairy_tale",
        "with": "unicorn"
      },
      {
        "toCreate": "frankenstein’s_monster",
        "with": "corpse"
      },
      {
        "toCreate": "frankenstein’s_monster",
        "with": "monster"
      },
      {
        "toCreate": "frankenstein’s_monster",
        "with": "monster"
      },
      {
        "toCreate": "ghost",
        "with": "graveyard"
      },
      {
        "toCreate": "ghost",
        "with": "grave"
      },
      {
        "toCreate": "ghost",
        "with": "gravestone"
      },
      {
        "toCreate": "ghost",
        "with": "night"
      },
      {
        "toCreate": "gingerbread_man",
        "with": "cookie"
      },
      {
        "toCreate": "gingerbread_man",
        "with": "cookie_dough"
      },
      {
        "toCreate": "gingerbread_man",
        "with": "dough"
      },
      {
        "toCreate": "gnome",
        "with": "garden"
      },
      {
        "toCreate": "golem",
        "with": "clay"
      },
      {
        "toCreate": "golem",
        "with": "statue"
      },
      {
        "toCreate": "hero",
        "with": "human"
      },
      {
        "toCreate": "kaiju",
        "with": "dinosaur"
      },
      {
        "toCreate": "legend",
        "with": "time"
      },
      {
        "toCreate": "legend",
        "with": "big"
      },
      {
        "toCreate": "legend",
        "with": "story"
      },
      {
        "toCreate": "legend",
        "with": "fairy_tale"
      },
      {
        "toCreate": "medusa",
        "with": "snake"
      },
      {
        "toCreate": "minotaur",
        "with": "cow"
      },
      {
        "toCreate": "nessie",
        "with": "lake"
      },
      {
        "toCreate": "nessie",
        "with": "dinosaur"
      },
      {
        "toCreate": "newspaper",
        "with": "paper"
      },
      {
        "toCreate": "philosophy",
        "with": "human"
      },
      {
        "toCreate": "pinocchio",
        "with": "golem"
      },
      {
        "toCreate": "pinocchio",
        "with": "wood"
      },
      {
        "toCreate": "santa",
        "with": "christmas_tree"
      },
      {
        "toCreate": "santa",
        "with": "christmas_stocking"
      },
      {
        "toCreate": "unicorn",
        "with": "horse"
      },
      {
        "toCreate": "witch",
        "with": "broom"
      },
      {
        "toCreate": "witch",
        "with": "cauldron"
      },
      {
        "toCreate": "yeti",
        "with": "mountain"
      },
      {
        "toCreate": "yeti",
        "with": "mountain_range"
      },
      {
        "toCreate": "yeti",
        "with": "glacier"
      },
      {
        "toCreate": "yeti",
        "with": "antarctica"
      },
      {
        "toCreate": "zombie",
        "with": "corpse"
      }
    ],
    "name": "Story"
  },
  "stream": {
    "combine": [
      {
        "toCreate": "beaver",
        "with": "animal"
      },
      {
        "toCreate": "bridge",
        "with": "wood"
      },
      {
        "toCreate": "bridge",
        "with": "metal"
      },
      {
        "toCreate": "bridge",
        "with": "steel"
      },
      {
        "toCreate": "dam",
        "with": "beaver"
      },
      {
        "toCreate": "dam",
        "with": "wall"
      },
      {
        "toCreate": "fountain",
        "with": "statue"
      },
      {
        "toCreate": "motion",
        "with": "science"
      },
      {
        "toCreate": "motion",
        "with": "philosophy"
      },
      {
        "toCreate": "oasis",
        "with": "desert"
      },
      {
        "toCreate": "river",
        "with": "big"
      },
      {
        "toCreate": "rivulet",
        "with": "small"
      },
      {
        "toCreate": "water_gun",
        "with": "gun"
      },
      {
        "toCreate": "water_lily",
        "with": "flower"
      },
      {
        "toCreate": "wheel",
        "with": "tool"
      }
    ],
    "name": "Stream"
  },
  "string_phone": {
    "combine": [],
    "name": "String Phone"
  },
  "stun_gun": {
    "combine": [],
    "name": "Stun Gun"
  },
  "sugar": {
    "combine": [
      {
        "toCreate": "cake",
        "with": "bread"
      },
      {
        "toCreate": "candy_cane",
        "with": "christmas_tree"
      },
      {
        "toCreate": "candy_cane",
        "with": "christmas_stocking"
      },
      {
        "toCreate": "candy_cane",
        "with": "santa"
      },
      {
        "toCreate": "candy_cane",
        "with": "reindeer"
      },
      {
        "toCreate": "caramel",
        "with": "heat"
      },
      {
        "toCreate": "chocolate",
        "with": "milk"
      },
      {
        "toCreate": "chocolate",
        "with": "oil"
      },
      {
        "toCreate": "chocolate",
        "with": "seed"
      },
      {
        "toCreate": "chocolate",
        "with": "coconut_milk"
      },
      {
        "toCreate": "cookie_dough",
        "with": "dough"
      },
      {
        "toCreate": "cotton_candy",
        "with": "air"
      },
      {
        "toCreate": "cotton_candy",
        "with": "thread"
      },
      {
        "toCreate": "cotton_candy",
        "with": "cloud"
      },
      {
        "toCreate": "jam",
        "with": "juice"
      },
      {
        "toCreate": "maple_syrup",
        "with": "sap"
      },
      {
        "toCreate": "marshmallows",
        "with": "campfire"
      },
      {
        "toCreate": "sprinkles",
        "with": "confetti"
      },
      {
        "toCreate": "sprinkles",
        "with": "rainbow"
      },
      {
        "toCreate": "sprinkles",
        "with": "double_rainbow!"
      },
      {
        "toCreate": "sprinkles",
        "with": "paint"
      }
    ],
    "name": "Sugar"
  },
  "sun": {
    "combine": [
      {
        "toCreate": "alcohol",
        "with": "fruit"
      },
      {
        "toCreate": "ash",
        "with": "vampire"
      },
      {
        "toCreate": "aurora",
        "with": "atmosphere"
      },
      {
        "toCreate": "big",
        "with": "philosophy"
      },
      {
        "toCreate": "black_hole",
        "with": "pressure"
      },
      {
        "toCreate": "black_hole",
        "with": "darkness"
      },
      {
        "toCreate": "brick",
        "with": "mud"
      },
      {
        "toCreate": "brick",
        "with": "clay"
      },
      {
        "toCreate": "carrot",
        "with": "snowman"
      },
      {
        "toCreate": "day",
        "with": "sky"
      },
      {
        "toCreate": "day",
        "with": "night"
      },
      {
        "toCreate": "day",
        "with": "time"
      },
      {
        "toCreate": "eclipse",
        "with": "moon"
      },
      {
        "toCreate": "electricity",
        "with": "solar_cell"
      },
      {
        "toCreate": "gold",
        "with": "metal"
      },
      {
        "toCreate": "gold",
        "with": "steel"
      },
      {
        "toCreate": "hay",
        "with": "grass"
      },
      {
        "toCreate": "jerky",
        "with": "meat"
      },
      {
        "toCreate": "jerky",
        "with": "steak"
      },
      {
        "toCreate": "meteoroid",
        "with": "rock"
      },
      {
        "toCreate": "meteoroid",
        "with": "stone"
      },
      {
        "toCreate": "meteoroid",
        "with": "boulder"
      },
      {
        "toCreate": "oxygen",
        "with": "plant"
      },
      {
        "toCreate": "oxygen",
        "with": "tree"
      },
      {
        "toCreate": "oxygen",
        "with": "grass"
      },
      {
        "toCreate": "oxygen",
        "with": "algae"
      },
      {
        "toCreate": "plasma",
        "with": "science"
      },
      {
        "toCreate": "rainbow",
        "with": "rain"
      },
      {
        "toCreate": "rainbow",
        "with": "water"
      },
      {
        "toCreate": "rainbow",
        "with": "prism"
      },
      {
        "toCreate": "salt",
        "with": "ocean"
      },
      {
        "toCreate": "salt",
        "with": "sea"
      },
      {
        "toCreate": "sky",
        "with": "atmosphere"
      },
      {
        "toCreate": "sky",
        "with": "moon"
      },
      {
        "toCreate": "solar_cell",
        "with": "tool"
      },
      {
        "toCreate": "solar_cell",
        "with": "machine"
      },
      {
        "toCreate": "solar_cell",
        "with": "electricity"
      },
      {
        "toCreate": "solar_cell",
        "with": "energy"
      },
      {
        "toCreate": "solar_system",
        "with": "planet"
      },
      {
        "toCreate": "solar_system",
        "with": "container"
      },
      {
        "toCreate": "space",
        "with": "star"
      },
      {
        "toCreate": "star",
        "with": "space"
      },
      {
        "toCreate": "sundial",
        "with": "wheel"
      },
      {
        "toCreate": "sundial",
        "with": "tool"
      },
      {
        "toCreate": "sundial",
        "with": "watch"
      },
      {
        "toCreate": "sundial",
        "with": "clock"
      },
      {
        "toCreate": "sunflower",
        "with": "plant"
      },
      {
        "toCreate": "sunflower",
        "with": "flower"
      },
      {
        "toCreate": "sunglasses",
        "with": "glasses"
      },
      {
        "toCreate": "supernova",
        "with": "explosion"
      }
    ],
    "name": "Sun"
  },
  "sundial": {
    "combine": [
      {
        "toCreate": "clock",
        "with": "machine"
      },
      {
        "toCreate": "clock",
        "with": "tool"
      },
      {
        "toCreate": "clock",
        "with": "wheel"
      },
      {
        "toCreate": "clock",
        "with": "electricity"
      }
    ],
    "name": "Sundial"
  },
  "sunflower": {
    "combine": [
      {
        "toCreate": "oil",
        "with": "pressure"
      },
      {
        "toCreate": "oil",
        "with": "stone"
      },
      {
        "toCreate": "oil",
        "with": "wheel"
      },
      {
        "toCreate": "oil",
        "with": "windmill"
      },
      {
        "toCreate": "perfume",
        "with": "water"
      },
      {
        "toCreate": "perfume",
        "with": "steam"
      },
      {
        "toCreate": "perfume",
        "with": "alcohol"
      }
    ],
    "name": "Sunflower"
  },
  "sunglasses": {
    "combine": [
      {
        "toCreate": "ski_goggles",
        "with": "snow"
      },
      {
        "toCreate": "ski_goggles",
        "with": "cold"
      },
      {
        "toCreate": "ski_goggles",
        "with": "glacier"
      },
      {
        "toCreate": "ski_goggles",
        "with": "skier"
      },
      {
        "toCreate": "swim_goggles",
        "with": "water"
      },
      {
        "toCreate": "swim_goggles",
        "with": "lake"
      },
      {
        "toCreate": "swim_goggles",
        "with": "sea"
      },
      {
        "toCreate": "swim_goggles",
        "with": "ocean"
      },
      {
        "toCreate": "swim_goggles",
        "with": "river"
      }
    ],
    "name": "Sunglasses"
  },
  "supernova": {
    "combine": [
      {
        "toCreate": "galaxy",
        "with": "container"
      },
      {
        "toCreate": "telescope",
        "with": "glass"
      }
    ],
    "name": "Supernova"
  },
  "surfer": {
    "combine": [
      {
        "toCreate": "idea",
        "with": "surfer"
      },
      {
        "toCreate": "snowboard",
        "with": "snow"
      },
      {
        "toCreate": "snowboard",
        "with": "ice"
      },
      {
        "toCreate": "snowboard",
        "with": "mountain"
      },
      {
        "toCreate": "snowboard",
        "with": "mountain_range"
      },
      {
        "toCreate": "statue",
        "with": "medusa"
      },
      {
        "toCreate": "windsurfer",
        "with": "wind"
      }
    ],
    "name": "Surfer"
  },
  "sushi": {
    "combine": [],
    "name": "Sushi"
  },
  "swamp": {
    "combine": [
      {
        "toCreate": "alligator",
        "with": "lizard"
      },
      {
        "toCreate": "lizard",
        "with": "animal"
      },
      {
        "toCreate": "lizard",
        "with": "egg"
      },
      {
        "toCreate": "peat",
        "with": "plant"
      },
      {
        "toCreate": "peat",
        "with": "pressure"
      },
      {
        "toCreate": "peat",
        "with": "time"
      },
      {
        "toCreate": "quicksand",
        "with": "sand"
      },
      {
        "toCreate": "reed",
        "with": "grass"
      },
      {
        "toCreate": "reed",
        "with": "plant"
      },
      {
        "toCreate": "sickness",
        "with": "human"
      }
    ],
    "name": "Swamp"
  },
  "sweater": {
    "combine": [],
    "name": "Sweater"
  },
  "swim_goggles": {
    "combine": [
      {
        "toCreate": "swimmer",
        "with": "human"
      }
    ],
    "name": "Swim Goggles"
  },
  "swimmer": {
    "combine": [
      {
        "toCreate": "diver",
        "with": "scuba_tank"
      },
      {
        "toCreate": "idea",
        "with": "swimmer"
      },
      {
        "toCreate": "mermaid",
        "with": "magic"
      },
      {
        "toCreate": "mermaid",
        "with": "fish"
      },
      {
        "toCreate": "swimming_pool",
        "with": "house"
      }
    ],
    "name": "Swimmer"
  },
  "swimming_pool": {
    "combine": [
      {
        "toCreate": "aquarium",
        "with": "small"
      },
      {
        "toCreate": "swimmer",
        "with": "human"
      }
    ],
    "name": "Swimming Pool"
  },
  "sword": {
    "combine": [
      {
        "toCreate": "bandage",
        "with": "fabric"
      },
      {
        "toCreate": "bayonet",
        "with": "gun"
      },
      {
        "toCreate": "blood",
        "with": "warrior"
      },
      {
        "toCreate": "coconut_milk",
        "with": "coconut"
      },
      {
        "toCreate": "excalibur",
        "with": "stone"
      },
      {
        "toCreate": "excalibur",
        "with": "legend"
      },
      {
        "toCreate": "excalibur",
        "with": "story"
      },
      {
        "toCreate": "excalibur",
        "with": "monarch"
      },
      {
        "toCreate": "excalibur",
        "with": "lake"
      },
      {
        "toCreate": "excalibur",
        "with": "fairy_tale"
      },
      {
        "toCreate": "katana",
        "with": "shuriken"
      },
      {
        "toCreate": "katana",
        "with": "ninja"
      },
      {
        "toCreate": "knife",
        "with": "cook"
      },
      {
        "toCreate": "leather",
        "with": "cow"
      },
      {
        "toCreate": "leather",
        "with": "pig"
      },
      {
        "toCreate": "leather",
        "with": "sheep"
      },
      {
        "toCreate": "light_sword",
        "with": "light"
      },
      {
        "toCreate": "light_sword",
        "with": "energy"
      },
      {
        "toCreate": "light_sword",
        "with": "electricity"
      },
      {
        "toCreate": "light_sword",
        "with": "space"
      },
      {
        "toCreate": "light_sword",
        "with": "force_knight"
      },
      {
        "toCreate": "meat",
        "with": "livestock"
      },
      {
        "toCreate": "meat",
        "with": "chicken"
      },
      {
        "toCreate": "meat",
        "with": "pig"
      },
      {
        "toCreate": "meat",
        "with": "cow"
      },
      {
        "toCreate": "meat",
        "with": "animal"
      },
      {
        "toCreate": "meat",
        "with": "shark"
      },
      {
        "toCreate": "pencil_sharpener",
        "with": "pencil"
      },
      {
        "toCreate": "pirate",
        "with": "sailor"
      },
      {
        "toCreate": "scalpel",
        "with": "doctor"
      },
      {
        "toCreate": "scalpel",
        "with": "hospital"
      },
      {
        "toCreate": "scalpel",
        "with": "ambulance"
      },
      {
        "toCreate": "scissors",
        "with": "sword"
      },
      {
        "toCreate": "scissors",
        "with": "blade"
      },
      {
        "toCreate": "scissors",
        "with": "paper"
      },
      {
        "toCreate": "scythe",
        "with": "grass"
      },
      {
        "toCreate": "scythe",
        "with": "wheat"
      },
      {
        "toCreate": "shuriken",
        "with": "star"
      },
      {
        "toCreate": "swordfish",
        "with": "fish"
      },
      {
        "toCreate": "swordfish",
        "with": "shark"
      },
      {
        "toCreate": "wand",
        "with": "wizard"
      },
      {
        "toCreate": "warrior",
        "with": "human"
      },
      {
        "toCreate": "wax",
        "with": "beehive"
      },
      {
        "toCreate": "wood",
        "with": "tree"
      }
    ],
    "name": "Sword"
  },
  "swordfish": {
    "combine": [
      {
        "toCreate": "fishing_rod",
        "with": "tool"
      },
      {
        "toCreate": "fishing_rod",
        "with": "wood"
      },
      {
        "toCreate": "fishing_rod",
        "with": "wire"
      },
      {
        "toCreate": "fishing_rod",
        "with": "thread"
      },
      {
        "toCreate": "meat",
        "with": "tool"
      },
      {
        "toCreate": "meat",
        "with": "axe"
      },
      {
        "toCreate": "meat",
        "with": "butcher"
      },
      {
        "toCreate": "meat",
        "with": "net"
      },
      {
        "toCreate": "narwhal",
        "with": "unicorn"
      },
      {
        "toCreate": "net",
        "with": "rope"
      }
    ],
    "name": "Swordfish"
  },
  "syringe": {
    "combine": [],
    "name": "Syringe"
  },
  "tank": {
    "combine": [],
    "name": "Tank"
  },
  "tea": {
    "combine": [
      {
        "toCreate": "cup",
        "with": "container"
      },
      {
        "toCreate": "cup",
        "with": "bottle"
      },
      {
        "toCreate": "iced_tea",
        "with": "ice"
      },
      {
        "toCreate": "iced_tea",
        "with": "cold"
      },
      {
        "toCreate": "iced_tea",
        "with": "snow"
      },
      {
        "toCreate": "soda",
        "with": "carbon_dioxide"
      }
    ],
    "name": "Tea"
  },
  "telescope": {
    "combine": [
      {
        "toCreate": "science",
        "with": "human"
      },
      {
        "toCreate": "star",
        "with": "night"
      },
      {
        "toCreate": "star",
        "with": "space"
      }
    ],
    "name": "Telescope"
  },
  "tent": {
    "combine": [],
    "name": "Tent"
  },
  "the_one_ring": {
    "combine": [],
    "name": "The One Ring"
  },
  "thermometer": {
    "combine": [
      {
        "toCreate": "cold",
        "with": "space"
      }
    ],
    "name": "Thermometer"
  },
  "thread": {
    "combine": [
      {
        "toCreate": "candle",
        "with": "wax"
      },
      {
        "toCreate": "cashmere",
        "with": "mountain_goat"
      },
      {
        "toCreate": "cotton",
        "with": "plant"
      },
      {
        "toCreate": "cotton_candy",
        "with": "sugar"
      },
      {
        "toCreate": "fabric",
        "with": "machine"
      },
      {
        "toCreate": "fabric",
        "with": "tool"
      },
      {
        "toCreate": "fabric",
        "with": "wheel"
      },
      {
        "toCreate": "fishing_rod",
        "with": "fish"
      },
      {
        "toCreate": "fishing_rod",
        "with": "swordfish"
      },
      {
        "toCreate": "fishing_rod",
        "with": "piranha"
      },
      {
        "toCreate": "needle",
        "with": "metal"
      },
      {
        "toCreate": "needle",
        "with": "steel"
      },
      {
        "toCreate": "needle",
        "with": "tool"
      },
      {
        "toCreate": "rope",
        "with": "thread"
      },
      {
        "toCreate": "rope",
        "with": "wire"
      },
      {
        "toCreate": "sewing_machine",
        "with": "machine"
      },
      {
        "toCreate": "sewing_machine",
        "with": "electricity"
      },
      {
        "toCreate": "sewing_machine",
        "with": "robot"
      },
      {
        "toCreate": "spaghetti",
        "with": "pasta"
      },
      {
        "toCreate": "spider",
        "with": "animal"
      },
      {
        "toCreate": "string_phone",
        "with": "paper_cup"
      },
      {
        "toCreate": "string_phone",
        "with": "cup"
      },
      {
        "toCreate": "vine",
        "with": "rainforest"
      },
      {
        "toCreate": "web",
        "with": "spider"
      }
    ],
    "name": "Thread"
  },
  "tide": {
    "combine": [
      {
        "toCreate": "ocean",
        "with": "container"
      }
    ],
    "name": "Tide"
  },
  "time": {
    "combine": [
      {
        "toCreate": "alcohol",
        "with": "juice"
      },
      {
        "toCreate": "ash",
        "with": "campfire"
      },
      {
        "toCreate": "bird",
        "with": "dinosaur"
      },
      {
        "toCreate": "bird",
        "with": "pterodactyl"
      },
      {
        "toCreate": "bone",
        "with": "corpse"
      },
      {
        "toCreate": "cheese",
        "with": "milk"
      },
      {
        "toCreate": "clock",
        "with": "electricity"
      },
      {
        "toCreate": "clock",
        "with": "wheel"
      },
      {
        "toCreate": "clock",
        "with": "tool"
      },
      {
        "toCreate": "clock",
        "with": "machine"
      },
      {
        "toCreate": "coal",
        "with": "peat"
      },
      {
        "toCreate": "corpse",
        "with": "human"
      },
      {
        "toCreate": "dawn",
        "with": "night"
      },
      {
        "toCreate": "day",
        "with": "sun"
      },
      {
        "toCreate": "day",
        "with": "dawn"
      },
      {
        "toCreate": "death",
        "with": "life"
      },
      {
        "toCreate": "dinosaur",
        "with": "lizard"
      },
      {
        "toCreate": "duck",
        "with": "duckling"
      },
      {
        "toCreate": "eruption",
        "with": "volcano"
      },
      {
        "toCreate": "family_tree",
        "with": "family"
      },
      {
        "toCreate": "flood",
        "with": "rain"
      },
      {
        "toCreate": "fossil",
        "with": "dinosaur"
      },
      {
        "toCreate": "fossil",
        "with": "bone"
      },
      {
        "toCreate": "fossil",
        "with": "grave"
      },
      {
        "toCreate": "fossil",
        "with": "skeleton"
      },
      {
        "toCreate": "fruit",
        "with": "flower"
      },
      {
        "toCreate": "glacier",
        "with": "ice"
      },
      {
        "toCreate": "honey",
        "with": "bee"
      },
      {
        "toCreate": "hourglass",
        "with": "sand"
      },
      {
        "toCreate": "hourglass",
        "with": "glass"
      },
      {
        "toCreate": "hourglass",
        "with": "container"
      },
      {
        "toCreate": "human",
        "with": "animal"
      },
      {
        "toCreate": "human",
        "with": "monkey"
      },
      {
        "toCreate": "legend",
        "with": "story"
      },
      {
        "toCreate": "legend",
        "with": "fairy_tale"
      },
      {
        "toCreate": "life",
        "with": "primordial_soup"
      },
      {
        "toCreate": "mold",
        "with": "bread"
      },
      {
        "toCreate": "mold",
        "with": "vegetable"
      },
      {
        "toCreate": "mold",
        "with": "fruit"
      },
      {
        "toCreate": "moon",
        "with": "sky"
      },
      {
        "toCreate": "night",
        "with": "day"
      },
      {
        "toCreate": "night",
        "with": "moon"
      },
      {
        "toCreate": "night",
        "with": "twilight"
      },
      {
        "toCreate": "peat",
        "with": "swamp"
      },
      {
        "toCreate": "peat",
        "with": "grass"
      },
      {
        "toCreate": "petroleum",
        "with": "fossil"
      },
      {
        "toCreate": "ruins",
        "with": "castle"
      },
      {
        "toCreate": "ruins",
        "with": "house"
      },
      {
        "toCreate": "ruins",
        "with": "village"
      },
      {
        "toCreate": "ruins",
        "with": "city"
      },
      {
        "toCreate": "ruins",
        "with": "skyscraper"
      },
      {
        "toCreate": "ruins",
        "with": "farm"
      },
      {
        "toCreate": "ruins",
        "with": "hospital"
      },
      {
        "toCreate": "seed",
        "with": "flower"
      },
      {
        "toCreate": "skeleton",
        "with": "corpse"
      },
      {
        "toCreate": "sloth",
        "with": "animal"
      },
      {
        "toCreate": "smoke",
        "with": "campfire"
      },
      {
        "toCreate": "tide",
        "with": "ocean"
      },
      {
        "toCreate": "tide",
        "with": "sea"
      },
      {
        "toCreate": "twilight",
        "with": "day"
      },
      {
        "toCreate": "vinegar",
        "with": "wine"
      }
    ],
    "name": "Time"
  },
  "titanic": {
    "combine": [],
    "name": "Titanic"
  },
  "toast": {
    "combine": [
      {
        "toCreate": "baker",
        "with": "human"
      },
      {
        "toCreate": "grilled_cheese",
        "with": "cheese"
      }
    ],
    "name": "Toast"
  },
  "tobacco": {
    "combine": [
      {
        "toCreate": "cigarette",
        "with": "paper"
      },
      {
        "toCreate": "pipe",
        "with": "wood"
      },
      {
        "toCreate": "pipe",
        "with": "tool"
      },
      {
        "toCreate": "pipe",
        "with": "container"
      }
    ],
    "name": "Tobacco"
  },
  "tool": {
    "combine": [
      {
        "toCreate": "axe",
        "with": "wood"
      },
      {
        "toCreate": "axe",
        "with": "lumberjack"
      },
      {
        "toCreate": "boiler",
        "with": "pressure"
      },
      {
        "toCreate": "boiler",
        "with": "steam"
      },
      {
        "toCreate": "bullet",
        "with": "gunpowder"
      },
      {
        "toCreate": "butter",
        "with": "milk"
      },
      {
        "toCreate": "cashmere",
        "with": "mountain_goat"
      },
      {
        "toCreate": "cheese",
        "with": "milk"
      },
      {
        "toCreate": "clock",
        "with": "time"
      },
      {
        "toCreate": "clock",
        "with": "sundial"
      },
      {
        "toCreate": "coconut_milk",
        "with": "coconut"
      },
      {
        "toCreate": "computer",
        "with": "hacker"
      },
      {
        "toCreate": "fabric",
        "with": "thread"
      },
      {
        "toCreate": "field",
        "with": "earth"
      },
      {
        "toCreate": "field",
        "with": "land"
      },
      {
        "toCreate": "field",
        "with": "soil"
      },
      {
        "toCreate": "fishing_rod",
        "with": "fish"
      },
      {
        "toCreate": "fishing_rod",
        "with": "swordfish"
      },
      {
        "toCreate": "fishing_rod",
        "with": "piranha"
      },
      {
        "toCreate": "flashlight",
        "with": "light"
      },
      {
        "toCreate": "flashlight",
        "with": "light_bulb"
      },
      {
        "toCreate": "flashlight",
        "with": "lamp"
      },
      {
        "toCreate": "hammer",
        "with": "metal"
      },
      {
        "toCreate": "hammer",
        "with": "steel"
      },
      {
        "toCreate": "hammer",
        "with": "stone"
      },
      {
        "toCreate": "hammer",
        "with": "rock"
      },
      {
        "toCreate": "hammer",
        "with": "woodpecker"
      },
      {
        "toCreate": "house",
        "with": "wall"
      },
      {
        "toCreate": "human",
        "with": "animal"
      },
      {
        "toCreate": "human",
        "with": "monkey"
      },
      {
        "toCreate": "lawn_mower",
        "with": "grass"
      },
      {
        "toCreate": "lawn_mower",
        "with": "scythe"
      },
      {
        "toCreate": "leather",
        "with": "cow"
      },
      {
        "toCreate": "leather",
        "with": "sheep"
      },
      {
        "toCreate": "leather",
        "with": "pig"
      },
      {
        "toCreate": "lens",
        "with": "glass"
      },
      {
        "toCreate": "machine",
        "with": "wheel"
      },
      {
        "toCreate": "machine",
        "with": "tool"
      },
      {
        "toCreate": "machine",
        "with": "boiler"
      },
      {
        "toCreate": "machine",
        "with": "chain"
      },
      {
        "toCreate": "machine",
        "with": "engineer"
      },
      {
        "toCreate": "meat",
        "with": "livestock"
      },
      {
        "toCreate": "meat",
        "with": "chicken"
      },
      {
        "toCreate": "meat",
        "with": "pig"
      },
      {
        "toCreate": "meat",
        "with": "cow"
      },
      {
        "toCreate": "meat",
        "with": "animal"
      },
      {
        "toCreate": "meat",
        "with": "fish"
      },
      {
        "toCreate": "meat",
        "with": "swordfish"
      },
      {
        "toCreate": "meat",
        "with": "flying_fish"
      },
      {
        "toCreate": "meat",
        "with": "shark"
      },
      {
        "toCreate": "meat",
        "with": "frog"
      },
      {
        "toCreate": "metal",
        "with": "ore"
      },
      {
        "toCreate": "microscope",
        "with": "lens"
      },
      {
        "toCreate": "milk",
        "with": "cow"
      },
      {
        "toCreate": "milk",
        "with": "goat"
      },
      {
        "toCreate": "needle",
        "with": "thread"
      },
      {
        "toCreate": "omelette",
        "with": "egg"
      },
      {
        "toCreate": "paint",
        "with": "rainbow"
      },
      {
        "toCreate": "paint",
        "with": "double_rainbow!"
      },
      {
        "toCreate": "pipe",
        "with": "tobacco"
      },
      {
        "toCreate": "pitchfork",
        "with": "hay"
      },
      {
        "toCreate": "plow",
        "with": "field"
      },
      {
        "toCreate": "pottery",
        "with": "clay"
      },
      {
        "toCreate": "rope",
        "with": "wire"
      },
      {
        "toCreate": "rope",
        "with": "sailboat"
      },
      {
        "toCreate": "rope",
        "with": "pirate_ship"
      },
      {
        "toCreate": "rope",
        "with": "boat"
      },
      {
        "toCreate": "saddle",
        "with": "horse"
      },
      {
        "toCreate": "safety_glasses",
        "with": "glasses"
      },
      {
        "toCreate": "shovel",
        "with": "gardner"
      },
      {
        "toCreate": "solar_cell",
        "with": "sun"
      },
      {
        "toCreate": "solar_cell",
        "with": "light"
      },
      {
        "toCreate": "stethoscope",
        "with": "doctor"
      },
      {
        "toCreate": "stethoscope",
        "with": "sound"
      },
      {
        "toCreate": "stethoscope",
        "with": "hospital"
      },
      {
        "toCreate": "sundial",
        "with": "sun"
      },
      {
        "toCreate": "sundial",
        "with": "day"
      },
      {
        "toCreate": "sundial",
        "with": "light"
      },
      {
        "toCreate": "sweater",
        "with": "wool"
      },
      {
        "toCreate": "syringe",
        "with": "needle"
      },
      {
        "toCreate": "thermometer",
        "with": "quicksilver"
      },
      {
        "toCreate": "thread",
        "with": "cotton"
      },
      {
        "toCreate": "toolbox",
        "with": "container"
      },
      {
        "toCreate": "toolbox",
        "with": "safe"
      },
      {
        "toCreate": "toolbox",
        "with": "box"
      },
      {
        "toCreate": "umbrella",
        "with": "rain"
      },
      {
        "toCreate": "umbrella",
        "with": "storm"
      },
      {
        "toCreate": "wand",
        "with": "wizard"
      },
      {
        "toCreate": "wax",
        "with": "beehive"
      },
      {
        "toCreate": "wheel",
        "with": "motion"
      },
      {
        "toCreate": "wheel",
        "with": "water"
      },
      {
        "toCreate": "wheel",
        "with": "stream"
      },
      {
        "toCreate": "wheel",
        "with": "river"
      },
      {
        "toCreate": "wood",
        "with": "tree"
      },
      {
        "toCreate": "wood",
        "with": "forest"
      },
      {
        "toCreate": "wool",
        "with": "sheep"
      }
    ],
    "name": "Tool"
  },
  "toolbox": {
    "combine": [
      {
        "toCreate": "closet",
        "with": "container"
      }
    ],
    "name": "Toolbox"
  },
  "tornado": {
    "combine": [
      {
        "toCreate": "blizzard",
        "with": "snow"
      },
      {
        "toCreate": "hurricane",
        "with": "ocean"
      },
      {
        "toCreate": "hurricane",
        "with": "sea"
      },
      {
        "toCreate": "motion",
        "with": "science"
      },
      {
        "toCreate": "motion",
        "with": "philosophy"
      },
      {
        "toCreate": "sandstorm",
        "with": "sand"
      },
      {
        "toCreate": "sandstorm",
        "with": "desert"
      }
    ],
    "name": "Tornado"
  },
  "toucan": {
    "combine": [],
    "name": "Toucan"
  },
  "tractor": {
    "combine": [
      {
        "toCreate": "farm",
        "with": "house"
      },
      {
        "toCreate": "garage",
        "with": "house"
      },
      {
        "toCreate": "garage",
        "with": "container"
      },
      {
        "toCreate": "garage",
        "with": "wall"
      },
      {
        "toCreate": "garage",
        "with": "barn"
      },
      {
        "toCreate": "hay_bale",
        "with": "hay"
      }
    ],
    "name": "Tractor"
  },
  "train": {
    "combine": [
      {
        "toCreate": "airplane",
        "with": "bird"
      },
      {
        "toCreate": "bus",
        "with": "car"
      },
      {
        "toCreate": "rocket",
        "with": "atmosphere"
      },
      {
        "toCreate": "roller_coaster",
        "with": "park"
      },
      {
        "toCreate": "trainyard",
        "with": "container"
      },
      {
        "toCreate": "trainyard",
        "with": "house"
      },
      {
        "toCreate": "trainyard",
        "with": "wall"
      },
      {
        "toCreate": "trainyard",
        "with": "garage"
      },
      {
        "toCreate": "tunnel",
        "with": "mountain"
      },
      {
        "toCreate": "tunnel",
        "with": "mountain_range"
      },
      {
        "toCreate": "tunnel",
        "with": "hill"
      }
    ],
    "name": "Train"
  },
  "trainyard": {
    "combine": [],
    "name": "Trainyard"
  },
  "treasure": {
    "combine": [
      {
        "toCreate": "treasure_map",
        "with": "map"
      }
    ],
    "name": "Treasure"
  },
  "treasure_map": {
    "combine": [
      {
        "toCreate": "treasure",
        "with": "pirate"
      },
      {
        "toCreate": "treasure",
        "with": "pirate_ship"
      },
      {
        "toCreate": "treasure",
        "with": "island"
      },
      {
        "toCreate": "treasure",
        "with": "sailor"
      }
    ],
    "name": "Treasure Map"
  },
  "tree": {
    "combine": [
      {
        "toCreate": "ash",
        "with": "fire"
      },
      {
        "toCreate": "beehive",
        "with": "bee"
      },
      {
        "toCreate": "bonsai_tree",
        "with": "scissors"
      },
      {
        "toCreate": "bonsai_tree",
        "with": "pottery"
      },
      {
        "toCreate": "bonsai_tree",
        "with": "small"
      },
      {
        "toCreate": "bonsai_tree",
        "with": "wire"
      },
      {
        "toCreate": "cactus",
        "with": "sand"
      },
      {
        "toCreate": "cactus",
        "with": "desert"
      },
      {
        "toCreate": "carbon_dioxide",
        "with": "night"
      },
      {
        "toCreate": "charcoal",
        "with": "fire"
      },
      {
        "toCreate": "christmas_tree",
        "with": "light_bulb"
      },
      {
        "toCreate": "christmas_tree",
        "with": "star"
      },
      {
        "toCreate": "christmas_tree",
        "with": "candle"
      },
      {
        "toCreate": "christmas_tree",
        "with": "light"
      },
      {
        "toCreate": "christmas_tree",
        "with": "gift"
      },
      {
        "toCreate": "coral",
        "with": "ocean"
      },
      {
        "toCreate": "coral",
        "with": "sea"
      },
      {
        "toCreate": "dam",
        "with": "beaver"
      },
      {
        "toCreate": "dew",
        "with": "fog"
      },
      {
        "toCreate": "dew",
        "with": "dawn"
      },
      {
        "toCreate": "family_tree",
        "with": "family"
      },
      {
        "toCreate": "family_tree",
        "with": "village"
      },
      {
        "toCreate": "forest",
        "with": "tree"
      },
      {
        "toCreate": "forest",
        "with": "plant"
      },
      {
        "toCreate": "forest",
        "with": "land"
      },
      {
        "toCreate": "forest",
        "with": "earth"
      },
      {
        "toCreate": "forest",
        "with": "container"
      },
      {
        "toCreate": "fruit",
        "with": "flower"
      },
      {
        "toCreate": "fruit",
        "with": "farmer"
      },
      {
        "toCreate": "fruit_tree",
        "with": "fruit"
      },
      {
        "toCreate": "greenhouse",
        "with": "glass"
      },
      {
        "toCreate": "greenhouse",
        "with": "aquarium"
      },
      {
        "toCreate": "leaf",
        "with": "wind"
      },
      {
        "toCreate": "lumberjack",
        "with": "human"
      },
      {
        "toCreate": "monkey",
        "with": "animal"
      },
      {
        "toCreate": "nest",
        "with": "bird"
      },
      {
        "toCreate": "nest",
        "with": "egg"
      },
      {
        "toCreate": "nuts",
        "with": "farmer"
      },
      {
        "toCreate": "nuts",
        "with": "domestication"
      },
      {
        "toCreate": "nuts",
        "with": "field"
      },
      {
        "toCreate": "oasis",
        "with": "desert"
      },
      {
        "toCreate": "orchard",
        "with": "fruit_tree"
      },
      {
        "toCreate": "oxygen",
        "with": "sun"
      },
      {
        "toCreate": "oxygen",
        "with": "carbon_dioxide"
      },
      {
        "toCreate": "palm",
        "with": "beach"
      },
      {
        "toCreate": "palm",
        "with": "island"
      },
      {
        "toCreate": "palm",
        "with": "sand"
      },
      {
        "toCreate": "plant",
        "with": "small"
      },
      {
        "toCreate": "sap",
        "with": "blade"
      },
      {
        "toCreate": "sloth",
        "with": "manatee"
      },
      {
        "toCreate": "smoke",
        "with": "fire"
      },
      {
        "toCreate": "squirrel",
        "with": "mouse"
      },
      {
        "toCreate": "swamp",
        "with": "mud"
      },
      {
        "toCreate": "swamp",
        "with": "lake"
      },
      {
        "toCreate": "treehouse",
        "with": "house"
      },
      {
        "toCreate": "treehouse",
        "with": "wood"
      },
      {
        "toCreate": "wood",
        "with": "tool"
      },
      {
        "toCreate": "wood",
        "with": "axe"
      },
      {
        "toCreate": "wood",
        "with": "chainsaw"
      },
      {
        "toCreate": "wood",
        "with": "sword"
      },
      {
        "toCreate": "wood",
        "with": "lumberjack"
      },
      {
        "toCreate": "woodpecker",
        "with": "bird"
      }
    ],
    "name": "Tree"
  },
  "treehouse": {
    "combine": [],
    "name": "Treehouse"
  },
  "trojan_horse": {
    "combine": [],
    "name": "Trojan Horse"
  },
  "tsunami": {
    "combine": [
      {
        "toCreate": "flood",
        "with": "city"
      },
      {
        "toCreate": "flood",
        "with": "house"
      }
    ],
    "name": "Tsunami"
  },
  "tunnel": {
    "combine": [],
    "name": "Tunnel"
  },
  "turtle": {
    "combine": [
      {
        "toCreate": "chameleon",
        "with": "rainbow"
      },
      {
        "toCreate": "chameleon",
        "with": "double_rainbow!"
      },
      {
        "toCreate": "egg",
        "with": "turtle"
      },
      {
        "toCreate": "ninja_turtle",
        "with": "ninja"
      },
      {
        "toCreate": "ninja_turtle",
        "with": "shuriken"
      },
      {
        "toCreate": "ninja_turtle",
        "with": "katana"
      }
    ],
    "name": "Turtle"
  },
  "twilight": {
    "combine": [
      {
        "toCreate": "darkness",
        "with": "sky"
      },
      {
        "toCreate": "night",
        "with": "time"
      },
      {
        "toCreate": "owl",
        "with": "bird"
      }
    ],
    "name": "Twilight"
  },
  "tyrannosaurus_rex": {
    "combine": [
      {
        "toCreate": "egg",
        "with": "tyrannosaurus_rex"
      }
    ],
    "name": "Tyrannosaurus Rex"
  },
  "ufo": {
    "combine": [],
    "name": "UFO"
  },
  "umbrella": {
    "combine": [
      {
        "toCreate": "closet",
        "with": "container"
      },
      {
        "toCreate": "parachute",
        "with": "pilot"
      },
      {
        "toCreate": "parachute",
        "with": "airplane"
      },
      {
        "toCreate": "parachute",
        "with": "sky"
      },
      {
        "toCreate": "parachute",
        "with": "atmosphere"
      }
    ],
    "name": "Umbrella"
  },
  "unicorn": {
    "combine": [
      {
        "toCreate": "fairy_tale",
        "with": "story"
      },
      {
        "toCreate": "narwhal",
        "with": "fish"
      },
      {
        "toCreate": "narwhal",
        "with": "ocean"
      },
      {
        "toCreate": "narwhal",
        "with": "sea"
      },
      {
        "toCreate": "narwhal",
        "with": "water"
      },
      {
        "toCreate": "narwhal",
        "with": "shark"
      },
      {
        "toCreate": "narwhal",
        "with": "swordfish"
      },
      {
        "toCreate": "narwhal",
        "with": "flying_fish"
      },
      {
        "toCreate": "pegasus",
        "with": "bird"
      },
      {
        "toCreate": "pegasus",
        "with": "sky"
      },
      {
        "toCreate": "wizard",
        "with": "human"
      }
    ],
    "name": "Unicorn"
  },
  "universe": {
    "combine": [
      {
        "toCreate": "big",
        "with": "philosophy"
      },
      {
        "toCreate": "science",
        "with": "human"
      },
      {
        "toCreate": "telescope",
        "with": "glass"
      }
    ],
    "name": "Universe"
  },
  "vacuum_cleaner": {
    "combine": [
      {
        "toCreate": "closet",
        "with": "container"
      },
      {
        "toCreate": "robot_vacuum",
        "with": "robot"
      },
      {
        "toCreate": "robot_vacuum",
        "with": "computer"
      }
    ],
    "name": "Vacuum Cleaner"
  },
  "vampire": {
    "combine": [
      {
        "toCreate": "ash",
        "with": "sun"
      },
      {
        "toCreate": "ash",
        "with": "dawn"
      },
      {
        "toCreate": "castle",
        "with": "house"
      },
      {
        "toCreate": "castle",
        "with": "wall"
      },
      {
        "toCreate": "coffin",
        "with": "container"
      },
      {
        "toCreate": "vampire",
        "with": "human"
      }
    ],
    "name": "Vampire"
  },
  "vase": {
    "combine": [],
    "name": "Vase"
  },
  "vault": {
    "combine": [
      {
        "toCreate": "bank",
        "with": "house"
      }
    ],
    "name": "Vault"
  },
  "vegetable": {
    "combine": [
      {
        "toCreate": "carrot",
        "with": "snowman"
      },
      {
        "toCreate": "carrot",
        "with": "grass"
      },
      {
        "toCreate": "carrot",
        "with": "garden"
      },
      {
        "toCreate": "coconut",
        "with": "palm"
      },
      {
        "toCreate": "coconut",
        "with": "beach"
      },
      {
        "toCreate": "cook",
        "with": "human"
      },
      {
        "toCreate": "french_fries",
        "with": "oil"
      },
      {
        "toCreate": "jack-o-lantern",
        "with": "skeleton"
      },
      {
        "toCreate": "jack-o-lantern",
        "with": "ghost"
      },
      {
        "toCreate": "jack-o-lantern",
        "with": "night"
      },
      {
        "toCreate": "juice",
        "with": "pressure"
      },
      {
        "toCreate": "juice",
        "with": "stone"
      },
      {
        "toCreate": "juice",
        "with": "rock"
      },
      {
        "toCreate": "juice",
        "with": "explosion"
      },
      {
        "toCreate": "juice",
        "with": "water"
      },
      {
        "toCreate": "mold",
        "with": "bacteria"
      },
      {
        "toCreate": "mold",
        "with": "time"
      },
      {
        "toCreate": "potato",
        "with": "earth"
      },
      {
        "toCreate": "pumpkin",
        "with": "field"
      },
      {
        "toCreate": "pumpkin",
        "with": "farmer"
      },
      {
        "toCreate": "pumpkin",
        "with": "jack-o-lantern"
      },
      {
        "toCreate": "recipe",
        "with": "paper"
      },
      {
        "toCreate": "recipe",
        "with": "newspaper"
      },
      {
        "toCreate": "recipe",
        "with": "newspaper"
      },
      {
        "toCreate": "sandwich",
        "with": "bread"
      }
    ],
    "name": "Vegetable"
  },
  "venus": {
    "combine": [
      {
        "toCreate": "alien",
        "with": "life"
      },
      {
        "toCreate": "astronaut",
        "with": "human"
      },
      {
        "toCreate": "solar_system",
        "with": "container"
      }
    ],
    "name": "Venus"
  },
  "village": {
    "combine": [
      {
        "toCreate": "bakery",
        "with": "bread"
      },
      {
        "toCreate": "bakery",
        "with": "baker"
      },
      {
        "toCreate": "bakery",
        "with": "donut"
      },
      {
        "toCreate": "bakery",
        "with": "cake"
      },
      {
        "toCreate": "city",
        "with": "village"
      },
      {
        "toCreate": "city",
        "with": "skyscraper"
      },
      {
        "toCreate": "city",
        "with": "big"
      },
      {
        "toCreate": "family_tree",
        "with": "tree"
      },
      {
        "toCreate": "firestation",
        "with": "firefighter"
      },
      {
        "toCreate": "map",
        "with": "paper"
      },
      {
        "toCreate": "park",
        "with": "forest"
      },
      {
        "toCreate": "park",
        "with": "garden"
      },
      {
        "toCreate": "park",
        "with": "grass"
      },
      {
        "toCreate": "park",
        "with": "field"
      },
      {
        "toCreate": "pigeon",
        "with": "bird"
      },
      {
        "toCreate": "rat",
        "with": "mouse"
      },
      {
        "toCreate": "ruins",
        "with": "time"
      },
      {
        "toCreate": "skyscraper",
        "with": "sky"
      },
      {
        "toCreate": "space_station",
        "with": "space"
      },
      {
        "toCreate": "space_station",
        "with": "atmosphere"
      },
      {
        "toCreate": "tent",
        "with": "fabric"
      }
    ],
    "name": "Village"
  },
  "vine": {
    "combine": [],
    "name": "Vine"
  },
  "vinegar": {
    "combine": [],
    "name": "Vinegar"
  },
  "volcano": {
    "combine": [
      {
        "toCreate": "eruption",
        "with": "pressure"
      },
      {
        "toCreate": "eruption",
        "with": "time"
      },
      {
        "toCreate": "explosion",
        "with": "pressure"
      },
      {
        "toCreate": "explosion",
        "with": "petroleum"
      },
      {
        "toCreate": "flamethrower",
        "with": "gun"
      },
      {
        "toCreate": "island",
        "with": "ocean"
      },
      {
        "toCreate": "island",
        "with": "sea"
      },
      {
        "toCreate": "lava_lamp",
        "with": "lamp"
      },
      {
        "toCreate": "life",
        "with": "primordial_soup"
      },
      {
        "toCreate": "magma",
        "with": "science"
      },
      {
        "toCreate": "the_one_ring",
        "with": "ring"
      },
      {
        "toCreate": "venus",
        "with": "planet"
      }
    ],
    "name": "Volcano"
  },
  "vulture": {
    "combine": [
      {
        "toCreate": "airplane",
        "with": "metal"
      },
      {
        "toCreate": "airplane",
        "with": "steel"
      },
      {
        "toCreate": "airplane",
        "with": "machine"
      },
      {
        "toCreate": "bone",
        "with": "corpse"
      },
      {
        "toCreate": "bone",
        "with": "meat"
      },
      {
        "toCreate": "desert",
        "with": "sand"
      },
      {
        "toCreate": "egg",
        "with": "vulture"
      },
      {
        "toCreate": "origami",
        "with": "paper"
      },
      {
        "toCreate": "pterodactyl",
        "with": "dinosaur"
      }
    ],
    "name": "Vulture"
  },
  "wagon": {
    "combine": [
      {
        "toCreate": "car",
        "with": "combustion_engine"
      },
      {
        "toCreate": "electric_car",
        "with": "electricity"
      },
      {
        "toCreate": "electric_car",
        "with": "wind_turbine"
      },
      {
        "toCreate": "electric_car",
        "with": "solar_cell"
      },
      {
        "toCreate": "firetruck",
        "with": "firefighter"
      },
      {
        "toCreate": "firetruck",
        "with": "fire"
      },
      {
        "toCreate": "firetruck",
        "with": "fire_extinguisher"
      },
      {
        "toCreate": "ice_cream_truck",
        "with": "ice_cream"
      },
      {
        "toCreate": "roller_coaster",
        "with": "park"
      },
      {
        "toCreate": "sleigh",
        "with": "snow"
      },
      {
        "toCreate": "sleigh",
        "with": "ice"
      },
      {
        "toCreate": "sleigh",
        "with": "arctic"
      },
      {
        "toCreate": "sleigh",
        "with": "antarctica"
      },
      {
        "toCreate": "tractor",
        "with": "farmer"
      },
      {
        "toCreate": "tractor",
        "with": "field"
      },
      {
        "toCreate": "tractor",
        "with": "cow"
      },
      {
        "toCreate": "train",
        "with": "steam_engine"
      }
    ],
    "name": "Wagon"
  },
  "wall": {
    "combine": [
      {
        "toCreate": "beehive",
        "with": "bee"
      },
      {
        "toCreate": "birdcage",
        "with": "bird"
      },
      {
        "toCreate": "birdhouse",
        "with": "bird"
      },
      {
        "toCreate": "cage",
        "with": "wolf"
      },
      {
        "toCreate": "cage",
        "with": "fox"
      },
      {
        "toCreate": "cage",
        "with": "lion"
      },
      {
        "toCreate": "cage",
        "with": "hamster"
      },
      {
        "toCreate": "castle",
        "with": "knight"
      },
      {
        "toCreate": "castle",
        "with": "warrior"
      },
      {
        "toCreate": "castle",
        "with": "vampire"
      },
      {
        "toCreate": "castle",
        "with": "monarch"
      },
      {
        "toCreate": "chicken_coop",
        "with": "chicken"
      },
      {
        "toCreate": "dam",
        "with": "river"
      },
      {
        "toCreate": "dam",
        "with": "stream"
      },
      {
        "toCreate": "doghouse",
        "with": "dog"
      },
      {
        "toCreate": "doghouse",
        "with": "husky"
      },
      {
        "toCreate": "fence",
        "with": "wood"
      },
      {
        "toCreate": "fence",
        "with": "field"
      },
      {
        "toCreate": "fence",
        "with": "grass"
      },
      {
        "toCreate": "fence",
        "with": "garden"
      },
      {
        "toCreate": "fireplace",
        "with": "campfire"
      },
      {
        "toCreate": "garage",
        "with": "car"
      },
      {
        "toCreate": "garage",
        "with": "ambulance"
      },
      {
        "toCreate": "garage",
        "with": "motorcycle"
      },
      {
        "toCreate": "garage",
        "with": "bus"
      },
      {
        "toCreate": "garage",
        "with": "sleigh"
      },
      {
        "toCreate": "garage",
        "with": "electric_car"
      },
      {
        "toCreate": "garage",
        "with": "snowmobile"
      },
      {
        "toCreate": "garage",
        "with": "tractor"
      },
      {
        "toCreate": "garage",
        "with": "rv"
      },
      {
        "toCreate": "garage",
        "with": "ice_cream_truck"
      },
      {
        "toCreate": "hangar",
        "with": "airplane"
      },
      {
        "toCreate": "hangar",
        "with": "seaplane"
      },
      {
        "toCreate": "hangar",
        "with": "helicopter"
      },
      {
        "toCreate": "hangar",
        "with": "rocket"
      },
      {
        "toCreate": "hangar",
        "with": "spaceship"
      },
      {
        "toCreate": "hedge",
        "with": "plant"
      },
      {
        "toCreate": "hedge",
        "with": "leaf"
      },
      {
        "toCreate": "hedge",
        "with": "garden"
      },
      {
        "toCreate": "hospital",
        "with": "sickness"
      },
      {
        "toCreate": "hospital",
        "with": "ambulance"
      },
      {
        "toCreate": "hospital",
        "with": "doctor"
      },
      {
        "toCreate": "house",
        "with": "wall"
      },
      {
        "toCreate": "house",
        "with": "human"
      },
      {
        "toCreate": "house",
        "with": "tool"
      },
      {
        "toCreate": "ivy",
        "with": "plant"
      },
      {
        "toCreate": "ivy",
        "with": "grass"
      },
      {
        "toCreate": "mouse",
        "with": "cheese"
      },
      {
        "toCreate": "post_office",
        "with": "mailman"
      },
      {
        "toCreate": "post_office",
        "with": "letter"
      },
      {
        "toCreate": "silo",
        "with": "wheat"
      },
      {
        "toCreate": "space_station",
        "with": "space"
      },
      {
        "toCreate": "space_station",
        "with": "atmosphere"
      },
      {
        "toCreate": "tent",
        "with": "fabric"
      },
      {
        "toCreate": "trainyard",
        "with": "train"
      },
      {
        "toCreate": "wagon",
        "with": "cart"
      },
      {
        "toCreate": "wax",
        "with": "beehive"
      },
      {
        "toCreate": "windmill",
        "with": "wheel"
      }
    ],
    "name": "Wall"
  },
  "wand": {
    "combine": [],
    "name": "Wand"
  },
  "warmth": {
    "combine": [
      {
        "toCreate": "bread",
        "with": "dough"
      },
      {
        "toCreate": "toast",
        "with": "bread"
      },
      {
        "toCreate": "toast",
        "with": "sandwich"
      }
    ],
    "name": "Warmth"
  },
  "warrior": {
    "combine": [
      {
        "toCreate": "blood",
        "with": "sword"
      },
      {
        "toCreate": "castle",
        "with": "stone"
      },
      {
        "toCreate": "castle",
        "with": "wall"
      },
      {
        "toCreate": "castle",
        "with": "house"
      },
      {
        "toCreate": "force_knight",
        "with": "light_sword"
      },
      {
        "toCreate": "grenade",
        "with": "explosion"
      },
      {
        "toCreate": "idea",
        "with": "warrior"
      },
      {
        "toCreate": "knight",
        "with": "armor"
      },
      {
        "toCreate": "knight",
        "with": "hero"
      },
      {
        "toCreate": "knight",
        "with": "horse"
      }
    ],
    "name": "Warrior"
  },
  "watch": {
    "combine": [
      {
        "toCreate": "alarm_clock",
        "with": "sound"
      },
      {
        "toCreate": "alarm_clock",
        "with": "dawn"
      },
      {
        "toCreate": "alarm_clock",
        "with": "bell"
      },
      {
        "toCreate": "clock",
        "with": "big"
      },
      {
        "toCreate": "egg_timer",
        "with": "egg"
      },
      {
        "toCreate": "sundial",
        "with": "sun"
      },
      {
        "toCreate": "sundial",
        "with": "light"
      }
    ],
    "name": "Watch"
  },
  "water": {
    "combine": [
      {
        "toCreate": "algae",
        "with": "plant"
      },
      {
        "toCreate": "algae",
        "with": "grass"
      },
      {
        "toCreate": "aquarium",
        "with": "glass"
      },
      {
        "toCreate": "ash",
        "with": "campfire"
      },
      {
        "toCreate": "beach",
        "with": "sand"
      },
      {
        "toCreate": "boat",
        "with": "wood"
      },
      {
        "toCreate": "bottle",
        "with": "container"
      },
      {
        "toCreate": "chicken_soup",
        "with": "chicken"
      },
      {
        "toCreate": "chicken_soup",
        "with": "chicken_wing"
      },
      {
        "toCreate": "cloud",
        "with": "atmosphere"
      },
      {
        "toCreate": "cloud",
        "with": "sky"
      },
      {
        "toCreate": "dam",
        "with": "beaver"
      },
      {
        "toCreate": "dew",
        "with": "grass"
      },
      {
        "toCreate": "dew",
        "with": "dawn"
      },
      {
        "toCreate": "dough",
        "with": "flour"
      },
      {
        "toCreate": "duck",
        "with": "bird"
      },
      {
        "toCreate": "duck",
        "with": "owl"
      },
      {
        "toCreate": "duck",
        "with": "pigeon"
      },
      {
        "toCreate": "duck",
        "with": "chicken"
      },
      {
        "toCreate": "fish",
        "with": "animal"
      },
      {
        "toCreate": "fish",
        "with": "egg"
      },
      {
        "toCreate": "fountain",
        "with": "statue"
      },
      {
        "toCreate": "fruit",
        "with": "flower"
      },
      {
        "toCreate": "hippo",
        "with": "horse"
      },
      {
        "toCreate": "hippo",
        "with": "cow"
      },
      {
        "toCreate": "ice",
        "with": "cold"
      },
      {
        "toCreate": "ice",
        "with": "solid"
      },
      {
        "toCreate": "juice",
        "with": "fruit"
      },
      {
        "toCreate": "juice",
        "with": "vegetable"
      },
      {
        "toCreate": "lake",
        "with": "pond"
      },
      {
        "toCreate": "liquid",
        "with": "idea"
      },
      {
        "toCreate": "liquid",
        "with": "science"
      },
      {
        "toCreate": "milk",
        "with": "cow"
      },
      {
        "toCreate": "milk",
        "with": "goat"
      },
      {
        "toCreate": "milk_shake",
        "with": "ice_cream"
      },
      {
        "toCreate": "mist",
        "with": "air"
      },
      {
        "toCreate": "mud",
        "with": "earth"
      },
      {
        "toCreate": "mud",
        "with": "soil"
      },
      {
        "toCreate": "narwhal",
        "with": "unicorn"
      },
      {
        "toCreate": "oasis",
        "with": "desert"
      },
      {
        "toCreate": "obsidian",
        "with": "lava"
      },
      {
        "toCreate": "ocean",
        "with": "sea"
      },
      {
        "toCreate": "paint",
        "with": "rainbow"
      },
      {
        "toCreate": "paint",
        "with": "double_rainbow!"
      },
      {
        "toCreate": "paint",
        "with": "pencil"
      },
      {
        "toCreate": "paper",
        "with": "wood"
      },
      {
        "toCreate": "perfume",
        "with": "flower"
      },
      {
        "toCreate": "perfume",
        "with": "rose"
      },
      {
        "toCreate": "perfume",
        "with": "sunflower"
      },
      {
        "toCreate": "petroleum",
        "with": "fossil"
      },
      {
        "toCreate": "plankton",
        "with": "life"
      },
      {
        "toCreate": "plankton",
        "with": "bacteria"
      },
      {
        "toCreate": "plant",
        "with": "seed"
      },
      {
        "toCreate": "pond",
        "with": "puddle"
      },
      {
        "toCreate": "puddle",
        "with": "water"
      },
      {
        "toCreate": "rainbow",
        "with": "light"
      },
      {
        "toCreate": "rainbow",
        "with": "sun"
      },
      {
        "toCreate": "river",
        "with": "mountain"
      },
      {
        "toCreate": "roe",
        "with": "egg"
      },
      {
        "toCreate": "sea",
        "with": "lake"
      },
      {
        "toCreate": "seahorse",
        "with": "horse"
      },
      {
        "toCreate": "seal",
        "with": "dog"
      },
      {
        "toCreate": "seaplane",
        "with": "airplane"
      },
      {
        "toCreate": "smoke",
        "with": "campfire"
      },
      {
        "toCreate": "soda",
        "with": "carbon_dioxide"
      },
      {
        "toCreate": "steam",
        "with": "heat"
      },
      {
        "toCreate": "steam",
        "with": "fire"
      },
      {
        "toCreate": "steam",
        "with": "lava"
      },
      {
        "toCreate": "steam",
        "with": "gas"
      },
      {
        "toCreate": "swim_goggles",
        "with": "glasses"
      },
      {
        "toCreate": "swim_goggles",
        "with": "sunglasses"
      },
      {
        "toCreate": "swimmer",
        "with": "human"
      },
      {
        "toCreate": "tea",
        "with": "leaf"
      },
      {
        "toCreate": "water_gun",
        "with": "gun"
      },
      {
        "toCreate": "water_pipe",
        "with": "pipe"
      },
      {
        "toCreate": "wheel",
        "with": "tool"
      }
    ],
    "name": "Water"
  },
  "water_gun": {
    "combine": [],
    "name": "Water Gun"
  },
  "water_lily": {
    "combine": [],
    "name": "Water Lily"
  },
  "water_pipe": {
    "combine": [],
    "name": "Water Pipe"
  },
  "waterfall": {
    "combine": [],
    "name": "Waterfall"
  },
  "wave": {
    "combine": [
      {
        "toCreate": "avalanche",
        "with": "glacier"
      },
      {
        "toCreate": "avalanche",
        "with": "mountain"
      },
      {
        "toCreate": "avalanche",
        "with": "mountain_range"
      },
      {
        "toCreate": "beach",
        "with": "sand"
      },
      {
        "toCreate": "sound",
        "with": "air"
      },
      {
        "toCreate": "sound",
        "with": "human"
      },
      {
        "toCreate": "sound",
        "with": "wolf"
      },
      {
        "toCreate": "sound",
        "with": "animal"
      },
      {
        "toCreate": "surfer",
        "with": "human"
      }
    ],
    "name": "Wave"
  },
  "wax": {
    "combine": [
      {
        "toCreate": "candle",
        "with": "thread"
      },
      {
        "toCreate": "candle",
        "with": "fire"
      },
      {
        "toCreate": "candle",
        "with": "light"
      },
      {
        "toCreate": "candle",
        "with": "flashlight"
      },
      {
        "toCreate": "candle",
        "with": "lamp"
      },
      {
        "toCreate": "crayon",
        "with": "rainbow"
      },
      {
        "toCreate": "crayon",
        "with": "double_rainbow!"
      },
      {
        "toCreate": "crayon",
        "with": "paint"
      },
      {
        "toCreate": "soap",
        "with": "ash"
      },
      {
        "toCreate": "soap",
        "with": "oil"
      },
      {
        "toCreate": "soap",
        "with": "clay"
      }
    ],
    "name": "Wax"
  },
  "web": {
    "combine": [
      {
        "toCreate": "internet",
        "with": "computer"
      },
      {
        "toCreate": "spider",
        "with": "animal"
      }
    ],
    "name": "Web"
  },
  "werewolf": {
    "combine": [
      {
        "toCreate": "minotaur",
        "with": "cow"
      },
      {
        "toCreate": "werewolf",
        "with": "human"
      }
    ],
    "name": "Werewolf"
  },
  "wheat": {
    "combine": [
      {
        "toCreate": "alcohol",
        "with": "fruit"
      },
      {
        "toCreate": "alcohol",
        "with": "juice"
      },
      {
        "toCreate": "beer",
        "with": "alcohol"
      },
      {
        "toCreate": "cereal",
        "with": "milk"
      },
      {
        "toCreate": "cereal",
        "with": "chocolate_milk"
      },
      {
        "toCreate": "cereal",
        "with": "coconut_milk"
      },
      {
        "toCreate": "flour",
        "with": "wheat"
      },
      {
        "toCreate": "flour",
        "with": "stone"
      },
      {
        "toCreate": "flour",
        "with": "windmill"
      },
      {
        "toCreate": "flour",
        "with": "wheel"
      },
      {
        "toCreate": "flour",
        "with": "rock"
      },
      {
        "toCreate": "scythe",
        "with": "blade"
      },
      {
        "toCreate": "scythe",
        "with": "sword"
      },
      {
        "toCreate": "scythe",
        "with": "axe"
      },
      {
        "toCreate": "silo",
        "with": "container"
      },
      {
        "toCreate": "silo",
        "with": "house"
      },
      {
        "toCreate": "silo",
        "with": "barn"
      },
      {
        "toCreate": "silo",
        "with": "wall"
      },
      {
        "toCreate": "silo",
        "with": "farm"
      },
      {
        "toCreate": "silo",
        "with": "safe"
      },
      {
        "toCreate": "silo",
        "with": "bank"
      },
      {
        "toCreate": "windmill",
        "with": "wind"
      }
    ],
    "name": "Wheat"
  },
  "wheel": {
    "combine": [
      {
        "toCreate": "bicycle",
        "with": "wheel"
      },
      {
        "toCreate": "bicycle",
        "with": "chain"
      },
      {
        "toCreate": "bicycle",
        "with": "machine"
      },
      {
        "toCreate": "car",
        "with": "metal"
      },
      {
        "toCreate": "car",
        "with": "steel"
      },
      {
        "toCreate": "car",
        "with": "bicycle"
      },
      {
        "toCreate": "car",
        "with": "motorcycle"
      },
      {
        "toCreate": "cart",
        "with": "wood"
      },
      {
        "toCreate": "clock",
        "with": "time"
      },
      {
        "toCreate": "clock",
        "with": "sundial"
      },
      {
        "toCreate": "combustion_engine",
        "with": "petroleum"
      },
      {
        "toCreate": "cyclist",
        "with": "human"
      },
      {
        "toCreate": "donut",
        "with": "dough"
      },
      {
        "toCreate": "donut",
        "with": "cookie_dough"
      },
      {
        "toCreate": "fabric",
        "with": "thread"
      },
      {
        "toCreate": "flour",
        "with": "wheat"
      },
      {
        "toCreate": "hamster",
        "with": "mouse"
      },
      {
        "toCreate": "hamster",
        "with": "rat"
      },
      {
        "toCreate": "machine",
        "with": "tool"
      },
      {
        "toCreate": "machine",
        "with": "boiler"
      },
      {
        "toCreate": "oil",
        "with": "sunflower"
      },
      {
        "toCreate": "pizza",
        "with": "cheese"
      },
      {
        "toCreate": "pottery",
        "with": "clay"
      },
      {
        "toCreate": "skateboard",
        "with": "snowboard"
      },
      {
        "toCreate": "skateboard",
        "with": "skier"
      },
      {
        "toCreate": "skateboard",
        "with": "ski_goggles"
      },
      {
        "toCreate": "steam_engine",
        "with": "steam"
      },
      {
        "toCreate": "steam_engine",
        "with": "boiler"
      },
      {
        "toCreate": "sundial",
        "with": "sun"
      },
      {
        "toCreate": "sundial",
        "with": "day"
      },
      {
        "toCreate": "sundial",
        "with": "light"
      },
      {
        "toCreate": "thread",
        "with": "cotton"
      },
      {
        "toCreate": "train",
        "with": "steam_engine"
      },
      {
        "toCreate": "windmill",
        "with": "wind"
      },
      {
        "toCreate": "windmill",
        "with": "wall"
      }
    ],
    "name": "Wheel"
  },
  "wild_boar": {
    "combine": [
      {
        "toCreate": "lasso",
        "with": "rope"
      }
    ],
    "name": "Wild Boar"
  },
  "wind": {
    "combine": [
      {
        "toCreate": "blizzard",
        "with": "snow"
      },
      {
        "toCreate": "cold",
        "with": "human"
      },
      {
        "toCreate": "dune",
        "with": "desert"
      },
      {
        "toCreate": "dune",
        "with": "sand"
      },
      {
        "toCreate": "dune",
        "with": "beach"
      },
      {
        "toCreate": "electricity",
        "with": "wind_turbine"
      },
      {
        "toCreate": "flute",
        "with": "wood"
      },
      {
        "toCreate": "gust",
        "with": "small"
      },
      {
        "toCreate": "hail",
        "with": "ice"
      },
      {
        "toCreate": "kite",
        "with": "paper"
      },
      {
        "toCreate": "leaf",
        "with": "tree"
      },
      {
        "toCreate": "leaf",
        "with": "flower"
      },
      {
        "toCreate": "leaf",
        "with": "orchard"
      },
      {
        "toCreate": "leaf",
        "with": "forest"
      },
      {
        "toCreate": "motion",
        "with": "science"
      },
      {
        "toCreate": "motion",
        "with": "idea"
      },
      {
        "toCreate": "motion",
        "with": "philosophy"
      },
      {
        "toCreate": "pollen",
        "with": "plant"
      },
      {
        "toCreate": "pollen",
        "with": "flower"
      },
      {
        "toCreate": "rust",
        "with": "metal"
      },
      {
        "toCreate": "rust",
        "with": "steel"
      },
      {
        "toCreate": "sailboat",
        "with": "boat"
      },
      {
        "toCreate": "sailboat",
        "with": "steamboat"
      },
      {
        "toCreate": "sand",
        "with": "stone"
      },
      {
        "toCreate": "sand",
        "with": "rock"
      },
      {
        "toCreate": "sand",
        "with": "pebble"
      },
      {
        "toCreate": "sandstorm",
        "with": "desert"
      },
      {
        "toCreate": "storm",
        "with": "rain"
      },
      {
        "toCreate": "tornado",
        "with": "storm"
      },
      {
        "toCreate": "tornado",
        "with": "big"
      },
      {
        "toCreate": "tornado",
        "with": "wind"
      },
      {
        "toCreate": "tornado",
        "with": "motion"
      },
      {
        "toCreate": "wave",
        "with": "ocean"
      },
      {
        "toCreate": "wave",
        "with": "sea"
      },
      {
        "toCreate": "wave",
        "with": "lake"
      },
      {
        "toCreate": "wind_turbine",
        "with": "electricity"
      },
      {
        "toCreate": "windmill",
        "with": "house"
      },
      {
        "toCreate": "windmill",
        "with": "wheel"
      },
      {
        "toCreate": "windmill",
        "with": "wheat"
      },
      {
        "toCreate": "windmill",
        "with": "flour"
      },
      {
        "toCreate": "windsurfer",
        "with": "surfer"
      }
    ],
    "name": "Wind"
  },
  "wind_turbine": {
    "combine": [
      {
        "toCreate": "blender",
        "with": "blade"
      },
      {
        "toCreate": "electric_car",
        "with": "car"
      },
      {
        "toCreate": "electric_car",
        "with": "wagon"
      },
      {
        "toCreate": "electricity",
        "with": "wind"
      },
      {
        "toCreate": "electricity",
        "with": "motion"
      },
      {
        "toCreate": "electricity",
        "with": "storm"
      },
      {
        "toCreate": "electricity",
        "with": "sandstorm"
      },
      {
        "toCreate": "helicopter",
        "with": "airplane"
      },
      {
        "toCreate": "helicopter",
        "with": "seaplane"
      }
    ],
    "name": "Wind Turbine"
  },
  "windmill": {
    "combine": [
      {
        "toCreate": "blender",
        "with": "blade"
      },
      {
        "toCreate": "don_quixote",
        "with": "knight"
      },
      {
        "toCreate": "don_quixote",
        "with": "story"
      },
      {
        "toCreate": "don_quixote",
        "with": "legend"
      },
      {
        "toCreate": "don_quixote",
        "with": "hero"
      },
      {
        "toCreate": "flour",
        "with": "wheat"
      },
      {
        "toCreate": "helicopter",
        "with": "airplane"
      },
      {
        "toCreate": "helicopter",
        "with": "seaplane"
      },
      {
        "toCreate": "lawn_mower",
        "with": "scythe"
      },
      {
        "toCreate": "oil",
        "with": "sunflower"
      },
      {
        "toCreate": "wind_turbine",
        "with": "electricity"
      },
      {
        "toCreate": "wind_turbine",
        "with": "machine"
      },
      {
        "toCreate": "wind_turbine",
        "with": "electrician"
      }
    ],
    "name": "Windmill"
  },
  "windsurfer": {
    "combine": [],
    "name": "Windsurfer"
  },
  "wine": {
    "combine": [
      {
        "toCreate": "bottle",
        "with": "container"
      },
      {
        "toCreate": "drunk",
        "with": "human"
      },
      {
        "toCreate": "sugar",
        "with": "fire"
      },
      {
        "toCreate": "sugar",
        "with": "energy"
      },
      {
        "toCreate": "vinegar",
        "with": "oxygen"
      },
      {
        "toCreate": "vinegar",
        "with": "air"
      },
      {
        "toCreate": "vinegar",
        "with": "time"
      }
    ],
    "name": "Wine"
  },
  "wire": {
    "combine": [
      {
        "toCreate": "bonsai_tree",
        "with": "tree"
      },
      {
        "toCreate": "bow",
        "with": "wood"
      },
      {
        "toCreate": "cable_car",
        "with": "mountain"
      },
      {
        "toCreate": "cable_car",
        "with": "mountain_range"
      },
      {
        "toCreate": "chain",
        "with": "metal"
      },
      {
        "toCreate": "chain",
        "with": "steel"
      },
      {
        "toCreate": "computer",
        "with": "hacker"
      },
      {
        "toCreate": "dynamite",
        "with": "gunpowder"
      },
      {
        "toCreate": "electrician",
        "with": "human"
      },
      {
        "toCreate": "fishing_rod",
        "with": "fish"
      },
      {
        "toCreate": "fishing_rod",
        "with": "swordfish"
      },
      {
        "toCreate": "fishing_rod",
        "with": "piranha"
      },
      {
        "toCreate": "harp",
        "with": "angel"
      },
      {
        "toCreate": "harp",
        "with": "music"
      },
      {
        "toCreate": "internet",
        "with": "computer"
      },
      {
        "toCreate": "optical_fiber",
        "with": "light"
      },
      {
        "toCreate": "rope",
        "with": "thread"
      },
      {
        "toCreate": "rope",
        "with": "tool"
      },
      {
        "toCreate": "snake",
        "with": "animal"
      },
      {
        "toCreate": "spaghetti",
        "with": "pasta"
      },
      {
        "toCreate": "steel_wool",
        "with": "wool"
      },
      {
        "toCreate": "string_phone",
        "with": "paper_cup"
      },
      {
        "toCreate": "string_phone",
        "with": "cup"
      },
      {
        "toCreate": "stun_gun",
        "with": "gun"
      },
      {
        "toCreate": "vine",
        "with": "rainforest"
      }
    ],
    "name": "Wire"
  },
  "witch": {
    "combine": [
      {
        "toCreate": "cauldron",
        "with": "metal"
      },
      {
        "toCreate": "cauldron",
        "with": "steel"
      },
      {
        "toCreate": "cauldron",
        "with": "fireplace"
      },
      {
        "toCreate": "cauldron",
        "with": "campfire"
      },
      {
        "toCreate": "crystal_ball",
        "with": "glass"
      },
      {
        "toCreate": "crystal_ball",
        "with": "snow_globe"
      },
      {
        "toCreate": "magic",
        "with": "energy"
      }
    ],
    "name": "Witch"
  },
  "wizard": {
    "combine": [
      {
        "toCreate": "crystal_ball",
        "with": "glass"
      },
      {
        "toCreate": "crystal_ball",
        "with": "snow_globe"
      },
      {
        "toCreate": "faun",
        "with": "goat"
      },
      {
        "toCreate": "faun",
        "with": "mountain_goat"
      },
      {
        "toCreate": "magic",
        "with": "energy"
      },
      {
        "toCreate": "owl",
        "with": "bird"
      },
      {
        "toCreate": "wand",
        "with": "wood"
      },
      {
        "toCreate": "wand",
        "with": "sword"
      },
      {
        "toCreate": "wand",
        "with": "tool"
      },
      {
        "toCreate": "wand",
        "with": "pencil"
      },
      {
        "toCreate": "witch",
        "with": "broom"
      }
    ],
    "name": "Wizard"
  },
  "wolf": {
    "combine": [
      {
        "toCreate": "bone",
        "with": "corpse"
      },
      {
        "toCreate": "bone",
        "with": "meat"
      },
      {
        "toCreate": "cage",
        "with": "metal"
      },
      {
        "toCreate": "cage",
        "with": "steel"
      },
      {
        "toCreate": "cage",
        "with": "wall"
      },
      {
        "toCreate": "cave",
        "with": "house"
      },
      {
        "toCreate": "cave",
        "with": "container"
      },
      {
        "toCreate": "dog",
        "with": "domestication"
      },
      {
        "toCreate": "dog",
        "with": "farmer"
      },
      {
        "toCreate": "dog",
        "with": "farm"
      },
      {
        "toCreate": "dog",
        "with": "barn"
      },
      {
        "toCreate": "dog",
        "with": "field"
      },
      {
        "toCreate": "dog",
        "with": "bone"
      },
      {
        "toCreate": "dog",
        "with": "campfire"
      },
      {
        "toCreate": "fox",
        "with": "chicken"
      },
      {
        "toCreate": "fox",
        "with": "chicken_coop"
      },
      {
        "toCreate": "piranha",
        "with": "fish"
      },
      {
        "toCreate": "shark",
        "with": "ocean"
      },
      {
        "toCreate": "shark",
        "with": "sea"
      },
      {
        "toCreate": "shark",
        "with": "fish"
      },
      {
        "toCreate": "sound",
        "with": "wave"
      },
      {
        "toCreate": "werewolf",
        "with": "human"
      }
    ],
    "name": "Wolf"
  },
  "wood": {
    "combine": [
      {
        "toCreate": "arrow",
        "with": "bullet"
      },
      {
        "toCreate": "arrow",
        "with": "bow"
      },
      {
        "toCreate": "axe",
        "with": "blade"
      },
      {
        "toCreate": "axe",
        "with": "tool"
      },
      {
        "toCreate": "axe",
        "with": "stone"
      },
      {
        "toCreate": "beaver",
        "with": "animal"
      },
      {
        "toCreate": "beehive",
        "with": "bee"
      },
      {
        "toCreate": "bell",
        "with": "sound"
      },
      {
        "toCreate": "boat",
        "with": "water"
      },
      {
        "toCreate": "boat",
        "with": "ocean"
      },
      {
        "toCreate": "boat",
        "with": "sea"
      },
      {
        "toCreate": "boat",
        "with": "lake"
      },
      {
        "toCreate": "boat",
        "with": "river"
      },
      {
        "toCreate": "book",
        "with": "paper"
      },
      {
        "toCreate": "bow",
        "with": "rope"
      },
      {
        "toCreate": "bow",
        "with": "wire"
      },
      {
        "toCreate": "bridge",
        "with": "river"
      },
      {
        "toCreate": "bridge",
        "with": "stream"
      },
      {
        "toCreate": "broom",
        "with": "hay"
      },
      {
        "toCreate": "bucket",
        "with": "container"
      },
      {
        "toCreate": "campfire",
        "with": "fire"
      },
      {
        "toCreate": "campfire",
        "with": "flamethrower"
      },
      {
        "toCreate": "cart",
        "with": "wheel"
      },
      {
        "toCreate": "charcoal",
        "with": "fire"
      },
      {
        "toCreate": "coffin",
        "with": "corpse"
      },
      {
        "toCreate": "cutting_board",
        "with": "cook"
      },
      {
        "toCreate": "drum",
        "with": "leather"
      },
      {
        "toCreate": "drum",
        "with": "fabric"
      },
      {
        "toCreate": "drum",
        "with": "music"
      },
      {
        "toCreate": "fence",
        "with": "wall"
      },
      {
        "toCreate": "fence",
        "with": "field"
      },
      {
        "toCreate": "fence",
        "with": "garden"
      },
      {
        "toCreate": "fireplace",
        "with": "container"
      },
      {
        "toCreate": "fishing_rod",
        "with": "fish"
      },
      {
        "toCreate": "fishing_rod",
        "with": "swordfish"
      },
      {
        "toCreate": "fishing_rod",
        "with": "piranha"
      },
      {
        "toCreate": "flute",
        "with": "wind"
      },
      {
        "toCreate": "flute",
        "with": "air"
      },
      {
        "toCreate": "fruit_tree",
        "with": "fruit"
      },
      {
        "toCreate": "log_cabin",
        "with": "house"
      },
      {
        "toCreate": "lumberjack",
        "with": "human"
      },
      {
        "toCreate": "mailbox",
        "with": "letter"
      },
      {
        "toCreate": "mailbox",
        "with": "mailman"
      },
      {
        "toCreate": "mirror",
        "with": "glass"
      },
      {
        "toCreate": "mousetrap",
        "with": "cheese"
      },
      {
        "toCreate": "paper",
        "with": "pressure"
      },
      {
        "toCreate": "paper",
        "with": "water"
      },
      {
        "toCreate": "paper",
        "with": "machine"
      },
      {
        "toCreate": "paper",
        "with": "blender"
      },
      {
        "toCreate": "pencil",
        "with": "charcoal"
      },
      {
        "toCreate": "pencil",
        "with": "coal"
      },
      {
        "toCreate": "pinocchio",
        "with": "life"
      },
      {
        "toCreate": "pinocchio",
        "with": "story"
      },
      {
        "toCreate": "pipe",
        "with": "tobacco"
      },
      {
        "toCreate": "plow",
        "with": "earth"
      },
      {
        "toCreate": "plow",
        "with": "field"
      },
      {
        "toCreate": "popsicle",
        "with": "juice"
      },
      {
        "toCreate": "ruler",
        "with": "pencil"
      },
      {
        "toCreate": "smoke",
        "with": "fire"
      },
      {
        "toCreate": "snowboard",
        "with": "snow"
      },
      {
        "toCreate": "snowboard",
        "with": "ice"
      },
      {
        "toCreate": "sword",
        "with": "blade"
      },
      {
        "toCreate": "tent",
        "with": "fabric"
      },
      {
        "toCreate": "tool",
        "with": "human"
      },
      {
        "toCreate": "tool",
        "with": "metal"
      },
      {
        "toCreate": "tool",
        "with": "steel"
      },
      {
        "toCreate": "tool",
        "with": "stone"
      },
      {
        "toCreate": "tool",
        "with": "rock"
      },
      {
        "toCreate": "tree",
        "with": "plant"
      },
      {
        "toCreate": "treehouse",
        "with": "tree"
      },
      {
        "toCreate": "trojan_horse",
        "with": "horse"
      },
      {
        "toCreate": "wall",
        "with": "wood"
      },
      {
        "toCreate": "wand",
        "with": "wizard"
      },
      {
        "toCreate": "woodpecker",
        "with": "bird"
      }
    ],
    "name": "Wood"
  },
  "woodpecker": {
    "combine": [
      {
        "toCreate": "egg",
        "with": "woodpecker"
      },
      {
        "toCreate": "hammer",
        "with": "tool"
      }
    ],
    "name": "Woodpecker"
  },
  "wool": {
    "combine": [
      {
        "toCreate": "cashmere",
        "with": "mountain_goat"
      },
      {
        "toCreate": "christmas_stocking",
        "with": "fireplace"
      },
      {
        "toCreate": "christmas_stocking",
        "with": "christmas_tree"
      },
      {
        "toCreate": "christmas_stocking",
        "with": "reindeer"
      },
      {
        "toCreate": "christmas_stocking",
        "with": "santa"
      },
      {
        "toCreate": "cotton",
        "with": "plant"
      },
      {
        "toCreate": "sheep",
        "with": "livestock"
      },
      {
        "toCreate": "steel_wool",
        "with": "steel"
      },
      {
        "toCreate": "steel_wool",
        "with": "metal"
      },
      {
        "toCreate": "steel_wool",
        "with": "wire"
      },
      {
        "toCreate": "sweater",
        "with": "tool"
      },
      {
        "toCreate": "sweater",
        "with": "human"
      }
    ],
    "name": "Wool"
  },
  "wrapping_paper": {
    "combine": [
      {
        "toCreate": "gift",
        "with": "santa"
      },
      {
        "toCreate": "gift",
        "with": "christmas_tree"
      },
      {
        "toCreate": "gift",
        "with": "christmas_stocking"
      },
      {
        "toCreate": "gift",
        "with": "fireplace"
      }
    ],
    "name": "Wrapping Paper"
  },
  "yeti": {
    "combine": [],
    "name": "Yeti"
  },
  "yogurt": {
    "combine": [
      {
        "toCreate": "bucket",
        "with": "container"
      },
      {
        "toCreate": "frozen_yogurt",
        "with": "cold"
      },
      {
        "toCreate": "frozen_yogurt",
        "with": "ice"
      },
      {
        "toCreate": "milk_shake",
        "with": "ice_cream"
      }
    ],
    "name": "Yogurt"
  },
  "zombie": {
    "combine": [
      {
        "toCreate": "frankenstein’s_monster",
        "with": "electricity"
      },
      {
        "toCreate": "frankenstein’s_monster",
        "with": "lightning"
      },
      {
        "toCreate": "grim_reaper",
        "with": "scythe"
      },
      {
        "toCreate": "zombie",
        "with": "human"
      }
    ],
    "name": "Zombie"
  },
  "zoo": {
    "combine": [],
    "name": "Zoo"
  }
};

export default ELEMENTS;
