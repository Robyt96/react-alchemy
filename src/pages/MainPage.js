import React, { useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { SoundsContextProvider } from '../store/soundsContext';
import { fetchElements } from '../store/elementsActions';
import ElementsContainer from '../components/ElementsContainer';

// let firstRender = true;
// let firstRenderDisplayed = true;

const MainPage = () => {
  const firstRender = useRef(true);
  const firstRenderDisplayed = useRef(true);

  const token = useSelector(state => state.auth.token);
  const userId = useSelector(state => state.auth.userId);
  const displayedElements = useSelector(state => state.elements.present.displayedElements);
  const discoveredElements = useSelector(state => state.elements.present.discoveredElements);

  const dispatch = useDispatch();
  
  useEffect(() => {
    dispatch(fetchElements(userId, token));
  }, [dispatch, userId, token]);

  useEffect(() => {
    if (firstRenderDisplayed.current) {
      firstRenderDisplayed.current = false;
      return;
    }
    localStorage.setItem(userId, JSON.stringify(displayedElements));
  }, [displayedElements, userId]);

  useEffect(() => {
    if (firstRender.current) {
      firstRender.current = false;
      return;
    }

    const storeDiscoveredElements = async (elements) => {
      try {
        const base_path = process.env.REACT_APP_FIREBASE_BASE_PATH;
        const response = await fetch(
          `${base_path}/users/${userId}.json?auth=${token}`,
          {
            method: 'PUT',
            body: JSON.stringify({
              discoveredElements: elements
            }),
            headers: {
              'Content-Type': 'application/json'
            }
          }
        );
  
        if (!response.ok) {
          const data = await response.json();
          console.log(data)
          throw new Error(data.error.message);
        }

      } catch (error) {
        console.log(error.message);
      }
    }

    storeDiscoveredElements(discoveredElements);
  }, [discoveredElements, token, userId]);

  return (
    <SoundsContextProvider>
      <ElementsContainer />
    </SoundsContextProvider>
  );
};
 
export default MainPage;
