import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';

import { tryAutoLogin } from '../store/authActions';
import AuthForm from '../components/auth/AuthForm';

const AuthPage = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(tryAutoLogin());
  }, [dispatch]);

  return (
    <AuthForm />
  );
};
 
export default AuthPage;
