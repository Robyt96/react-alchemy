import React, { useState, useEffect, useCallback } from 'react';
import useSound from 'use-sound';

import useInteraction from '../hooks/useInteraction';
import backgroundMusic from '../assets/sounds/modular-ambient-04-792.mp3';

const SoundsContext = React.createContext({
  soundsVolume: 0.5,
  musicVolume: 0.5,
  changeSoundsVolume: (volume) => {},
  changeMusicVolume: (volume) => {}
});

let timer;

export const SoundsContextProvider = (props) => {
  const [musicVolume, setMusicVolume] = useState(0.5);
  const [soundsVolume, setSoundsVolume] = useState(0.5);
  const interacted = useInteraction();
  const [play, { stop, duration, isPlaying }] = useSound(backgroundMusic, {
    volume: musicVolume
  });

  const startMusic = useCallback(() => {
    // console.log('starting music');
    play();
    if (duration) {
      timer = setTimeout(() => {
        // console.log('restarting music');
        startMusic();
      }, duration);
    }
  }, [play, duration]);

  const stopMusic = useCallback(() => {
    // console.log('stopping music');
    stop();
    clearTimeout(timer);
  }, [stop]);

  useEffect(() => {
    if (interacted) {
      // console.log('starting music use effect');
      startMusic();
    }
  }, [interacted, startMusic]);

  useEffect(() => {
    // on component unmount (on user logout)
    return () => {
      // console.log('cleaning function');
      stopMusic()
    };
  }, [stopMusic]);

  const changeMusicVolume = (volume) => {
    if (volume === '0') {
      stopMusic();
    } else {
      if (!isPlaying) {
        // console.log('starting music changeMusicVolume')
        startMusic();
      }
    }
    setMusicVolume(+volume);
  };

  const changeSoundsVolume = (volume) => {
    setSoundsVolume(+volume);
  };

  return (
    <SoundsContext.Provider value={{
      soundsVolume,
      musicVolume,
      changeMusicVolume,
      changeSoundsVolume,
    }}>
      {props.children}
    </SoundsContext.Provider>
  );
}

export default SoundsContext
