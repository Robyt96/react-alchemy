import { createSlice } from '@reduxjs/toolkit';

import ELEMENTS from '../elements';

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

const getNewElementsIds = (elementIdA, elementIdB) => {
  const possibleElements = ELEMENTS[elementIdA].combine;
  const createdElementsId = possibleElements.filter((combineObj) => {
    return combineObj.with === elementIdB;
  }).map(combineObj => {
    return combineObj.toCreate
  });

  return createdElementsId;
}

const getInitialElements = () => {
  const { innerWidth: width, innerHeight: height } = window; // set initial state based on this

  const offset = width > 600 ? -90 : 0;

  const initialElements = {
    fire1: { elementId: 'fire', top: height/2-65, left: width/2-20+offset },
    air1: { elementId: 'air', top: height/2-20, left: width/2+25+offset },
    water1: { elementId: 'water', top: height/2+25, left: width/2-20+offset },
    earth1: { elementId: 'earth', top: height/2-20, left: width/2-65+offset }
  };

  return initialElements;
}

const initialState = {
  displayedElements: getInitialElements(),
  discoveredElements: ['fire', 'air', 'water', 'earth'],
  newElementsToShow: [],
  specialElementToShow: null
};

const elementsSlice = createSlice({
  name: 'elements',
  initialState: initialState,
  reducers: {
    moveElement(state, action) {
      const { key, left, top } = action.payload;
      state.displayedElements[key].left = left;
      state.displayedElements[key].top = top;
    },
    removeElement(state, action) {
      const key = action.payload;
      delete state.displayedElements[key];
    },
    duplicateElement(state, action) {
      const { left, top, elementId, key } = action.payload;
      const time = new Date().getTime().toString();
      const duplicatedId = elementId + time;
      state.displayedElements[duplicatedId] = {
        ...state.displayedElements[key],
        left: left + 50,
        top: top + 50
      };
    },
    removeAllElements(state) {
      state.displayedElements = {};
    },
    addStartingElements(state, action) {
      const { left, top } = action.payload;
      const time = new Date().getTime().toString();
      state.displayedElements = {
        ...state.displayedElements,
        ['fire' + time]: { elementId: 'fire', top: top-65, left: left-20},
        ['air' + time]: { elementId: 'air', top: top-20, left: left+25 },
        ['water' + time]: { elementId: 'water', top: top+25, left: left-20},
        ['earth' + time]: { elementId: 'earth', top: top-20, left: left-65},
      };
    },
    addElements(state, action) {
      const { elementsIds, maxLeft, maxTop } = action.payload;
      const time = new Date().getTime().toString();
      for (const id of elementsIds) {
        state.displayedElements[id + time] = {
          elementId: id,
          top: Math.floor(maxTop/2) + getRandomInt(-maxTop/4, maxTop/4),
          left: Math.floor(maxLeft/2) + getRandomInt(-maxLeft/4, maxLeft/4),
          isTerminal: ELEMENTS[id].combine.length === 0
        }
      }
    },
    combineElements(state, action) {
      const {
        key: droppedElementKey,
        elementId: droppedElementId,
        left,
        top
      } = action.payload.droppedElement;
      const coveredElementKey = action.payload.coveredElementKey;
      const newElementsIds = getNewElementsIds(
        droppedElementId, state.displayedElements[coveredElementKey].elementId
      );
      if (newElementsIds.length > 0) {
        delete state.displayedElements[droppedElementKey];
        delete state.displayedElements[coveredElementKey];
        for (const id of newElementsIds) {
          const newKey = id + new Date().getTime().toString();
          state.displayedElements[newKey] = {
            elementId: id,
            top: top + getRandomInt(-15, 15),
            left: left + getRandomInt(-15, 15),
            isTerminal: ELEMENTS[id].combine.length === 0
          };
          if (!state.discoveredElements.includes(id)) {
            if (state.discoveredElements.length === 99) {
              //i.e. the 100th element has been unlocked
              state.discoveredElements.push('time');
              state.specialElementToShow = 'time';
              state.displayedElements[id + new Date().getTime().toString()] = {
                elementId: 'time',
                top: top + getRandomInt(-15, 15),
                left: left + getRandomInt(-15, 15)
              };
            }
            state.discoveredElements.push(id);
            state.newElementsToShow.push(id);
          } else {
            // adding justCreated prop to play sound only if the element was
            // already discovered
            state.displayedElements[newKey].justCreated = true;
          }
        }
      } else {
        state.displayedElements[droppedElementKey].left = left;
        state.displayedElements[droppedElementKey].top = top;
      }
    },
    setDiscoveredElements(state, action) {
      const discoveredElements = action.payload;
      state.discoveredElements = discoveredElements;
    },
    setDisplayedElements(state, action) {
      const displayedElements = action.payload;
      state.displayedElements = displayedElements;
    },
    clearNewElementsToShow(state) {
      state.newElementsToShow = [];
    },
    clearSpecialElementToShow(state) {
      state.specialElementToShow = null;
    },
    resetElements() {
      return initialState;
    },
    clearJustCreatedProp(state, action) {
      const key = action.payload;
      if (state.displayedElements[key] && state.displayedElements[key].justCreated) {
        delete state.displayedElements[key].justCreated;
      }
    }
  }
});

export const elementsActions = elementsSlice.actions;

export default elementsSlice;
