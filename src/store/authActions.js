import { authActions } from './authSlice';
import { elementsActions } from './elementsSlice';

let timer;

const getNewToken = async (refreshToken, dispatch) => {
  dispatch(authActions.authStart());

  try {
    const api_key = process.env.REACT_APP_FIREBASE_API_KEY;
    const res = await fetch(
      `https://securetoken.googleapis.com/v1/token?key=${api_key}`,
      {
        method: 'POST',
        body: `grant_type=refresh_token&refresh_token=${refreshToken}`,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      }
    );

    let data;
    if (!res.ok) {
      data = await res.json();
      console.log(data)
      throw new Error(data.error.message);
    }

    data = await res.json();
    const currentTime = new Date().getTime();
    const expirationTime = currentTime + data.expires_in*1000;
    const expirationDate = new Date(expirationTime);
    localStorage.setItem('token', data.id_token);
    localStorage.setItem('refreshToken', data.refresh_token);
    localStorage.setItem('expirationDate', expirationDate);
    localStorage.setItem('userId', data.user_id);

    timer = setTimeout(() => {
      getNewToken(data.refresh_token, dispatch);
    }, data.expires_in*1000 - 300*1000);

    const email = localStorage.getItem('email');
    dispatch(authActions.setUserData({
      token: data.id_token,
      userId: data.user_id,
      email: email
    }));

  } catch (error) {
    console.log(error);
    dispatch(authActions.authFail(error.message));
  }
};

export const authenticate = (email, password, isSignUp) => {
  return async dispatch => {
    dispatch(authActions.authStart());
    
    const api_key = process.env.REACT_APP_FIREBASE_API_KEY;
    let url = `https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=${api_key}`;
    if (!isSignUp) {
      url = `https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${api_key}`
    };

    try {
      const res = await fetch(url, {
        method: 'POST',
        body: JSON.stringify({
          email: email,
          password: password,
          returnSecureToken: true,
        }),
        headers: {
          'Content-Type': 'application/json',
        },
      });

      let data;
      if (!res.ok) {
        data = await res.json();
        console.log(data)
        throw new Error(data.error.message);
      }

      data = await res.json();
      const currentTime = new Date().getTime();
      const expirationTime = currentTime + data.expiresIn*1000;
      const expirationDate = new Date(expirationTime);
      localStorage.setItem('token', data.idToken);
      localStorage.setItem('refreshToken', data.refreshToken);
      localStorage.setItem('expirationDate', expirationDate);
      localStorage.setItem('userId', data.localId);
      localStorage.setItem('email', email);

      timer = setTimeout(() => {
        getNewToken(data.refreshToken, dispatch);
      }, data.expiresIn*1000 - 300*1000);

      dispatch(authActions.setUserData({
        token: data.idToken,
        userId: data.localId,
        email: email
      }));
    } catch (error) {
      console.log(error);
      let errorMessage;
      switch (error.message) {
        case "EMAIL_EXISTS":
          errorMessage = "Email address already registered. Please log in.";
          break;
        case "EMAIL_NOT_FOUND":
          errorMessage = "Email address not found.";
          break;
        case "INVALID_PASSWORD":
          errorMessage = "The password is incorrect. Please try again.";
          break;
        default:
          errorMessage = error.message;
      }
      dispatch(authActions.authFail(errorMessage));
    }
  }
}

export const tryAutoLogin = () => {
  return async dispatch => {
    const refreshToken = localStorage.getItem('refreshToken');
    if (refreshToken) {
      getNewToken(refreshToken, dispatch);
    }
  }
}

export const logout = () => {
  return dispatch => {
    localStorage.removeItem('token');
    localStorage.removeItem('refreshToken');
    localStorage.removeItem('expirationDate');
    localStorage.removeItem('userId');
    localStorage.removeItem('email');
    clearTimeout(timer);
    dispatch(elementsActions.resetElements())
    dispatch(authActions.logout());
  }
}
