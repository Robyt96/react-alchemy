import { elementsActions } from './elementsSlice';

export const fetchElements = (userId, token) => {
  return async dispatch => {
    const base_path = process.env.REACT_APP_FIREBASE_BASE_PATH;
    const response = await fetch(
      `${base_path}/users/${userId}.json?auth=${token}`
    );
    const data = await response.json();
    
    if (data && data.discoveredElements) {
      const discoveredElements = data.discoveredElements.filter(element => {
        return element !== null;
      });
      dispatch(elementsActions.setDiscoveredElements(discoveredElements));
    }

    const displayedElements = JSON.parse(localStorage.getItem(userId));
    if (displayedElements) {
      dispatch(elementsActions.setDisplayedElements(displayedElements));
    }
    
    dispatch({ type: 'CLEAR_HISTORY' });
  };
};

// export const storeDisplayedElements = (displayedElements) => {
//   return dispatch => {
//     localStorage.setItem('displayedElements', JSON.stringify(displayedElements));
//   }
// };
