import { configureStore } from '@reduxjs/toolkit';

import undoable from './undoableReducer';
import elementsSlice from './elementsSlice';
import authSlice from './authSlice';

const store = configureStore({
  reducer: 
  {
    elements: undoable(elementsSlice.reducer),
    auth: authSlice.reducer
  }
});

export default store;
