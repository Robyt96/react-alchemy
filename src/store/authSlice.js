import { createSlice } from '@reduxjs/toolkit';

const authSlice = createSlice({
  name: 'auth',
  initialState: {
    token: null,
    userId: null,
    email: null,
    error: null,
    loading: false
  },
  reducers: {
    authStart(state) {
      state.error = null;
      state.loading = true;
    },
    logout(state) {
      state.token = null;
      state.userId = null;
      state.email = null;
    },
    setUserData(state, action) {
      const { token, userId, email } = action.payload;
      state.token = token;
      state.userId = userId;
      state.email = email;
      state.error = null;
      state.loading = false;
    },
    authFail(state, action) {
      const error = action.payload;
      state.error = error;
      state.loading = false; 
    }
  }
})

export const authActions = authSlice.actions;

export default authSlice;
