import { useLayoutEffect, useRef, useState } from 'react';

export default function useDimensions() {
  const ref = useRef();
  const [dimensions, setDimensions] = useState({});
  useLayoutEffect(() => {
    if (!ref.current) return;
    setDimensions(ref.current.getBoundingClientRect().toJSON());
  }, [ref.current]);

  return [ref, dimensions];
}
