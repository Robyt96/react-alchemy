# React Alchemy

A single page web application built with React where you can play Alchemy!

## The application
The running application is hosted on a [GitLab page](https://robyt96.gitlab.io/react-alchemy/), try it out!
Create an account (only an username and a password are required) so that your progresses can be saved and start playing.

In the game you start with only four elements (fire, water, earth and air) and your goal is to unclock all the elements. Simply drag an element and drop on another one to try combining them. You can use the sidebar to remove elements from the screen or to duplicate them (keep in mind some elements can be combined with themselves). You can also double click on an empty space on the screen to add the four starting elements or double click on an element to duplicate it.

The layout is very responsive, so that it is possible to play on both PC desktops and mobile devices (with horizontal and vertical orientation).

## Implementation
This project was created with the only purporse of practicing web development with modern React and its features, especially React hooks and functional components.

Here there is a list of the main libraries and tools used:
- This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app)
- [React DnD](https://github.com/react-dnd/react-dnd) to manage drag and drop
- [Redux Toolkit](https://github.com/reduxjs/redux-toolkit) to manage app-wide state
- [React Router](https://github.com/ReactTraining/react-router) to route users to the authentication or the main page
- [React Icons](https://github.com/react-icons/react-icons) to easily show nice icons
- [useSound](https://github.com/joshwcomeau/use-sound), a nice hook for managing sound effects
- [Firebase](https://firebase.google.com/) as a backend to store users progress and manage authentication


## Run locally
In order to run the application in development mode on your machine, you first need to setup a new [Firebase](https://firebase.google.com/) project. There make sure you enable email and password authentication and configure a real time database with the following security rules:
```
{
 "rules": {
   "users": {
     ".read": "auth != null",
     ".write": "auth != null",
     "$uid": {
       ".read": "auth != null && auth.uid == $uid",
       ".write": "auth != null && auth.uid == $uid"
     }
   }
 }
}
```

After that, add a `.env` file in the root folder (on the same level as `package.json`, NOT in the src folder) and write in it the following environment variables:
```
REACT_APP_FIREBASE_BASE_PATH=[Your firebase project url]
REACT_APP_FIREBASE_API_KEY=[Your api key]
```
and replace [Your firebase project url] with the url you find in the Realtime Database section and [Your api key] with the API key you find in the project settings.

With the Firebase backend set up, you should be able to run
```
npm install
npm start
```
to install all dependecies and run the development server.

### GitLab CI configuration
If you want to use the GitLab CI, make sure on your GitLab repository you create the CI Variables `FIREBASE_BASE_PATH` and `FIREBASE_API_KEY` with the same values you set in the `.env` file.